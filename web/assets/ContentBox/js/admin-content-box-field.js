var AdminContentBoxFieldHandler = function (content_box_field) {

    this.contentBoxField = content_box_field;
    this.maxNumber = parseInt(content_box_field.attr('data-max-number'));

    var that = this;

    content_box_field.on('click', '.js-remove-row', function (e) {
        e.preventDefault();
        var remove_button = $(this);
        remove_button.closest('.row').remove();
        that.updateContentBoxAddingOptions();
    });

    content_box_field.on('click', '.content-box-field-make-separator', function (e) {
        e.preventDefault();
        that.makeSeparator($(this).attr('data-base-name'));
    });

    content_box_field.on('click', '.content-box-field-make-text-block', function (e) {
        e.preventDefault();
        that.makeTextBlock($(this).attr('data-base-name'));
    });

    content_box_field.find('.content-box-field-ckeditor').each(function () {
        $(this).ckeditor({
            customConfig: that.contentBoxField.attr('data-ckeditor-config-file-url'),
            language: that.contentBoxField.attr('data-field-language')
        });
    });

    content_box_field.find('.content-box-field-image-typeahead').typeahead(
        {},
        {
            name: 'image-search-results-set',
            limit: 20,
            display: function (datum) {
                return datum.title;
            },
            source: function (query, syncResults, asyncResults) {
                $.get(
                    content_box_field.attr('data-image-search-results-url'),
                    {
                        'q': query,
                        'image_types': content_box_field.attr('data-image-types')
                    },
                    function (data) {
                        asyncResults(data);
                    }
                );
            },
            templates: {
                suggestion: function (datum) {
                    var image_tag = '';
                    if (datum.image != '') {
                        image_tag = '<div class="row__thumbnail"><figure class="thumbnail"><img src="' + that.escapeHtml(datum.image) + '" class="checkered"></figure></div>';
                    }
                    return ''
                        + '<div class="row">'
                        + '<div class="row__wrapper">'
                        + '<div class="row__header">'
                        + '<div class="row__id">' + that.escapeHtml(datum.singular_human_type) + ' #' + datum.id + '</div>'
                        + '<h3 class="row__title">' + that.escapeHtml(datum.title) + '</h3>'
                        + '<h4 class="row__hint">' + that.escapeHtml(datum.hint) + '</h4>'
                        + '</div>'
                        + image_tag
                        + '</div>'
                        + '</div>';
                }
            }
        }
    ).on('typeahead:selected', function (e, datum) {
        that.makeImage(content_box_field.attr('data-base-name'), datum.type, datum.id);
        content_box_field.find('.content-box-field-image-typeahead').typeahead('val', '');
    });

    content_box_field.find('.content-box-field-media-typeahead').typeahead(
        {},
        {
            name: 'media-search-results-set',
            limit: 20,
            display: function (datum) {
                return datum.title;
            },
            source: function (query, syncResults, asyncResults) {
                $.get(
                    content_box_field.attr('data-media-search-results-url'),
                    {
                        'q': query,
                        'media_types': content_box_field.attr('data-media-types')
                    },
                    function (data) {
                        asyncResults(data);
                    }
                );
            },
            templates: {
                suggestion: function (datum) {
                    var image_tag = '';
                    if (datum.image != '') {
                        image_tag = '<div class="row__thumbnail"><figure class="thumbnail"><img src="' + that.escapeHtml(datum.image) + '" class="checkered"></figure></div>';
                    }
                    return ''
                        + '<div class="row">'
                        + '<div class="row__wrapper">'
                        + '<div class="row__header">'
                        + '<div class="row__id">' + that.escapeHtml(datum.singular_human_type) + ' #' + datum.id + '</div>'
                        + '<h3 class="row__title">' + that.escapeHtml(datum.title) + '</h3>'
                        + '<h4 class="row__hint">' + that.escapeHtml(datum.hint) + '</h4>'
                        + '</div>'
                        + image_tag
                        + '</div>'
                        + '</div>';
                }
            }
        }
    ).on('typeahead:selected', function (e, datum) {
        that.makeMedia(content_box_field.attr('data-base-name'), datum.type, datum.id);
        content_box_field.find('.content-box-field-media-typeahead').typeahead('val', '');
    });

    content_box_field.find('.content-box-field-content-unit-typeahead').typeahead(
        {},
        {
            name: 'content-unit-search-results-set',
            limit: 20,
            display: function (datum) {
                return datum.title;
            },
            source: function (query, syncResults, asyncResults) {
                $.get(
                    content_box_field.attr('data-content-unit-search-results-url'),
                    {
                        'q': query,
                        'content_unit_types': content_box_field.attr('data-content-unit-types')
                    },
                    function (data) {
                        asyncResults(data);
                    }
                );
            },
            templates: {
                suggestion: function (datum) {
                    var image_tag = '';
                    if (datum.image != '') {
                        image_tag = '<div class="row__thumbnail"><figure class="thumbnail"><img src="' + that.escapeHtml(datum.image) + '" class="checkered"></figure></div>';
                    }
                    return ''
                        + '<div class="row">'
                        + '<div class="row__wrapper">'
                        + '<div class="row__header">'
                        + '<div class="row__id">' + that.escapeHtml(datum.singular_human_type) + ' #' + datum.id + '</div>'
                        + '<h3 class="row__title">' + that.escapeHtml(datum.title) + '</h3>'
                        + '<h4 class="row__hint">' + that.escapeHtml(datum.hint) + '</h4>'
                        + '</div>'
                        + image_tag
                        + '</div>'
                        + '</div>';
                }
            }
        }
    ).on('typeahead:selected', function (e, datum) {
        that.makeContentUnit(content_box_field.attr('data-base-name'), datum.type, datum.id);
        content_box_field.find('.content-box-field-content-unit-typeahead').typeahead('val', '');
    });

    content_box_field.find('.fileupload').fileupload({
        maxChunkSize: 1000000,
        dataType: 'json',
        dropZone: content_box_field.find('.dropzone'),
        progressall: function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
            content_box_field.find('.dropzone').find('.dropzone__bar').css('width', progress + '%');
            content_box_field.find('.dropzone').find('.dropzone__info').html(progress + '%');
        },
        add: function (e, data) {
            content_box_field.find('.dropzone').find('.dropzone__load').addClass('loading');
            if (data.autoUpload || (data.autoUpload !== false && $(this).fileupload('option', 'autoUpload'))) {
                data.process().done(function () {
                data.submit();
                });
            }
        },
        done: function (e, data) {
            content_box_field.find('.dropzone').find('.dropzone__load').removeClass('loading');
            $.each(data.result[content_box_field.attr('data-base-name') + '_files'], function (index, file) {
                $.post(that.contentBoxField.attr('data-create-item-from-tmp-file-url'), { file_name: file.name }, function (data) {
                    that.addItem(data);
                });
            });
        }
    });

    content_box_field.on('click', '.js-add-media-from-url', function (e) {
        e.preventDefault();
        $.post(
            that.contentBoxField.attr('data-create-item-from-url-url'),
            { 
                url: content_box_field.find('.new-url').val()
            },
            function (data) {
                that.addItem(data);
                content_box_field.find('.new-url').val('');
            }
        );
        content_box_field.closest('form').trigger('change');
    });

    // Fix issue when a CKEditor moves inside of the DOM.
    content_box_field.on('update', '.js-sortable', function (e) {
        var cke_element = $(e.originalEvent.item).find('.cke' );
        if (cke_element.length == 0) {
            return;
        }
        var cke_id = cke_element.attr('id');
        cke_id = cke_id.replace('cke_','');
        var cke_instance = CKEDITOR.instances[cke_id];
        $('#' + cke_id).html(cke_instance.getData());
        CKEDITOR.remove(cke_instance);
        $('#cke_' + cke_id).remove();
        CKEDITOR.replace(cke_id, {
            customConfig: that.contentBoxField.attr('data-ckeditor-config-file-url'),
            language: that.contentBoxField.attr('data-field-language')
        });
    });

    this.updateContentBoxAddingOptions();

}

AdminContentBoxFieldHandler.prototype = {

    'updateContentBoxAddingOptions': function () {
        if (this.contentBoxField.find('.row').length < this.maxNumber) {
            this.contentBoxField.find('.content-box-adding-actions').show();
        } else {
            this.contentBoxField.find('.content-box-adding-actions').hide();
        }
    },

    'addItem': function (item) {
        //this.contentBoxField.find('.typeahead').val('');
        var new_item = null;
        if (this.contentBoxField.find('.row').length < this.maxNumber) {
            new_item = this.contentBoxField.find('.list').append(item);
        }
        this.updateContentBoxAddingOptions();
        return new_item;
    },

    'makeSeparator': function (base_name) {
        var that = this;
        $.get(
            this.contentBoxField.attr('data-make-separator-url'),
            {
                type: 'separator',
                base_name: base_name
            },
            function (data) {
                that.addItem(data);
            }
        );
        this.updateContentBoxAddingOptions();
    },

    'makeTextBlock': function (base_name) {
        var that = this;
        $.get(
            this.contentBoxField.attr('data-make-text-block-url'),
            {
                type: 'html',
                base_name: base_name
            },
            function (data) {
                that.addItem(data).find('.content-box-field-ckeditor').ckeditor({
                    customConfig: that.contentBoxField.attr('data-ckeditor-config-file-url'),
                    language: that.contentBoxField.attr('data-field-language')
                });
            }
        );
        this.updateContentBoxAddingOptions();
    },

    'makeImage': function (base_name, reference_type, reference_id) {
        var that = this;
        $.get(
            this.contentBoxField.attr('data-make-image-url'),
            {
                type: 'image',
                base_name: base_name,
                reference_type: reference_type,
                reference_id: reference_id
            },
            function (data) {
                that.addItem(data);
            }
        );
        this.updateContentBoxAddingOptions();
    },

    'makeMedia': function (base_name, reference_type, reference_id) {
        var that = this;
        $.get(
            this.contentBoxField.attr('data-make-media-url'),
            {
                type: 'media',
                base_name: base_name,
                reference_type: reference_type,
                reference_id: reference_id
            },
            function (data) {
                that.addItem(data);
            }
        );
        this.updateContentBoxAddingOptions();
    },

    'makeContentUnit': function (base_name, reference_type, reference_id) {
        var that = this;
        $.get(
            this.contentBoxField.attr('data-make-content-unit-url'),
            {
                type: 'content_unit',
                base_name: base_name,
                reference_type: reference_type,
                reference_id: reference_id
            },
            function (data) {
                that.addItem(data);
            }
        );
        this.updateContentBoxAddingOptions();
    },

    'escapeHtml': function (s) {
        return s.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
    }

}

$(function () {
    $('.content-box-field').each(function () {
        var admin_content_box_field_handler = new AdminContentBoxFieldHandler($(this));
    });
});

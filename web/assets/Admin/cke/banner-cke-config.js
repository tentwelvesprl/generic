CKEDITOR.editorConfig = function( config ) {
    config.language = 'en';
    config.toolbarGroups = [
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
        { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
        { name: 'forms', groups: [ 'forms' ] },
        { name: 'tools', groups: [ 'tools' ] },
        { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
        { name: 'about', groups: [ 'about' ] },
        '/',
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'links', groups: [ 'links' ] },
        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
        { name: 'styles', groups: [ 'styles' ] },
        { name: 'colors', groups: [ 'colors' ] },
        { name: 'insert', groups: [ 'insert' ] },
        { name: 'others', groups: [ 'others' ] }
    ];

    config.removeButtons = 'Templates,Save,NewPage,Preview,Print,SelectAll,Find,Replace,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Underline,CopyFormatting,JustifyLeft,JustifyCenter,JustifyRight,JustifyBlock,Language,BidiRtl,BidiLtr,Anchor,Image,Flash,Table,Smiley,PageBreak,Iframe,Font,FontSize,TextColor,BGColor,Format,About,Blockquote,CreateDiv';

    config.extraPlugins = 'autogrow';
    config.autoGrow_minHeight = 64;
    config.autoGrow_maxHeight = 512;
    config.autoGrow_onStartup = true;
    config.linkDefaultProtocol = 'https://';

    config.allowedContent = 'p strong em sub sup br h2 h3;';
    config.disallowedContent = '*[style,width,height]';
    config.contentsCss = basepath+'/assets/Front/css/cke.min.css';
    config.bodyClass = 'cke cke--banner';

    config.ignoreEmptyParagraph = true;
    config.startupOutlineBlocks = true;
    config.removePlugins = 'showborders';
    config.removePlugins = 'showborders';config.specialChars = config.specialChars.concat( [ [ '&nbsp;', 'Non-breaking space' ], [ '&#8209;', 'Non-breaking hyphen' ] ] );

    config.stylesSet = [
    { name: 'title', element: 'h2' },
    { name: 'subtitle', element: 'h3' },
    { name: 'paragraph', element: 'p' },
    ];

};

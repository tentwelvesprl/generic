CKEDITOR.editorConfig = function( config ) {
    config.language = 'en';
    config.toolbarGroups = [
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
        { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
        { name: 'forms', groups: [ 'forms' ] },
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'links', groups: [ 'links' ] },
        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
        { name: 'styles', groups: [ 'styles' ] },
        { name: 'colors', groups: [ 'colors' ] },
        { name: 'insert', groups: [ 'insert' ] },
        { name: 'others', groups: [ 'others' ] },
        { name: 'tools', groups: [ 'tools' ] },
        { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
        { name: 'about', groups: [ 'about' ] }
    ];

    config.removeButtons = 'Templates,Save,NewPage,Preview,Print,SelectAll,Find,Replace,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Underline,CopyFormatting,JustifyLeft,JustifyCenter,JustifyRight,JustifyBlock,Language,BidiRtl,BidiLtr,Anchor,Image,Flash,Table,Smiley,PageBreak,Iframe,Font,FontSize,TextColor,BGColor,About,Blockquote,CreateDiv,NumberedList,BulletedList,Indent,Outdent,Format,Styles,Maximize,HorizontalRule,Copy,Cut,SpellChecker,Scayt';

    config.extraPlugins = 'autogrow';
    config.autoGrow_minHeight = 64;
    config.autoGrow_maxHeight = 512;
    config.autoGrow_onStartup = true;
    config.linkDefaultProtocol = 'https://';

    config.allowedContent = 'p strong em sub sup br';
    config.disallowedContent = '*[style,width,height]';
    config.contentsCss = basepath+'/assets/Front/css/cke.min.css';
    config.bodyClass = 'teaser';

    config.ignoreEmptyParagraph = true;
    config.startupOutlineBlocks = true;
    config.removePlugins = 'showborders';
    config.removePlugins = 'showborders';config.specialChars = config.specialChars.concat( [ [ '&nbsp;', 'Non-breaking space' ], [ '&#8209;', 'Non-breaking hyphen' ] ] );
    
};

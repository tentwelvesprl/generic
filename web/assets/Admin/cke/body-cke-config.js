CKEDITOR.editorConfig = function( config ) {
    config.language = 'en';
    config.toolbarGroups = [
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
        { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
        { name: 'forms', groups: [ 'forms' ] },
        { name: 'tools', groups: [ 'tools' ] },
        { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
        { name: 'about', groups: [ 'about' ] },
        '/',
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'links', groups: [ 'links' ] },
        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
        { name: 'styles', groups: [ 'styles' ] },
        { name: 'colors', groups: [ 'colors' ] },
        { name: 'insert', groups: [ 'insert' ] },
        { name: 'others', groups: [ 'others' ] }
    ];

    config.removeButtons = 'Templates,Save,NewPage,Preview,Print,SelectAll,Find,Replace,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Underline,CopyFormatting,JustifyLeft,JustifyCenter,JustifyRight,JustifyBlock,Language,BidiRtl,BidiLtr,Anchor,Image,Flash,Smiley,PageBreak,Iframe,Font,FontSize,TextColor,BGColor,Format,About,CreateDiv';

    config.scayt_autoStartup = true;
    config.scayt_sLang = 'en_EN';

    config.extraPlugins = 'autogrow';
    config.autoGrow_minHeight = 64;
    config.autoGrow_maxHeight = 512;
    config.autoGrow_onStartup = true;
    config.linkDefaultProtocol = 'https://';

    config.allowedContent = 'h3 h4 h5 p strong em sub sup li ul ol br blockquote hr table tr q cite; th td[colspan,rowspan]{text-align,vertical-align,white-space}; a[!href,target];';
    config.disallowedContent = '*[style,width,height]';
    config.contentsCss = basepath+'/assets/Front/css/cke.min.css';
    config.bodyClass = 'cke cke--body';
    
    config.ignoreEmptyParagraph = true;
    config.startupOutlineBlocks = true;
    config.specialChars = config.specialChars.concat( [ [ '&nbsp;', 'Non-breaking space' ], [ '&#8209;', 'Non-breaking hyphen' ] ] );

    config.stylesSet = [
    { name: 'paragraph', element: 'p' },
    { name: 'first heading', element: 'h3' },
    { name: 'second heading', element: 'h4' },
    { name: 'third heading', element: 'h5' },
    { name: 'quote', element: 'q' },
    { name: 'quote source', element: 'cite' },
    ];

};

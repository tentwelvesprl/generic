var AdminContentUnitReferencesAdvancedSearchHandler = function (search_filter) {

    this.searchFilter = search_filter;

    var that = this;
    var content_unit_types = search_filter.attr('data-content-unit-types');

    that.updateContentUnitReferencesAddingOptions();

    search_filter.on('click', '.js-remove-row', function (e) {
        e.preventDefault();
        var remove_button = $(this);
        remove_button.closest('.row').remove();
        search_filter.find('.search_value').val('');
        that.updateContentUnitReferencesAddingOptions();
    });

    search_filter.find('.typeahead').typeahead(
        {},
        {
            name: 'search-results-set',
            limit: 20,
            display: function (datum) {
                return datum.title;
            },
            source: function (query, syncResults, asyncResults) {
                $.get(
                    search_filter.attr('data-search-results-url'),
                    {
                        'q': query,
                        'content_unit_types': content_unit_types
                    },
                    function (data) {
                        asyncResults(data);
                    }
                );
            },
            templates: {
                suggestion: function (datum) {
                    var image_tag = '';
                    if (datum.image != '') {
                        image_tag = '<div class="row__thumbnail"><figure class="thumbnail"><img src="' + that.escapeHtml(datum.image) + '" class="checkered"></figure></div>';
                    }
                    return ''
                        + '<div class="row">'
                        + '<div class="row__wrapper">'
                        + '<div class="row__header">'
                        + '<div class="row__id">' + that.escapeHtml(datum.singular_human_type) + ' #' + datum.id + '</div>'
                        + '<h3 class="row__title">' + that.escapeHtml(datum.title) + '</h3>'
                        + '<h4 class="row__hint">' + that.escapeHtml(datum.hint) + '</h4>'
                        + '</div>'
                        + image_tag
                        + '</div>'
                        + '</div>';
                }
            }
        }
    ).on('typeahead:selected',
        function (obj, datum) {
            that.addItem(datum);
            search_filter.find('.search_value').val(datum.type + ':' + datum.id);

            // search_filter.find('.search_label').text(datum.title + ' (' + datum.singular_human_type + ' #' + datum.id + ')');
            // search_filter.find('.search_value').val(datum.type + ':' + datum.id);
            // search_filter.find('.search_clear').show();

            search_filter.find('.typeahead').typeahead('val', '');
        }
    );

    // search_filter.find('.search_clear').on('click', function (e) {
    //     search_filter.find('.search_label').text('');
    //     search_filter.find('.search_value').val('');
    //     search_filter.find('.search_clear').hide();
    //     search_filter.find('.typeahead').typeahead('val', '');
    // });

}

AdminContentUnitReferencesAdvancedSearchHandler.prototype = {

    'addItem': function (d) {
        if (this.searchFilter.find('.list').find('li.row').length < 1) {
            this.searchFilter.find('.typeahead').val('');
            var item = ''
                + '<li class="row">'
                + '<div class="row__wrapper">'
                + '<div class="row__header">'
                + '<div class="row__id">'
                + this.escapeHtml(d.singular_human_type) + ' #' + d.id
                + '</div>'
                + '<h3 class="row__title">' + this.escapeHtml(d.title) + '</h3>'
                + '</div>'
                + '<div class="row__tools">'
                + '<button class="js-remove-row"><svg class="icon"><use xlink:href="' + basepath + '/assets/Admin/img/feather-sprite.svg#x-circle"></use></svg></button>'
                + '</div>'
                + '</div>'
                + '</li>';
            this.searchFilter.find('.list').append(item);
        }
        this.updateContentUnitReferencesAddingOptions();
    },

    'updateContentUnitReferencesAddingOptions': function () {
        if (this.searchFilter.find('.list').find('.row').length < 1) {
            this.searchFilter.find('.content-unit-references-adding-actions').show();
        } else {
            this.searchFilter.find('.content-unit-references-adding-actions').hide();
        }
    },

    'escapeHtml': function (s) {
        return s.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
    }

}

$(function () {
    $('.content-unit-references-advanced-search').each(function () {
        var admin_content_unit_references_advanced_search_handler = new AdminContentUnitReferencesAdvancedSearchHandler($(this));
    });
});

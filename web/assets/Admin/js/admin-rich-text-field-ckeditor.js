$(
    function () {
        $('.rich-text-field').each(function() {
            var that = $(this);
            that.find('textarea').ckeditor({
                customConfig: that.data('ckeditor-config-file-url'),
                language: that.data('ckeditor-language')
            });
        });
    }
);

var AdminMediaFieldHandler = function (media_field) {

    this.mediaField = media_field;
    this.maxNumber = parseInt(media_field.attr('data-max-number'));

    var that = this;
    var new_item_url = media_field.attr('data-new-item-url');
    var create_item_from_url_url = media_field.attr('data-create-item-from-url-url');
    var media_types = media_field.attr('data-media-types');

    media_field.on('click', '.js-remove-row', function (e) {
        e.preventDefault();
        var remove_button = $(this);
        remove_button.closest('.row').remove();
        that.updateMediaAddingOptions();
    });

    media_field.on('click', '.js-add-from-url', function (e) {
        e.preventDefault();
        $.post(create_item_from_url_url, { url: media_field.find('.new_url').val() }, function (data) {
            that.addItem(data);
            media_field.find('.new_url').val('');
        });
        media_field.closest('form').trigger('change');
    });

    media_field.find('.typeahead').typeahead(
        {},
        {
            name: 'search-results-set',
            limit: 20,
            display: function (datum) {
                return datum.title;
            },
            source: function (query, syncResults, asyncResults) {
                $.get(
                    media_field.attr('data-search-results-url'),
                    {
                        'q': query,
                        'media_types': media_types
                    },
                    function (data) {
                        asyncResults(data);
                    }
                );
            },
            templates: {
                suggestion: function (datum) {
                    var image_tag = '';
                    if (datum.image != '') {
                        image_tag = '<div class="row__thumbnail"><figure class="thumbnail"><img src="' + that.escapeHtml(datum.image) + '" class="checkered"></figure></div>';
                    }
                    return ''
                        + '<div class="row">'
                        + '<div class="row__wrapper">'
                        + '<div class="row__header">'
                        + '<div class="row__id">' + that.escapeHtml(datum.singular_human_type) + ' #' + datum.id + '</div>'
                        + '<h3 class="row__title">' + that.escapeHtml(datum.title) + '</h3>'
                        + '<h4 class="row__hint">' + that.escapeHtml(datum.hint) + '</h4>'
                        + '</div>'
                        + image_tag
                        + '</div>'
                        + '</div>';
                }
            }
        }
    ).on('typeahead:selected', function (e, datum) {
        $.get(
            new_item_url,
            {
                type: datum.type,
                id: datum.id
            },
            function (data) {
                that.addItem(data);
            }
        );
        media_field.find('.typeahead').typeahead('val', '');
    });

    this.updateMediaAddingOptions();

}

AdminMediaFieldHandler.prototype = {

    'addItem': function (item) {
        if (this.mediaField.find('.row').length < this.maxNumber) {
            this.mediaField.find('.list').append(item);
        }
        this.updateMediaAddingOptions();
    },

    'updateMediaAddingOptions': function () {
        if (this.mediaField.find('.row').length < this.maxNumber) {
            this.mediaField.find('.media-adding-actions').show();
        } else {
            this.mediaField.find('.media-adding-actions').hide();
        }
    },

    'escapeHtml': function (s) {
        return s.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
    }

}

$(function() {
    $('.media-field').each(function() {
        var admin_media_field_handler = new AdminMediaFieldHandler($(this));
    });
});

var AdminFileFieldHandler = function (file_field) {

    this.fileField = file_field;
    this.maxNumber = parseInt(file_field.attr('data-max-number'));

    var that = this;
    var new_item_url = file_field.attr('data-new-item-url');
    var create_item_from_tmp_file_url = file_field.attr('data-create-item-from-tmp-file-url');
    var file_types = file_field.attr('data-file-types');
    var param_name = file_field.attr('data-base-name') + '_files';
    var $dropzone = file_field.find('.dropzone');

    file_field.on('click', '.js-remove-row', function (e) {
        e.preventDefault();
        var remove_button = $(this);
        remove_button.closest('.row').remove();
        that.updateFileAddingOptions();
    });

    file_field.find('.fileupload').fileupload({
        maxChunkSize: 1000000,
        dataType: 'json',
        dropZone: $dropzone,
        progressall: function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
            $dropzone.find('.dropzone__bar').css('width', progress + '%');
            $dropzone.find('.dropzone__info').html(progress + '%');
        },
        add: function (e, data) {
            $dropzone.find('.dropzone__load').addClass('loading');
            if (data.autoUpload || (data.autoUpload !== false && $(this).fileupload('option', 'autoUpload'))) {
                    data.process().done(function () {
                    data.submit();
                });
            }
        },
        done: function (e, data) {
            $dropzone.find('.dropzone__load').removeClass('loading');
            $.each(data.result[param_name], function (index, file) {
                $.post(create_item_from_tmp_file_url, { file_name: file.name }, function (data) {
                    that.addItem(data);
                });
            });
        }
    });

    file_field.find('.typeahead').typeahead(
        {},
        {
            name: 'search-results-set',
            limit: 20,
            display: function (datum) {
                return datum.title;
            },
            source: function (query, syncResults, asyncResults) {
                $.get(
                    file_field.attr('data-search-results-url'),
                    {
                        'q': query,
                        'file_types': file_types
                    },
                    function (data) {
                        asyncResults(data);
                    }
                );
            },
            templates: {
                suggestion: function (datum) {
                    var image_tag = '';
                    if (datum.image != '') {
                        image_tag = '<div class="row__thumbnail"><figure class="thumbnail"><img src="' + that.escapeHtml(datum.image) + '" class="checkered"></figure></div>';
                    }
                    return ''
                        + '<div class="row">'
                        + '<div class="row__wrapper">'
                        + '<div class="row__header">'
                        + '<div class="row__id">' + that.escapeHtml(datum.singular_human_type) + ' #' + datum.id + '</div>'
                        + '<h3 class="row__title">' + that.escapeHtml(datum.title) + '</h3>'
                        + '<h4 class="row__hint">' + that.escapeHtml(datum.hint) + '</h4>'
                        + '</div>'
                        + image_tag
                        + '</div>'
                        + '</div>';
                }
            }
        }
    ).on('typeahead:selected', function (e, datum) {
        $.get(
            new_item_url,
            {
                type: datum.type,
                id: datum.id
            },
            function (data) {
                that.addItem(data);
            }
        );
        file_field.find('.typeahead').typeahead('val', '');
    });

    this.updateFileAddingOptions();

}

AdminFileFieldHandler.prototype = {

    'addItem': function (item) {
        if (this.fileField.find('.row').length < this.maxNumber) {
            this.fileField.find('.list').append(item);
        }
        this.updateFileAddingOptions();
    },

    'updateFileAddingOptions': function () {
        if (this.fileField.find('.row').length < this.maxNumber) {
            this.fileField.find('.file-adding-actions').show();
        } else {
            this.fileField.find('.file-adding-actions').hide();
        }
    },

    'escapeHtml': function (s) {
        return s.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
    }

}

$(function() {
    $('.file-field').each(function() {
        var admin_file_field_handler = new AdminFileFieldHandler($(this));
    });
});

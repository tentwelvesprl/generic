var AdminImageFieldHandler = function (image_field) {

    this.imageField = image_field;
    this.maxNumber = parseInt(image_field.attr('data-max-number'));

    var that = this;
    var new_item_url = image_field.attr('data-new-item-url');
    var create_item_from_url_url = image_field.attr('data-create-item-from-url-url');
    var create_item_from_tmp_file_url = image_field.attr('data-create-item-from-tmp-file-url');
    var image_types = image_field.attr('data-image-types');
    var param_name = image_field.attr('data-base-name') + '_files';
    var $dropzone = image_field.find('.dropzone');

    image_field.on('click', '.js-remove-row', function (e) {
        e.preventDefault();
        var remove_button = $(this);
        remove_button.closest('.row').remove();
        that.updateImageAddingOptions();
    });

    image_field.on('click', '.js-add-from-url', function (e) {
        e.preventDefault();
        $.post(create_item_from_url_url, { url: image_field.find('.new_url').val() }, function (data) {
            that.addItem(data);
            image_field.find('.new_url').val('');
        });
        image_field.closest('form').trigger('change');
    });

    image_field.find('.fileupload').fileupload({
        maxChunkSize: 1000000,
        dataType: 'json',
        dropZone: $dropzone,
        progressall: function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
            $dropzone.find('.dropzone__bar').css('width', progress + '%');
            $dropzone.find('.dropzone__info').html(progress + '%');
        },
        add: function (e, data) {
            $dropzone.find('.dropzone__load').addClass('loading');
            if (data.autoUpload || (data.autoUpload !== false && $(this).fileupload('option', 'autoUpload'))) {
                data.process().done(function () {
                data.submit();
                });
            }
        },
        done: function (e, data) {
            $dropzone.find('.dropzone__load').removeClass('loading');
            $.each(data.result[param_name], function (index, file) {
                $.post(create_item_from_tmp_file_url, { file_name: file.name }, function (data) {
                    that.addItem(data);
                });
            });
        }
    });

    image_field.find('.typeahead').typeahead(
        {},
        {
            name: 'search-results-set',
            limit: 20,
            display: function (datum) {
                return datum.title;
            },
            source: function (query, syncResults, asyncResults) {
                $.get(
                    image_field.attr('data-search-results-url'),
                    {
                        'q': query,
                        'image_types': image_types
                    },
                    function (data) {
                        asyncResults(data);
                    }
                );
            },
            templates: {
                suggestion: function (datum) {
                    var image_tag = '';
                    if (datum.image != '') {
                        image_tag = '<div class="row__thumbnail"><figure class="thumbnail"><img src="' + that.escapeHtml(datum.image) + '" class="checkered"></figure></div>';
                    }
                    return ''
                        + '<div class="row">'
                        + '<div class="row__wrapper">'
                        + '<div class="row__header">'
                        + '<div class="row__id">' + that.escapeHtml(datum.singular_human_type) + ' #' + datum.id + '</div>'
                        + '<h3 class="row__title">' + that.escapeHtml(datum.title) + '</h3>'
                        + '<h4 class="row__hint">' + that.escapeHtml(datum.hint) + '</h4>'
                        + '</div>'
                        + image_tag
                        + '</div>'
                        + '</div>';
                }
            }
        }
    ).on('typeahead:selected', function (e, datum) {
        $.get(
            new_item_url,
            {
                type: datum.type,
                id: datum.id
            },
            function (data) {
                that.addItem(data);
            }
        );
        image_field.find('.typeahead').typeahead('val', '');
    });

    this.updateImageAddingOptions();

}

AdminImageFieldHandler.prototype = {

    'addItem': function (item) {
        if (this.imageField.find('.js-valid-row').length < this.maxNumber) {
            this.imageField.find('.list').append(item);
        }
        this.updateImageAddingOptions();
    },

    'updateImageAddingOptions': function () {
        if (this.imageField.find('.js-valid-row').length < this.maxNumber) {
            this.imageField.find('.image-adding-actions').show();
        } else {
            this.imageField.find('.image-adding-actions').hide();
        }
    },

    'escapeHtml': function (s) {
        return s.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
    }

}

$(function() {
    $('.image-field').each(function() {
        var admin_image_field_handler = new AdminImageFieldHandler($(this));
    });
});

var AdminPerformancesFieldHandler = function (performances_field) {

    this.performancesField = performances_field;

    var that = this;
    var new_item_url = performances_field.attr('data-new-item-url');

    performances_field.on('click', '.js-remove-row', function (e) {
        e.preventDefault();
        $r = $(this).closest('.row')
        $r.slideUp('fast', function() { $r.remove(); });
    });

    performances_field.on('click', '.js-add-row', function (e) {
        e.preventDefault();
        var last_item = performances_field.find('.row').last();
        var ts = Math.floor(Date.now() / 1000);
        var venue_id = 0;
        if (last_item.length > 0) {
            var last_date = that.dateStringToDate(
                last_item.find('.dt-date').val() + 'T' + last_item.find('.dt-time').val()
            );
            ts = Math.floor(last_date.getTime() / 1000) + 24 * 3600;
            venue_id = parseInt(last_item.find('.venue_id').val());
        }
        $.get(
            new_item_url,
            {
                ts: ts,
                venue_id: venue_id
            },
            function (data) {
                that.addItem(data);
            }
        );
        performances_field.closest('form').trigger('change');
    });

    performances_field.on('change', '.all-day', function (e) {
        var cb = $(this);
        var time_is_disabled = false;
        if (cb.prop('checked')) {
            time_is_disabled = true;
        }
        cb.closest('.row').find('.dt-time').prop('disabled', time_is_disabled);
        cb.closest('.row').find('.dt-end-time').prop('disabled', time_is_disabled);
    });

}

AdminPerformancesFieldHandler.prototype = {

    'addItem': function (item) {
        this.performancesField.find('.list').append(item);
    },

    'dateStringToDate': function (s) {
        var elements = s.split(/[^0-9]/);
        if (elements.length >= 5) {
            return new Date(elements[0], parseInt(elements[1]) - 1, elements[2], elements[3], elements[4]);
        }
        return new Date();
    }

}

$(function() {
    $('.performances-field').each(function() {
        var admin_performances_field_handler = new AdminPerformancesFieldHandler($(this));
    });
});

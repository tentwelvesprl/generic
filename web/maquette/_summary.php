<div class="section">

    <div class="wrapper">

        <h1><a href="?inc=examples/index">Templates</a></h1>
        <ul>
            <li><a href="?inc=examples/home">Homepage</a></li>
        </ul>

        <h1><a href="?inc=styleguide/index">Styleguide</a></h1>
        <ul>
            <li><a href="?inc=styleguide/font_specimen">Font Specimen</a></li>
            <li><a href="?inc=styleguide/defaults">Defaults</a></li>
            <li><a href="?inc=styleguide/grids">Grids</a></li>
            <li><a href="?inc=styleguide/items">Items</a></li>
            <li><a href="?inc=styleguide/elements">Elements</a></li>
            <li><a href="?inc=styleguide/inputs">Inputs</a></li>
            <li><a href="?inc=styleguide/buttons">Buttons</a></li>
        </ul>

        <h1><a href="?inc=templates/index">Templates</a></h1>
        <ul>
            <li><a href="?inc=templates/Homepage__view">Homepage:view</a></li>
            <li><a href="?inc=templates/LandingPage__view">LandingPage:view</a></li>
            <li><a href="?inc=templates/StaticPage__view">StaticPage:view</a></li>
            <li><a href="?inc=templates/Article__index">Article:index</a></li>
            <li><a href="?inc=templates/Article__view">Article:view</a></li>
            <li><a href="?inc=templates/error404">Error 404</a></li>
        </ul>

    </div>

</div>
<header class="main-header">

    <div class="wrapper">

        <div class="main-header__bar">

            <div class="main-header__brand" aria-labelledby="logoTitle"><a href=".">Logo</a></div>

            <ul class="main-header__shortcuts">
                <li><a href="?inc=_summary" <?php if (isset($marker) && $marker == 'summary') echo ' class="active" aria-current="page"'; ?>>Summary</a></li>
                <li><a href="?inc=templates/StaticPage__view" class="btn">Static Page</a></li>
                <li><a href="#"><?php echo randIpsum(1, 3, $words); ?></a></li>
            </ul>

            <button aria-label="Menu" class="main-header__burger" id="burger">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewbox="0 0 32 32">
                    <defs>
                        <rect id="def-slice" width="32" height="4" rx="0" ry="0" fill="currentColor" />
                    </defs>
                    <use xlink:href="#def-slice" class="slice slice--1" x="0" y="4" />
                    <use xlink:href="#def-slice" class="slice slice--2" x="0" y="14" />
                    <use xlink:href="#def-slice" class="slice slice--3" x="0" y="24" />
                </svg>
            </button>

            <ul class="main-header__langs langs">
                <li class="is-active"><a href="#">en</a></li>
                <li><a href="#">fr</a></li>
                <li><a href="#">nl</a></li>
            </ul>

            <form method="GET" action="./" class="main-header__search search">
                <input type="hidden" name="inc" value="templates/Search__results">
                <input type="search" name="q" placeholder="Rechercher" autocomplete="off" class="search__input" aria-label="Rechercher">
                <button type="submit" class="search__submit" aria-label="Soumettre">
                    <svg>
                        <use xlink:href="../assets/Front/img/sprite.svg#search" />
                    </svg>
                </button>
            </form>

        </div>

    </div>

    <div class="main-header__nav">
        <div class="wrapper">
            <ul class="main-header__menu">
                <li><a href="?inc=_summary" <?php if (isset($marker) && $marker == 'summary') echo ' class="active" aria-current="page"'; ?>>Summary</a></li>
                <li><a href="?inc=templates/Article__index&marker=article" <?php if (isset($marker) && $marker == 'article') echo ' class="active" aria-current="page"'; ?>>Articles</a></li>
                <li><a href="?inc=templates/LandingPage__view&marker=lp" <?php if (isset($marker) && $marker == 'lp') echo ' class="active" aria-current="page"'; ?>>Landing Page</a></li>
                <li><a href="#"><?php echo randIpsum(1, 3, $words); ?></a></li>
                <li><a href="#"><?php echo randIpsum(1, 3, $words); ?></a></li>
                <li><a href="#"><?php echo randIpsum(1, 3, $words); ?></a></li>
            </ul>
        </div>
    </div>

</header>
<?php

$INIT['base_href'] = $_SERVER['REQUEST_URI'];

if (isset($_GET['inc'])) $inc = $_GET['inc']; else $inc='_summary';
if (isset($_GET['marker'])) $marker = $_GET['marker']; else $marker='homepage';

$words = explode(" ", $lorem);

function UrlStrPrepare($s){
    $s = str_replace("'", ' ', $s);
    $s = str_replace("’", ' ', $s);
    $s = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $s);
    $s = preg_replace('/[^[:alnum:]\\s\\-]/', '', $s);
    $s = preg_replace('/\\s+/', '-', trim($s));
    $s = strtolower($s);
    return $s;
}

function randDate($format='Y-m-d', $startDate='', $endDate=''){
    if (empty($startDate)) {
        $startDate = date('Y-m-d');
    }
    if (empty($endDate)) {
        $endDate = $startDate;
    }
    $days = round((strtotime($endDate) - strtotime($startDate)) / (60 * 60 * 24));
   $n = rand(0,$days);
   return date($format,strtotime("$startDate + $n days"));
}

function randIpsum($min=1, $max=20, $w){
    $m = count($w)-1;
    for ($x=1, $rw=rand($min,$max); $x<=$rw; $x++) {
        $ar[]=$w[rand(0,$m)];
    }
    return ucfirst(implode(' ', $ar));
}


$formats_json = json_decode(file_get_contents("../../private/app/config/img_formats.json"), true);
$formats = array();
foreach ($formats_json['img_generation_config'] as $format_ref => $format_def) {
    $f = '';
    if(isset($format_def['transformations'])) {
        foreach ($format_def['transformations'] as $k => $v) {
            switch ($k) {
                case 'resize_around_pivot_point':
                $f .= '&amp;mode=resize_around_pivot_point&amp;width='.$v['width'].'&amp;height='.$v['height'];
                break;
                case 'fit_to_max_dimensions':
                $f .= '&amp;mode=fit_to_max_dimensions&amp;width='.$v['width'].'&amp;height='.$v['height'];
                break;
                case 'fit_to_height':
                $f .= '&amp;mode='.$k.'&amp;height='.$v['height'];
                break;
                default:
                $f .= '&amp;mode='.$k.'&amp;width='.$v['width'];
                break;
            }
        }
    }
    if (isset($format_def['desaturate'])) $f .= '&amp;desaturate=true'.$format_def['desaturate'];
    if (isset($format_def['blur'])) $f .= '&amp;blur='.$format_def['blur'];
    if (isset($format_def['jpeg_quality'])) $f .= '&amp;q='.$format_def['jpeg_quality'];
    if (isset($format_def['mime_type'])) $f .= '&amp;type='.str_replace('image/','',$format_def['mime_type']);
    $formats[$format_ref] = $f;
};

function getImgSizes($src) {
    if (isset($src)) {
        $dim = getimagesize($src);
        $sizes = array(
            'w' => $dim[0],
            'h' => $dim[1],
            'ratio' => $dim[0] / $dim[1],
        );
        return $sizes;
    }
};

function resImgHelper($src, $format, $alt="", $class="", $ratio=0) {
    if (isset($src) && isset($GLOBALS['formats_json']['img_helper_config'][$format]) ) {
        $settings = $GLOBALS['formats_json']['img_helper_config'][$format];
        $picture = '<picture>';
        if (isset($settings['sources'])) :

        $picture .= '<!--[if IE 9]><video style="display: none;"><![endif]-->
        ';
        foreach($settings['sources'] as $source) {

            $picture .= '<source';
            $srcsetstr = array();
            foreach ($source['srcset'] as $n => $srcset) {
                $srcsetstr[$n] = './img.php?file='.$src.$GLOBALS['formats'][$srcset[0]];
                if (isset($srcset[1])) $srcsetstr[$n] .= ' '.$srcset[1];
            }
            if (!empty($srcsetstr)) $picture .= ' srcset="'.implode(', ', $srcsetstr).'"';
            if (isset($source['params'])) $picture .= ' '.$source['params'];
            $picture .= ' >';

        }
        $picture .= '
        <!--[if IE 9]></video><![endif]-->
        ';

        $picture .= '<img';
        $picture .= ' loading="lazy" class="lazyload '.$class.'"';
        if (isset($settings['srcset'])) {
            $srcsetstr = array();
            foreach ($settings['srcset'] as $n => $srcset) {
                $srcsetstr[$n] = './img.php?file='.$src.$GLOBALS['formats'][$srcset[0]];
                if (isset($srcset[1])) $srcsetstr[$n] .= ' '.$srcset[1];
            }
            $picture .= ' srcset="'.$srcsetstr[0].'" data-srcset="'.implode(', ', $srcsetstr).'"';
        }
        if ($ratio > 0) $picture .= ' data-aspectratio="' . $ratio . '"';
        $picture .= ' src="./img.php?file='.$src.$GLOBALS['formats'][$settings['src']].'" alt="'.$alt.'"';
        if (isset($settings['params'])) $picture .= ' '.$settings['params'];
        $picture .= ' >';
        $picture .= '
    </picture>
    ';
    return $picture;

    else :
        $img = '<img';
    if (!empty($settings) && is_array($settings)) {

        if (isset($settings['srcset'])) {
            $srcsetstr = array();
            foreach ($settings['srcset'] as $n => $srcset) {
                $srcsetstr[$n] = './img.php?file='.$src.$GLOBALS['formats'][$srcset[0]];
                if (isset($srcset[1])) $srcsetstr[$n] .= ' '.$srcset[1];
            }
            $img .= ' srcset="'.$srcsetstr[0].'" data-srcset="'.implode(', ', $srcsetstr).'"';
            if ($ratio > 0) $img .= ' data-aspectratio="' . $ratio . '"';
            if (isset($settings['params'])) $img .= ' '.$settings['params'];
        }
        $img .= ' src="./img.php?file='.$src.$GLOBALS['formats'][$settings['src']].'"';

    }
    $img .= ' loading="lazy" class="lazyload '.$class.'"';
    $img .= ' alt="'.$alt.'" >';
    return $img;

    endif;

}
}
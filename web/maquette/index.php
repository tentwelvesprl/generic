<?php
require_once "config.php";
require_once "init.php";
?>
<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $inc . ' / ' . $CONFIG['site_name']; ?></title>
    <meta name="description" content="">
    <meta property="og:title" content="">
    <meta property="og:type" content="">
    <meta property="og:url" content="">
    <meta property="og:image" content="">
    <meta property="og:description" content="">
    <meta property="og:site_name" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="../assets/Front/css/main.min.css?f9a75e0123e2db8c" media="all">
    <link rel="stylesheet" href="../assets/Front/css/modules/pagination.min.css?d30b80e457733aef" media="all">
    <link rel="stylesheet" href="../assets/Front/css/print.min.css?f2f7e28bb85cc894" media="print">
    <?php include "_favicons.php"; ?>
    <!--<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-XXXXXXXX-X', 'auto');
    ga('send', 'pageview');
</script>-->
</head>

<body>
    <script>
        if (window.document.documentMode) {
            document.write('<div class="load-alert ie-alert">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade it</a> for a better web experience.</div>');
        }
    </script>
    <noscript>
        <div class="load-alert js-alert">For full functionality of this website, it is best to <a href="https://www.whatismybrowser.com/guides/how-to-enable-javascript/" target="_blank">enable Javascript</a> in your web browser.</div>
    </noscript>

    <div class="skip-links">
        <a href="#main"><span class="v-h">Aller au </span>contenu principal</a>
        <a href="#menu" class="js-menu"><span class="v-h">Aller au </span>menu</a>
    </div>

    <?php include "_header.php"; ?>
    <main class="main-container">
        <?php include $inc . ".php"; ?>
    </main>
    <?php include "_footer.php"; ?>

    <script defer src="../assets/Front/js/plugins.min.js?5acf69667d09e942"></script>
    <script defer src="../assets/Front/js/main.min.js?bb4b7ced214de4c2"></script>

</body>

</html>
<?php
$CONFIG['site_name'] = "Generic";

$figs = glob("assets/figs/*.jpg");
array_unshift($figs, count($figs));

$lorem = "English The quick brown fox jumps over the lazy dog. Jamaican Chruu, a kwik di kwik brong fox a jomp huova di liezi daag de, yu no siit? Irish An ḃfuil do ċroí ag bualaḋ ó ḟaitíos an ġrá a ṁeall lena ṗóg éada ó ṡlí do leasa ṫú? D'ḟuascail Íosa Úrṁac na hÓiġe Beannaiṫe pór Éava agus Áḋaiṁ. Dutch Pa's wĳze lynx bezag vroom het fikse aquaduct. German Falsches Üben von Xylophonmusik quält jeden größeren Zwerg. German Im finſteren Jagdſchloß am offenen Felsquellwaſſer patzte der affig-flatterhafte kauzig-höf‌liche Bäcker über ſeinem verſifften kniffligen C-Xylophon. Norwegian Blåbærsyltetøy (blueberry jam, includes every extra letter used in Norwegian). Swedish Flygande bäckasiner söka strax hwila på mjuka tuvor. Icelandic Sævör grét áðan því úlpan var ónýt. Finnish Törkylempijävongahdus (This is a perfect pangram, every letter appears only once. Translating it is an art on its own, but I'll say rude lover's yelp. :-D) Finnish Albert osti fagotin ja töräytti puhkuvan melodian. (Albert bought a bassoon and hooted an impressive melody.) Finnish On sangen hauskaa, että polkupyörä on maanteiden jokapäiväinen ilmiö. (It's pleasantly amusing, that the bicycle is an everyday sight on the roads.) Polish Pchnąć w tę łódź jeża lub osiem skrzyń fig. Czech Příliš žluťoučký kůň úpěl ďábelské ódy. Slovak Starý kôň na hŕbe kníh žuje tíško povädnuté ruže, na stĺpe sa ďateľ učí kvákať novú ódu o živote. Slovenian Šerif bo za domačo vajo spet kuhal žgance. Greek (monotonic) ξεσκεπάζω την ψυχοφθόρα βδελυγμία Greek (polytonic) ξεσκεπάζω τὴν ψυχοφθόρα βδελυγμία Russian Съешь же ещё этих мягких французских булок да выпей чаю. Russian В чащах юга жил-был цитрус? Да, но фальшивый экземпляр! ёъ. Bulgarian Жълтата дюля беше щастлива, че пухът, който цъфна, замръзна като гьон. Sami (Northern) Vuol Ruoŧa geđggiid leat máŋga luosa ja čuovžža. Hungarian Árvíztűrő tükörfúrógép. Spanish El pingüino Wenceslao hizo kilómetros bajo exhaustiva lluvia y frío, añoraba a su querido cachorro. Spanish Volé cigüeña que jamás cruzó París, exhibe flor de kiwi y atún. Portuguese O próximo vôo à noite sobre o Atlântico, põe freqüentemente o único médico. French Les naïfs ægithales hâtifs pondant à Noël où il gèle sont sûrs d'être déçus en voyant leurs drôles d'œufs abîmés. Esperanto Eĥoŝanĝo ĉiuĵaŭde Esperanto Laŭ Ludoviko Zamenhof bongustas freŝa ĉeĥa manĝaĵo kun spicoj. Hebrew זה כיףן. Japanese (Hiragana) いろはにほへど ちりぬるを わがよたれぞ つねならむ うゐのおくやま けふこえて あさきゆめみじ ゑひもせず 🔥 ❤️ 🤖 🚀 ▸ ► ▶︎ ➤ → ▷ ⎈";

$pangrams = array(
    'Bâchez la queue du wagon-taxi avec les pyjamas du fakir',
    'Démasquez-vous ou je gifle les eaux blanches du WC au krypton !',
    'Portez ce vieux whisky à la juge blonde qui fume',
    'Refoulez ces barbus fatigués venus hanter les pommiers que Jacky Kennedy planta aux confins du Delaware',
    'Pour faire ce bon punch: rhum vieux, kiwi, jus d’orange, quelques noix de coco, zestes de citron et vanille. raccourci mais manque toujours le Y',
    'Amazingly few discotheques provide jukeboxes.',
    'Vous parlez d’une histoire ! Ce xylophoniste belge a flashé sur moi alors que je mangeais un kiwi en pianotant sur mon ordinateur portable !',
    'Zéphyrin allait au zoo chaque week-end jouer avec les animaux et surtout voir les belles girafes',
    'Portez ce vieux whisky au juge blond qui fume',
    'Voyez ce bon fakir moqueur pousser un wagon en jouant du xylophone',
    'The quick brown fox jumps over the lazy dog',
    'Le vif renard brun saute par-dessus le chien paresseux.',
    'Le vif lynx grimpe jusqu’au wok du chat zébré',
    'Voix ambiguë d’un cœur qui, au zéphyr, préfère les jattes de kiwis',
    'Mon pauvre zébu ankylosé choque deux fois ton wagon jaune',
    'Pomme exquise chez La Frite: Bintje, Nevsky, King Edward',
    'Voyez ce jeu exquis wallon, de graphie en kit mais bref',
    'Prouvez, beau juge, que le fameux sandwich au yak tue',
    'Vieux pelage que je modifie : breitschwanz ou yak ?',
    'Fougueux, j’enivre la squaw au pack de beau zythum',
    'Ketch, yawl, jonque flambant neuve... jugez des prix !',
    'Voyez le brick géant que j’examine près du wharf',
    'Vous jugez bien plus fameux ce quart de whisky',
    'Portez au juge cinq bols de vos fameux whisky',
    'Jugez qu’un vieux whisky blond pur malt fonce',
    'Le moujik équipé de faux breitschwanz voyage',
    'Faux kwachas ? Quel projet de voyage zambien !',
    'Kiwi fade, aptéryx, quel jambon vous gâchez !',
    'Fripon, mixez l’abject whisky qui vidange',
    'Vif juge, trempez ce blond whisky aqueux',
    'Vif P-DG mentor, exhibez la squaw jockey',
    'Juge, flambez l’exquis patchwork d’Yvon',
    'Perchez dix, vingt woks. Qu’y flambé-je ?',
    'Whisky vert : jugez cinq fox d’aplomb',
    );
array_unshift($pangrams, count($pangrams));
$CONFIG['pangrams'] = $pangrams;

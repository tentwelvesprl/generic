<div class="section wrapper">

    <?php include "elements/_breadcrumb.php"; ?>

    <header class="header">
        <h1 class="title"><?php echo randIpsum(2, 12, $words); ?></h1>
        <h2 class="subtitle"><?php echo randIpsum(2, 12, $words); ?></h2>
        <div class="cke cke--teaser">
            <p><?php echo randIpsum(2, 12, $words); ?></p>
        </div>
    </header>

    <ul class="items">
        <?php for ($n = 1, $r = rand(2, 7); $n <= $r; $n++) {
            include "elements/_Nav__item.php";
        } ?>
    </ul>
</div>
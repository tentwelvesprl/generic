<div class="section wrapper">

    <?php include "elements/_breadcrumb.php"; ?>

    <header class="header">
        <h1 class="title"><?php echo randIpsum(2, 12, $words); ?></h1>
        <h2 class="subtitle"><?php echo randIpsum(2, 12, $words); ?></h2>
    </header>

    <?php include "elements/_content_box.php"; ?>

    <?php include "elements/_files.php"; ?>
    <?php include "elements/_medias.php"; ?>
    <?php include "elements/_images.php"; ?>

</div>
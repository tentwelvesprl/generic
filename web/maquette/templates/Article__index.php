<div class="section wrapper">

    <header class="header">
        <h1 class="title">Articles</h1>
    </header>

    <ul class="items">
        <?php for ($n = 1, $r = rand(2, 7); $n <= $r; $n++) {
            include "elements/_Article__item.php";
        } ?>
    </ul>

    <?php include "elements/_pagination.php"; ?>

</div>
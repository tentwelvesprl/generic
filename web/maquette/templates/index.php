<div class="section wrapper">

    <header class="header">
        <h1 class="title">Templates</h1>
    </header>

    <ul class="items">
        <li class="item">
            <a href="?inc=templates/Homepage__view">
                <h3 class="title">Homepage</h3>
            </a>
        </li>
        <li class="item">
            <a href="?inc=templates/LandingPage__view">
                <h3 class="title">Landing Page</h3>
            </a>
        </li>
        <li class="item">
            <a href="?inc=templates/StaticPage__view">
                <h3 class="title">Static Page</h3>
            </a>
        </li>
        <li class="item">
            <a href="?inc=templates/Article__index">
                <h3 class="title">Article list</h3>
            </a>
        </li>
        <li class="item">
            <a href="?inc=templates/Article__view">
                <h3 class="title">Article</h3>
            </a>
        </li>
        <li class="item">
            <a href="?inc=templates/error404">
                <h3 class="title">Error 404</h3>
            </a>
        </li>
    </ul>

</div>
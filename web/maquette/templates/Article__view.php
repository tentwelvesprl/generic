<div class="section wrapper">

    <nav class="breadcrumb">
        <h6 class="v-h">Breadcrumb</h6>
        <ul>
            <li>/ <a href="?inc=templates/NewsArticle__index">News</a></li>
        </ul>
    </nav>

    <header class="header">
        <h1 class="title"><?php echo randIpsum(2, 12, $words); ?></h1>
        <h2 class="subtitle"><?php echo randIpsum(2, 12, $words); ?></h2>
    </header>

    <?php include "elements/_content_box.php"; ?>

    <?php include "elements/_files.php"; ?>

</div>
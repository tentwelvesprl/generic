<?php if (!isset($tmax)) $tmax = rand(1, 6); ?>
<?php for ($t = 0; $t <= $tmax; $t++) {
    switch (rand(1, 15)) {
        case 1:
        case 2: ?>
            <h2><?php echo randIpsum(2, 6, $words); ?>
            </h2>
        <?php break;
        case 3: ?>
            <h3><?php echo randIpsum(2, 6, $words); ?>
            </h3>
        <?php
        case 4: ?>
            <hr>
        <?php break;
        case 5: ?>
            <p><strong>bold</strong> — <em>italic</em> — <a href="#">link</a> — <sup>sup</sup> — <sub>sub</sub> — <q>quote</q> — <cite>cite</cite></p>
        <?php break;
        case 6: ?>
            <blockquote>
                <p>
                    <q><?php echo randIpsum(6, 22, $words); ?></q><br>
                    <cite><?php echo randIpsum(6, 3, $words); ?></cite>
                </p>
            </blockquote>
        <?php break;
        case 7: ?>
            <ol>
                <?php for ($x = 1, $rx = rand(1, 5); $x <= $rx; $x++) { ?>
                    <li><?php echo randIpsum(3, 10, $words); ?>
                    </li>
                <?php } ?>
            </ol>
        <?php break;
        case 8: ?>
            <ul>
                <?php for ($x = 1, $rx = rand(1, 5); $x <= $rx; $x++) { ?>
                    <li><?php echo randIpsum(3, 10, $words); ?>
                    </li>
                <?php } ?>
            </ul>
        <?php break;
        case 9: ?>
            <ul>
                <li><?php echo randIpsum(3, 10, $words); ?></li>
                <li><?php echo randIpsum(3, 10, $words); ?></li>
                    <ol>
                        <li><?php echo randIpsum(3, 10, $words); ?></li>
                        <li><?php echo randIpsum(3, 10, $words); ?></li>
                    </ol>
                </li>
                <li><?php echo randIpsum(3, 10, $words); ?></li>
            </ul>
            <ol>
                <li><?php echo randIpsum(3, 10, $words); ?></li>
                <li><?php echo randIpsum(3, 10, $words); ?>
                    <ul>
                        <li><?php echo randIpsum(3, 10, $words); ?></li>
                        <li><?php echo randIpsum(3, 10, $words); ?></li>
                    </ul>
                </li>
                <li><?php echo randIpsum(3, 10, $words); ?></li>
            </ol>
        <?php break;
        case 10: ?>
            <?php
            $table_rows = rand(1, 4);
            $table_columns = rand(1, 4);
            ?>
            <table>
                <tr>
                    <?php for ($y = 0; $y <= $table_columns; $y++) { ?>
                        <th><?php echo randIpsum(1, 3, $words); ?>
                        </th>
                    <?php } ?>
                </tr>
                <?php for ($x = 0; $x <= $table_rows; $x++) { ?>
                    <tr>
                        <?php for ($y = 0; $y <= $table_columns; $y++) { ?>
                            <td>
                                <p><?php echo randIpsum(1, 6, $words); ?>
                                </p>
                            </td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            </table>
        <?php break;
        case 11: ?>
            <div class="box">
                <h2><?php echo randIpsum(2, 4, $words); ?></h2>
                <p><?php echo randIpsum(20, 40, $words); ?></p>
            </div>
        <?php break;
        case 12: ?>
            <p><a href="#" class="cta"><?php echo randIpsum(1, 6, $words); ?></a></p>
        <?php break;
        case 13: ?>
            <h3 class="dropdown"><?php echo randIpsum(2, 6, $words); ?></h3>
            <p><?php echo randIpsum(6, 12, $words); ?></p>
        <?php break;
        default: ?>
            <p><?php echo randIpsum(20, 200, $words); ?>
            </p>
<?php break;
    }
} ?>
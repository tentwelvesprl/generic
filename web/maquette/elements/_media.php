<?php
$videos = array(
    array('id' => 'ywWBy6J5gz8', 'w' => 560, 'h' => 315, 'ratio' => 0.5625),
    array('id' => 'XaqR3G_NVoo', 'w' => 560, 'h' => 315, 'ratio' => 0.5625),
    array('id' => 'ROalU379l3U', 'w' => 640, 'h' => 315, 'ratio' => 0.5625),
    array('id' => 'Ns4TPTC8whw', 'w' => 560, 'h' => 315, 'ratio' => 0.5625),
    array('id' => 'CmPA7zE8mx0', 'w' => 560, 'h' => 315, 'ratio' => 0.5625),
    array('id' => 'lyZQPjUT5B4', 'w' => 560, 'h' => 315, 'ratio' => 0.5625)
);
$video = $videos[rand(0, 5)];
?>
<figure>
    <div class="ratio" style="padding-bottom:<?php echo $video['ratio'] * 100; ?>%">
        <div class="embed-plyr">
            <iframe width="<?php echo $video['w']; ?>" height="<?php echo $video['h']; ?>" src="https://www.youtube.com/embed/<?php echo $video['id']; ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
    <figcaption>
        <?php echo randIpsum(3, 10, $words); ?>
    </figcaption>
</figure>
<?php
$fig = $figs[rand(1, $figs[0])];
$sizes = getImgSizes($fig);
if ($sizes['ratio'] > 1) $orient = 'horz'; else $orient = 'vert';
?>
<figure class="<?php echo $orient; ?>">
    <div class="ratio" style="padding-bottom:<?php echo 100 / $sizes['ratio']; ?>%">
        <?php echo resImgHelper($fig, 'natural', randIpsum(6, 12, $words), '', $sizes['ratio']); ?>
    </div>
    <figcaption>
        <?php echo randIpsum(3, 10, $words); ?>
    </figcaption>
</figure>
<?php
for ($r = 0, $rmax = rand(6, 12); $r <= $rmax; $r++) {
    switch (rand(1, 10)) {
        case 1:
        case 2:
            include 'elements/_gallery.php';
            break;
        case 3: ?>
            <hr class="v-h" />
        <?php break;
        case 4:
            include 'elements/_sponsors.php';
            break;
        default: ?>
            <div class="cke cke--body">
                <?php include 'elements/_body.php'; ?>
            </div>
<?php break;
    }
} ?>
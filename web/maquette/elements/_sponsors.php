<?php
$logos = glob("assets/logos/*.png");
array_unshift($logos, count($logos));
$r = rand(1, 5);
if ($r) { ?>
    <div class="sponsors">
        <ul>
            <?php for ($n = 0; $n <= $r; $n++) {
                include "elements/_Sponsor__item.php";
            } ?>
        </ul>
    </div>
<?php }

<?php
$i_max = rand(0, 5);
$v_max = rand(0, 5);
$num = $i_max + $v_max;
if ($num > 0) {
?>
    <div class="items<?php if ($num < 3) echo ' items--'.$num; ?>">
        <ul>
        <?php for ($v = 0; $v < $v_max; $v++) {
            echo '<li>';
            include 'elements/_media.php';
            echo '</li>';
        } ?>
        <?php for ($i = 0; $i < $i_max; $i++) {
            echo '<li>';
            include 'elements/_image.php';
            echo '</li>';
        } ?>
        </ul>
    </div>
<?php } ?>
<?php if (isset($_GET['page'])) $page = $_GET['page']; else $page = 1; ?>

<nav class="pager">
	<ul class="pager__list">
		<?php if ($page != 1) { ?>
			<li class="pager__arrow pager__arrow--prev">
				<a href="?inc=<?php echo $inc?>&amp;page=<?php echo ($page - 1) ?>"><
				</a>
			</li>
		<?php } ?>
		<?php
		$r = $page+rand(0,10);
		for ($i=1; $i<=$r; $i++) {
			if ($i == $page) {
				echo '<li class="pager__item"><span class="pager__num pager__num--active">'. $i .'</span></li>';
			} else {
				echo '<li class="pager__item"><a class="pager__num pager__num--link" href="?inc='.$inc.'&amp;page='. $i .'">'. $i .'</a></li>';
			}
		} ?>
		<?php if ($page != $r) {?>
			<li class="pager__arrow pager__arrow--next">
				<a href="?inc=<?php echo $inc?>&amp;page=<?php echo ($page + 1) ?>">>
				</a>
			</li>
		<?php } ?>

	</ul>
</nav>

<li class="item">
    <a href="?inc=templates/StaticPage__view">
        <figure>
            <?php echo resImgHelper($figs[rand(1,$figs[0])], 'natural'); ?>
        </figure>
        <h3 class="title"><?php echo randIpsum(2, 12, $words); ?></h3>
        <h4 class="subtitle"><?php echo randIpsum(2, 12, $words); ?></h4>
        <p><?php echo randIpsum(2, 12, $words); ?></p>
    </a>
</li>

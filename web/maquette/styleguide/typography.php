<div class="section wrapper">

    <header class="header">
        <nav class="breadcrumb" aria-label="Breadcrumb">
            <ul>
                <li>
                    <a href="?inc=styleguide/index" class="js-back">Typography</a>
                </li>
            </ul>
        </nav>
        <h1 class="title">Items</h1>
    </header>

    <div class="cke cke--body">
        <?php $tmax = 12;
            include 'elements/_body.php'; ?>
    </div>

</div>
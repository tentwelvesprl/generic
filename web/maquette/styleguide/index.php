<div class="section wrapper">

        <header class="header">
            <h1 class="title">Styleguides</h1>
            <h2 class="subtitle">Essential markup, classnames and styles to build the UI</h2>
        </header>

        <ul class="items">
            <li class="item">
                <a href="?inc=styleguide/font_specimen">
                    <h3 class="title">Font Specimen</h3>
                    <p>Check your font sizes on a scale</p>
                </a>
            </li>
            <li class="item">
                <a href="?inc=styleguide/defaults">
                    <h3 class="title">Defaults</h3>
                    <p>Default styles for default markup</p>
                </a>
            </li>
            <li class="item">
                <a href="?inc=styleguide/grids">
                    <h3 class="title">Grids</h3>
                    <p>Grid system</p>
                </a>
            </li>
            <li class="item">
                <a href="?inc=styleguide/items">
                    <h3 class="title">Items</h3>
                    <p>items, cards, lists, rows</p>
                </a>
            </li>
            <li class="item">
                <a href="?inc=styleguide/elements">
                    <h3 class="title">Elements</h3>
                    <p>breadcrumb, pagination, filters, submenu</p>
                </a>
            </li>
            <li class="item">
                <a href="?inc=styleguide/inputs">
                    <h3 class="title">Inputs</h3>
                    <p>All you need to create forms</p>
                </a>
            </li>
            <li class="item">
                <a href="?inc=styleguide/buttons">
                    <h3 class="title">Buttons</h3>
                    <p>Link, button, call-to-action</p>
                </a>
            </li>
        </ul>

</div>
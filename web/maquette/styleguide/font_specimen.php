<div class="section wrapper">
    <header class="header">
        <nav class="breadcrumb" aria-label="Breadcrumb">
            <ul>
                <li>
                    <a href="?inc=styleguide/index">Styleguides</a>
                </li>
            </ul>
        </nav>
        <h1 class="title">Font specimen</h1>
    </header>
    <div class="cke">
        <?php for ($n = 6; $n <= 24; $n++) {
            echo '<p style="font-size:' . ($n * 2) . 'px; line-height: 1.25; margin-bottom: 16px"><strong><em>' . ($n * 2) . '</em> :</strong> ' . $pangrams[rand(1, $pangrams[0])] . '</p>';
        } ?>
    </div>
</div>
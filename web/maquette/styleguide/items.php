<div class="section wrapper">

    <header class="header">
        <nav class="breadcrumb" aria-label="Breadcrumb">
            <ul>
                <li>
                    <a href="?inc=styleguide/index" class="js-back">Styleguides</a>
                </li>
            </ul>
        </nav>
        <h1 class="title">Items</h1>
    </header>

    <hr>

    <h2>Gallery</h2>
    <?php include "elements/_gallery.php"; ?>
    </ul>

    <hr>

    <h2>Files</h2>
    <?php include "elements/_files.php"; ?>

</div>
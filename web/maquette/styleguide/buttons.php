<div class="section wrapper">

    <header class="header">
        <nav class="breadcrumb" aria-label="Breadcrumb">
            <ul>
                <li><a href="?inc=styleguide/index">Styleguides</a></li>
            </ul>
        </nav>
        <h1 class="title">Buttons</h1>
    </header>
    <a href="#">a.link</a>

    <hr>

    <a href="#" class="btn">a.btn</a>
    <button class="btn">button.btn</button>

    <hr>

    <a href="#" class="btn btn--large">a.btn large</a>
    <button class="btn btn--large">button.btn large</button>

    <hr>

    <a href="#" class="cta">a.cta</a>
    <button class="cta">button.cta</button>

    <hr>

    <a href="#" class="cta cta--large">a.btn--large</a>
    <button class="cta cta--large">button.btn--large</button>

</div>
<div class="section wrapper">

    <header class="header">
        <nav class="breadcrumb" aria-label="Breadcrumb">
            <ul>
                <li>
                    <a href="?inc=styleguide/index" class="js-back">Styleguides</a>
                </li>
            </ul>
        </nav>
        <h1 class="title">Grids</h1>
    </header>

    <h2>.items--2</h2>
    <ul class="items items--2">
        <?php for ($n = 1; $n <= rand(2, 12); $n++) { ?>
            <li>
                <figure class="ratio"> <?php echo $n; ?> / 1--2</figure>
            </li>
        <?php } ?>
    </ul>

    <hr>

    <h2>.items--3</h2>
    <ul class="items items--3">
        <?php for ($n = 1; $n <= rand(3, 12); $n++) { ?>
            <li>
                <figure class="ratio"> <?php echo $n; ?> / 1--3</figure>
            </li>
        <?php } ?>
    </ul>

    <h2>.items--4</h2>
    <ul class="items items--4">
        <?php for ($n = 1; $n <= rand(4, 12); $n++) { ?>
            <li>
                <figure class="ratio"> <?php echo $n; ?> / 1--4</figure>
            </li>
        <?php } ?>
    </ul>

    <hr>

    <h2>.items--5</h2>
    <ul class="items items--5">
        <?php for ($n = 1; $n <= rand(6, 12); $n++) { ?>
            <li>
                <figure class="ratio"> <?php echo $n; ?> / 1--5</figure>
            </li>
        <?php } ?>
    </ul>

    <hr>

    <h2>.items--6</h2>
    <ul class="items items--6">
        <?php for ($n = 1; $n <= rand(6, 12); $n++) { ?>
            <li>
                <figure class="ratio"> <?php echo $n; ?> / 1--6</figure>
            </li>
        <?php } ?>
    </ul>

    <hr>

    <h2>.no-orphans</h2>
    <?php
    $r = rand(2, 7);
    if ($r % 2) {
        $parity = 'odd';
    } else {
        $parity = 'even';
    }
    ?>
    <ul class="items items--no-orphans items--no-orphans--<?php echo $parity; ?>
    <?php if ($r % 3 == 0)  echo ' items--no-orphans--triple'; ?> items--no-orphans--<?php echo $r; ?>">
        <?php for ($n = 1; $n <= $r; $n++) { ?>
            <li>
                <figure class="ratio"> <?php echo $n; ?></figure>
            </li>
        <?php } ?>
    </ul>

</div>
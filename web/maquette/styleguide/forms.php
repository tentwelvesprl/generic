<div class="section wrapper">
    <header class="header">
        <nav class="breadcrumb" aria-label="Breadcrumb">
            <ul>
                <li>
                    <a href="?inc=styleguide/index">Styleguides</a>
                </li>
            </ul>
        </nav>
        <h1 class="title">Forms</h1>
    </header>

    <div class="alert alert--error">
        <h6 class="alert__title">titre du message d’erreur</h6>
        <p class="alert__text">
            <?php echo randIpsum(2, 12, $words); ?>
        </p>
        <button class="alert__close">fermer</button>
    </div>

    <div class="alert alert--warning">
        <h6 class="alert__title">titre du message d’alerte</h6>
        <p class="alert__text">
            <?php echo randIpsum(2, 12, $words); ?>
        </p>
        <button class="alert__close">fermer</button>
    </div>

    <div class="alert alert--success">
        <h6 class="alert__title">titre du message de confirmation</h6>
        <p class="alert__text">
            <?php echo randIpsum(2, 12, $words); ?>
        </p>
        <button class="alert__close">fermer</button>
    </div>

    <form action="#">

        <div class="input">
            <label>Text input</label>
            <input type="text" placeholder="I'm placeholder text">
        </div>

        <div class="input">
            <label for="Maxlength">input with maxlength</label>
            <input name="Maxlength" id="Maxlength" type="text" maxlength="25">
        </div>


        <div class="input error">
            <label>Text input <u>There’s an error</u></label>
            <input type="text" placeholder="I'm placeholder text">
        </div>

        <div class="input">
            <label>Readonly input</label>
            <input type="text" value="Read only text input" readonly>
        </div>

        <div class="input">
            <label>Disabled input</label>
            <input type="text" value="Disabled text input" disabled>
        </div>

        <div class="input">
            <label for="Required">Required input <u>*</u></label>
            <input name="Required" id="Required" type="text" autocomplete="no" required data-valid="notEmpty">
        </div>

        <div class="input">
            <label>Textarea</label>
            <textarea cols="30" rows="5" maxlength="600" placeholder="I'm placeholder text"></textarea>
        </div>

        <div class="input">
            <label>Email input</label>
            <input type="email" data-valid="validEmail">
        </div>

        <div class="input">
            <label>Search input</label>
            <input type="search">
        </div>

        <div class="input">
            <label>Tel input</label>
            <input type="tel" data-valid="validPhone">
        </div>

        <div class="input">
            <label>Postcode input</label>
            <input type="text" pattern="[0-9]{4-5}" data-valid="validPostcode">
        </div>

        <div class="input inline">
            <label>Phone (International)</label>
            <input type="text" name="field_country_code" maxlength="3" /> - <input type="text" name="field_city_code" maxlength="4" /> - <input type="text" name="field_phone_int" maxlength="8" />
        </div>

        <div class="input">
            <label>URL input</label>
            <input type="url">
        </div>

        <div class="input input-ext-url">
            <label>External URL input</label>
            <input type="url" placeholder="http://" data-valid="validUrl">
            <button>➜</button>
        </div>

        <div class="input">
            <label>Password input</label>
            <input type="password" value="password">
        </div>

        <fieldset>
            <legend>Grid system</legend>

            <div class="input input-20 alpha">
                <label>.input-20 <u>.&alpha;</u></label>
                <input type="text">
            </div>
            <div class="input input-20">
                <label>.input-20</label>
                <input type="text">
            </div>
            <div class="input input-20">
                <label>.input-20</label>
                <input type="text">
            </div>
            <div class="input input-20">
                <label>.input-20</label>
                <input type="text">
            </div>
            <div class="input input-20 omega">
                <label>.input-20 <u>.&omega;</u></label>
                <input type="text">
            </div>

            <div class="input input-20 alpha">
                <label>.input-20 <u>.&alpha;</u></label>
                <input type="text">
            </div>
            <div class="input input-40">
                <label>.input-40</label>
                <input type="text">
            </div>
            <div class="input input-40 omega">
                <label>.input-40 <u>.&omega;</u></label>
                <input type="text">
            </div>

            <div class="input input-20 alpha">
                <label>.input-20 <u>.&alpha;</u></label>
                <input type="text">
            </div>
            <div class="input input-80 omega">
                <label>.input-80 <u>.&omega;</u></label>
                <input type="text">
            </div>

            <div class="input input-33 alpha">
                <label>.input-33 <u>.&alpha;</u></label>
                <input type="text">
            </div>
            <div class="input input-33">
                <label>.input-33</label>
                <input type="text">
            </div>
            <div class="input input-33 omega">
                <label>.input-33 <u>.&omega;</u></label>
                <input type="text">
            </div>

            <div class="input input-33 alpha">
                <label>.input-33 <u>.&alpha;</u></label>
                <input type="text">
            </div>
            <div class="input input-66 omega">
                <label>.input-66 <u>.&omega;</u></label>
                <input type="text">
            </div>

            <div class="input input-25 alpha">
                <label>.input-25 <u>.&alpha;</u></label>
                <input type="text">
            </div>
            <div class="input input-25">
                <label>.input-25</label>
                <input type="text">
            </div>
            <div class="input input-25">
                <label>.input-25</label>
                <input type="text">
            </div>
            <div class="input input-25 omega">
                <label>.input-25 <u>.&omega;</u></label>
                <input type="text">
            </div>

            <div class="input input-25 alpha">
                <label>.input-25 <u>.&alpha;</u></label>
                <input type="text">
            </div>
            <div class="input input-50">
                <label>.input-50</label>
                <input type="text">
            </div>
            <div class="input input-25 omega">
                <label>.input-25 <u>.&omega;</u></label>
                <input type="text">
            </div>

            <div class="input input-25 alpha">
                <label>.input-25 <u>.&alpha;</u></label>
                <input type="text">
            </div>
            <div class="input input-75 omega">
                <label>.input-75 <u>.&omega;</u></label>
                <input type="text">
            </div>

            <div class="input input-50 alpha">
                <label>.input-50 <u>.&alpha;</u></label>
                <input type="text">
            </div>
            <div class="input input-50 omega">
                <label>.input-50 <u>.&omega;</u></label>
                <input type="text">
            </div>

            <div class="input">
                <label>.input-50 <u>.&omega;</u></label>
                <input type="text">
            </div>

        </fieldset>

        <fieldset>

            <legend>Browser Default</legend>

            <div class="input">
                <label>Select field</label>
                <select>
                    <option>Option 01</option>
                    <option>Option 02</option>
                </select>
            </div>

            <div class="input">
                <label>Multiple select field</label>
                <select multiple size="5">
                    <option>Option 1</option>
                    <option>Option 2</option>
                    <option>Option 3</option>
                    <option>Option 4</option>
                    <option>Option 5</option>
                    <option>Option 6</option>
                    <option>Option 7</option>
                    <option>Option 8</option>
                    <option>Option 9</option>
                    <option>Option 10</option>
                </select>
            </div>

            <div class="input radios">
                <input type="hidden" name="rad_">
                <label><input type="radio" name="rad_" value="1"> Radio input 1</label>
                <label><input type="radio" name="rad_" value="2"> Radio input 2</label>
            </div>

            <div class="input checkboxes">
                <label><input type="checkbox" value="1"> Checkbox input 1</label>
                <label><input type="checkbox" value="2"> Checkbox input 2</label>
            </div>

            <div class="input">
                <label>File input</label>
                <input type="file" accept=".jpg,.png,.pdf">
            </div>

        </fieldset>


        <fieldset>
            <legend>HTML5 input elements <u>critical if unsupported</u></legend>

            <div class="input">
                <label>Color input</label>
                <input type="color" pattern="/^#?([a-f0-9]{6}|[a-f0-9]{3})$/" value="#000000" placeholder="#000000" data-valid="validColor">
            </div>

            <div class="input">
                <label>Number input</label>
                <input type="number" value="5" min="0" max="10" data-valid="validInteger">
            </div>

            <div class="input input-range">
                <label>Range input</label>
                <input type="range" id="range" value="0" min="0" max="100">
                <output for="range">0</output>
                <script>
                    if (document.querySelector) {
                        document.querySelector('#range').onchange = function(e) {
                            e.target.nextElementSibling.innerText = e.target.value;
                        }
                    }
                </script>
            </div>

            <div class="input">
                <label>Date input</label>
                <input type="date" data-valid="validDate">
            </div>

            <div class="input">
                <label>Month input</label>
                <input type="month">
            </div>

            <div class="input">
                <label>Week input</label>
                <input type="week">
            </div>

            <div class="input">
                <label>Time input</label>
                <input type="time">
            </div>

            <div class="input">
                <label>Datetime-local input</label>
                <input type="datetime-local">
            </div>
        </fieldset>

    </form>

</div>
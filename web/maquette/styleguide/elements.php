<div class="section wrapper">

    <header class="header">
        <nav class="breadcrumb" aria-label="Breadcrumb">
            <ul>
                <li>
                    <a href="?inc=styleguide/index">Styleguides</a>
                </li>
            </ul>
        </nav>
        <h1 class="title">Elements</h1>
    </header>

    <h2 class="title title--section">Breadcrumb</h2>
    <?php include "elements/_breadcrumb.php"; ?>

    <hr>

    <h2 class="title title--section">Pagination</h2>
    <?php include "elements/_pagination.php"; ?>

</div>
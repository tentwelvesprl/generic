module.exports = function(grunt) {
  'use strict';
  var timestamp = Date.now();
  require('time-grunt')(grunt);

  const sass = require('sass');

  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),
    env: grunt.file.readJSON('environment.json'),

    dir: {
      web: '../web/',
      assets: '../web/assets/Front/',
      maquette: '../web/maquette/',
      views: '../private/app/views/'
    },

    concat: {
      options: {
        separator: ';\n'
      },
      plugins: {
        src: ['scripts/plugins/**/*.js', '!**/_*.js'],
        dest: '.tmp/plugins.js'
      }
    },

    jshint: {
      files: ['scripts/main.js'],
      options: {
        jshintrc: '.jshintrc'
      }
    },

    uglify : {
      main: {
        options: {
          banner: '/*\n' +
          ' * <%= pkg.name %> <%= pkg.version %>\n' +
          ' *\n' +
          ' * modified on <%= grunt.template.today("dd-mm-yyyy") %>\n' +
          ' * by <%= pkg.author %>\n' +
          ' *\n' +
          ' */\n'
        },
        files: [{
            expand: true,
            cwd: 'scripts/',
            src: ['*.js', 'modules/*.js'],
            dest: '<%= dir.assets %>js/',
            ext: '.min.js'
        }]
      },
      plugins: {
        files: {
          '<%= dir.assets %>js/plugins.min.js' : '.tmp/plugins.js'
        }
      }
    },

    svgstore: {
        options: {
            cleanup: true,
            inheritviewbox: true,
            includeTitleElement: false,
            symbol: {
              preserveAspectRatio: 'xMidYMid meet'
            }
        },
        default : {
            files: {
                '<%= dir.assets %>img/sprite.svg' : ['svg/*.svg'],
            }
        }
    },

  sass: {
    options: {
      implementation: sass,
      sourceMap: '<%= env.map %>'
    },
    dist: {
      files: [{
        expand: true,
        cwd: 'sass/',
        src: ['**/*.sass', '**/*.scss'],
        dest: '.tmp/css/',
        ext: '.css'
     }]
    }
  },

  postcss: {
    options: {
      map: {
        inline: '<%= env.map %>'
      },
      processors: [
      require('postcss-normalize')(),
      require('postcss-easing-gradients')(),
      require('pixrem')(),
      require('autoprefixer')(),
      require('cssnano')()
      ],
      failOnError: true
    },
    dist: {
      expand: true,
      cwd: '.tmp/css/',
      src: '**/*.css',
      dest: '<%= dir.assets %>css/',
      ext: '.min.css'
    }
  },

  watch: {
    options: {
      spawn: false,
    },
    svg: {
      files: ['svg/*.svg'],
      tasks: ['svgstore']
    },
    sass: {
      files: ['sass/**/*.sass', 'sass/**/*.scss'],
      tasks: ['sass', 'postcss']
    },
    js: {
      files: ['scripts/*.js', 'scripts/modules/*.js'],
      tasks: ['jshint', 'uglify:main']
    },
    plugins: {
      files: ['scripts/plugins/**/*.js'],
      tasks: ['concat:plugins', 'uglify:plugins']
    }
  },

  browserSync: {
    dev: {
      files: {
        src : [
        '<%= dir.assets %>css/**/*.css',
        '<%= dir.assets %>js/*.min.js',
        '<%= dir.maquette %>**/*.php',
        '<%= dir.views %>**/*.twig'
        ]
      },
      options: {
        watchTask: true,
        ghostMode: {
          clicks: false,
          forms: false,
          scroll: false
        },
        proxy: '<%= env.proxy %>',
        online: '<%= env.online %>',
        open: '<%= env.open %>',
        startPath: '<%= env.startPath %>'
      }
    }
  },

  filesToBust: ['web/maquette/index.php', 'private/app/views/**/*.twig'],
  replace: {
    options: {
      patterns: [
      {
        match: /\.(js|css)\?([0-9a-f]{16})"/g,
        replacement: '.$1"'
      }
      ]
    },
    dist: {
      files: [{
        expand: true,
        cwd: '../',
        dest: '../',
        src: '<%= filesToBust %>'
      }]
    }
  },
  cacheBust: {
    options: {
      assets: ['css/**/*.css', 'js/*.js'],
      baseDir: '<%= dir.assets %>',
      queryString: true,
      createCopies: false,
      urlPrefixes: ['{{ global.request.basepath }}/assets/Front', '../assets/Front']
    },
    dist: {
      files: [{
        expand: true,
        cwd: '../',
        src: '<%= filesToBust %>'
      }]
    }
  }

});

require('load-grunt-tasks')(grunt);

grunt.registerTask('dev', ['replace:dist', 'browserSync', 'watch']);
grunt.registerTask('css', ['sass', 'postcss']);
grunt.registerTask('js', ['jshint', 'concat', 'uglify']);
grunt.registerTask('svg', 'svgstore');
grunt.registerTask('bust', ['replace:dist', 'cacheBust:dist']);
grunt.registerTask('default', ['css', 'jshint', 'svgstore', 'concat', 'uglify', 'bust']);

};

let ww = window.innerWidth;
let vh = window.innerHeight * 0.01;

document.documentElement.style.setProperty('--vh', vh + 'px');

// throttle
let throttleTimer;
const throttle = (callback, time) => {
    if (throttleTimer) {
        return;
    }
    throttleTimer = true;
    setTimeout(() => {
        callback();
        throttleTimer = false;
    }, time);
};
// debounce
let debounceTimer;
const debounce = (callback, time) => {
    window.clearTimeout(debounceTimer);
    debounceTimer = window.setTimeout(callback, time);
};

// for collecting siblings
let getSiblings = function (e) {
    let siblings = [];
    if (! e.parentNode) {
        return siblings;
    }
    let sibling = e.parentNode.firstChild;
    while (sibling) {
        if (sibling.nodeType === 1 && sibling !== e) {
            siblings.push(sibling);
        }
        sibling = sibling.nextSibling;
    }
    return siblings;
};

// wwCalc
const wwCalc = () => {
    ww = window.innerWidth;
    vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', vh + 'px');
};
window.addEventListener('load', wwCalc, false);
window.addEventListener('resize', () => {
    throttle(wwCalc, 100);
}, false);

var menuFold = () => {
    const burger = document.getElementById('burger');
    burger.addEventListener('click', (e) => {
        e.preventDefault();
        this.blur();
        document.body.classList.toggle('menu-open');
    });
};

document.addEventListener('DOMContentLoaded', function () {
    menuFold();
});
<?php
namespace App\Homepage;

use Viktor\Pack\Base\Badge\Badge;
use Viktor\Pack\Content\Article\AbstractArticle;

class Homepage extends AbstractArticle
{
    public function getType()
    {
        return 'homepage';
    }

    public function getSingularHumanType()
    {
        return 'homepage';
    }

    public function getPluralHumanType()
    {
        return 'homepages';
    }

    public function getTitle()
    {
        $fields = $this->getDataset()->getFields();
        $title = trim($fields['title']->getPlainTextRepresentation());
        if ($title == '') {
            $title = 'Untitled ' . $this->getSingularHumanType() . ' #' . $this->getId();
        }
        return $title;
    }

    public function getHtmlTitle()
    {
        $html_title = htmlspecialchars($this->getTitle());
        return $html_title;
    }

    public function getDescription()
    {
        return '';
    }

    public function getImage()
    {
        $image = null;
        return $image;
    }

    public function getBadge()
    {
        $homepage_helper = new HomepageHelper($this->getApp());
        $homepage_id = $homepage_helper->getLastPublishedHomepageId($this->getApp()['i18n']->getLanguage());
        if ($homepage_id == $this->getId()) {
            return new Badge(239, 63, 15, 'Active now');
        }
        return null;
    }

    public function getDatasetCombinedDescription()
    {
        $options = array(
            'add_seo_fields' => true,
        );
        $sections = array(
            '' => array(
                'label' => 'Main',
                'collapse' => false,
            ),
        );
        $fields = array(
            'title' => array(
                'translatable' => false,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\TextField',
                    'options'    => array(
                        'indexed' => true,
                        'source_type' => 'plain_text',
                    ),
                    'full_text_index_weights' => array(
                        'general' => 10,
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\TextFieldWidget',
                    'options'    => array(
                        'label' => 'Title',
                        'searchable' => true,
                        'sortable' => true,
                        'legend' => 'This title is for internal use only. It will not be displayed on the website.'
                    ),
                ),
            ),
            'banners' => array(
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\ContentUnitReferencesField',
                    'options'    => array(
                        'content_unit_types' => array('banner'),
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\ContentUnitReferencesFieldWidget',
                    'options'    => array(
                        'label' => 'Banners',
                    ),
                ),
            ),
        );
        return array(
            'options' => $options,
            'sections' => $sections,
            'fields' => $fields,
        );
    }

    public function getPublicationOptions()
    {
        $options = array(
            'use_published' => true,
            'use_start_datetime' => true,
            'use_end_datetime' => false,
        );
        return $options;
    }

}

<?php
namespace App\Homepage;

use Viktor\Application;

class HomepageHelper
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function getApp()
    {
        return $this->app;
    }

    public function getLastPublishedHomepageId($language)
    {
        $homepage_model = $this->getApp()['content_unit_factory']->create('homepage');
        $homepages_ids = $homepage_model->getIds(
            array(
                array(
                    'type' => 'published_now'
                ),
            ),
            array(
                array(
                    'option' => 'publication_time',
                    'direction' => 'DESC',
                ),
            )
        );
        if (count($homepages_ids) > 0) {
            return $homepages_ids[0];
        } else {
            return 0;
        }
    }

}

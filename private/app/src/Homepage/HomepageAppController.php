<?php
namespace App\Homepage;

use App\App\AppHtmlPage;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Viktor\Application;
use Viktor\Pack\Content\Controller\HtmlPage;

class HomepageAppController extends AppHtmlPage
{
    public function index(Request $request, Application $app)
    {
        $available_languages = array_keys($app['viktor.config']['available_languages']);
        $default_language = $available_languages[0];
        $cookie_language = $request->cookies->get('preferred_language', '');
        $accepted_language = substr($request->headers->get('Accept-Language', ''), 0, 2);
        $language = $default_language;
        if (in_array($cookie_language, $available_languages)) {
            $language = $cookie_language;
        } elseif (in_array($accepted_language, $available_languages)) {
            $language = $accepted_language;
        }
        return $app->redirect($app->path('homepage', array('language' => $language)));
    }

    public function homepage(Request $request, Application $app, $language)
    {
        $this->init($request, $app);
        $app['i18n']->setLanguage($language);
        $language = $app['i18n']->getLanguage();
        $this->setLanguage($language);
        // Can we load the content unit?
        $homepage_helper = new HomepageHelper($app);
        $homepage_id = $homepage_helper->getLastPublishedHomepageId($language);
        $homepage = $app['content_unit_factory']->load('homepage', $homepage_id);
        if ($homepage == null) {
            $app->abort(404);
        }
        $homepage->setLanguage($language);
        if (!$homepage->canBeViewed()) {
            $app->abort(404);
        }
        // Prepare variables.
        $this->setUrl($app->path('homepage', array('language' => $language)));
        foreach ($app['viktor.config']['available_languages'] as $k => $v) {
            if ($k != $language) {
                $this->setAlternateUrl($k, $app->path('homepage', array('language' => $k)));
            }
        }
        $this->addMarker('homepage');
        // SEO.
        $homepage_digest = $homepage->getDigest();
        $this->setSeoTitle($homepage_digest['seo_title']);
        $this->setSeoDescription(trim(html_entity_decode(strip_tags($homepage_digest['seo_description']))));
        if (!empty($homepage_digest['seo_image'])) {
            $this->setSeoImage(
                $app->path(
                    'image-from-content-unit',
                    array(
                        'language' => $language,
                        'format' => 'og',
                        'type' => $homepage_digest['seo_image'][0]['type'],
                        'id' => $homepage_digest['seo_image'][0]['id'],
                    )
                )
            );
        }
        // Render.
        return $this->render(
            'Homepage/view.twig',
            array(
                'id' => $homepage->getId(),
            )
        );
    }

    public function view(Request $request, Application $app, $language, $id)
    {
        $this->init($request, $app);
        $app['i18n']->setLanguage($language);
        $language = $app['i18n']->getLanguage();
        $this->setLanguage($language);
        // Can we load the content unit?
        $homepage = $app['content_unit_factory']->load('homepage', $id);
        if ($homepage == null) {
            $app->abort(404);
        }
        $homepage->setLanguage($language);
        if (!$homepage->canBeEdited()) {
            $app->abort(404);
        }
        // Prepare variables.
        $app['main_content_unit']->set($homepage);
        // Render.
        return $this->render(
            'Homepage/view.twig',
            array(
                'id' => $homepage->getId(),
            )
        );
    }

}

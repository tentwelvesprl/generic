<?php
namespace App\Homepage;

use App\App\CacheHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\EventListener\AbstractSessionListener;
use Viktor\Application;

class HomepageController
{
    public function viewMainBlock(Request $request, Application $app)
    {
        $language = (string) $request->query->get('language', '');
        $id = (int) $request->query->get('id', 0);
        $homepage = $app['content_unit_factory']->load('homepage', $id);
        if ($homepage == null) {
            return '';
        }
        $homepage->setLanguage($language);
        $homepage_digest = $homepage->getDigest();
        // Cache.
        $cache_helper = new CacheHelper($app);
        $etag = md5(serialize(array(
            $cache_helper->getLastModificationTs()
        )));
        $response = new Response();
        $response->headers->set(AbstractSessionListener::NO_AUTO_CACHE_CONTROL_HEADER, 'true');
        $response->setETag($etag);
        $response->setPublic();
        if ($response->isNotModified($request)) {
            return $response;
        }
        // Render.
        return $app->render(
            'Homepage/viewMainBlock.twig',
            array(
                'language' => $language,
                'homepage' => $homepage_digest,
            ),
            $response
        );
    }

}

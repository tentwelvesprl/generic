<?php
namespace App\StaticPage;

use Viktor\Pack\Base\Helper\StringHelper;
use Viktor\Pack\Content\Article\AbstractArticle;

class StaticPage extends AbstractArticle
{
    public function getType()
    {
        return 'static-page';
    }

    public function getSingularHumanType()
    {
        return 'static page';
    }

    public function getPluralHumanType()
    {
        return 'static pages';
    }

    public function getTitle()
    {
        $fields = $this->getDataset()->getFields();
        $title = trim($fields['title_' . $this->getLanguage()]->getPlainTextRepresentation());
        if ($title == '') {
            $title = 'Untitled ' . $this->getSingularHumanType() . ' #' . $this->getId();
        }
        return $title;
    }

    public function getHtmlTitle()
    {
        $html_title = htmlspecialchars($this->getTitle());
        return $html_title;
    }

    public function getDescription()
    {
        $description = $this->getHtmlDescription();
        $description = preg_replace('#<br\s*/?>#i', "\n", $description);
        $description = html_entity_decode(strip_tags($description), ENT_QUOTES | ENT_HTML5);
        $description = trim($description);
        return $description;
    }

    public function getHtmlDescription()
    {
        $fields = $this->getDataset()->getFields();
        $html_description = trim($fields['item_description_' . $this->getLanguage()]->getProcessedValue());
        return $html_description;
    }

    public function getImage()
    {
        $image_reference = $this->getImageReference();
        if (empty($image_reference)) {
            return null;
        }
        $image = $this->getApp()['content_unit_factory']->load($image_reference['type'], $image_reference['id']);
        if ($image != null) {
            return $image->getImage();
        }
        return null;
    }

    public function getImageReference()
    {
        $fields = $this->getDataset()->getFields();
        if (!empty($fields['item_image']->get())) {
            return $fields['item_image']->get()[0];
        }
        return null;
    }

    public function generateDefaultSmartUrlCandidate($language)
    {
        $fields = $this->getDataset()->getFields();
        if (trim($fields['title_' . $language]->get()) == '') {
            return '';
        }
        return '/' . $language . '/' . StringHelper::slugify($fields['title_' . $language]->get());
    }

    public function getDatasetCombinedDescription()
    {
        $options = array(
            'add_seo_fields' => true,
            'add_smart_url_fields' => true,
        );
        $sections = array(
            '' => array(
                'label' => 'Main content',
                'collapse' => false,
            ),
            'item' => array(
                'label' => 'Item',
                'collapse' => true,
            ),
        );
        $fields = array(
            'title' => array(
                'translatable' => true,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\TextField',
                    'options'    => array(
                        'indexed' => true,
                        'source_type' => 'plain_text',
                    ),
                    'full_text_index_weights' => array(
                        'general' => 1000,
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\TextFieldWidget',
                    'options'    => array(
                        'label' => 'Title',
                        'searchable' => true,
                        'sortable' => true,
                    ),
                ),
            ),
            'subtitle' => array(
                'translatable' => true,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\TextField',
                    'options'    => array(
                        'indexed' => true,
                        'source_type' => 'plain_text',
                    ),
                    'full_text_index_weights' => array(
                        'general' => 500,
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\TextFieldWidget',
                    'options'    => array(
                        'label' => 'Subtitle',
                        'searchable' => true,
                        'sortable' => false,
                    ),
                ),
            ),
            'main_image' => array(
                'translatable' => false,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\ImageField',
                    'options'    => array(
                        'max_number' => 1,
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\ImageFieldWidget',
                    'options'    => array(
                        'label' => 'Main image',
                    ),
                ),
            ),
            'content_box' => array(
                'translatable' => true,
                'model' => array(
                    'class_name' => 'Viktor\Pack\ContentBox\Field\ContentBoxField',
                    'options'    => array(
                        'authorized_tags' => '<p><h3><h4><h5><ul><ol><li><strong><em><sub><sup><br><hr><a><q><cite><blockquote><table><tr><td>',
                        'media_type' => 'media',
                        'content_unit_types' => array('news-article'),
                    ),
                    'full_text_index_weights' => array(
                        'general' => 1,
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\ContentBox\Field\ContentBoxFieldWidget',
                    'options'    => array(
                        'label' => 'Content box',
                        'ckeditor_config_file_url' => '/assets/Admin/cke/body-cke-config.js',
                    ),
                ),
            ),
            'files' => array(
                'translatable' => false,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\FileField',
                    'options'    => array(
                        'file_content_unit_type' => 'file',
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\FileFieldWidget',
                    'options'    => array(
                        'label' => 'Files',
                    ),
                ),
            ),
            'item_description' => array(
                'translatable' => true,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\TextField',
                    'options'    => array(
                        'indexed' => true,
                        'authorized_tags' => '<p><strong><em><sub><sup><br>',
                        'source_type' => 'html',
                    ),
                    'full_text_index_weights' => array(
                        'general' => 100,
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\TextFieldWidget',
                    'options'    => array(
                        'label' => 'Item description',
                        'legend' => 'Define a text for the item.',
                        'searchable' => true,
                        'sortable' => false,
                        'section' => 'item',
                        'ckeditor_config_file_url' => '/assets/Admin/cke/teaser-cke-config.js',
                    ),
                ),
            ),
            'item_image' => array(
                'translatable' => false,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\ImageField',
                    'options'    => array(
                        'max_number' => 1,
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\ImageFieldWidget',
                    'options'    => array(
                        'label' => 'Item image',
                        'legend' => 'Define an image for the item.',
                        'section' => 'item',
                    ),
                ),
            ),
        );
        return array(
            'options' => $options,
            'sections' => $sections,
            'fields' => $fields
        );
    }

}

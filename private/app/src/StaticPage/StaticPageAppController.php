<?php
namespace App\StaticPage;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\App\AppHtmlPage;
use Viktor\Application;

class StaticPageAppController extends AppHtmlPage
{
    public function view(Request $request, Application $app, $language, $id)
    {
        $this->init($request, $app);
        $app['i18n']->setLanguage($language);
        $language = $app['i18n']->getLanguage();
        $this->setLanguage($language);
        // Can we load the content unit?
        $static_page = $app['content_unit_factory']->load('static-page', $id);
        if ($static_page == null) {
            $app->abort(404);
        }
        $static_page->setLanguage($language);
        if (!$static_page->canBeViewed()) {
            $app->abort(404);
        }
        // Prepare variables.
        $app['main_content_unit']->set($static_page);
        // Render.
        return $this->render(
            'StaticPage/view.twig',
            array(
                'id' => $static_page->getId(),
            )
        );
    }

}

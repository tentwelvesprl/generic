<?php
namespace App\StaticPage;

use Viktor\Application;

class StaticPageHelper
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function getApp()
    {
        return $this->app;
    }

    public function getPublishedStaticPagesIds($language)
    {
        $static_page_model = $this->getApp()['content_unit_factory']->create('static-page');
        $static_page_model->setLanguage($language);
        $static_page_ids = $static_page_model->getIds(
            array(
                array(
                    'type' => 'published_now',
                ),
            )
        );
        return $static_page_ids;
    }

}

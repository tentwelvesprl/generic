<?php
namespace App\StaticPage;

use App\App\CacheHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\EventListener\AbstractSessionListener;
use Viktor\Application;

class StaticPageController
{
    public function viewMainBlock(Request $request, Application $app)
    {
        $language = (string) $request->query->get('language', '');
        $id = (int) $request->query->get('id', 0);
        $markers = (array) $request->query->get('markers', array());
        $static_page = $app['content_unit_factory']->load('static-page', $id);
        if ($static_page == null) {
            return '';
        }
        $static_page->setLanguage($language);
        $static_page_digest = $static_page->getDigest();
        // Cache.
        $cache_helper = new CacheHelper($app);
        $etag = md5(serialize(array(
            $cache_helper->getLastModificationTs()
        )));
        $response = new Response();
        $response->headers->set(AbstractSessionListener::NO_AUTO_CACHE_CONTROL_HEADER, 'true');
        $response->setETag($etag);
        $response->setPublic();
        if ($response->isNotModified($request)) {
            return $response;
        }
        // Render.
        return $app->render(
            'StaticPage/viewMainBlock.twig',
            array(
                'language' => $language,
                'markers' => $markers,
                'static_page' => $static_page_digest,
                'regrouped_content_box' => $static_page->getDataset()->getFields()['content_box_' . $language]->getRegroupedDigest(),
            ),
            $response
        );
    }

    public function item(Request $request, Application $app)
    {
        $language = (string) $request->query->get('language', '');
        $id = (int) $request->query->get('id', 0);
        $static_page = $app['content_unit_factory']->load('static-page', $id);
        if ($static_page == null) {
            return '';
        }
        $static_page->setLanguage($language);
        $static_page_digest = $static_page->getDigest();
        // Cache.
        $cache_helper = new CacheHelper($app);
        $etag = md5(serialize(array(
            $cache_helper->getLastModificationTs()
        )));
        $response = new Response();
        $response->headers->set(AbstractSessionListener::NO_AUTO_CACHE_CONTROL_HEADER, 'true');
        $response->setETag($etag);
        $response->setPublic();
        if ($response->isNotModified($request)) {
            return $response;
        }
        // Render.
        return $app->render(
            'StaticPage/item.twig',
            array(
                'language' => $language,
                'static_page' => $static_page_digest,
            ),
            $response
        );
    }

}

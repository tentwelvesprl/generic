<?php
namespace App\StaticImage;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\EventListener\AbstractSessionListener;
use Viktor\Application;
use Viktor\Pack\Base\Image\ImageGenerator;
use Viktor\Pack\Base\Image\SourceImage;

class StaticImageAppController
{
    public function imageFromFile(Request $request, Application $app, $language, $format)
    {
        // Check image path validity.
        $path = (string) $request->query->get('path', '');
        $normalized_path = realpath($app['viktor.app_path'] . $path);
        if ($normalized_path == '') {
            $app->abort(404, 'The original image file does not exist.');
        }
        $authorized = false;
        foreach ($app['viktor.config']['static_images_authorized_directories'] as $authorized_dir) {
            $normalized_authorized_dir = realpath($app['viktor.app_path'] . $authorized_dir);
            if (strpos($normalized_path, $normalized_authorized_dir) === 0) {
                $authorized = true;
            }
        }
        // Register formats.
        $json = file_get_contents($app['viktor.app_path'] . '/config/img_formats.json');
        $a = json_decode($json, true);
        foreach ($a['img_generation_config'] as $format_name => $format_description) {
            $transformations = array();
            $output_format = array(
                'mime_type' => 'image/jpeg',
                'options' => array('jpeg_quality' => 80),
            );
            if (isset($format_description['mime_type'])) {
                $output_format['mime_type'] = $format_description['mime_type'];
            }
            if (isset($format_description['jpeg_quality'])) {
                $output_format['options']['jpeg_quality'] = $format_description['jpeg_quality'];
            }
            foreach ($format_description['transformations'] as $t_name => $t_desc) {
                $transformations[] = array(
                    'name' => $t_name,
                    'options' => $t_desc,
                );
            }
            $app['image_generator.formats']->registerFormat(
                $format_name, array(
                    'transformations' => $transformations,
                    'output_format' => $output_format,
                    'max_age' => 60 * 60 * 24 * 365.25,
                )
            );
        }
        // Create the source image object.
        $source_image = new SourceImage($normalized_path);
        // Generate the image.
        $format_description = $app['image_generator.formats']->getFormatDescription($format);
        if (!is_array($format_description)) {
            $app->abort(404, 'Unknown format.');
        }
        $transformations = array();
        $mime_type = 'image/png';
        $output_format_options = array();
        $max_age = 0;
        if (isset($format_description['transformations'])) {
            $transformations = $format_description['transformations'];
        }
        if (isset($format_description['output_format']) && is_array($format_description['output_format'])) {
            $output_format = $format_description['output_format'];
            if (isset($output_format['mime_type'])) {
                $mime_type = $output_format['mime_type'];
            }
            if (isset($output_format['options'])) {
                $output_format_options = $output_format['options'];
            }
        }
        if (isset($format_description['max_age'])) {
            $max_age = (int) $format_description['max_age'];
        }
        if (isset($format_description['shared_max_age'])) {
            $shared_max_age = (int) $format_description['shared_max_age'];
        }
        $response = new Response();
        $response->headers->set(AbstractSessionListener::NO_AUTO_CACHE_CONTROL_HEADER, 'true');
        $response->setETag(md5(''
            . $source_image->getEtag()
            . serialize($format_description)
        ));
        $response->setPublic();
        if ($response->isNotModified($request)) {
            return $response;
        }
        if ($max_age > 0) {
            $response->setMaxAge($max_age);
        }
        if ($shared_max_age > 0) {
            $response->setSharedMaxAge($shared_max_age);
        }
        $image_generator = new ImageGenerator($app, $source_image);
        foreach ($transformations as $transformation) {
            $image_generator->addTransformation($transformation);
        }
        $image_generator->setOutputFormat($mime_type, $output_format_options);
        return $image_generator->getResponse($response);
    }

}

<?php
namespace App\File;

use Viktor\Pack\Content\FileContentUnit\AbstractFileContentUnit;

class File extends AbstractFileContentUnit
{
    public function getType()
    {
        return 'file';
    }

    public function getSingularHumanType()
    {
        return 'file';
    }

    public function getPluralHumanType()
    {
        return 'files';
    }

}

<?php
namespace App\File;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\EventListener\AbstractSessionListener;
use Viktor\Application;

class FileController
{
    public function item(Request $request, Application $app)
    {
        // Prepare variables.
        $language = (string) $request->query->get('language', '');
        $type = (string) $request->query->get('type', '');
        $id = (string) $request->query->get('id', '');
        $file = $app['content_unit_factory']->load($type, $id);
        if ($file == null) {
            return '';
        }
        $file->setLanguage($language);
        // Cache.
        $etag = md5(serialize(array(
            $file->getModificationDateTime()->format('U'),
        )));
        $response = new Response();
        $response->headers->set(AbstractSessionListener::NO_AUTO_CACHE_CONTROL_HEADER, 'true');
        $response->setETag($etag);
        $response->setPublic();
        if ($response->isNotModified($request)) {
            return $response;
        }
        // Go!
        return $app->render(
            'File/item.twig',
            array(
                'language' => $language,
                'file' => $file->getDigest(),
                'file_info' => $file->getAttachment()->getFileInfoDigest(),
            ),
            $response
        );
    }

}

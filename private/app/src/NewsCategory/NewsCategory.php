<?php

namespace App\NewsCategory;

use Viktor\Pack\Content\Node\AbstractNode;

class NewsCategory extends AbstractNode
{
     public function getType()
    {
        return 'news-category';
    }

    public function getSingularHumanType()
    {
        return 'news category';
    }

    public function getPluralHumanType()
    {
        return 'news categories';
    }

}
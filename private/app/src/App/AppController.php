<?php
namespace App\App;

use App\App\CacheHelper;
use App\Navigation\NavigationHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\EventListener\AbstractSessionListener;
use Viktor\Application;

class AppController
{
    public function errorXxxMainBlock(Request $request, Application $app)
    {
        $language = (string) $request->query->get('language', '');
        $error_code = (int) $request->query->get('error_code', 418);
        $error_text = (string) $request->query->get('error_text', 'I\'m a teapot');
        // Cache.
        $cache_helper = new CacheHelper($app);
        $etag = md5(serialize(array(
            $cache_helper->getLastModificationTs()
        )));
        $response = new Response();
        $response->headers->set(AbstractSessionListener::NO_AUTO_CACHE_CONTROL_HEADER, 'true');
        $response->setETag($etag);
        $response->setPublic();
        if ($response->isNotModified($request)) {
            return $response;
        }
        // Render.
        return $app->render(
            'App/errorXxxMainBlock.twig',
            array(
                'language' => $language,
                'error_code' => $error_code,
                'error_text' => $error_text,
            ),
            $response
        );
    }

    public function mainMenu(Request $request, Application $app)
    {
        $language = (string) $request->query->get('language', '');
        $markers = (array) $request->get('markers', array());
        $alternate_urls = (array) $request->get('alternate_urls', array());
        // Cache.
        $cache_helper = new CacheHelper($app);
        $etag = md5(serialize(array(
            $cache_helper->getLastModificationTs()
        )));
        $response = new Response();
        $response->headers->set(AbstractSessionListener::NO_AUTO_CACHE_CONTROL_HEADER, 'true');
        $response->setETag($etag);
        $response->setPublic();
        if ($response->isNotModified($request)) {
            return $response;
        }
        // Prepare variables.
        $navigation_helper = new NavigationHelper($app, $language, $markers);
        $main_menu_children = array();
        foreach ($navigation_helper->getFlatList() as $key => $value) {
            if ($key == $app['viktor.config']['main_menu_node_id']) {
                $main_menu_children = $value;
            }
        }
        // Render.
        return $app->render(
            'App/mainMenu.twig',
            array(
                'language' => $language,
                'navigation_helper' => $navigation_helper,
                'alternate_urls' => $alternate_urls,
                'main_menu_children' => $main_menu_children,
            ),
            $response
        );
    }

    public function breadcrumb(Request $request, Application $app)
    {
        $language = (string) $request->get('language', '');
        $markers = (array) $request->get('markers', array());
        $show_last_item = (bool) $request->get('show_last_item', false);
        // Cache.
        $cache_helper = new CacheHelper($app);
        $etag = md5(serialize(array(
            $cache_helper->getLastModificationTs(),
        )));
        $response = new Response();
        $response->headers->set(AbstractSessionListener::NO_AUTO_CACHE_CONTROL_HEADER, 'true');
        $response->setETag($etag);
        $response->setPublic();
        if ($response->isNotModified($request)) {
            return $response;
        }
        // Prepare variables.
        $navigation_helper = new NavigationHelper($app, $language, $markers);
        $elements_ids = $navigation_helper->getBreadcrumb();
        if ($show_last_item) {
            $elements_ids = array_slice($elements_ids, 1, count($elements_ids) - 1);
        } else {
            $elements_ids = array_slice($elements_ids, 1, count($elements_ids) - 2);
        }
        // Go!
        return $app->render(
            'App/breadcrumb.twig',
            array(
                'language' => $language,
                'elements_ids' => $elements_ids,
                'elements' => $navigation_helper->getElements(),
            ),
            $response
        );
    }

}

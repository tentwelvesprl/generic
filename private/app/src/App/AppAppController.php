<?php
namespace App\App;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\App\AppHtmlPage;
use Viktor\Application;

class AppAppController extends AppHtmlPage
{
    public function errorXxx(Request $request, Application $app)
    {
        $this->init($request, $app);
        $error_code = (int) $request->query->get('error_code', 418);
        $error_text = (string) $request->query->get('error_text', 'I\'m a teapot');
        // Try to guess the language.
        $master_request = $app['request_stack']->getMasterRequest();
        $path_info = $master_request->getPathInfo();
        $hypothetical_language = substr($path_info, 1, 2);
        if (isset($app['viktor.config']['available_languages'][$hypothetical_language])) {
            $language = $hypothetical_language;
        } else {
            $language = $this->getPreferredLanguage($master_request, $app);
        }
        $app['i18n']->setLanguage($language);
        $language = $app['i18n']->getLanguage();
        $this->setLanguage($language);
        // Render.
        return $this->render(
            'App/errorXxx.twig',
            array(
                'error_code' => $error_code,
                'error_text' => $error_text,
            )
        );
    }

}

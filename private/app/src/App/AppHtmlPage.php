<?php
namespace App\App;

use App\Navigation\NavigationHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Viktor\Application;
use Viktor\Pack\Content\ContentUnit\HasSeoPropertiesInterface;
use Viktor\Pack\Content\Controller\HtmlPage;

class AppHtmlPage extends HtmlPage
{
    private $markers;
    private $seoTitle;
    private $seoHtmlTitle;
    private $seoDescription;
    private $seoHtmlDescription;
    private $seoImageReference;

    public function __construct()
    {
        parent::__construct();
        $this->markers = array();
        $this->seoTitle = '';
        $this->seoDescription = '';
        $this->seoImageReference = null;
    }

    public function init(Request $request, Application $app)
    {
        parent::init($request, $app);
        $app['i18n']->loadDictionary($app['viktor.app_path'] . '/app.dict.txt');
    }

    public function setLanguage($language)
    {
        parent::setLanguage($language);
    }

    public function getMarkers()
    {
        return $this->markers;
    }

    public function addMarker($marker)
    {
        $this->markers[] = $marker;
    }

    public function getSeoTitle()
    {
        return $this->seoTitle;
    }

    public function setSeoTitle($seo_title)
    {
        $this->seoTitle = (string) $seo_title;
    }

    public function getSeoHtmlTitle()
    {
        return $this->seoHtmlTitle;
    }

    public function setSeoHtmlTitle($seo_html_title)
    {
        $this->seoHtmlTitle = (string) $seo_html_title;
    }

    public function getSeoDescription()
    {
        return $this->seoDescription;
    }

    public function setSeoDescription($seo_description)
    {
        $this->seoDescription = (string) $seo_description;
    }

    public function getSeoHtmlDescription()
    {
        return $this->seoHtmlDescription;
    }

    public function setSeoHtmlDescription($seo_html_description)
    {
        $this->seoHtmlDescription = (string) $seo_html_description;
    }

    public function getSeoImageReference()
    {
        return $this->seoImageReference;
    }

    public function setSeoImageReference($seo_image_reference)
    {
        $this->seoImageReference = (array) $seo_image_reference;
    }

    public function render($view, array $variables, Response $response = null)
    {
        // Global user variable.
        $variables['user'] = $this->getApp()['viktor.user']->getDigest();
        // Handle section properties.
        $navigation_helper = new NavigationHelper($this->getApp(), $this->getLanguage(), $this->getMarkers());
        $elements_ids = $navigation_helper->getBreadcrumb();
        $sorted_flat_list = array_keys($navigation_helper->getFlatList());
        if (count($sorted_flat_list) > 1) {
            // Add the first node of the navigation tree as a default node.
            array_unshift($elements_ids, $sorted_flat_list[1]);
        }
        foreach ($elements_ids as $element_id) {
            $node = $this->getApp()['content_unit_factory']->load('menu-item', $element_id);
            if ($node == null) {
                continue;
            }
            $node->setLanguage($this->getLanguage());
            $node_digest = $node->getDigest();
            if ($node instanceof HasSeoPropertiesInterface) {
                $this->setSeoTitle($node->getSeoTitle());
                $this->setSeoHtmlTitle($node->getSeoHtmlTitle());
                $this->setSeoDescription($node->getSeoDescription());
                $this->setSeoHtmlDescription($node->getSeoHtmlDescription());
                $this->setSeoImageReference($node->getSeoImageReference());
            } else {
                $this->setSeoTitle($node->getTitle());
                $this->setSeoHtmlTitle($node->getHtmlTitle());
                $this->setSeoDescription($node->getDescription());
                $this->setSeoHtmlDescription($node->getHtmlDescription());
                $this->setSeoImageReference($node->getImageReference());
            }
        }
        // Handle main content unit.
        $main_content_unit = $this->getApp()['main_content_unit']->get();
        if ($main_content_unit != null) {
            $main_content_unit_digest = $main_content_unit->getDigest();
            $variables['main_content_unit'] = $main_content_unit_digest;
            $this->addMarker($main_content_unit->getType());
            $this->addMarker($main_content_unit->getType() . ':' . $main_content_unit->getId());
            $fields = $main_content_unit->getDataset()->getFields();
            if ($main_content_unit instanceof HasSeoPropertiesInterface) {
                $this->setSeoTitle($main_content_unit->getSeoTitle());
                $this->setSeoHtmlTitle($main_content_unit->getSeoHtmlTitle());
                $this->setSeoDescription($main_content_unit->getSeoDescription());
                $this->setSeoHtmlDescription($main_content_unit->getSeoHtmlDescription());
                $this->setSeoImageReference($main_content_unit->getSeoImageReference());
            } else {
                $this->setSeoTitle($main_content_unit->getTitle());
                $this->setSeoHtmlTitle($main_content_unit->getHtmlTitle());
                $this->setSeoDescription($main_content_unit->getDescription());
                $this->setSeoHtmlDescription($main_content_unit->getHtmlDescription());
                $this->setSeoImageReference($main_content_unit->getImageReference());
            }
        }
        // Prepare render.
        $variables['markers'] = $this->getMarkers();
        $variables['seo_title'] = $this->getSeoTitle();
        $variables['seo_html_title'] = $this->getSeoHtmlTitle();
        $variables['seo_description'] = $this->getSeoDescription();
        $variables['seo_html_description'] = $this->getSeoHtmlDescription();
        $variables['seo_image_reference'] = $this->getSeoImageReference();
        // Go!
        if ($response == null) {
            $response = new Response();
        }
        $response = parent::render($view, $variables, $response);
        return $response;
    }

}

<?php
namespace App\App;

use Viktor\Application;

class CacheHelper
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function getApp()
    {
        return $this->app;
    }

    public function getLastModificationTs()
    {
        static $mts = null;
        if ($mts !== null) {
            return $mts;
        }
        $query_builder = $this->getApp()['db']->createQueryBuilder();
        $query_builder
            ->select(
                'MAX(article.modification_uts) AS mts'
            )
            ->from('article', 'article')
        ;
        $result = $query_builder->execute();
        $row = $result->fetch();
        $article_mts = $row['mts'];
        $query_builder = $this->getApp()['db']->createQueryBuilder();
        $query_builder
            ->select(
                'MAX(node.modification_uts) AS mts'
            )
            ->from('node', 'node')
        ;
        $result = $query_builder->execute();
        $row = $result->fetch();
        $node_mts = $row['mts'];
        $query_builder = $this->getApp()['db']->createQueryBuilder();
        $query_builder
            ->select(
                'MAX(image.modification_uts) AS mts'
            )
            ->from('image', 'image')
        ;
        $result = $query_builder->execute();
        $row = $result->fetch();
        $image_mts = $row['mts'];
        $query_builder = $this->getApp()['db']->createQueryBuilder();
        $query_builder
            ->select(
                'MAX(publication_index.start_uts) AS mts'
            )
            ->from('publication_index', 'publication_index')
            ->where('publication_index.start_uts <= :now')
            ->setParameter('now', time())
        ;
        $result = $query_builder->execute();
        $row = $result->fetch();
        $publication_start_mts = $row['mts'];
        $query_builder = $this->getApp()['db']->createQueryBuilder();
        $query_builder
            ->select(
                'MAX(performances_field_index.ts) AS mts'
            )
            ->from('performances_field_index', 'performances_field_index')
            ->where('performances_field_index.ts <= :now')
            ->setParameter('now', time())
        ;
        $result = $query_builder->execute();
        $row = $result->fetch();
        $performances_mts = $row['mts'];
        $mts = max($article_mts, $node_mts, $image_mts, $publication_start_mts, $performances_mts);
        return $mts;
    }

}

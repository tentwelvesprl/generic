<?php
namespace App\Banner;

use App\App\CacheHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\EventListener\AbstractSessionListener;
use Viktor\Application;

class BannerController
{
    public function item(Request $request, Application $app)
    {
        $language = (string) $request->query->get('language', '');
        $id = (int) $request->query->get('id', 0);
        $banner = $app['content_unit_factory']->load('banner', $id);
        if ($banner == null) {
            return '';
        }
        $banner->setLanguage($language);
        $banner_digest = $banner->getDigest();
        // Cache.
        $cache_helper = new CacheHelper($app);
        $etag = md5(serialize(array(
            $cache_helper->getLastModificationTs()
        )));
        $response = new Response();
        $response->headers->set(AbstractSessionListener::NO_AUTO_CACHE_CONTROL_HEADER, 'true');
        $response->setETag($etag);
        $response->setPublic();
        if ($response->isNotModified($request)) {
            return $response;
        }
        // Prepare variables.
        $url = $banner_digest['url'];
        $linked_content_unit = null;
        $linked_content_unit_digest = null;
        if (!empty($banner_digest['linked_content_unit'])) {
            $linked_content_unit = $app['content_unit_factory']->load(
                $banner_digest['linked_content_unit'][0]['type'],
                $banner_digest['linked_content_unit'][0]['id']
            );
            if ($linked_content_unit != null) {
                $linked_content_unit->setLanguage($language);
                $url = $linked_content_unit->getViewPageUrl();
                $linked_content_unit_digest = $linked_content_unit->getDigest();
            }
        }
        // Render.
        return $app->render(
            'Banner/item.twig',
            array(
                'language' => $language,
                'banner' => $banner_digest,
                'url' => $url,
                'linked_content_unit' => $linked_content_unit_digest,
            ),
            $response
        );
    }

}


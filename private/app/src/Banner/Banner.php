<?php
namespace App\Banner;

use Viktor\Pack\Content\Article\AbstractArticle;

class Banner extends AbstractArticle
{
    public function getType()
    {
        return 'banner';
    }

    public function getSingularHumanType()
    {
        return 'banner';
    }

    public function getPluralHumanType()
    {
        return 'banners';
    }

    public function getTitle()
    {
        $fields = $this->getDataset()->getFields();
        $title = trim($fields['title_' . $this->getLanguage()]->getPlainTextRepresentation());
        if ($title == '') {
            $title = 'Untitled ' . $this->getSingularHumanType() . ' #' . $this->getId();
        }
        return $title;
    }

    public function getHtmlTitle()
    {
        $html_title = htmlspecialchars($this->getTitle());
        return $html_title;
    }

    public function getImage()
    {
        $image_reference = $this->getImageReference();
        if (empty($image_reference)) {
            return null;
        }
        $image = $this->getApp()['content_unit_factory']->load($image_reference['type'], $image_reference['id']);
        if ($image != null) {
            return $image->getImage();
        }
        return null;
    }

    public function getImageReference()
    {
        $fields = $this->getDataset()->getFields();
        if (!empty($fields['main_image']->get())) {
            return $fields['main_image']->get()[0];
        }
        return null;
    }

    public function getHint()
    {
        $hint = '';
        return $hint;
    }

    public function getDatasetCombinedDescription()
    {
        $sections = array(
            '' => array(
                'label' => 'Main',
                'collapse' => false,
            ),
            'destination' => array(
                'label' => 'Destination',
                'collapse' => false,
            ),
        );
        $fields = array(
            'title' => array(
                'translatable' => true,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\TextField',
                    'options'    => array(
                        'indexed' => true,
                        'source_type' => 'plain_text',
                    ),
                    'full_text_index_weights' => array(
                        'general' => 10,
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\TextFieldWidget',
                    'options'    => array(
                        'label' => 'Title',
                        'searchable' => true,
                        'sortable' => true,
                    ),
                ),
            ),
            'main_image' => array(
                'translatable' => false,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\ImageField',
                    'options'    => array(
                        'max_number' => 1,
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\ImageFieldWidget',
                    'options'    => array(
                        'label' => 'Image',
                        'legend' => 'jpg | max-width ≈ 3072px | max-height ≈ 2048px | quality ≥ 80 | sRGB',
                    ),
                ),
            ),
            'linked_content_unit' => array(
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\ContentUnitReferencesField',
                    'options'    => array(
                        'content_unit_types' => array('static-page', 'news-article'),
                        'max_number' => 1,
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\ContentUnitReferencesFieldWidget',
                    'options'    => array(
                        'label' => 'Linked content unit',
                        'section' => 'destination',
                    ),
                ),
            ),
            'url' => array(
                'translatable' => true,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\TextField',
                    'options'    => array(
                        'indexed' => false,
                        'source_type' => 'plain_text',
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\TextFieldWidget',
                    'options'    => array(
                        'label' => 'Url',
                        'searchable' => false,
                        'sortable' => false,
                        'section' => 'destination',
                    ),
                ),
            ),
            'target' => array(
                'translatable' => false,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\ListField',
                    'options'    => array(
                        'items' => array(
                            '_self' => array(
                                'label' => 'Current tab',
                                'selected' => 1,
                            ),
                            '_blank' => array(
                                'label' => 'New tab',
                                'selected' => 0,
                            ),
                        ),
                        'multiple' => false,
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\ListFieldWidget',
                    'options'    => array(
                        'label' => 'Open in',
                        'display' => 'normal',
                        'section' => 'destination',
                    ),
                ),
            ),
        );
        return array(
            'sections' => $sections,
            'fields' => $fields
        );
    }

    public function getPublicationOptions()
    {
        $options = array(
            'use_published' => false,
            'use_start_datetime' => false,
            'use_end_datetime' => false,
        );
        return $options;
    }

}

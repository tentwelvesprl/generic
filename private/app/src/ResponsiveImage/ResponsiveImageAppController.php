<?php
namespace App\ResponsiveImage;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Viktor\Application;
use Viktor\Pack\Base\Image\ImageAppController;

class ResponsiveImageAppController
{
    public function imageFromContentUnit(Request $request, Application $app, $language, $format, $type, $id)
    {
        $json = file_get_contents($app['viktor.app_path'] . '/config/img_formats.json');
        $a = json_decode($json, true);
        foreach ($a['img_generation_config'] as $format_name => $format_description) {
            $transformations = array();
            $output_format = array(
                'mime_type' => 'image/jpeg',
                'options' => array('jpeg_quality' => 80),
            );
            if (isset($format_description['mime_type'])) {
                $output_format['mime_type'] = $format_description['mime_type'];
            }
            if (isset($format_description['jpeg_quality'])) {
                $output_format['options']['jpeg_quality'] = $format_description['jpeg_quality'];
            }
            foreach ($format_description['transformations'] as $t_name => $t_desc) {
                $transformations[] = array(
                    'name' => $t_name,
                    'options' => $t_desc,
                );
            }
            $app['image_generator.formats']->registerFormat(
                $format_name, array(
                    'transformations' => $transformations,
                    'output_format' => $output_format,
                    'max_age' => 60 * 60 * 24 * 365.25,
                )
            );
        }
        $image_app_controller = new ImageAppController();
        return $image_app_controller->imageFromContentUnit($request, $app, $language, $format, $type, $id);
    }

}

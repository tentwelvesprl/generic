<?php
namespace App\ResponsiveImage;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Viktor\Application;

class ResponsiveImageController
{
    public function picture(Request $request, Application $app, $language, $responsive_format, $type, $id, $additional_class = '', $alt = '', $title = '', $etag = 'etag', $ratio = '', $loading = 'lazy', $objectfit = '', $objectposition = '')
    {
        if (isset($app['viktor.config']['cdn_base_url'])) {
            $cdn_base_url = $app['viktor.config']['cdn_base_url'];
        } else {
            $cdn_base_url = '';
        }
        $output = '';
        $json = file_get_contents($app['viktor.app_path'] . '/config/img_formats.json');
        $a = json_decode($json, true);
        $output_img = '';
        $picture_sources = array();
        if (isset($a['img_helper_config'][$responsive_format])) {
            $format_config = $a['img_helper_config'][$responsive_format];
            $img_src = '';
            $img_srcset = '';
            $img_params = '';
            $img_class = '';
            if (isset($format_config['src'])) {
                if (!empty($format_config['src'])) {
                    $url = $cdn_base_url . $app->path('responsive-image-from-content-unit', array(
                        'language' => $language,
                        'format' => $format_config['src'],
                        'type' => $type,
                        'id' => $id,
                        'etag' => $etag,
                    ));
                    $img_src = ' src="' . $url . '"';
                }
                $img_srcsets_array = array();
                if (isset($format_config['srcset'])) {
                    foreach ($format_config['srcset'] as $value) {
                        $srcset = $cdn_base_url . $app->path('responsive-image-from-content-unit', array(
                            'language' => $language,
                            'format' => $value[0],
                            'type' => $type,
                            'id' => $id,
                            'etag' => $etag,
                        ));
                        if (!empty($value[1])) $srcset .= ' ' . $value[1];
                        $img_srcsets_array[] = $srcset;
                    }
                }
                if (!empty($img_srcsets_array)) $img_srcset =  ' srcset="' . $url . ' 16w" data-srcset="' . implode(', ', $img_srcsets_array) . '"';
                if (!empty($format_config['params'])) $img_params = ' ' . $format_config['params'];
                if (!empty($ratio)) $img_params .= ' width="288" height="' . round((1 / $ratio) * 288) . '" data-aspectratio="' . $ratio . '"';
                if (!empty($objectfit)) $img_params .= ' data-object-fit="' . $objectfit . '"';
                if ($loading == 'eager') $img_params .= ' fetchpriority="high"';
                if (!empty($objectposition)) $img_params .= ' data-object-position="' . $objectposition . '" style="object-position: ' . $objectposition . ';"';
                if (!empty($additional_class)) $img_class .= ' ' . $additional_class;
                $img_class = ' class="lazyload' . $img_class . '"';
                $output_img = '<img loading="lazy"' . $img_src . $img_srcset . $img_params . $img_class . ' alt="' . $alt . '"' . ' title="' . $title . '"' . '>';
            }
            if (isset($format_config['sources'])) {
                foreach ($format_config['sources'] as $def_source) {
                    $source_srcsets = array();
                    foreach ($def_source['srcset'] as $value) {
                        $cursrcset = $cdn_base_url . $app->path('responsive-image-from-content-unit', array(
                            'language' => $language,
                            'format' => $value[0],
                            'type' => $type,
                            'id' => $id,
                            'etag' => $etag,
                        ));
                        if (!empty($value[1])) $cursrcset .= ' ' . $value[1];
                        $source_srcsets[] = $cursrcset;
                    }
                    if (!empty($source_srcsets)) {
                        $source = '<source srcset="' . $url . ' 16w" data-srcset="' . implode(', ', $source_srcsets) . '"';
                        if (!empty($def_source['params'])) $source .= ' ' . $def_source['params'];
                        if (!empty($ratio)) $source .= ' data-aspectratio="' . $ratio . '"';
                        $source .= '>';
                        $picture_sources[] = $source;
                    }
                }
            }
        }
        if (!empty($picture_sources)) {
            $output = ''
                . '<picture>'
                . implode("\n", $picture_sources) . "\n"
                . $output_img . "\n"
                . '</picture>';
        } elseif (!empty($output_img)) {
            $output = $output_img;
        }
        return $output;
    }

}

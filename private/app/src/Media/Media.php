<?php
namespace App\Media;

use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Viktor\Pack\Base\Image\SourceImage;
use Viktor\Pack\Content\Media\AbstractMedia;

class Media extends AbstractMedia
{
    public function getType()
    {
        return 'media';
    }

    public function getSingularHumanType()
    {
        return 'embed';
    }

    public function getPluralHumanType()
    {
        return 'embeds';
    }

}

<?php
namespace App\Media;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\EventListener\AbstractSessionListener;
use Viktor\Application;
use Viktor\Pack\Base\Image\ImageGenerator;

class MediaController
{
    public function item(Request $request, Application $app)
    {
        // Prepare variables.
        $language = (string) $request->query->get('language', '');
        $id = (int) $request->query->get('id', '');
        $template = (string) $request->query->get('template', 'item');
        $show_figcaption = (bool) $request->query->get('show_figcaption', false);
        $media = $app['content_unit_factory']->load('media', $id);
        if ($media == null) {
            return '';
        }
        $media->setLanguage($language);
        // Cache.
        $etag = md5(serialize(array(
            $media->getModificationDateTime()->format('U'),
        )));
        $response = new Response();
        $response->headers->set(AbstractSessionListener::NO_AUTO_CACHE_CONTROL_HEADER, 'true');
        $response->setETag($etag);
        $response->setPublic();
        if ($response->isNotModified($request)) {
            return $response;
        }
        $media_digest = $media->getDigest();
        // Ratio.
        $ratio = 0.0;
        $source_image = $media->getImage();
        if ($source_image) {
            try {
                $image_generator = new ImageGenerator($app, $source_image);
                $image_generator->load();
                if ($image_generator->getHeight() > 0) {
                    $ratio = ($image_generator->getWidth() / $image_generator->getHeight()) * 100.0;
                }
            } catch (Exception $e) {
                // Fail silently...
            }
        }
        if ($ratio < 0.1) {
            $ratio = (16.0 / 9.0) * 100.0;
        }
        // Source
        $embed_src = '';
        preg_match('/src="([^"]+)"/', $media_digest['_embed_info']['embed_code'], $match);
        $embed_src = $match[1];
        $url_host = parse_url($embed_src, PHP_URL_HOST);
        $host_elements = explode('.', $url_host);
        $embed_provider = '';
        if (count($host_elements) >= 2) {
            $embed_provider= $host_elements[count($host_elements) - 2];
        }
        // Go!
        return $app->render(
            'Media/' . $template . '.twig',
            array(
                'language' => $language,
                'media' => $media_digest,
                'embed_src' => $embed_src,
                'embed_provider' => $embed_provider,
                'ratio' => $ratio,
                'show_figcaption' => $show_figcaption,
            ),
            $response
        );
    }

}

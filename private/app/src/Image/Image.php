<?php
namespace App\Image;

use Viktor\Pack\Content\Image\AbstractImage;

class Image extends AbstractImage
{
    public function getType()
    {
        return 'image';
    }

    public function getSingularHumanType()
    {
        return 'image';
    }

    public function getPluralHumanType()
    {
        return 'images';
    }

}

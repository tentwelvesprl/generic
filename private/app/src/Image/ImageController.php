<?php
namespace App\Image;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\EventListener\AbstractSessionListener;
use Viktor\Application;
use Viktor\Pack\Base\Image\HasAnUploadedImageInterface;
use Viktor\Pack\Base\Image\ImageDocumentInterface;
use Viktor\Pack\Base\Image\ImageGenerator;
use Viktor\Pack\Base\Image\SourceImage;

class ImageController
{
    public function item(Request $request, Application $app)
    {
        // Prepare variables.
        $language = (string) $request->query->get('language', '');
        $template = (string) $request->query->get('template', 'img');
        $type = (string) $request->query->get('type', '');
        $id = (string) $request->query->get('id', '');
        $format = (string) $request->query->get('format', 'natural');
        $aspectratio = (float) $request->query->get('aspectratio', 0);
        $loading = (string) $request->query->get('loading', 'lazy');
        $additional_class = (string) $request->query->get('additional_class', '');
        $show_figcaption = (bool) $request->query->get('show_figcaption', false);
        $content_unit = $app['content_unit_factory']->load($type, $id);
        if ($content_unit == null) {
            return '';
        }
        $content_unit->setLanguage($language);
        // Cache.
        $etag = md5(serialize(array(
            $content_unit->getModificationDateTime()->format('U'),
        )));
        $response = new Response();
        $response->headers->set(AbstractSessionListener::NO_AUTO_CACHE_CONTROL_HEADER, 'true');
        $response->setETag($etag);
        $response->setPublic();
        if ($response->isNotModified($request)) {
            return $response;
        }
        // Prepare variables.
        $width = 0;
        $height = 0;
        $area = 0;
        $ratio = 1.0;
        $path = '';
        $caption = '';
        $html_caption = '';
        $credits = '';
        $html_credits = '';
        $alt_text = '';
        $html_alt_text = '';
        if ($content_unit instanceof HasAnUploadedImageInterface) {
            $width = $content_unit->getUploadedImage()->getWidth();
            $height = $content_unit->getUploadedImage()->getHeight();
            $area = $content_unit->getUploadedImage()->getArea();
            $ratio = $content_unit->getUploadedImage()->getRatio();
            $path = $content_unit->getUploadedImage()->getFilePath();
        } elseif ($content_unit->getImage() instanceof SourceImage) {
            $source_image = $content_unit->getImage();
            try {
                $image_generator = new ImageGenerator($app, $source_image);
                $image_generator->load();
                $width = $image_generator->getWidth();
                $height = $image_generator->getHeight();
                $area = $width * $height;
                if ($height > 0) {
                    $ratio = $width / $height;
                }
                $path = $source_image->getPath();
            } catch (Exception $e) {
                $app->abort(404, 'Could not load image.');
            }
        } else {
            $app->abort(404, 'No image attached to this content unit.');
        }
        if ($content_unit instanceof ImageDocumentInterface) {
            $caption = $content_unit->getCaption();
            $html_caption = $content_unit->getHtmlCaption();
            $credits = $content_unit->getCredits();
            $html_credits = $content_unit->getHtmlCredits();
            $alt_text = $content_unit->getAltText();
            $html_alt_text = $content_unit->getHtmlAltText();
        }
        // Go!
        return $app->render(
            'Image/' . $template . '.twig',
            array(
                'language' => $language,
                'content_unit' => $content_unit->getDigest(),
                'width' => $width,
                'height' => $height,
                'area' => $area,
                'ratio' => $ratio,
                'caption' => $caption,
                'html_caption' => $html_caption,
                'credits' => $credits,
                'html_credits' => $html_credits,
                'alt_text' => $alt_text,
                'html_alt_text' => $html_alt_text,
                'format' => $format,
                'aspectratio' => $aspectratio,
                'loading' => $loading,
                'additional_class' => $additional_class,
                'show_figcaption' => $show_figcaption,
                'has_alpha' => preg_match('/\.png$/i', $path),
            ),
            $response
        );
    }

}

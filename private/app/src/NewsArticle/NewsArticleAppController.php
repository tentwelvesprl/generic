<?php
namespace App\NewsArticle;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\App\AppHtmlPage;
use Viktor\Application;

class NewsArticleAppController extends AppHtmlPage
{

    public function index(Request $request, Application $app, $language)
    {
        $this->init($request, $app);
        $app['i18n']->setLanguage($language);
        $language = $app['i18n']->getLanguage();
        $this->setLanguage($language);
        // Pagination.
        $current_page = (int) $request->query->get('_current_page', 1);
        if ($current_page < 1) {
            $current_page = 1;
        }
        // Prepare variables.
        $this->setUrl($app->path('news-article-index', array('language' => $language)));
        foreach ($app['viktor.config']['available_languages'] as $k => $v) {
            if ($k != $language) {
                $this->setAlternateUrl($k, $app->path('news-article-index', array('language' => $k)));
            }
        }
        $this->addMarker('news-article');
        $this->addMarker('news-article-index');
        $this->setTitle($app['i18n']->getTranslation('News', $language));
        // Render.
        return $this->render(
            'NewsArticle/index.twig',
            array(
                'current_page' => $current_page,
            )
        );
    }

    public function view(Request $request, Application $app, $language, $id)
    {
        $this->init($request, $app);
        $app['i18n']->setLanguage($language);
        $language = $app['i18n']->getLanguage();
        $this->setLanguage($language);
        // Can we load the content unit?
        $news_article = $app['content_unit_factory']->load('news-article', $id);
        if ($news_article == null) {
            $app->abort(404);
        }
        $news_article->setLanguage($language);
        if (!$news_article->canBeViewed()) {
            $app->abort(404);
        }
        // Prepare variables.
        $app['main_content_unit']->set($news_article);
        // Render.
        return $this->render(
            'NewsArticle/view.twig',
            array(
                'id' => $news_article->getId(),
            )
        );
    }

}

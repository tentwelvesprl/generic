<?php
namespace App\NewsArticle;

use App\App\CacheHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\EventListener\AbstractSessionListener;
use Viktor\Application;
use Viktor\Pack\Base\Pagination\Pagination;

class NewsArticleController
{
    public function indexMainBlock(Request $request, Application $app)
    {
        $language = (string) $request->query->get('language', '');
        $markers = (array) $request->query->get('markers', array());
        // Cache.
        $cache_helper = new CacheHelper($app);
        $etag = md5(serialize(array(
            $cache_helper->getLastModificationTs()
        )));
        $response = new Response();
        $response->headers->set(AbstractSessionListener::NO_AUTO_CACHE_CONTROL_HEADER, 'true');
        $response->setETag($etag);
        $response->setPublic();
        if ($response->isNotModified($request)) {
            return $response;
        }
        // Prepare variables.
        $news_article_helper = new NewsArticleHelper($app);
        $news_article_ids = $news_article_helper->getPublishedNewsArticlesIds($language);
        // Pagination.
        $current_page = (int) $request->query->get('current_page', 1);
        $nb_items_per_page = 12;
        $pseudo_request = Request::create($app->path('news-article-index', array('language' => $language)), 'GET');
        $pagination = new Pagination($pseudo_request, $app, count($news_article_ids), $nb_items_per_page, $current_page, array($nb_items_per_page), '');
        $news_article_ids_to_show = array_slice($news_article_ids, ($current_page - 1) * $nb_items_per_page, $nb_items_per_page);
        // Render.
        return $app->render(
            'NewsArticle/indexMainBlock.twig',
            array(
                'language' => $language,
                'markers' => $markers,
                'news_article_ids' => $news_article_ids_to_show,
                'pagination' => $pagination->getPaginationItems(),
            ),
            $response
        );
    }

    public function viewMainBlock(Request $request, Application $app)
    {
        $language = (string) $request->query->get('language', '');
        $id = (int) $request->query->get('id', 0);
        $markers = (array) $request->query->get('markers', array());
        $news_article = $app['content_unit_factory']->load('news-article', $id);
        if ($news_article == null) {
            return '';
        }
        $news_article->setLanguage($language);
        $news_article_digest = $news_article->getDigest();
        // Cache.
        $cache_helper = new CacheHelper($app);
        $etag = md5(serialize(array(
            $cache_helper->getLastModificationTs()
        )));
        $response = new Response();
        $response->headers->set(AbstractSessionListener::NO_AUTO_CACHE_CONTROL_HEADER, 'true');
        $response->setETag($etag);
        $response->setPublic();
        if ($response->isNotModified($request)) {
            return $response;
        }
        // Render.
        return $app->render(
            'NewsArticle/viewMainBlock.twig',
            array(
                'language' => $language,
                'markers' => $markers,
                'news_article' => $news_article_digest,
            ),
            $response
        );
    }

    public function item(Request $request, Application $app)
    {
        $language = (string) $request->query->get('language', '');
        $id = (int) $request->query->get('id', 0);
        $news_article = $app['content_unit_factory']->load('news-article', $id);
        if ($news_article == null) {
            return '';
        }
        $news_article->setLanguage($language);
        $news_article_digest = $news_article->getDigest();
        // Cache.
        $cache_helper = new CacheHelper($app);
        $etag = md5(serialize(array(
            $cache_helper->getLastModificationTs()
        )));
        $response = new Response();
        $response->headers->set(AbstractSessionListener::NO_AUTO_CACHE_CONTROL_HEADER, 'true');
        $response->setETag($etag);
        $response->setPublic();
        if ($response->isNotModified($request)) {
            return $response;
        }
        // Render.
        return $app->render(
            'NewsArticle/item.twig',
            array(
                'language' => $language,
                'news_article' => $news_article_digest,
            ),
            $response
        );
    }

}

<?php
namespace App\NewsArticle;

use Viktor\Application;

class NewsArticleHelper
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function getApp()
    {
        return $this->app;
    }

    public function getPublishedNewsArticlesIds($language)
    {
        $news_article_model = $this->getApp()['content_unit_factory']->create('news-article');
        $news_article_model->setLanguage($language);
        $news_article_ids = $news_article_model->getIds(
            array(
                array(
                    'type' => 'published_now',
                ),
            ),
            array(
                array(
                    'option' => 'publication_time',
                    'direction' => 'DESC',
                ),
            )
        );
        return $news_article_ids;
    }

}

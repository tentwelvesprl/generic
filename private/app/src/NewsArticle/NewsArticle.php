<?php
namespace App\NewsArticle;

use Viktor\Pack\Base\Helper\StringHelper;
use Viktor\Pack\Content\Article\AbstractArticle;

class NewsArticle extends AbstractArticle
{
    public function getType()
    {
        return 'news-article';
    }

    public function getSingularHumanType()
    {
        return 'news article';
    }

    public function getPluralHumanType()
    {
        return 'news articles';
    }

    public function getTitle()
    {
        $fields = $this->getDataset()->getFields();
        $title = trim($fields['title_' . $this->getLanguage()]->getPlainTextRepresentation());
        if ($title == '') {
            $title = 'Untitled ' . $this->getSingularHumanType() . ' #' . $this->getId();
        }
        return $title;
    }

    public function getHtmlTitle()
    {
        $html_title = htmlspecialchars($this->getTitle());
        return $html_title;
    }

    public function getDescription()
    {
        $description = $this->getHtmlDescription();
        $description = preg_replace('#<br\s*/?>#i', "\n", $description);
        $description = html_entity_decode(strip_tags($description), ENT_QUOTES | ENT_HTML5);
        $description = trim($description);
        return $description;
    }

    public function getHtmlDescription()
    {
        $fields = $this->getDataset()->getFields();
        $html_description = trim($fields['item_description_' . $this->getLanguage()]->getProcessedValue());
        return $html_description;
    }

    public function getImage()
    {
        $image_reference = $this->getImageReference();
        if (empty($image_reference)) {
            return null;
        }
        $image = $this->getApp()['content_unit_factory']->load($image_reference['type'], $image_reference['id']);
        if ($image != null) {
            return $image->getImage();
        }
        return null;
    }

    public function getImageReference()
    {
        $fields = $this->getDataset()->getFields();
        if (!empty($fields['item_image']->get())) {
            return $fields['item_image']->get()[0];
        }
        if (!empty($fields['main_image']->get())) {
            return $fields['main_image']->get()[0];
        }
        return null;
    }

    public function generateDefaultSmartUrlCandidate($language)
    {
        $fields = $this->getDataset()->getFields();
        if (trim($fields['title_' . $language]->get()) == '') {
            return '';
        }
        return '/' . $language . '/news/' . StringHelper::slugify($fields['title_' . $language]->get());
    }

    public function getDatasetCombinedDescription()
    {
        $options = array(
            'add_seo_fields' => true,
            'add_smart_url_fields' => true,
        );
        $sections = array(
            '' => array(
                'label' => 'Main information',
                'collapse' => false,
            ),
            'documents' => array(
                'label' => 'Images, embeds and files',
                'collapse' => true,
            ),
            'item' => array(
                'label' => 'Item',
                'collapse' => true,
            ),
        );
        $fields = array(
            'title' => array(
                'translatable' => true,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\TextField',
                    'options'    => array(
                        'indexed' => true,
                        'source_type' => 'plain_text',
                    ),
                    'full_text_index_weights' => array(
                        'general' => 1000,
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\TextFieldWidget',
                    'options'    => array(
                        'label' => 'Title',
                        'searchable' => true,
                        'sortable' => true,
                    ),
                ),
            ),
            'body' => array(
                'translatable' => true,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\TextField',
                    'options'    => array(
                        'indexed' => true,
                        'authorized_tags' => '<p><h3><h4><h5><ul><ol><li><strong><em><sub><sup><br><hr><a><q><cite><blockquote><table><tr><td><span>',
                        'source_type' => 'html',
                    ),
                    'full_text_index_weights' => array(
                        'general' => 1,
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\TextFieldWidget',
                    'options'    => array(
                        'label' => 'Body',
                        'searchable' => true,
                        'ckeditor_config_file_url' => '/assets/Admin/cke/body-cke-config.js',
                    ),
                ),
            ),
            'main_image' => array(
                'translatable' => false,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\ImageField',
                    'options'    => array(
                        'max_number' => 1,
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\ImageFieldWidget',
                    'options'    => array(
                        'label' => 'Main image',
                        'legend' => 'jpg | max-width ≈ 3072px | max-height ≈ 2048px | quality ≥ 80 | sRGB',
                    ),
                ),
            ),
            'images' => array(
                'translatable' => false,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\ImageField',
                    'options'    => array(),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\ImageFieldWidget',
                    'options'    => array(
                        'label' => 'Image gallery',
                        'section' => 'documents',
                        'legend' => 'jpg | max-width ≈ 3072px | max-height ≈ 2048px | quality ≥ 80 | sRGB',
                    ),
                ),
            ),
            'medias' => array(
                'translatable' => false,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\MediaField',
                    'options'    => array(
                        'media_type' => 'media',
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\MediaFieldWidget',
                    'options'    => array(
                        'label' => 'Embeds',
                        'section' => 'documents',
                    ),
                ),
            ),
            'files' => array(
                'translatable' => false,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\FileField',
                    'options'    => array(
                        'file_content_unit_type' => 'file',
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\FileFieldWidget',
                    'options'    => array(
                        'label' => 'Files',
                        'section' => 'documents',
                    ),
                ),
            ),
            'item_description' => array(
                'translatable' => true,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\TextField',
                    'options'    => array(
                        'indexed' => true,
                        'authorized_tags' => '<p><strong><em><sub><sup><br>',
                        'source_type' => 'html',
                    ),
                    'full_text_index_weights' => array(
                        'general' => 100,
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\TextFieldWidget',
                    'options'    => array(
                        'label' => 'Item description',
                        'legend' => 'Define a text for the item.',
                        'searchable' => true,
                        'sortable' => false,
                        'section' => 'item',
                        'ckeditor_config_file_url' => '/assets/Admin/cke/teaser-cke-config.js',
                    ),
                ),
            ),
            'item_image' => array(
                'translatable' => false,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\ImageField',
                    'options'    => array(
                        'max_number' => 1,
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\ImageFieldWidget',
                    'options'    => array(
                        'label' => 'Item image',
                        'legend' => 'Define an image for the item.',
                        'section' => 'item',
                    ),
                ),
            ),
        );
        return array(
            'options' => $options,
            'sections' => $sections,
            'fields' => $fields
        );
    }

    public function getPublicationOptions()
    {
        $options = array(
            'use_published' => true,
            'use_start_datetime' => true,
            'use_end_datetime' => false,
        );
        return $options;
    }

}

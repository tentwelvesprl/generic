<?php
namespace App\Navigation;

use Viktor\Pack\Content\Node\AbstractNode;

class MenuItem extends AbstractNode
{
    public function getType()
    {
        return 'menu-item';
    }

    public function getSingularHumanType()
    {
        return 'menu item';
    }

    public function getPluralHumanType()
    {
        return 'menu items';
    }

    public function getTitle()
    {
        $fields = $this->getDataset()->getFields();
        $title = $fields['title_' . $this->getLanguage()]->getPlainTextRepresentation();
        if ($title == '') {
            $title = 'Untitled ' . $this->getSingularHumanType() . ' #' . $this->getId();
        }
        return $title;
    }

    public function getHint()
    {
        $fields = $this->getDataset()->getFields();
        $hint = '';
        if (!empty($fields['linked_content_unit']->get())) {
            $content_unit = $this->getApp()['content_unit_factory']->load($fields['linked_content_unit']->get()[0]['type'], $fields['linked_content_unit']->get()[0]['id']);
            if ($content_unit != null) {
                $hint = ucfirst($content_unit->getSingularHumanType() . ' #' . $content_unit->getId());
            }
        }
        if ($fields['url_' . $this->getLanguage()]->getPlainTextRepresentation() != '') {
            $hint = $fields['url_' . $this->getLanguage()]->getPlainTextRepresentation();
        }
        return $hint;
    }

    public function getDescription()
    {
        $description = '';
        return $description;
    }

    public function getImage()
    {
        return null;
    }

    public function getViewPageUrl()
    {
        return '';
    }

    public function getDatasetCombinedDescription()
    {
        $options = array(
            'add_seo_fields' => true,
        );

        $sections = array(
            '' => array(
                'label' => 'Main information',
                'collapse' => false,
            ),
        );
        $fields = array(
            'title' => array(
                'translatable' => true,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\TextField',
                    'options'    => array(
                        'source_type' => 'plain_text',
                        'indexed' => true,
                    ),
                    'full_text_index_weights' => array(
                        'general' => 10,
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\TextFieldWidget',
                    'options'    => array(
                        'label' => 'Title',
                        'mandatory' => false,
                    ),
                ),
            ),
            'linked_content_unit' => array(
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\ContentUnitReferencesField',
                    'options'    => array(
                        'content_unit_types' => array('static-page', 'news-article'),
                        'max_number' => 1,
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\ContentUnitReferencesFieldWidget',
                    'options'    => array(
                        'label' => 'Linked content unit',
                    ),
                ),
            ),
            'url' => array(
                'translatable' => true,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\TextField',
                    'options'    => array(
                        'source_type' => 'plain_text',
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\TextFieldWidget',
                    'options'    => array(
                        'label' => 'Url',
                    ),
                ),
            ),
            'target' => array(
                'translatable' => false,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\ListField',
                    'options'    => array(
                        'items' => array(
                            '_self' => array(
                                'label' => 'Current tab',
                                'selected' => 1,
                            ),
                            '_blank' => array(
                                'label' => 'New tab',
                                'selected' => 0,
                            ),
                        ),
                        'multiple' => false,
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\ListFieldWidget',
                    'options'    => array(
                        'label' => 'Open in',
                        'display' => 'normal',
                    ),
                ),
            ),
            'marker' => array(
                'translatable' => false,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\TextField',
                    'options'    => array(
                        'source_type' => 'plain_text',
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\TextFieldWidget',
                    'options'    => array(
                        'label' => 'Marker',
                        'section' => '',
                    ),
                ),
            ),
        );
        return array(
            'options' => $options,
            'sections' => $sections,
            'fields' => $fields,
        );
    }

    public function getPublicationOptions()
    {
        $options = array(
            'use_published' => true,
            'use_start_datetime' => false,
            'use_end_datetime' => false,
        );
        return $options;
    }

}

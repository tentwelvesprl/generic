<?php
namespace App\Navigation;

use Viktor\Application;
use Viktor\Pack\Base\Publication\PublishableInterface;
use Viktor\Pack\Base\Tree\TreeStructure;

class NavigationHelper
{
    private $app = null;
    private $language = '';
    private $flatList = array();
    private $structure = array();
    private $treeStructure = null;
    protected $elements = array();
    private $breadcrumb = array();

    public function __construct(Application $app, $language, $markers)
    {
        $this->app = $app;
        $this->language = $language;
        $this->treeStructure = new TreeStructure($app, 'menu-item');
        $this->treeStructure->load();
        $this->addPseudoElement(0, 0);
        foreach ($this->treeStructure->getItems() as $k => $v) {
            $this->elements[$k] = array(
                'title' => '',
                'html_title' => '',
                'description' => '',
                'html_description' => '',
                'image_reference' => null,
                'linked_content_unit_reference' => null,
                'url' => '#',
                'active' => false,
                'has_an_active_descendant' => false,
                'marker' => '',
                'published' => false,
                'visible' => false,
                'node' => null,
                'target' => '_self',
            );
            $node = $app['content_unit_factory']->load('menu-item', $k);
            if ($node != null) {
                $node->setLanguage($language);
                $this->elements[$k]['node'] = $node->getDigest();
                $this->elements[$k]['url'] = '#';
                $this->elements[$k]['marker'] = 'menu-item' . ':' . $node->getId();
                $this->elements[$k]['published'] = $node->getPublication()->isPublishedNow();
                $node_fields = $node->getDataset()->getFields();
                $linked_content_unit_references = $node_fields['linked_content_unit']->get();
                if (isset($linked_content_unit_references[0])) {
                    $linked_content_unit = $app['content_unit_factory']->load(
                        $linked_content_unit_references[0]['type'],
                        $linked_content_unit_references[0]['id']
                    );
                    if ($linked_content_unit != null) {
                        $linked_content_unit->setLanguage($language);
                        $this->elements[$k]['title'] = $linked_content_unit->getTitle();
                        $this->elements[$k]['html_title'] = $linked_content_unit->getHtmlTitle();
                        $this->elements[$k]['description'] = $linked_content_unit->getDescription();
                        $this->elements[$k]['html_description'] = $linked_content_unit->getHtmlDescription();
                        $this->elements[$k]['image_reference'] = $linked_content_unit->getImageReference();
                        $this->elements[$k]['linked_content_unit_reference'] = $linked_content_unit_references[0];
                        $this->elements[$k]['url'] = $linked_content_unit->getViewPageUrl();
                        $this->elements[$k]['marker'] = $linked_content_unit->getType() . ':' . $linked_content_unit->getId();
                        if ($linked_content_unit instanceof PublishableInterface) {
                            $this->elements[$k]['published'] = $node->getPublication()->isPublishedNow() && $linked_content_unit->getPublication()->isPublishedNow();
                        }
                    }
                }
                if ($node_fields['url_' . $language]->get() != '') {
                    $url = $node_fields['url_' . $language]->get();
                    if (substr($url, 0, 4) != 'http') {
                        $url = $app['request_stack']->getCurrentRequest()->getBaseUrl() . $url;
                    }
                    $this->elements[$k]['url'] = $url;
                }
                if ($node_fields['marker']->get() != '') {
                    $this->elements[$k]['marker'] = $node_fields['marker']->get();
                }
                $this->elements[$k]['target'] = $node_fields['target']->getDigest()[0];
                if ($node->getTitle() != '') {
                    $this->elements[$k]['title'] = $node->getTitle();
                    $this->elements[$k]['html_title'] = $node->getHtmlTitle();
                }
                if ($node->getDescription() != '') {
                    $this->elements[$k]['description'] = $node->getDescription();
                    $this->elements[$k]['html_description'] = $node->getHtmlDescription();
                }
                if (!empty($node->getImageReference())) {
                    $this->elements[$k]['image_reference'] = $node->getImageReference();
                }
                if ($node->getViewPageUrl() != '') {
                    $this->elements[$k]['url'] = $node->getViewPageUrl();
                }
                $this->elements[$k]['visible'] = $node->canBeViewed();
            }
        }
        // Hook: add pseudo elements.
        $this->beforeProcessing();
        $processing_result = $this->treeStructure->processStructure();
        $this->flatList = $processing_result['flat_list'];
        $this->structure = $processing_result['structure'];
        // Find active element.
        $active_k = 0;
        $breadcrumb_ks = array();
        foreach ($this->flatList as $k => $v) {
            if ($this->elements[$k]['published'] && in_array($this->elements[$k]['marker'], $markers)) {
                $active_k = $k;
            }
        }
        $breadcrumb_ks[] = $active_k;
        $this->elements[$active_k]['active'] = 'true';
        $this->elements[$active_k]['has_an_active_descendant'] = true;
        $reversed_flat_list = array_reverse($this->flatList, true);
        foreach ($reversed_flat_list as $k => $children) {
            $children_ks = array_keys($children);
            if (count(array_intersect($children_ks, $breadcrumb_ks)) > 0) {
                $breadcrumb_ks[] = $k;
                $this->elements[$k]['has_an_active_descendant'] = true;
            }
        }
        $this->breadcrumb = array_reverse($breadcrumb_ks);
    }

    public function getApp()
    {
        return $this->app;
    }

    public function getLanguage()
    {
        return $this->language;
    }

    public function getTreeStructure()
    {
        return $this->treeStructure;
    }

    public function getFlatList()
    {
        return $this->flatList;
    }

    public function getStructure()
    {
        return $this->structure;
    }

    public function getBreadcrumb()
    {
        return $this->breadcrumb;
    }

    public function getElements()
    {
        return $this->elements;
    }

    public function getActiveElementKey()
    {
        return $this->breadcrumb[count($this->breadcrumb) - 1];
    }

    public function getParentElementKey($child_key)
    {
        foreach ($this->getFlatList() as $k => $v) {
            if (isset($v[$child_key])) {
                return $k;
            }
        }
        return 0;
    }

    public function addPseudoElement($id, $parent_id)
    {
        $this->treeStructure->addItem($id, $parent_id);
        $this->elements[$id] = array(
            'title' => 'vxcvcxvxc',
            'html_title' => '',
            'description' => '',
            'html_description' => '',
            'image_reference' => null,
            'linked_content_unit_reference' => null,
            'url' => '#',
            'active' => false,
            'has_an_active_descendant' => false,
            'marker' => '',
            'visible' => false,
            'published' => false,
            'node' => null,
            'target' => '_self',
        );
        return $this;
    }

    public function findElementIdByMarker($marker)
    {
        $marker = (string) $marker;
        foreach ($this->elements as $k => $element) {
            if ($element['marker'] == $marker) {
                return $k;
            }
        }
        return 0;
    }

    public function beforeProcessing()
    {
        return $this;
    }

}

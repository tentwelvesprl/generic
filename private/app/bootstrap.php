<?php
use Viktor\Application;

$autoloader = require_once __DIR__ . '/../vendor/autoload.php';



$environment = trim(file_get_contents(__DIR__ . '/../environment.txt'));



$app_namespace = 'App';
$packs_names = array('Base', 'Admin', 'Content', 'Agenda', 'ContentBox');
$app_values = array(); // Can be used to define non-standard directory locations.

$app = new Application($environment, $app_namespace, $packs_names, $autoloader, $app_values);

$app->init();

return $app;

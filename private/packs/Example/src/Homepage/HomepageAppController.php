<?php
namespace Viktor\Pack\Example\Homepage;

use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;

class HomepageAppController
{
    public function homepage(Request $request, Application $app)
    {
        return $app->render(
            '@Example/Homepage/homepage.twig',
            array()
        );
    }

}

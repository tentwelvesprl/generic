<?php

/*
 * Copyright 2019 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Example;

use Viktor\Application;
use Viktor\PackInterface;

class ExamplePack implements PackInterface
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
        return $this;
    }

    public function getName()
    {
        return 'Example';
    }

    public function getPath()
    {
        return realpath(__DIR__ . '/..');
    }

    public function install() {

    }

    public function init()
    {
        // Firewall.
        $pack_controllers_behind_general_firewall = array(
            'Viktor\Pack\Example\Homepage\HomepageAppController',
        );
        $this->app['viktor.controllers_behind_general_firewall'] = array_merge(
            $this->app['viktor.controllers_behind_general_firewall'],
            $pack_controllers_behind_general_firewall
        );
        // Twig configuration.
        $this->app['twig.loader.filesystem']->addPath(__DIR__ . '/../views', 'Example');
        return $this;
    }

    public function addConsoleCommands() {
    }

}

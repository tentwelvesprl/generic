<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Agenda;

use Doctrine\DBAL\Schema\Table;
use Exception;
use Viktor\Application;
use Viktor\PackInterface;
use Viktor\Pack\Base\User\SecretaryUser;

class AgendaPack implements PackInterface
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
        return $this;
    }

    public function getName()
    {
        return 'Agenda';
    }

    public function getPath()
    {
        return realpath(__DIR__ . '/..');
    }

    public function install() {
        $app = $this->app;
        // Copy the static assets into the public folder.
        $this->copyRecursively($this->getPath() . '/assets', $this->app['viktor.web_path'] . '/assets/Agenda');
        $schema = $app['db']->getSchemaManager();
        // Create 'performances_field_index' table.
        if (!$schema->tablesExist('performances_field_index')) {
            $table = new Table('performances_field_index');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('id', 'integer', array('unsigned' => true));
            $table->addColumn('type', 'string', array('length' => 64));
            $table->addColumn('field_name', 'string', array('length' => 64));
            $table->addColumn('k', 'integer', array('unsigned' => true));
            $table->addColumn('ts', 'integer', array('unsigned' => true));
            $table->addColumn('date_only', 'string', array('length' => 10));
            $table->addColumn('time_only', 'string', array('length' => 8));
            $table->addColumn('venue_id', 'integer', array('unsigned' => true));
            $table->addColumn('external_id', 'string', array('length' => 64));
            $table->setPrimaryKey(array('type', 'field_name', 'id', 'k'));
            $table->addIndex(array('ts'));
            $table->addIndex(array('date_only'));
            $table->addIndex(array('time_only'));
            $table->addIndex(array('venue_id'));
            $table->addIndex(array('external_id'));
            $schema->createTable($table);
        }
        return;
    }

    public function init()
    {
        $app = $this->app;
        // Twig configuration.
        $this->app['twig.loader.filesystem']->addPath(__DIR__ . '/../views', 'Agenda');
        // Firewall.
        $pack_controllers_behind_general_firewall = array(
            'Viktor\Pack\Agenda\Field\PerformancesFieldAppController',
        );
        $this->app['viktor.controllers_behind_general_firewall'] = array_merge(
            $this->app['viktor.controllers_behind_general_firewall'],
            $pack_controllers_behind_general_firewall
        );
        return $this;
    }

    private function copyRecursively($source, $dest)
    {
        if (is_file($source)) {
            return copy($source, $dest);
        }
        if (!is_dir($dest)) {
            mkdir($dest);
        }
        $dir = dir($source);
        while (($entry = $dir->read()) !== false) {
            if ($entry[0] == '.') {
                continue;
            }
            if ($dest !== "$source/$entry") {
                $this->copyRecursively("$source/$entry", "$dest/$entry");
            }
        }
        $dir->close();
        return true;
    }

    public function addConsoleCommands() {
    }

}

<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Agenda\Field;

use DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Viktor\Application;

class PerformancesFieldAppController
{
    public function item(Request $request, Application $app, $language, $base_name)
    {
        $id = (string) $request->query->get('id', '');
        if ($id == '') {
            $id = uniqid();
        }
        $ts = (int) $request->query->get('ts', 0);
        $ts_end = (int) $request->query->get('ts_end', 0);
        $undefined_dt_end = (bool) $request->query->get('undefined_dt_end', true);
        $all_day = (bool) $request->query->get('all_day', false);
        $external_id = (string) $request->query->get('external_id', '');
        $venue_id = (int) $request->query->get('venue_id', 0);
        $sale_status = (string) $request->query->get('sale_status', '');
        $tickets_urls = (array) $request->query->get('tickets_urls', array());
        $use_tickets_urls = (bool) $request->query->get('use_tickets_urls', false);
        $remarks = (array) $request->query->get('remarks', array());
        $use_remarks = (bool) $request->query->get('use_remarks', false);
        $venue_content_unit_type = (string) $request->query->get('venue_content_unit_type', '');
        $possible_sale_statuses = (array) $request->query->get('possible_sale_statuses', array());
        $disabled = (bool) $request->query->get('disabled', false);
        $validated_tickets_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $l => $l_name) {
            if ($use_tickets_urls && isset($tickets_urls[$l])) {
                $validated_tickets_urls[$l] = $tickets_urls[$l];
            } else {
                $validated_tickets_urls[$l] = '';
            }
        }
        $validated_remarks = array();
        foreach ($app['viktor.config']['available_languages'] as $l => $l_name) {
            if ($use_remarks && isset($remarks[$l])) {
                $validated_remarks[$l] = $remarks[$l];
            } else {
                $validated_remarks[$l] = '';
            }
        }
        $venues_digests = array();
        if ($venue_content_unit_type != '') {
            $venue_model = $app['content_unit_factory']->create($venue_content_unit_type);
            $venues_ids = $venue_model->getIds();
            foreach ($venues_ids as $vid) {
                $venue = $app['content_unit_factory']->load($venue_content_unit_type, $vid);
                $venue->setLanguage($language);
                $venues_digests[] = $venue->getDigest();
            }
        }
        $dt = new DateTime();
        $dt->setTimestamp($ts);
        $dt_end = new DateTime();
        $dt_end->setTimestamp($ts_end);
        return $app->render('@Agenda/Field/performancesFieldItem.twig', array(
            'language' => $language,
            'base_name' => $base_name,
            'id' => $id,
            'dt' => $dt,
            'ts' => $ts,
            'dt_end' => $dt_end,
            'ts_end' => $ts_end,
            'undefined_dt_end' => $undefined_dt_end,
            'all_day' => $all_day,
            'venue_id' => $venue_id,
            'venues' => $venues_digests,
            'external_id' => $external_id,
            'tickets_urls' => $validated_tickets_urls,
            'remarks' => $validated_remarks,
            'sale_status' => $sale_status,
            'possible_sale_statuses' => $possible_sale_statuses,
            'use_tickets_urls' => $use_tickets_urls,
            'use_remarks' => $use_remarks,
            'language' => $language,
            'base_name' => $base_name,
            'disabled' => $disabled,
        ));
    }

}

<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Agenda\Field;

use DateTime;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Admin\Field\AbstractFieldWidget;
use Viktor\Pack\Base\Field\AbstractField;

class PerformancesFieldWidget extends AbstractFieldWidget
{
    public function __construct(array $options, AbstractField $field, $prefix)
    {
        parent::__construct($options, $field, $prefix);
        $this->options = array_merge($this->getDefaultOptions(), $options);
    }

    public function getDefaultOptions()
    {
        $parent_default_options = parent::getDefaultOptions();
        $default_options = array(
        );
        return array_merge($parent_default_options, $default_options);
    }

    public function form(Request $request, Application $app, $language)
    {
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName();
        $invalid_field_flashbag_name = 'invalid-field-' . $input_name;
        $error_flashbag_name = 'error-' . $input_name;
        $prefill_bag_name = 'prefill-' . $input_name;
        $value = $this->getField()->get();
        $prefill = $app['session']->get($prefill_bag_name);
        if ($prefill !== null) {
            $value = $prefill;
        }
        $this->removePrefilledData($request, $app);
        $options = $this->getField()->getOptions();
        $possible_sale_statuses = array();
        foreach ($options['possible_sale_statuses'] as $k => $v) {
            $possible_sale_statuses[$k] = $v['label'];
        }
        return $app['twig']->render(
            '@Agenda/Field/performancesFieldForm.twig',
            array(
                'name' => $input_name,
                'value' => $value,
                'field_options' => $this->getField()->getOptions(),
                'widget_options' => $this->getOptions(),
                'metadata' => $this->getField()->getAllMetadata(),
                'invalid_field_flashbag_name' => $invalid_field_flashbag_name,
                'error_flashbag_name' => $error_flashbag_name,
                'language' => $language,
                'field_language' => $this->getField()->getMetadata('language'),
                'translatable' => $this->getField()->getMetadata('translatable'),
                'possible_sale_statuses' => $possible_sale_statuses,
                'disabled' => !$this->getField()->getContentUnit()->canBeEdited(),
            )
        );
    }

    public function advancedSearchForm(Request $request, Application $app, $language)
    {
        return '';
    }

    public function set(Request $request, Application $app)
    {
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName();
        $value = array();
        $input_dts_dates = $request->request->get($input_name . '_dt_date', array());
        $input_dts_times = $request->request->get($input_name . '_dt_time', array());
        $input_dts_end_dates = $request->request->get($input_name . '_dt_end_date', array());
        $input_dts_end_times = $request->request->get($input_name . '_dt_end_time', array());
        $all_days = $request->request->get($input_name . '_all_day', array());
        $performances_ids = $request->request->get($input_name . '_id', array());
        $venues_ids = $request->request->get($input_name . '_venue_id', array());
        $external_ids = $request->request->get($input_name . '_external_id', array());
        $tickets_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language => $language_name) {
            $tickets_urls[$language] = $request->request->get($input_name . '_tickets_url_' . $language, array());
        }
        $remarks = array();
        foreach ($app['viktor.config']['available_languages'] as $language => $language_name) {
            $remarks[$language] = $request->request->get($input_name . '_remark_' . $language, array());
        }
        $sale_statuses = $request->request->get($input_name . '_sale_status', array());
        foreach ($input_dts_dates as $k => $input_dt_date) {
            if (isset($performances_ids[$k])) {
                $performance_id = $performances_ids[$k];
            } else {
                $performance_id = '';
            }
            if (isset($venues_ids[$k])) {
                $venue_id = (int) $venues_ids[$k];
            } else {
                $venue_id = 0;
            }
            if (isset($external_ids[$k])) {
                $external_id = (string) $external_ids[$k];
            } else {
                $external_ids = '';
            }
            $tickets_urls_for_this_performance = array();
            foreach ($app['viktor.config']['available_languages'] as $language => $language_name) {
                if (isset($tickets_urls[$language][$k])) {
                    $tickets_urls_for_this_performance[$language] = $tickets_urls[$language][$k];
                } else {
                    $tickets_urls_for_this_performance[$language] = '';
                }
            }
            $remarks_for_this_performance = array();
            foreach ($app['viktor.config']['available_languages'] as $language => $language_name) {
                if (isset($remarks[$language][$k])) {
                    $remarks_for_this_performance[$language] = $remarks[$language][$k];
                } else {
                    $remarks_for_this_performance[$language] = '';
                }
            }
            if (isset($sale_statuses[$k])) {
                $sale_status = $sale_statuses[$k];
            } else {
                $sale_status = '';
            }
            $ts = 0;
            if (isset($input_dts_dates[$k])) {
                if ($input_dts_dates[$k] != '') {
                    if (isset($input_dts_times[$k]) && $input_dts_times[$k] != '') {
                        $ts = @strtotime($input_dts_dates[$k] . 'T' . $input_dts_times[$k]);
                    } else {
                        $ts = @strtotime($input_dts_dates[$k] . 'T' . '00:00:00');
                    }
                }
            }
            $ts_end = 0;
            $undefined_dt_end = true;
            if (isset($input_dts_end_dates[$k])) {
                if ($input_dts_end_dates[$k] != '') {
                    if (isset($input_dts_end_times[$k]) && $input_dts_end_times[$k] != '') {
                        $ts_end = @strtotime($input_dts_end_dates[$k] . 'T' . $input_dts_end_times[$k]);
                    } else {
                        $ts_end = @strtotime($input_dts_end_dates[$k] . 'T' . '00:00:00');
                    }
                    $undefined_dt_end = false;
                }
            }
            $all_day = false;
            if (isset($all_days[$k]) && $all_days[$k] == '1') {
                $all_day = true;
            }
            $value[] = array(
                'id' => $performance_id,
                'ts' => $ts,
                'ts_end' => $ts_end,
                'undefined_dt_end' => $undefined_dt_end,
                'all_day' => $all_day,
                'venue_id' => $venue_id,
                'external_id' => $external_id,
                'tickets_urls' => $tickets_urls_for_this_performance,
                'remarks' => $remarks_for_this_performance,
                'sale_status' => $sale_status,
            );
        }
        $this->getField()->set($value);
    }

    public function storePrefilledData(Request $request, Application $app)
    {
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName();
        $prefill_bag_name = 'prefill-' . $input_name;
        $value = array();
        $performances_ids = $request->request->get($input_name . '_id', array());
        $input_dts_dates = $request->request->get($input_name . '_dt_date', array());
        $input_dts_times = $request->request->get($input_name . '_dt_time', array());
        $input_dts_end_dates = $request->request->get($input_name . '_dt_end_date', array());
        $input_dts_end_times = $request->request->get($input_name . '_dt_end_time', array());
        $input_venues_ids = $request->request->get($input_name . '_venue_id', array());
        $input_external_ids = $request->request->get($input_name . '_external_id', array());
        $input_sale_statuses = $request->request->get($input_name . '_sale_status', array());
        $input_all_days = $request->request->get($input_name . '_all_day', array());
        foreach ($performances_ids as $k => $performance_id) {
            $v = $this->getField()->getDefaultPerformanceArray();
            $v['id'] = $performance_id;
            if (isset($input_dts_dates[$k]) && $input_dts_dates[$k] != '' && isset($input_dts_times[$k]) && $input_dts_times[$k] != '') {
                try {
                    $dt = new DateTime($input_dts_dates[$k] . ' ' . $input_dts_times[$k]);
                    $v['ts'] = $dt->format('U');
                } catch (Exception $e) {
                    // Silently fail...
                }
            }
            if (isset($input_dts_end_dates[$k]) && $input_dts_end_dates[$k] != '' && isset($input_dts_end_times[$k]) && $input_dts_end_times[$k] != '') {
                try {
                    $dt_end = new DateTime($input_dts_end_dates[$k] . ' ' . $input_dts_end_times[$k]);
                    $v['ts_end'] = $dt_end->format('U');
                } catch (Exception $e) {
                    // Silently fail...
                }
            }
            if (isset($input_venues_ids[$k])) {
                $v['venue_id'] = (int) $input_venues_ids[$k];
            }
            if (isset($external_ids[$k])) {
                $v['external_id'] = (int) $external_ids[$k];
            }
            if (isset($input_sale_statuses[$k])) {
                $v['sale_status'] = (string) $input_sale_statuses[$k];
            }
            if (isset($input_all_days[$k])) {
                $v['all_day'] = (bool) $input_all_days[$k];
            }
            $value[] = $v;
        }
        $app['session']->set($prefill_bag_name, $value);
    }

    public function removePrefilledData(Request $request, Application $app)
    {
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName();
        $prefill_bag_name = 'prefill-' . $input_name;
        $app['session']->remove($prefill_bag_name);
    }

    public function validate(Request $request, Application $app)
    {
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName();
        $invalid_field_flashbag_name = 'invalid-field-' . $input_name;
        $error_flashbag_name = 'error-' . $input_name;
        return true;
    }

    public function getFilters(Request $request, Application $app)
    {
        $filters = array();
        return $filters;
    }

    public function storeAdvancedSearchData(Request $request, Application $app)
    {
    }

    public function getSortOptions()
    {
        $sort_options = array();
        $options = $this->getOptions();
        $label = $options['label'];
        if ($options['sortable']) {
            $sort_options = array(
                'field_' . $this->getField()->getName() => $label,
            );
        }
        return $sort_options;
    }

}

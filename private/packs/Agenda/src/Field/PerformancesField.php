<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Agenda\Field;

use DateTime;
use Doctrine\DBAL\Query\QueryBuilder;
use Viktor\Pack\Base\Dataset\Dataset;
use Viktor\Pack\Base\Field\AbstractField;

class PerformancesField extends AbstractField
{
    private $value;

    public function __construct(Dataset $dataset, array $options, $field_name, array $metadata, array $full_text_index_weights)
    {
        parent::__construct($dataset, $options, $field_name, $metadata, $full_text_index_weights);
        $this->value = array();
        return;
    }

    public function getDefaultOptions()
    {
        return array(
            'venue_content_unit_type' => 'venue',
            'default_sale_status' => 'not_on_sale',
            'use_tickets_urls' => true,
            'use_remarks' => true,
            'possible_sale_statuses' => array(
                'not_on_sale' => array(
                    'can_be_sold' => false,
                    'can_be_shown' => false,
                    'label' => 'Not on sale',
                ),
                'on_sale' => array(
                    'can_be_sold' => true,
                    'can_be_shown' => true,
                    'label' => 'On sale',
                ),
                'last_tickets' => array(
                    'can_be_sold' => true,
                    'can_be_shown' => true,
                    'label' => 'Last tickets',
                ),
                'sold_out' => array(
                    'can_be_sold' => false,
                    'can_be_shown' => true,
                    'label' => 'Sold out',
                ),
                'cancelled' => array(
                    'can_be_sold' => false,
                    'can_be_shown' => true,
                    'label' => 'Cancelled',
                ),
                'postponed' => array(
                    'can_be_sold' => false,
                    'can_be_shown' => true,
                    'label' => 'Postponed',
                ),
            ),
        );
    }

    public function getDefaultPerformanceArray()
    {
        $id = uniqid();
        $tickets_urls = array();
        foreach ($this->getApp()['viktor.config']['available_languages'] as $language => $language_name) {
            $tickets_urls[$language] = '';
        }
        $remarks = array();
        foreach ($this->getApp()['viktor.config']['available_languages'] as $language => $language_name) {
            $remarks[$language] = '';
        }
        $options = $this->getOptions();
        return array(
            'id' => $id,
            'ts' => 0,
            'dt' => new DateTime(),
            'ts_end' => 0,
            'dt_end' => new DateTime(),
            'undefined_dt_end' => true,
            'all_day' => false,
            'venue_id' => 0,
            'external_id' => '',
            'tickets_urls' => $tickets_urls,
            'remarks' => $remarks,
            'sale_status' => $options['default_sale_status'],
        );
    }

    public function get()
    {
        if (!is_array($this->value)) {
            $this->value = array();
        }
        return $this->value;
    }

    public function set($value = null)
    {
        $this->value = array();
        if (!is_array($value)) {
            $value = array();
        }
        $options = $this->getOptions();
        $possible_sale_statuses = $options['possible_sale_statuses'];
        foreach ($value as $v) {
            $new_v = $this->getDefaultPerformanceArray();
            if (isset($v['id']) && trim($v['id']) != '') {
                $new_v['id'] = (string) $v['id'];
            }
            $new_v['ts'] = (isset($v['ts']) ? (int) $v['ts'] : 0);
            $dt = new DateTime();
            $dt->setTimestamp((int) $new_v['ts']);
            $new_v['dt'] = $dt;
            // Defaults to one hour duration.
            if (isset($v['undefined_dt_end'])) {
                $new_v['undefined_dt_end'] = (bool) $v['undefined_dt_end'];
            }
            if ($new_v['undefined_dt_end']) {
                $new_v['ts_end'] = $new_v['ts'] + 3600;
            } else {
                $new_v['ts_end'] = (isset($v['ts_end']) ? (int) $v['ts_end'] : $new_v['ts'] + 3600);
            }
            $dt_end = new DateTime();
            $dt_end->setTimestamp((int) $new_v['ts_end']);
            $new_v['dt_end'] = $dt_end;
            // All day events.
            $new_v['all_day'] = false;
            if (isset($v['all_day'])) {
                $new_v['all_day'] = (bool) $v['all_day'];
            }
            if ($new_v['all_day']) {
                $new_v['dt']->setTime(0, 0, 0);
                $new_v['ts'] = $new_v['dt']->getTimestamp();
                $new_v['dt_end']->setTime(23, 59, 59);
                $new_v['ts_end'] = $new_v['dt_end']->getTimestamp();
            }
            // Make sure the end dt makes sense.
            if ($new_v['ts_end'] < $new_v['ts']) {
                $new_v['ts_end'] = $new_v['ts'] + 3600;
                $new_v['dt_end']->setTimestamp($new_v['ts_end']);
            }
            $new_v['venue_id'] = (isset($v['venue_id']) ? (int) $v['venue_id'] : 0);
            $new_v['external_id'] = (isset($v['external_id']) ? (string) $v['external_id'] : '');
            $tickets_urls = array();
            foreach ($this->getApp()['viktor.config']['available_languages'] as $language => $language_name) {
                $tickets_urls[$language] = (isset($v['tickets_urls'][$language]) ? (string) $v['tickets_urls'][$language] : '');
            }
            $new_v['tickets_urls'] = $tickets_urls;
            $remarks = array();
            foreach ($this->getApp()['viktor.config']['available_languages'] as $language => $language_name) {
                $remarks[$language] = (isset($v['remarks'][$language]) ? (string) $v['remarks'][$language] : '');
            }
            $new_v['remarks'] = $remarks;
            if (!isset($v['sale_status'])) {
                $new_v['sale_status'] = $options['default_sale_status'];
            } elseif (!isset($possible_sale_statuses[$v['sale_status']])) {
                $new_v['sale_status'] = $options['default_sale_status'];
            } else {
                $new_v['sale_status'] = $v['sale_status'];
            }
            $this->value[] = $new_v;
        }
        $ordered_performances = array();
        $ordered_performances_indices = array();
        $i = 0;
        foreach ($this->value as $v) {
            $key = ((string) $v['ts']) . '_' . sprintf('%04d', $i);
            $ordered_performances[] = $v;
            $ordered_performances_indices[] = $key;
            $i++;
        }
        array_multisort($ordered_performances_indices, $ordered_performances);
        $this->value = $ordered_performances;
        return $this;
    }

    public function getDigest()
    {
        $digest = $this->value;
        $possible_sale_statuses = $this->getOptions()['possible_sale_statuses'];
        foreach ($digest as $k => $performance) {
            $sale_status = $performance['sale_status'];
            if ($performance['ts'] >= time() && isset($possible_sale_statuses[$sale_status]) && $possible_sale_statuses[$sale_status]['can_be_sold']) {
                $digest[$k]['_can_be_sold'] = true;
            } else {
                $digest[$k]['_can_be_sold'] = false;
            }
            if ($performance['ts'] >= time() && isset($possible_sale_statuses[$sale_status]) && $possible_sale_statuses[$sale_status]['can_be_shown']) {
                $digest[$k]['_can_be_shown'] = true;
            } else {
                $digest[$k]['_can_be_shown'] = false;
            }
        }
        return $digest;
    }

    public function updateIndex()
    {
        $options = $this->getOptions();
        if ($options['indexed']) {
            $this->deleteFromIndex();
            foreach ($this->value as $k => $v) {
                $query_builder = $this->getApp()['db']->createQueryBuilder();
                $query_builder
                    ->insert('performances_field_index')
                    ->setValue('type', ':type')
                    ->setValue('id', ':id')
                    ->setValue('field_name', ':field_name')
                    ->setValue('k', ':k')
                    ->setValue('ts', ':ts')
                    ->setValue('date_only', ':date_only')
                    ->setValue('time_only', ':time_only')
                    ->setValue('venue_id', ':venue_id')
                    ->setValue('external_id', ':external_id')
                    ->setParameter('type', $this->getContentUnit()->getType())
                    ->setParameter('id', $this->getContentUnit()->getId())
                    ->setParameter('field_name', $this->getName())
                    ->setParameter('k', $k)
                    ->setParameter('ts', $v['ts'])
                    ->setParameter('date_only', $v['dt']->format('Y-m-d'))
                    ->setParameter('time_only', $v['dt']->format('H:i:s'))
                    ->setParameter('venue_id', $v['venue_id'])
                    ->setParameter('external_id', $v['external_id']);
                $query_builder->execute();
            }
        }
        return true;
    }

    public function deleteFromIndex()
    {
        $options = $this->getOptions();
        if ($options['indexed']) {
            $query_builder = $this->getApp()['db']->createQueryBuilder();
            $query_builder
                ->delete('performances_field_index')
                ->where('type = :type AND id = :id AND field_name = :field_name')
                ->setParameter('type', $this->getContentUnit()->getType())
                ->setParameter('id', $this->getContentUnit()->getId())
                ->setParameter('field_name', $this->getName());
            $query_builder->execute();
        }
        return true;
    }

    public function getPlainTextRepresentation()
    {
        $s = '';
        return $s;
    }

    public function getDecoratedIdsQueryBuilder(QueryBuilder $query_builder, array $filters, array $sort_options)
    {
        static $counter = 0;
        $need_to_join = false;
        $field_alias = 'f' . md5('performances_field_index_' . $this->getName());
        foreach ($filters as $filter) {
            if ((isset($filter['field_name'])) && $filter['field_name'] == $this->getName()) {
                $counter++;
                $need_to_join = true;
                if ($filter['type'] == 'performances_field_timestamp_comparison') {
                    $value = (int) $filter['value'];
                    $field_name = trim((string) $filter['field_name']);
                    $operator = trim((string) $filter['operator']);
                    switch ($operator) {
                        case 'less_than':
                            $sql_operator = '<';
                            break;
                        case 'less_than_or_equal_to':
                            $sql_operator = '<=';
                            break;
                        case 'equal_to':
                            $sql_operator = '=';
                            break;
                        case 'greater_than_or_equal_to':
                            $sql_operator = '>=';
                            break;
                        case 'greater_than':
                            $sql_operator = '>';
                            break;
                        default:
                            $sql_operator = '=';
                    }
                    $value_alias = 'v' . md5('performances_field_index_' . $this->getName() . '_value' . $counter);
                    $field_name_alias = 'n' . md5('performances_field_index_' . $this->getName() . '_field_name' . $counter);
                    // Note that the aliases are prefixed with a letter, so that
                    // Doctrine consider them as named parameters.
                    $query_builder
                        ->andWhere($field_alias . '.ts ' . $sql_operator . ' :' . $value_alias)
                        ->andWhere($field_alias . '.field_name = :' . $field_name_alias)
                        ->setParameter($value_alias, $value)
                        ->setParameter($field_name_alias, $field_name)
                    ;
                }
                if ($filter['type'] == 'performances_field_date_comparison') {
                    $value = (string) $filter['value'];
                    $field_name = trim((string) $filter['field_name']);
                    $operator = trim((string) $filter['operator']);
                    switch ($operator) {
                        case 'less_than':
                            $sql_operator = '<';
                            break;
                        case 'less_than_or_equal_to':
                            $sql_operator = '<=';
                            break;
                        case 'equal_to':
                            $sql_operator = '=';
                            break;
                        case 'greater_than_or_equal_to':
                            $sql_operator = '>=';
                            break;
                        case 'greater_than':
                            $sql_operator = '>';
                            break;
                        default:
                            $sql_operator = '=';
                    }
                    $value_alias = 'v' . md5('performances_field_index_' . $this->getName() . '_value' . $counter);
                    $field_name_alias = 'n' . md5('performances_field_index_' . $this->getName() . '_field_name' . $counter);
                    // Note that the aliases are prefixed with a letter, so that
                    // Doctrine consider them as named parameters.
                    $query_builder
                        ->andWhere($field_alias . '.date_only ' . $sql_operator . ' :' . $value_alias)
                        ->andWhere($field_alias . '.field_name = :' . $field_name_alias)
                        ->setParameter($value_alias, $value)
                        ->setParameter($field_name_alias, $field_name)
                    ;
                }
                if ($filter['type'] == 'performances_field_time_comparison') {
                    $value = (string) $filter['value'];
                    $field_name = trim((string) $filter['field_name']);
                    $operator = trim((string) $filter['operator']);
                    switch ($operator) {
                        case 'less_than':
                            $sql_operator = '<';
                            break;
                        case 'less_than_or_equal_to':
                            $sql_operator = '<=';
                            break;
                        case 'equal_to':
                            $sql_operator = '=';
                            break;
                        case 'greater_than_or_equal_to':
                            $sql_operator = '>=';
                            break;
                        case 'greater_than':
                            $sql_operator = '>';
                            break;
                        default:
                            $sql_operator = '=';
                    }
                    $value_alias = 'v' . md5('performances_field_index_' . $this->getName() . '_value');
                    $field_name_alias = 'n' . md5('performances_field_index_' . $this->getName() . '_field_name');
                    // Note that the aliases are prefixed with a letter, so that
                    // Doctrine consider them as named parameters.
                    $query_builder
                        ->andWhere($field_alias . '.time_only ' . $sql_operator . ' :' . $value_alias)
                        ->andWhere($field_alias . '.field_name = :' . $field_name_alias)
                        ->setParameter($value_alias, $value)
                        ->setParameter($field_name_alias, $field_name)
                    ;
                }
                if ($filter['type'] == 'performances_field_venues_ids') {
                    $raw_value = $filter['value'];
                    $field_name = trim((string) $filter['field_name']);
                    $value = array();
                    if (is_array($raw_value)) {
                        foreach ($raw_value as $v) {
                            $value[] = (int) $v;
                        }
                    } else {
                        $value[] = (int) $raw_value;
                    }
                    $field_name_alias = 'n' . md5('performances_field_index_' . $this->getName() . '_field_name');
                    // Note that the aliases are prefixed with a letter, so that
                    // Doctrine consider them as named parameters.
                    $query_builder
                        ->andWhere($field_alias . '.venue_id IN (' . implode(', ', $value) . ')')
                        ->andWhere($field_alias . '.field_name = :' . $field_name_alias)
                        ->setParameter($field_name_alias, $field_name)
                    ;
                }
                if ($filter['type'] == 'performances_field_exclude_venues_ids') {
                    $raw_value = $filter['value'];
                    $field_name = trim((string) $filter['field_name']);
                    $value = array();
                    if (is_array($raw_value)) {
                        foreach ($raw_value as $v) {
                            $value[] = (int) $v;
                        }
                    } else {
                        $value[] = (int) $raw_value;
                    }
                    $field_name_alias = 'n' . md5('performances_field_index_' . $this->getName() . '_field_name');
                    // Note that the aliases are prefixed with a letter, so that
                    // Doctrine consider them as named parameters.
                    $query_builder
                        ->andWhere($field_alias . '.venue_id NOT IN (' . implode(', ', $value) . ')')
                        ->andWhere($field_alias . '.field_name = :' . $field_name_alias)
                        ->setParameter($field_name_alias, $field_name)
                    ;
                }
            }
        }
        foreach ($sort_options as $sort_option) {
            if (!is_array($sort_option)) {
                continue;
            }
            $option = (string) $sort_option['option'];
            $direction = (string) $sort_option['direction'];
            switch ($option) {
                case 'field_' . $this->getName():
                    $need_to_join = true;
                    $query_builder
                        ->andWhere($field_alias . '.field_name = \'' . $this->getName() . '\'')
                        ->addOrderBy($field_alias . '.ts', $direction);
                    break;
            }
        }
        if ($need_to_join) {
            $query_builder
                ->innerJoin(
                    'component',
                    'performances_field_index', $field_alias,
                    'component.type = ' . $field_alias . '.type AND component.id = ' . $field_alias . '.id'
                );
        }
        return $query_builder;
    }

}

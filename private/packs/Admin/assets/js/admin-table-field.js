var AdminTableFieldHandler = function (table_field) {

    this.tableField = table_field;

    var that = this;
    var new_item_url = table_field.attr('data-new-item-url');

    table_field.on('click', '.js-remove-row', function (e) {
        e.preventDefault();
        $r = $(this).closest('.row')
        $r.slideUp('fast', function() { $r.remove(); });
    });

    table_field.on('click', '.js-add-row', function (e) {
        e.preventDefault();
        $.get(
            new_item_url,
            {
            },
            function (data) {
                that.addItem(data);
            }
        );
        table_field.closest('form').trigger('change');
    });

}

AdminTableFieldHandler.prototype = {

    'addItem': function (item) {
        this.tableField.find('.list').append(item);
    },

}

$(function() {
    $('.table-field').each(function() {
        var admin_table_field_handler = new AdminTableFieldHandler($(this));
    });
});

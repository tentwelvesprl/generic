var AdminAddressFieldHandler = function (address_field) {

    this.addressField = address_field;

    var that = this;

    this.addressField.on('click', '.js-location-update', function (e) {
        e.preventDefault();
        that.updateCoordinates();
    });

    this.addressField.on('click', '.js-location-preview', function (e) {
        e.preventDefault();
        that.preview();
    });

}

AdminAddressFieldHandler.prototype = {

    'updateCoordinates': function () {
        var that = this;
        var address = ''
            + that.addressField.find('.street_1').val() + ' '
            + that.addressField.find('.street_2').val() + ' '
            + that.addressField.find('.postal_code').val() + ' '
            + that.addressField.find('.city').val() + ' '
            + that.addressField.find('.country').val();
        var url = that.addressField.data('url');
        $.ajax({
            url: url,
            cache: false,
            data: {
                address: address
            },
            dataType: 'json'
        }).done(function (data) {
            if (data.status == 'ok') {
                that.addressField.find('.latitude').val(data.lat);
                that.addressField.find('.longitude').val(data.lng);
            } else {
                alert('An error occured. Please check the address fields.');
                that.addressField.find('.latitude').val('');
                that.addressField.find('.longitude').val('');
            }
        });
    },

    'preview': function () {
        var that = this;
        var url = 'https://maps.google.com/';
        var lat = parseFloat(that.addressField.find('.latitude').val());
        if (isNaN(lat)) {
            lat = 0.0;
        }
        var lng = parseFloat(that.addressField.find('.longitude').val());
        if (isNaN(lng)) {
            lng = 0.0;
        }
        url = url + '?q=' + lat + ',' + lng;
        window.open(url);
    }

}

$(function() {
    $('.address-field').each(function() {
        var admin_address_field_handler = new AdminAddressFieldHandler($(this));
    });
});

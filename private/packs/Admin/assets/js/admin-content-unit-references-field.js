var AdminContentUnitReferencesFieldHandler = function (content_unit_references_field) {

    this.contentUnitReferencesField = content_unit_references_field;
    this.maxNumber = parseInt(content_unit_references_field.attr('data-max-number'));

    var that = this;
    var new_item_url = content_unit_references_field.attr('data-new-item-url');
    var content_unit_types = content_unit_references_field.attr('data-content-unit-types');

    content_unit_references_field.on('click', '.js-remove-row', function (e) {
        e.preventDefault();
        var remove_button = $(this);
        remove_button.closest('.row').remove();
        that.updateContentUnitReferencesAddingOptions();
    });

    content_unit_references_field.find('.typeahead').typeahead(
        {},
        {
            name: 'search-results-set',
            limit: 20,
            display: function (datum) {
                return datum.title;
            },
            source: function (query, syncResults, asyncResults) {
                $.get(
                    content_unit_references_field.attr('data-search-results-url'),
                    {
                        'q': query,
                        'content_unit_types': content_unit_types
                    },
                    function (data) {
                        asyncResults(data);
                    }
                );
            },
            templates: {
                suggestion: function (datum) {
                    var image_tag = '';
                    if (datum.image != '') {
                        image_tag = '<div class="row__thumbnail"><figure class="thumbnail"><img src="' + that.escapeHtml(datum.image) + '" class="checkered"></figure></div>';
                    }
                    return ''
                        + '<div class="row">'
                        + '<div class="row__wrapper">'
                        + '<div class="row__header">'
                        + '<div class="row__id">' + that.escapeHtml(datum.singular_human_type) + ' #' + datum.id + '</div>'
                        + '<h3 class="row__title">' + that.escapeHtml(datum.title) + '</h3>'
                        + '<h4 class="row__hint">' + that.escapeHtml(datum.hint) + '</h4>'
                        + '</div>'
                        + image_tag
                        + '</div>'
                        + '</div>';
                }
            }
        }
    ).on('typeahead:selected', function (obj, datum) {
        $.get(
            new_item_url,
            {
                type: datum.type,
                id: datum.id
            },
            function (data) {
                that.addItem(data);
            }
        );
        content_unit_references_field.find('.typeahead').typeahead('val', '');
    });

    content_unit_references_field.on('click', '.content-unit-references-field-create', function (e) {
        e.preventDefault();
        that.createAndLinkContentUnit($(this).attr('data-type'), $(this).attr('data-base-name'), $(this).attr('data-singular-human-type'));
    });

    this.updateContentUnitReferencesAddingOptions();

}

AdminContentUnitReferencesFieldHandler.prototype = {

    'addItem': function (item) {
        this.contentUnitReferencesField.find('.typeahead').val('');
        if (this.contentUnitReferencesField.find('.row').length < this.maxNumber) {
            this.contentUnitReferencesField.find('.list').append(item);
        }
        this.updateContentUnitReferencesAddingOptions();
    },

    'updateContentUnitReferencesAddingOptions': function () {
        if (this.contentUnitReferencesField.find('.row').length < this.maxNumber) {
            this.contentUnitReferencesField.find('.content-unit-references-adding-actions').show();
        } else {
            this.contentUnitReferencesField.find('.content-unit-references-adding-actions').hide();
        }
    },

    'createAndLinkContentUnit': function (type, base_name, singular_human_type) {
        var that = this;
        var confirmation = window.confirm('This will create and save a new ' + singular_human_type + '. Do you want to continue?');
        if (confirmation) {
            $.post(
                this.contentUnitReferencesField.attr('data-create-item-url'),
                {
                    type: type,
                    base_name: base_name
                },
                function (data) {
                    if ('item' in data) {
                        that.addItem(data.item);
                    }
                }
            );
            this.updateContentUnitReferencesAddingOptions();
        }
    },

    'escapeHtml': function (s) {
        return s.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
    }

}

$(function () {
    $('.content-unit-references-field').each(function () {
        var admin_content_unit_references_field_handler = new AdminContentUnitReferencesFieldHandler($(this));
    });
});

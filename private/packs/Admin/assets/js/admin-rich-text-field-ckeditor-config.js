CKEDITOR.editorConfig = function( config ) {
    config.toolbarGroups = [
    { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
    { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
    { name: 'forms', groups: [ 'forms' ] },
    { name: 'tools', groups: [ 'tools' ] },
    { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
    { name: 'about', groups: [ 'about' ] },
    '/',
    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
    { name: 'links', groups: [ 'links' ] },
    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
    { name: 'styles', groups: [ 'styles' ] },
    { name: 'colors', groups: [ 'colors' ] },
    { name: 'insert', groups: [ 'insert' ] },
    { name: 'others', groups: [ 'others' ] }
    ];

    config.removeButtons = 'Templates,Save,NewPage,Preview,Print,SelectAll,Find,Replace,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Underline,CopyFormatting,JustifyLeft,JustifyCenter,JustifyRight,JustifyBlock,Language,BidiRtl,BidiLtr,Anchor,Image,Flash,Smiley,PageBreak,Iframe,Font,FontSize,TextColor,BGColor,Format,About';

    config.extraPlugins = 'autogrow,div';
    config.autoGrow_minHeight = 64;
    config.autoGrow_maxHeight = 512;
    config.autoGrow_onStartup = true;

    config.allowedContent = 'strong em sub sup li ul ol br h3 h4 h5 h6 table tr q cite blockquote; div(table-wrapper); th td[colspan,rowspan](th-caption){text-align,vertical-align,white-space}; p(small, headline); a[!href,target]; span(*)';
    config.contentsCss = basepath+'/assets/Admin/css/cke.css';
    config.bodyClass = 'cke';
    config.ignoreEmptyParagraph = true;

    config.stylesSet = [
    { name: 'paragraph', element: 'p' },
    { name: 'headline', element: 'p', attributes: { 'class': 'headline' } },
    { name: 'small', element: 'p', attributes: { 'class': 'small' } },
    { name: 'heading 3', element: 'h3' },
    { name: 'heading 4', element: 'h4' },
    { name: 'heading 5', element: 'h5' },
    { name: 'heading 6', element: 'h6' },
    { name: 'quote', element: 'q' },
    { name: 'cite', element: 'cite' },
    ];

};

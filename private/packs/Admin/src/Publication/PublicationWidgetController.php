<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Publication;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Admin\Publication\PublicationWidget;
use Viktor\Pack\Base\Publication\Publication;

class PublicationWidgetController
{
    public function form(
        Request $request,
        Application $app,
        Publication $publication,
        PublicationWidget $publication_widget,
        $language)
    {
        $base_name = $publication_widget->getPrefix() . '_publication';
        $prefill_bag_name = 'prefill-' . $base_name;
        $value = $publication->getContent();
        $prefill = $app['session']->get($prefill_bag_name);
        $publication_array = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $publication_array[$language_code] = array(
                'language_name' => $language_name,
                'published' => $value[$language_code]['published'],
                'start_datetime' => $value[$language_code]['start_datetime'],
                'end_datetime' => $value[$language_code]['end_datetime'],
            );
            if (is_array($prefill) && isset($prefill[$language_code])) {
                if (isset($prefill[$language_code]['published'])) {
                    $publication_array[$language_code]['published'] = $prefill[$language_code]['published'];
                }
                if (isset($prefill[$language_code]['start'])) {
                    $prefill_start_uts = strtotime($prefill[$language_code]['start']);
                    if ($prefill_start_uts) {
                        $publication_array[$language_code]['start_datetime']->setTimestamp($prefill_start_uts);
                    }
                }
                if (isset($prefill[$language_code]['end'])) {
                    $prefill_end_uts = strtotime($prefill[$language_code]['end']);
                    if ($prefill_end_uts) {
                        $publication_array[$language_code]['end_datetime']->setTimestamp($prefill_end_uts);
                    }
                }
            }
        }
        $publication_widget->removePrefilledData($request, $app);
        return $app['twig']->render(
            '@Admin/Publication/form.twig',
            array(
                'base_name' => $base_name,
                'publication' => $publication_array,
                'options' => $publication->getOptions(),
                'language' => $language,
                'digest' => $publication->getContentUnit()->getDigest(),
            )
        );
    }

    public function advancedSearchForm(
        Request $request,
        Application $app,
        Publication $publication,
        PublicationWidget $publication_widget)
    {
        $base_name = $publication_widget->getPrefix() . '_publication' . '_search';
        $search_data_bag_name = 'search_data_' . $base_name;
        $status_options = array();
        $publication_options = $publication->getOptions();
        if ($publication_options['use_published']) {
            $selected_status = (string) $app['session']->get($search_data_bag_name, '');
            $status_options = array(
                '' => array(
                    'label' => 'Any',
                    'selected' => ($selected_status == ''),
                ),
                'published_only' => array(
                    'label' => 'Published only',
                    'selected' => ($selected_status == 'published_only'),
                ),
                'not_published_only' => array(
                    'label' => 'Not published only',
                    'selected' => ($selected_status == 'not_published_only'),
                ),
            );
        }
        return $app['twig']->render(
            '@Admin/Publication/advancedSearchForm.twig',
            array(
                'base_name' => $base_name,
                'status_options' => $status_options,
                'options' => $publication->getOptions(),
            )
        );
    }

}

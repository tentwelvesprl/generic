<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Publication;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Base\Publication\Publication;

class PublicationWidget
{
    private $publication;
    private $prefix;

    public function __construct(Publication $publication, $prefix)
    {
        $this->publication = $publication;
        $this->prefix = (string) $prefix;
        return $this;
    }

    public function getPrefix()
    {
        return $this->prefix;
    }

    public function getPublication()
    {
        return $this->publication;
    }

    public function set(Request $request, Application $app)
    {
        $base_name = $this->getPrefix() . '_publication';
        $publication_options = $this->publication->getOptions();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            if ($publication_options['use_published']) {
                $published = (int) $request->request->get($base_name . '_published_' . $language_code, 0);
                $this->publication->setPublished($language_code, ($published == 1));
            }
            if ($publication_options['use_start_datetime']) {
                $start_uts = time();
                $start_str = ''
                    . (string) $request->request->get($base_name . '_start_date_' . $language_code, '')
                    . ' '
                    . (string) $request->request->get($base_name . '_start_time_' . $language_code, '');
                $new_start_uts = strtotime($start_str);
                if ($new_start_uts) {
                    $start_uts = $new_start_uts;
                }
                $this->publication->setStartDateTime($language_code, $start_uts);
            }
            if ($publication_options['use_end_datetime']) {
                $end_uts = time() + 365 * 24 * 3600;
                $end_str = ''
                    . (string) $request->request->get($base_name . '_end_date_' . $language_code, '')
                    . ' '
                    . (string) $request->request->get($base_name . '_end_time_' . $language_code, '');
                $new_end_uts = strtotime($end_str);
                if ($new_end_uts) {
                    $end_uts = $new_end_uts;
                }
                $this->publication->setEndDateTime($language_code, $end_uts);
            }
        }
    }

    public function storePrefilledData(Request $request, Application $app)
    {
        $base_name = $this->getPrefix() . '_publication';
        $prefill_bag_name = 'prefill-' . $base_name;
        $value = array();
        $publication_options = $this->publication->getOptions();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $published = 0;
            if ($publication_options['use_published']) {
                $published = (int) $request->request->get($base_name . '_published_' . $language_code, 0);
                $published = ($published == 1);
            }
            $start = '';
            if ($publication_options['use_start_datetime']) {
                $start = ''
                    . (string) $request->request->get($base_name . '_start_date_' . $language_code, '')
                    . ' '
                    . (string) $request->request->get($base_name . '_start_time_' . $language_code, '');
            }
            $end = '';
            if ($publication_options['use_end_datetime']) {
                $end = ''
                    . (string) $request->request->get($base_name . '_end_date_' . $language_code, '')
                    . ' '
                    . (string) $request->request->get($base_name . '_end_time_' . $language_code, '');
            }
            $value[$language_code] = array(
                'published' => $published,
                'start' => $start,
                'end' => $end,
            );
        }
        $app['session']->set($prefill_bag_name, $value);
    }

    public function removePrefilledData(Request $request, Application $app)
    {
        $base_name = $this->getPrefix() . '_publication';
        $prefill_bag_name = 'prefill-' . $base_name;
        $app['session']->remove($prefill_bag_name);
    }

    public function getFilters(Request $request, Application $app)
    {
        $filters = array();
        $base_name = $this->getPrefix() . '_publication' . '_search';
        $search_data_bag_name = 'search_data_' . $base_name;
        $publication_options = $this->publication->getOptions();
        if ($publication_options['use_published']) {
            $filters[] = array(
                'type' => 'publication_status',
                'value' => (string) $app['session']->get($search_data_bag_name, ''),
            );
        }
        return $filters;
    }

    public function storeAdvancedSearchData(Request $request, Application $app)
    {
        $base_name = $this->getPrefix() . '_publication' . '_search';
        $search_data_bag_name = 'search_data_' . $base_name;
        if ($request->query->get('reset', 0) == 1) {
            $app['session']->remove($search_data_bag_name);
        }
        $publication_options = $this->publication->getOptions();
        if ($publication_options['use_published']) {
            if ($request->query->has($base_name)) {
                $value = (string) $request->query->get($base_name, '');
                $app['session']->set($search_data_bag_name, $value);
            }
        }
    }

    public function getSortOptions()
    {
        $sort_options = array();
        $publication_options = $this->publication->getOptions();
        if ($publication_options['use_start_datetime']) {
            $sort_options = array(
                'publication_time' => 'Publication time',
            );
        }
        return $sort_options;
    }

    public function getActiveFiltersDescription(Request $request, Application $app)
    {
        $active_filters_description = array();
        $base_name = $this->getPrefix() . '_publication' . '_search';
        $search_data_bag_name = 'search_data_' . $base_name;
        $publication_options = $this->publication->getOptions();
        if ($publication_options['use_published']) {
            $value = (string) $app['session']->get($search_data_bag_name, '');
            if ($value == 'published_only') {
                $active_filters_description[] = array(
                    'name' => 'Publication status',
                    'description' => 'published only',
                );
            }
            if ($value == 'not_published_only') {
                $active_filters_description[] = array(
                    'name' => 'Publication status',
                    'description' => 'not_published only',
                );
            }
        }
        return $active_filters_description;
    }

}

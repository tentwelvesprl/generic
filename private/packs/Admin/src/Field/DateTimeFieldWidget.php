<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Field;

use DateTime;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Admin\Field\AbstractFieldWidget;
use Viktor\Pack\Base\Field\AbstractField;

class DateTimeFieldWidget extends AbstractFieldWidget
{
    public function __construct(array $options, AbstractField $field, $prefix)
    {
        parent::__construct($options, $field, $prefix);
        $this->options = array_merge($this->getDefaultOptions(), $options);
        $field_options = $this->getField()->getOptions();
        if (!$field_options['indexed']) {
            $this->options['searchable'] = false;
        }
        if (!$field_options['indexed']) {
            $this->options['sortable'] = false;
        }
    }

    public function getDefaultOptions()
    {
        $parent_default_options = parent::getDefaultOptions();
        $default_options = array(
            'display_time' => true,
            'searchable' => false,
            'sortable' => false,
        );
        return array_merge($parent_default_options, $default_options);
    }

    public function form(Request $request, Application $app, $language)
    {
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName();
        $invalid_field_flashbag_name = 'invalid-field-' . $input_name;
        $error_flashbag_name = 'error-' . $input_name;
        $prefill_bag_name = 'prefill-' . $input_name;
        $ts = $this->getField()->get();
        if ($ts === null) {
            $value = null;
        } else {
            $value = new DateTime();
            @$value->setTimestamp($ts);
        }
        $prefill = $app['session']->get($prefill_bag_name);
        if ($prefill !== null) {
            $value = $prefill;
        }
        $this->removePrefilledData($request, $app);
        return $app['twig']->render(
            '@Admin/Field/dateTimeFieldForm.twig',
            array(
                'name' => $input_name,
                'value' => $value,
                'widget_options' => $this->getOptions(),
                'metadata' => $this->getField()->getAllMetadata(),
                'invalid_field_flashbag_name' => $invalid_field_flashbag_name,
                'error_flashbag_name' => $error_flashbag_name,
                'language' => $language,
                'field_language' => $this->getField()->getMetadata('language'),
                'translatable' => $this->getField()->getMetadata('translatable'),
                'disabled' => !$this->getField()->getContentUnit()->canBeEdited(),
            )
        );
    }

    public function advancedSearchForm(Request $request, Application $app, $language)
    {
        $base_name = $this->getPrefix() . '_' . $this->getField()->getName() . '_search';
        $search_data_bag_base_name = 'search_data_' . $base_name;
        $value = array(
            'from_date' => (string) $app['session']->get($search_data_bag_base_name . '_from_date', ''),
            'to_date' => (string) $app['session']->get($search_data_bag_base_name . '_to_date', ''),
        );
        return $app['twig']->render(
            '@Admin/Field/dateTimeFieldAdvancedSearchForm.twig',
            array(
                'base_name' => $base_name,
                'value' => $value,
                'field_options' => $this->getField()->getOptions(),
                'widget_options' => $this->getOptions(),
            )
        );
    }

    public function getValueFromRequest(Request $request, Application $app)
    {
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName();
        $date_part = $request->request->get($input_name . '_date', '');
        $time_part = $request->request->get($input_name . '_time', '');
        if ($date_part == '') {
            return null;
        }
        if ($time_part == '') {
            $time_part = '00:00:00';
        }
        $value = $date_part . ' ' . $time_part;
        $dt = new DateTime();
        try {
            @$dt->modify($value);
            $ts = $dt->getTimestamp();
        } catch (Exception $e) {
            $ts = time();
        }
        return $ts;
    }

    public function set(Request $request, Application $app)
    {
        $value = $this->getValueFromRequest($request, $app);
        $this->getField()->set($value);
    }

    public function storePrefilledData(Request $request, Application $app)
    {
        $value = $this->getValueFromRequest($request, $app);
        $prefill_bag_name = 'prefill-' . $input_name;
        $app['session']->set($prefill_bag_name, $value);
    }

    public function removePrefilledData(Request $request, Application $app)
    {
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName();
        $prefill_bag_name = 'prefill-' . $input_name;
        $app['session']->remove($prefill_bag_name);
    }

    public function validate(Request $request, Application $app)
    {
        return true;
    }

    public function getFilters(Request $request, Application $app)
    {
        $filters = array();
        $options = $this->getOptions();
        if ($options['searchable']) {
            $base_name = $this->getPrefix() . '_' . $this->getField()->getName() . '_search';
            $search_data_bag_base_name = 'search_data_' . $base_name;
            $from_date = (string) $app['session']->get($search_data_bag_base_name . '_from_date', '');
            $to_date = (string) $app['session']->get($search_data_bag_base_name . '_to_date', '');
            if ($from_date != '') {
                $filters[] = array(
                    'type' => 'date_time_field_date_comparison',
                    'field_name' => $this->getField()->getName(),
                    'operator' => 'greater_than_or_equal_to',
                    'value' => $from_date,
                );
            }
            if ($to_date != '') {
                $filters[] = array(
                    'type' => 'date_time_field_date_comparison',
                    'field_name' => $this->getField()->getName(),
                    'operator' => 'less_than_or_equal_to',
                    'value' => $to_date,
                );
            }
        }
        return $filters;
    }

    public function storeAdvancedSearchData(Request $request, Application $app)
    {
        $options = $this->getOptions();
        if ($options['searchable']) {
            $base_name = $this->getPrefix() . '_' . $this->getField()->getName() . '_search';
            $search_data_bag_base_name = 'search_data_' . $base_name;
            if ($request->query->get('reset', 0) == 1) {
                $app['session']->remove($search_data_bag_base_name . '_from_date');
                $app['session']->remove($search_data_bag_base_name . '_to_date');
            }
            if ($request->query->has($base_name . '_from_date')) {
                $value = (string) $request->query->get($base_name . '_from_date', '');
                $app['session']->set($search_data_bag_base_name . '_from_date', $value);
            }
            if ($request->query->has($base_name . '_to_date')) {
                $value = (string) $request->query->get($base_name . '_to_date', '');
                $app['session']->set($search_data_bag_base_name . '_to_date', $value);
            }
        }
    }

    public function getSortOptions()
    {
        $sort_options = array();
        $options = $this->getOptions();
        $label = $options['label'];
        if ($options['sortable']) {
            $sort_options = array(
                'field_' . $this->getField()->getName() => $label,
            );
        }
        return $sort_options;
    }

    public function getActiveFiltersDescription(Request $request, Application $app)
    {
        $options = $this->getOptions();
        $base_name = $this->getPrefix() . '_' . $this->getField()->getName() . '_search';
        $active_filters_description = array();
        if ($options['searchable']) {
            $search_data_bag_base_name = 'search_data_' . $base_name;
            if ($app['session']->has($search_data_bag_base_name . '_from_date') && trim($app['session']->get($search_data_bag_base_name . '_from_date', '')) != '') {
                $value = (string) $app['session']->get($search_data_bag_base_name . '_from_date', '');
                $ts = @strtotime($value);
                if ($ts !== false) {
                    $active_filters_description[] = array(
                        'name' => $options['label'],
                        'description' => 'from ' . date('d.m.Y', $ts),
                    );
                }
            }
            if ($app['session']->has($search_data_bag_base_name . '_to_date') && trim($app['session']->get($search_data_bag_base_name . '_to_date', '')) != '') {
                $value = (string) $app['session']->get($search_data_bag_base_name . '_to_date', '');
                $ts = @strtotime($value);
                if ($ts !== false) {
                    $active_filters_description[] = array(
                        'name' => $options['label'],
                        'description' => 'to ' . date('d.m.Y', $ts),
                    );
                }
            }
        }
        return $active_filters_description;
    }

}

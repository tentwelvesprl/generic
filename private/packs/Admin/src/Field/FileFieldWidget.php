<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Field;

use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Admin\Field\AbstractFieldWidget;
use Viktor\Pack\Base\Field\AbstractField;

class FileFieldWidget extends AbstractFieldWidget
{
    public function __construct(array $options, AbstractField $field, $prefix)
    {
        parent::__construct($options, $field, $prefix);
        $this->options = array_merge($this->getDefaultOptions(), $options);
    }

    public function getDefaultOptions()
    {
        $parent_default_options = parent::getDefaultOptions();
        $default_options = array(
        );
        return array_merge($parent_default_options, $default_options);
    }

    public function form(Request $request, Application $app, $language)
    {
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName();
        $invalid_field_flashbag_name = 'invalid-field-' . $input_name;
        $error_flashbag_name = 'error-' . $input_name;
        $prefill_bag_name = 'prefill-' . $input_name;
        $value = $this->getField()->get();
        $prefill = $app['session']->get($prefill_bag_name);
        if ($prefill !== null) {
            $value = $prefill;
        }
        $this->removePrefilledData($request, $app);
        $options = $this->getField()->getOptions();
        $file_content_unit_model = $app['content_unit_factory']->create($options['file_content_unit_type']);
        return $app['twig']->render(
            '@Admin/Field/fileFieldForm.twig',
            array(
                'name' => $input_name,
                'value' => $value,
                'file_content_unit_model' => $file_content_unit_model->getDigest(),
                'options' => $this->getField()->getOptions(),
                'widget_options' => $this->getOptions(),
                'metadata' => $this->getField()->getAllMetadata(),
                'invalid_field_flashbag_name' => $invalid_field_flashbag_name,
                'error_flashbag_name' => $error_flashbag_name,
                'language' => $language,
                'field_language' => $this->getField()->getMetadata('language'),
                'translatable' => $this->getField()->getMetadata('translatable'),
                'disabled' => !$this->getField()->getContentUnit()->canBeEdited(),
            )
        );
    }

    public function advancedSearchForm(Request $request, Application $app, $language)
    {
        return $app['twig']->render(
            '@Admin/Field/fileFieldAdvancedSearchForm.twig',
            array()
        );
    }

    public function set(Request $request, Application $app)
    {
        $options = $this->getField()->getOptions();
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName();
        $ids = $request->request->get($input_name . '_id', array());
        if (is_array($ids)) {
            $value = array();
            foreach ($ids as $id) {
                $value[] = array(
                    'type' => $options['file_content_unit_type'],
                    'id' => (int) $id,
                );
            }
            $this->getField()->set($value);
        }
    }

    public function storePrefilledData(Request $request, Application $app)
    {
        $options = $this->getField()->getOptions();
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName();
        $prefill_bag_name = 'prefill-' . $input_name;
        $ids = $request->request->get($input_name . '_id', array());
        if (is_array($ids)) {
            $value = array();
            foreach ($ids as $id) {
                $value[] = array(
                    'type' => $options['file_content_unit_type'],
                    'id' => (int) $id,
                );
            }
        }
        $app['session']->set($prefill_bag_name, $value);
    }

    public function removePrefilledData(Request $request, Application $app)
    {
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName();
        $prefill_bag_name = 'prefill-' . $input_name;
        $app['session']->remove($prefill_bag_name);
    }

    public function validate(Request $request, Application $app)
    {
        return true;
    }

    public function getFilters(Request $request, Application $app)
    {
        $filters = array();
        return $filters;
    }

    public function storeAdvancedSearchData(Request $request, Application $app)
    {
    }

    public function getSortOptions()
    {
        $sort_options = array();
        return $sort_options;
    }

}

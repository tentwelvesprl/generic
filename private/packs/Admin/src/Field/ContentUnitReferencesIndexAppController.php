<?php

/*
 * Copyright 2019 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Field;

use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Admin\Admin\AdminHtmlPage;
use Viktor\Pack\Admin\Pagination\PaginationWidget;
use Viktor\Pack\Base\Pagination\Pagination;
use Viktor\Pack\Base\User\UnknownUser;

class ContentUnitReferencesIndexAppController extends AdminHtmlPage
{
    public function usage(Request $request, Application $app, $language, $type, $id)
    {
        $app['i18n']->setLanguage($language);
        $filter_group = $request->query->get('group', '');
        // Main content unit.
        $main_content_unit = $app['content_unit_factory']->load($type, $id);
        if (!is_a($main_content_unit, 'Viktor\Pack\Base\DbStore\HasADbStoreComponentInterface')) {
            $app->abort(404, 'Invalid content unit.');
        }
        if ($main_content_unit instanceof TranslatableInterface) {
            $main_content_unit->setLanguage($language);
        }
        // Content units that have a link to this content unit.
        $filter_model = null;
        if ($filter_group != '') {
            $filter_model = $app['content_unit_factory']->create($filter_group);
        }
        $referencing_content_units_array = $main_content_unit->getDbStoreComponent()->getReferencingContentUnits();
        $grouped_content_units_array = array();
        $available_groups = array();
        foreach ($referencing_content_units_array as $cu) {
            if (!isset($available_groups[$cu['type']])) {
                $cu_model = $app['content_unit_factory']->create($cu['type']);
                if ($cu_model !== null) {
                    $available_groups[$cu_model->getType()] = $cu_model->getDigest();
                }
            }
        }
        ksort($grouped_content_units_array);
        foreach ($referencing_content_units_array as $cu) {
            if ($filter_group != '' && $filter_group != $cu['type']) {
                continue;
            }
            if (!isset($grouped_content_units_array[$cu['type']])) {
                $cu_model = $app['content_unit_factory']->create($cu['type']);
                if ($cu_model !== null) {
                    $grouped_content_units_array[$cu['type']] = array(
                        'name' => $cu_model->getPluralHumanType(),
                        'content_units' => array(),
                    );
                }
            }
            if (isset($grouped_content_units_array[$cu['type']])) {
                $grouped_content_units_array[$cu['type']]['content_units'][] = $cu;
            }
        }
        ksort($grouped_content_units_array);
        $new_referencing_content_units_array = array();
        foreach ($grouped_content_units_array as $content_units_group) {
            foreach ($content_units_group['content_units'] as $cu) {
                $new_referencing_content_units_array[] = $cu;
            }
        }
        $referencing_content_units_array = $new_referencing_content_units_array;
        // Apply pagination.
        $pagination = new Pagination(
            $request,
            $app,
            count($referencing_content_units_array),
            10,
            1,
            array(10, 20, 50),
            'usage_' . $type . '_' . $id . '_' . $app['i18n']->getLanguage()
        );
        $pagination_widget = new PaginationWidget(
            $pagination,
            'usage_' . $type . '_' . $id . '_' . $app['i18n']->getLanguage()
        );
        $pagination_widget->storePaginationParameters($request, $app);
        $pagination_parameters = $pagination_widget->getStoredPaginationParameters($app);
        $pagination->setCurrentPage($pagination_parameters['current_page']);
        $pagination->setNbItemsPerPage($pagination_parameters['nb_items_per_page']);
        $referencing_content_units_to_display = array_slice(
            $referencing_content_units_array,
            ($pagination_parameters['current_page'] - 1) * $pagination_parameters['nb_items_per_page'],
            $pagination_parameters['nb_items_per_page']
        );
        // Load the content units.
        $referencing_content_units = array();
        foreach ($referencing_content_units_to_display as $v) {
            $referencing_content_unit = $app['content_unit_factory']->load($v['type'], $v['id']);
            if ($referencing_content_unit != null) {
                $referencing_content_units[] = $referencing_content_unit;
            }
        }
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $language_urls[$language_code] = $app->path(
                'admin-content-unit-references-index-usage',
                array(
                    'language' => $language,
                    'type' => $type,
                    'id' => $id,
                )
            );
        }
        $this->addMarker($main_content_unit->getType());
        $this->addMarker($main_content_unit->getType() . ':' . $main_content_unit->getId());
        $this->setTitle('Usage of ' . $main_content_unit->getTitle());
        return $this->render(
            $app,
            '@Admin/Field/usage.twig',
            array(
                'filter_model' => ($filter_model !== null ? $filter_model->getDigest() : null),
                'available_groups' => $available_groups,
                'main_content_unit_digest' => $main_content_unit->getDigest(),
                'main_content_unit' => $main_content_unit,
                'referencing_content_units' => $referencing_content_units,
                'pagination'  => $pagination,
                'pagination_widget' => $pagination_widget,
                'language_urls' => $language_urls,
                'language' => $app['i18n']->getLanguage(),
            )
        );
    }

}

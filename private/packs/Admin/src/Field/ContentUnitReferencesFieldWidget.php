<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Field;

use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Admin\Field\AbstractFieldWidget;
use Viktor\Pack\Base\Field\AbstractField;
use Viktor\Pack\Base\I18n\TranslatableInterface;
use Viktor\Pack\Base\Tree\NodeInterface;
use Viktor\Pack\Base\Tree\TreeStructure;

class ContentUnitReferencesFieldWidget extends AbstractFieldWidget
{
    public function __construct(array $options, AbstractField $field, $prefix)
    {
        parent::__construct($options, $field, $prefix);
        $this->options = array_merge($this->getDefaultOptions(), $options);
        $field_options = $this->getField()->getOptions();
        if (!$field_options['indexed']) {
            $this->options['searchable'] = false;
        }
        if (!$field_options['indexed']) {
            $this->options['sortable'] = false;
        }
    }

    public function getDefaultOptions()
    {
        $parent_default_options = parent::getDefaultOptions();
        $default_options = array(
            'searchable' => false,
        );
        return array_merge($parent_default_options, $default_options);
    }

    public function form(Request $request, Application $app, $language)
    {
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName();
        $invalid_field_flashbag_name = 'invalid-field-' . $input_name;
        $error_flashbag_name = 'error-' . $input_name;
        $prefill_bag_name = 'prefill-' . $input_name;
        $value = $this->getField()->get();
        $prefill = $app['session']->get($prefill_bag_name);
        if ($prefill !== null) {
            $new_value = array();
            if (is_array($prefill)) {
                foreach ($prefill as $reference_string) {
                    $reference = explode(':', $reference_string);
                    if (count($reference) == 2) {
                        $new_value[] = array(
                            'type' => $reference[0],
                            'id' => $reference[1],
                        );
                    }
                }
            }
            $value = $new_value;
        }
        $this->removePrefilledData($request, $app);
        // Find the types of content units that be created from this form
        // element.
        $content_units_that_can_be_created = array();
        $field_options = $this->getField()->getOptions();
        if (isset($field_options['content_unit_types'])) {
            foreach ($field_options['content_unit_types'] as $content_unit_type) {
                $content_unit_model = $app['content_unit_factory']->create($content_unit_type);
                if ($content_unit_model != null && $content_unit_model->canBeCreated()) {
                    $content_units_that_can_be_created[] = array(
                        'type' => $content_unit_model->getType(),
                        'singular_human_type' => $content_unit_model->getSingularHumanType(),
                    );
                }
            }
        }
        return $app['twig']->render(
            '@Admin/Field/contentUnitReferencesFieldForm.twig',
            array(
                'name' => $input_name,
                'value' => $value,
                'options' => $this->getField()->getOptions(),
                'widget_options' => $this->getOptions(),
                'metadata' => $this->getField()->getAllMetadata(),
                'invalid_field_flashbag_name' => $invalid_field_flashbag_name,
                'error_flashbag_name' => $error_flashbag_name,
                'language' => $language,
                'field_language' => $this->getField()->getMetadata('language'),
                'translatable' => $this->getField()->getMetadata('translatable'),
                'content_units_that_can_be_created' => $content_units_that_can_be_created,
                'disabled' => !$this->getField()->getContentUnit()->canBeEdited(),
            )
        );
    }

    public function advancedSearchForm(Request $request, Application $app, $language)
    {
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName() . '_search';
        $search_data_bag_name = 'search_data_' . $input_name;
        $value = (string) $app['session']->get($search_data_bag_name, '');
        $options = $this->getField()->getOptions();
        $widget_options = $this->getOptions();
        $linkable_content_units = array();
        $display_mode = 'none';
        if ($widget_options['searchable'] && isset($options['content_unit_types'][0])) {
            $content_unit_model = $app['content_unit_factory']->create($options['content_unit_types'][0]);
            if ($content_unit_model instanceof NodeInterface) {
                $display_mode = 'tree';
                 $tree_structure = new TreeStructure($app, $content_unit_model->getType());
                 $tree_structure->load();
                 $flat_list = $tree_structure->getFlatList();
                 $levels = $tree_structure->getLevels();
                 $content_unit_ids = array_keys($flat_list);
                 $content_unit_list = $app['content_unit_factory']->createList($content_unit_model->getType(), $content_unit_ids);
                 foreach ($content_unit_list as $content_unit) {
                     if ($content_unit instanceof TranslatableInterface) {
                         $content_unit->setLanguage($language);
                     }
                     $prefix = '';
                     for ($i = 1; $i < $levels[$content_unit->getId()]; $i++) {
                         $prefix .= '► ';
                     }
                     $linkable_content_units[] = array(
                         'type' => $content_unit->getType(),
                         'id' => $content_unit->getId(),
                         'title' => $prefix . ' ' . $content_unit->getTitle(),
                     );
                 }
            } else {
                $display_mode = 'typeahead';
            }
        }
        // Retrieve current value info.
        $linked_content_unit = null;
        $linked_content_unit_digest = null;
        $reference = explode(':', $value);
        if (count($reference) == 2) {
            $reference_type = $reference[0];
            $reference_id = $reference[1];
            $linked_content_unit = $app['content_unit_factory']->load($reference_type, $reference_id);
            if ($linked_content_unit != null) {
                if ($linked_content_unit instanceof TranslatableInterface) {
                    $linked_content_unit->setLanguage($app['i18n']->getLanguage());
                }
                $linked_content_unit_digest = $linked_content_unit->getDigest();
            }
        }
        return $app['twig']->render(
            '@Admin/Field/contentUnitReferencesFieldAdvancedSearchForm.twig',
            array(
                'name' => $input_name,
                'value' => $value,
                'widget_options' => $widget_options,
                'options' => $this->getField()->getOptions(),
                'language' => $language,
                'field_language' => $this->getField()->getMetadata('language'),
                'linkable_content_units' => $linkable_content_units,
                'display_mode' => $display_mode,
                'linked_content_unit' => $linked_content_unit_digest,
            )
        );
    }

    public function set(Request $request, Application $app)
    {
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName();
        $value = $request->request->get($input_name, array());
        $new_value = array();
        if (is_array($value)) {
            foreach ($value as $reference_string) {
                $reference = explode(':', $reference_string);
                if (count($reference) == 2) {
                    $new_value[] = array(
                        'type' => $reference[0],
                        'id' => $reference[1],
                    );
                }
            }
        }
        $this->getField()->set($new_value);
    }

    public function storePrefilledData(Request $request, Application $app)
    {
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName();
        $prefill_bag_name = 'prefill-' . $input_name;
        $value = $request->request->get($input_name, '');
        $app['session']->set($prefill_bag_name, $value);
    }

    public function removePrefilledData(Request $request, Application $app)
    {
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName();
        $prefill_bag_name = 'prefill-' . $input_name;
        $app['session']->remove($prefill_bag_name);
    }

    public function validate(Request $request, Application $app)
    {
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName();
        $invalid_field_flashbag_name = 'invalid-field-' . $input_name;
        $error_flashbag_name = 'error-' . $input_name;
        $value = $request->request->get($input_name, '');
        $options = $this->getOptions();
        return true;
    }

    public function getFilters(Request $request, Application $app)
    {
        $filters = array();
        $options = $this->getOptions();
        if ($options['searchable']) {
            $input_name = $this->getPrefix() . '_' . $this->getField()->getName() . '_search';
            $search_data_bag_name = 'search_data_' . $input_name;
            $reference = explode(':', $app['session']->get($search_data_bag_name, ''));
            if (count($reference) == 2) {
                $reference_type = $reference[0];
                $reference_id = $reference[1];
                $filters[] = array(
                    'type' => 'content_unit_references_index',
                    'field_name' => $this->getField()->getName(),
                    'reference_type' => $reference_type,
                    'reference_id' => $reference_id,
                );
            }
        }
        return $filters;
    }

    public function storeAdvancedSearchData(Request $request, Application $app)
    {
        $options = $this->getOptions();
        if ($options['searchable']) {
            $input_name = $this->getPrefix() . '_' . $this->getField()->getName() . '_search';
            $search_data_bag_name = 'search_data_' . $input_name;
            if ($request->query->get('reset', 0) == 1) {
                $app['session']->remove($search_data_bag_name);
            }
            if ($request->query->has($input_name)) {
                $value = (string) $request->query->get($input_name, '');
                $app['session']->set($search_data_bag_name, $value);
            }
        }
    }

    public function getSortOptions()
    {
        $sort_options = array();
        $options = $this->getOptions();
        $label = $options['label'];
        if ($options['sortable']) {
            $sort_options = array(
                'field_' . $this->getField()->getName() => $label,
            );
        }
        return $sort_options;
    }

    public function getActiveFiltersDescription(Request $request, Application $app)
    {
        $options = $this->getOptions();
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName() . '_search';
        $active_filters_description = array();
        if ($options['searchable']) {
            $search_data_bag_name = 'search_data_' . $input_name;
            if ($app['session']->has($search_data_bag_name) && trim($app['session']->get($search_data_bag_name, '')) != '') {
                $value = (string) $app['session']->get($search_data_bag_name, '');
                $reference = explode(':', $app['session']->get($search_data_bag_name, ''));
                if (count($reference) == 2) {
                    $reference_type = $reference[0];
                    $reference_id = $reference[1];
                    $linked_content_unit = $app['content_unit_factory']->load($reference_type, $reference_id);
                    if ($linked_content_unit != null) {
                        if ($linked_content_unit instanceof TranslatableInterface) {
                            $linked_content_unit->setLanguage($app['i18n']->getLanguage());
                        }
                        $active_filters_description[] = array(
                            'name' => $options['label'],
                            'description' =>
                                'linked to '
                                . $linked_content_unit->getTitle()
                                . ' (' . $linked_content_unit->getSingularHumanType() . ' #' . $linked_content_unit->getId() . ')',
                        );
                    }
                }
            }
        }
        return $active_filters_description;
    }

}

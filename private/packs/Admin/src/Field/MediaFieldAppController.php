<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Field;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Viktor\Application;
use Viktor\Pack\Base\EmbedInfo\HasAnEmbedInfoInterface;
use Viktor\Pack\Base\FullTextIndex\SearchEngine;
use Viktor\Pack\Base\I18n\TranslatableInterface;

class MediaFieldAppController
{
    public function item(Request $request, Application $app, $language, $type, $base_name)
    {
        $id = (int) $request->query->get('id', 0);
        $disabled = (bool) $request->query->get('disabled', false);
        $sortable = (bool) $request->query->get('sortable', false);
        $content_unit = $app['content_unit_factory']->load($type, $id);
        if ($id > 0 && $content_unit !== null && $content_unit->canBeListed() && $content_unit instanceof HasAnEmbedInfoInterface) {
            return $app['twig']->render(
                '@Admin/Field/mediaFieldItem.twig',
                array(
                    'language' => $language,
                    'media' => $content_unit,
                    'base_name' => $base_name,
                    'disabled' => $disabled,
                    'sortable' => $sortable,
                )
            );
        } else {
            return '';
        }
    }

    public function createItemFromUrl(Request $request, Application $app, $language, $type, $base_name)
    {
        $sortable = (bool) $request->query->get('sortable', false);
        try {
            $url = (string) $request->request->get('url', '');
            $content_unit = $app['content_unit_factory']->create($type);
            if ($content_unit == null || !$content_unit->canBeCreated() || !($content_unit instanceof HasAnEmbedInfoInterface)) {
                throw new Exception('The media could not be created.');
            }
            $content_unit->getEmbedInfo()->setValue(array('source_url' => $url));
            $content_unit->getEmbedInfo()->scrapeSourceUrl();
            $content_unit->save($app['viktor.user']);
            if ($content_unit->getId() == 0) {
                throw new Exception('The media could not be saved.');
            }
            return $app['twig']->render(
                '@Admin/Field/mediaFieldItem.twig',
                array(
                    'language' => $language,
                    'media' => $content_unit,
                    'base_name' => $base_name,
                    'disabled' => false,
                    'sortable' => $sortable,
                )
            );
        } catch (Exception $e) {
            return '';
        }
    }

    public function searchResults(Request $request, Application $app, $language)
    {
        $app['i18n']->setLanguage($language);
        $language = $app['i18n']->getLanguage();
        $media_types = explode(',', $request->query->get('media_types', ''));
        $media_searchable_types = array();
        $q = $request->query->get('q', '');
        foreach ($media_types as $media_type) {
            $media_model = $app['content_unit_factory']->create($media_type);
            if ($media_model != null && $media_model->canBeListed()) {
                $media_searchable_types[] = $media_model->getType();
            }
        }
        // Newly created content units.
        $newly_created_results = array();
        if (mb_strlen($q) < 3) {
            foreach ($media_searchable_types as $media_searchable_type) {
                $media_model = $app['content_unit_factory']->create($media_searchable_type);
                $ids = $media_model->getIds(
                    array(),
                    array(
                        array(
                            'option' => 'creation_datetime',
                            'direction' => 'DESC',
                        )
                    )
                );
                $ids = array_slice($ids, 0, 5);
                foreach ($ids as $id) {
                    $newly_created_results[] = array(
                        'type' => $media_searchable_type,
                        'id' => $id,
                    );
                }
            }
        }
        // Id based search.
        $id_based_results = array();
        if ((int) $q > 0) {
            $id = (int) $q;
            foreach ($media_searchable_types as $media_type) {
                $id_based_results[] = array(
                    'type' => $media_type,
                    'id' => $id,
                );
            }
        }
        // Full text search.
        $search_engine = new SearchEngine($app);
        $full_text_results = $search_engine->getSearchResults(
            $q,
            'media_' . $language,
            $media_searchable_types,
            array(
                'search_incomplete_tokens' => true,
                'max_number_of_results' => 50,
            )
        );
        // Load content units.
        $results = array_merge($newly_created_results, $id_based_results);
        $results = array_merge($results, $full_text_results);
        $medias = array();
        foreach ($results as $result) {
            $media = $app['content_unit_factory']->load($result['type'], $result['id']);
            if ($media) {
                if ($media instanceof TranslatableInterface) {
                    $media->setLanguage($language);
                }
                $medias[] = $media;
            }
        }
        $response = new Response();
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/json');
        $a = array();
        foreach ($medias as $media) {
            $a[] = array(
                'type' => $media->getType(),
                'id' => $media->getId(),
                'singular_human_type' => $media->getSingularHumanType(),
                'title' => $media->getTitle(),
                'hint' => $media->getHint(),
                'image' => ($media->getImage() != null ? $app->path('admin-content-unit-image-small', array('language' => $language, 'type' => $media->getType(), 'id' => $media->getId())) : ''),
            );
        }
        $content = json_encode($a);
        $response->setContent($content);
        return $response;
    }

}

<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Field;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Viktor\Application;
use Viktor\Pack\Base\DataFile\UploadHandler;
use Viktor\Pack\Base\DataFile\DataFile;
use Viktor\Pack\Base\FullTextIndex\SearchEngine;
use Viktor\Pack\Base\I18n\TranslatableInterface;

class FileFieldAppController
{
    public function item(Request $request, Application $app, $language, $type, $base_name)
    {
        $id = (int) $request->query->get('id', 0);
        $disabled = (bool) $request->query->get('disabled', false);
        $sortable = (bool) $request->query->get('sortable', false);
        $content_unit = $app['content_unit_factory']->load($type, $id);
        if ($id > 0 && $content_unit !== null && $content_unit->canBeListed()) {
            return $app['twig']->render(
                '@Admin/Field/fileFieldItem.twig',
                array(
                    'language' => $language,
                    'file_content_unit' => $content_unit,
                    'base_name' => $base_name,
                    'disabled' => $disabled,
                    'sortable' => $sortable,
                )
            );
        } else {
            return '';
        }
    }

    public function createItemFromTmpFile(Request $request, Application $app, $language, $type, $base_name)
    {
        $sortable = (bool) $request->query->get('sortable', false);
        try {
            $file_name = (string) $request->request->get('file_name', '');
            $content_unit = $app['content_unit_factory']->create($type);
            if ($content_unit == null || !$content_unit->canBeCreated()) {
                throw new Exception('The file could not be created.');
            }
            $data_file = new DataFile($app);
            if (!$data_file->createFromTmpFile($file_name)) {
                throw new Exception('The file could not be created.');
            }
            $content_unit->setAttachmentFilePath($data_file->getPath());
            $content_unit->save($app['viktor.user']);
            if ($content_unit->getId() == 0) {
                throw new Exception('The file could not be saved.');
            }
            return $app['twig']->render(
                '@Admin/Field/fileFieldItem.twig',
                array(
                    'language' => $language,
                    'file_content_unit' => $content_unit,
                    'base_name' => $base_name,
                    'disabled' => false,
                    'sortable' => $sortable,
                )
            );
        } catch (Exception $e) {
            return '';
        }
    }

    public function uploadHandler(Request $request, Application $app, $language, $type, $base_name)
    {
        try {
            $content_unit = $app['content_unit_factory']->create($type);
            if ($content_unit == null || !$content_unit->canBeCreated()) {
                throw new Exception('The file could not be created.');
            }
            $options = array(
                'upload_dir' => $app['viktor.tmp_path'] . '/',
                'param_name' => $base_name . '_files',
                'accept_file_types' => '//',
            );
            $upload_handler = new UploadHandler($options);
            exit();
        } catch (Exception $e) {
            $app->abort(404);
        }
    }

    public function searchResults(Request $request, Application $app, $language)
    {
        $app['i18n']->setLanguage($language);
        $language = $app['i18n']->getLanguage();
        $file_types = explode(',', $request->query->get('file_types', ''));
        $file_searchable_types = array();
        $q = $request->query->get('q', '');
        foreach ($file_types as $file_type) {
            $file_model = $app['content_unit_factory']->create($file_type);
            if ($file_model != null && $file_model->canBeListed()) {
                $file_searchable_types[] = $file_model->getType();
            }
        }
        // Newly created content units.
        $newly_created_results = array();
        if (mb_strlen($q) < 3) {
            foreach ($file_searchable_types as $file_searchable_type) {
                $file_model = $app['content_unit_factory']->create($file_searchable_type);
                $ids = $file_model->getIds(
                    array(),
                    array(
                        array(
                            'option' => 'creation_datetime',
                            'direction' => 'DESC',
                        )
                    )
                );
                $ids = array_slice($ids, 0, 5);
                foreach ($ids as $id) {
                    $newly_created_results[] = array(
                        'type' => $file_searchable_type,
                        'id' => $id,
                    );
                }
            }
        }
        // Id based search.
        $id_based_results = array();
        if ((int) $q > 0) {
            $id = (int) $q;
            foreach ($file_searchable_types as $file_type) {
                $id_based_results[] = array(
                    'type' => $file_type,
                    'id' => $id,
                );
            }
        }
        // Full text search.
        $search_engine = new SearchEngine($app);
        $full_text_results = $search_engine->getSearchResults(
            $q,
            'file_' . $language,
            $file_searchable_types,
            array(
                'search_incomplete_tokens' => true,
                'max_number_of_results' => 50,
            )
        );
        // Load content units.
        $results = array_merge($newly_created_results, $id_based_results);
        $results = array_merge($results, $full_text_results);
        $files = array();
        foreach ($results as $result) {
            $file = $app['content_unit_factory']->load($result['type'], $result['id']);
            if ($file) {
                if ($file instanceof TranslatableInterface) {
                    $file->setLanguage($language);
                }
                $files[] = $file;
            }
        }
        $response = new Response();
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/json');
        $a = array();
        foreach ($files as $file) {
            $a[] = array(
                'type' => $file->getType(),
                'id' => $file->getId(),
                'singular_human_type' => $file->getSingularHumanType(),
                'title' => $file->getTitle(),
                'hint' => $file->getHint(),
                'image' => ($file->getImage() != null ? $app->path('admin-content-unit-image-small', array('language' => $language, 'type' => $file->getType(), 'id' => $file->getId())) : ''),
            );
        }
        $content = json_encode($a);
        $response->setContent($content);
        return $response;
    }

}

<?php

/*
 * Copyright 2019 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Field;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Viktor\Application;

class AddressFieldAppController
{
    public function geocode(Request $request, Application $app, $language)
    {
        $address = (string) $request->query->get('address', '');
        $response = new Response();
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/json');
        $a = array(
            'status' => 'error',
            'lat' => 0.0,
            'lng' => 0.0,
            'error' => '',
        );
        $key = $app['viktor.config']['google_geocoding_api_key'];
        if (trim($key) == '') {
            $key = $app['viktor.config']['google_cloud_api_key'];
        }
        $url = ''
            . 'https://maps.googleapis.com/maps/api/geocode/json'
            . '?address=' . urlencode($address)
            . '&key=' . urlencode($key);
        $r = file_get_contents($url);
        $result = json_decode($r);
        if ($result !== null && !empty($result->results)) {
            $a['status'] = 'ok';
            $a['lat'] = (float) $result->results[0]->geometry->location->lat;
            $a['lng'] = (float) $result->results[0]->geometry->location->lng;
        } else {
            $a['error'] = $r;
        }
        $content = json_encode($a);
        $response->setContent($content);
        return $response;
    }

}

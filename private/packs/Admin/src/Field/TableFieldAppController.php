<?php

/*
 * Copyright 2021 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Field;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Viktor\Application;

class TableFieldAppController
{
    public function item(Request $request, Application $app, $language, $type, $field_name, $base_name)
    {
        $disabled = (bool) $request->query->get('disabled', false);
        $cu = $app['content_unit_factory']->create($type);
        if ($cu == null) {
            return '';
        }
        $fields = $cu->getDataset()->getFields();
        if (!isset($fields[$field_name])) {
            return '';
        }
        $options = $fields[$field_name]->getOptions();
        $id = uniqid();
        $row_value = array();
        foreach ($options['columns'] as $column_name => $column_description) {
            $row_value[$column_name] = '';
        }
        return $app->render('@Admin/Field/tableFieldItem.twig', array(
            'options' => $options,
            'base_name' => $base_name,
            'language' => $language,
            'id' => $id,
            'disabled' => $disabled,
            'row_value' => $row_value,
        ));
    }

}

<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Field;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Viktor\Application;
use Viktor\Pack\Base\DataFile\UploadHandler;
use Viktor\Pack\Base\DataFile\DataFile;
use Viktor\Pack\Base\FullTextIndex\SearchEngine;
use Viktor\Pack\Base\I18n\TranslatableInterface;

class ContentUnitReferencesFieldAppController
{
    public function item(Request $request, Application $app, $language, $base_name)
    {
        $type = (string) $request->query->get('type', '');
        $id = (int) $request->query->get('id', 0);
        $disabled = (bool) $request->query->get('disabled', false);
        $content_unit = $app['content_unit_factory']->load($type, $id);
        if ($id > 0 && $content_unit !== null && $content_unit->canBeListed()) {
            return $app['twig']->render(
                '@Admin/Field/contentUnitReferencesFieldItem.twig',
                array(
                    'language' => $language,
                    'content_unit' => $content_unit,
                    'base_name' => $base_name,
                    'disabled' => $disabled,
                )
            );
        } else {
            return '';
        }
    }

    public function searchResults(Request $request, Application $app, $language)
    {
        $app['i18n']->setLanguage($language);
        $language = $app['i18n']->getLanguage();
        $content_unit_types = explode(',', $request->query->get('content_unit_types', ''));
        $content_unit_searchable_types = array();
        $q = $request->query->get('q', '');
        foreach ($content_unit_types as $content_unit_type) {
            $content_unit_model = $app['content_unit_factory']->create($content_unit_type);
            if ($content_unit_model != null && $content_unit_model->canBeListed()) {
                $content_unit_searchable_types[] = $content_unit_model->getType();
            }
        }
        // Id based search.
        $id_based_results = array();
        if ((int) $q > 0) {
            $id = (int) $q;
            foreach ($content_unit_searchable_types as $content_unit_type) {
                $id_based_results[] = array(
                    'type' => $content_unit_type,
                    'id' => $id,
                );
            }
        }
        // Newly created content units.
        $newly_created_results = array();
        if (mb_strlen($q) < 3) {
            foreach ($content_unit_searchable_types as $content_unit_searchable_type) {
                $content_unit_model = $app['content_unit_factory']->create($content_unit_searchable_type);
                $ids = $content_unit_model->getIds(
                    array(),
                    array(
                        array(
                            'option' => 'creation_datetime',
                            'direction' => 'DESC',
                        )
                    )
                );
                $ids = array_slice($ids, 0, 5);
                foreach ($ids as $id) {
                    $newly_created_results[] = array(
                        'type' => $content_unit_searchable_type,
                        'id' => $id,
                    );
                }
            }
        }
        // Full text search.
        $search_engine = new SearchEngine($app);
        $full_text_results = $search_engine->getSearchResults(
            $q,
            'general_' . $language,
            $content_unit_searchable_types,
            array(
                'search_incomplete_tokens' => true,
                'max_number_of_results' => 50,
            )
        );
        // Load content units.
        $results = array_merge($newly_created_results, $id_based_results);
        $results = array_merge($results, $full_text_results);
        $content_units = array();
        foreach ($results as $result) {
            $content_unit = $app['content_unit_factory']->load($result['type'], $result['id']);
            if ($content_unit) {
                if ($content_unit instanceof TranslatableInterface) {
                    $content_unit->setLanguage($language);
                }
                $content_units[$result['type'] . ':' . $result['id']] = $content_unit;
            }
        }
        $response = new Response();
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/json');
        $a = array();
        foreach ($content_units as $content_unit) {
            $a[] = array(
                'type' => $content_unit->getType(),
                'id' => $content_unit->getId(),
                'singular_human_type' => $content_unit->getSingularHumanType(),
                'title' => $content_unit->getTitle(),
                'hint' => $content_unit->getHint(),
                'image' => ($content_unit->getImage() != null ? $app->path('admin-content-unit-image-small', array('language' => $language, 'type' => $content_unit->getType(), 'id' => $content_unit->getId())) : ''),
            );
        }
        $content = json_encode($a);
        $response->setContent($content);
        return $response;
    }

    public function createMinimalContentUnit(Request $request, Application $app, $language)
    {
        $type = $request->request->get('type', '');
        $base_name = $request->request->get('base_name', '');
        $response = new Response();
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/json');
        $a = array();
        $content_unit = $app['content_unit_factory']->create($type);
        if (is_object($content_unit)) {
            if ($content_unit->canBeCreated()) {
                $content_unit->save($app['viktor.user']);
                if ($content_unit instanceof TranslatableInterface) {
                    $content_unit->setLanguage($language);
                }
                if ($content_unit->getId() > 0) {
                    $a = array(
                        'type' => $content_unit->getType(),
                        'id' => $content_unit->getId(),
                        'item' => $app['twig']->render(
                            '@Admin/Field/contentUnitReferencesFieldItem.twig',
                            array(
                                'language' => $language,
                                'content_unit' => $content_unit,
                                'base_name' => $base_name,
                            )
                        ),
                        'edition_page_url' => $content_unit->getEditionPageUrl(),
                    );
                }
            }
        }
        $content = json_encode($a);
        $response->setContent($content);
        return $response;
    }

}

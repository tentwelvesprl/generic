<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Field;

use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Base\Field\AbstractField;

abstract class AbstractFieldWidget
{
    private $field;
    private $prefix;
    protected $options;

    public function __construct(array $options, AbstractField $field, $prefix)
    {
        $this->field = $field;
        $this->prefix = (string) $prefix;
        $this->options = array_merge($this->getDefaultOptions(), $options);
    }

    public function getDefaultOptions()
    {
        return array(
            'label' => $this->getField()->getName(),
            'legend' => '',
            'section' => '',
            'help_text_html' => '',
        );
    }

    public function getField()
    {
        return $this->field;
    }

    public function getPrefix()
    {
        return $this->prefix;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function form(Request $request, Application $app, $language)
    {
        return '';
    }

    public function advancedSearchForm(Request $request, Application $app, $language)
    {
        return '';
    }

    public function set(Request $request, Application $app)
    {
    }

    public function storePrefilledData(Request $request, Application $app)
    {
    }

    public function removePrefilledData(Request $request, Application $app)
    {
    }

    public function validate(Request $request, Application $app)
    {
        return true;
    }

    public function getFilters(Request $request, Application $app)
    {
        $filters = array();
        return $filters;
    }

    public function storeAdvancedSearchData(Request $request, Application $app)
    {
    }

    public function getSortOptions()
    {
        $sort_options = array();
        return $sort_options;
    }

    public function getActiveFiltersDescription(Request $request, Application $app)
    {
        $active_filters_description = array();
        return $active_filters_description;
    }

}

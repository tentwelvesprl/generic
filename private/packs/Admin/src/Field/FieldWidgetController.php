<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Field;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Admin\Dataset\DatasetWidget;
use Viktor\Pack\Admin\Field\AbstractFieldWidget;
use Viktor\Pack\Base\Dataset\Dataset;

class FieldWidgetController
{
    public function form(
        Request $request,
        Application $app,
        AbstractFieldWidget $field_widget,
        $language)
    {
        return $field_widget->form($request, $app, $language);
    }

    public function advancedSearchForm(
        Request $request,
        Application $app,
        AbstractFieldWidget $field_widget,
        $language)
    {
        return $field_widget->advancedSearchForm($request, $app, $language);
    }

}

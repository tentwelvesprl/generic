<?php

/*
 * Copyright 2022 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Field;

use Parsedown;
use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Admin\Field\AbstractFieldWidget;
use Viktor\Pack\Base\Field\AbstractField;

class TextFieldWidget extends AbstractFieldWidget
{
    public function __construct(array $options, AbstractField $field, $prefix)
    {
        parent::__construct($options, $field, $prefix);
        $this->options = array_merge($this->getDefaultOptions(), $options);
        $field_options = $this->getField()->getOptions();
        if (!$field_options['indexed']) {
            $this->options['searchable'] = false;
        }
        if (!$field_options['indexed']) {
            $this->options['sortable'] = false;
        }
    }

    public function getDefaultOptions()
    {
        $parent_default_options = parent::getDefaultOptions();
        $default_options = array(
            'mandatory' => false,
            'searchable' => false,
            'sortable' => false,
            'ckeditor_config_file_url' => '/assets/Admin/js/admin-text-field-ckeditor-config.js',
            'rows' => 4,
        );
        return array_merge($parent_default_options, $default_options);
    }

    public function form(Request $request, Application $app, $language)
    {
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName();
        $invalid_field_flashbag_name = 'invalid-field-' . $input_name;
        $error_flashbag_name = 'error-' . $input_name;
        $prefill_bag_name = 'prefill-' . $input_name;
        $source_value = $this->getField()->get();
        $prefill = $app['session']->get($prefill_bag_name);
        if ($prefill !== null) {
            $source_value = $prefill;
        }
        $this->removePrefilledData($request, $app);
        return $app['twig']->render(
            '@Admin/Field/textFieldForm.twig',
            array(
                'name' => $input_name,
                'source_value' => $source_value,
                'field_options' => $this->getField()->getOptions(),
                'widget_options' => $this->getOptions(),
                'metadata' => $this->getField()->getAllMetadata(),
                'invalid_field_flashbag_name' => $invalid_field_flashbag_name,
                'error_flashbag_name' => $error_flashbag_name,
                'language' => $language,
                'field_language' => $this->getField()->getMetadata('language'),
                'translatable' => $this->getField()->getMetadata('translatable'),
                'disabled' => !$this->getField()->getContentUnit()->canBeEdited(),
            )
        );
    }

    public function advancedSearchForm(Request $request, Application $app, $language)
    {
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName() . '_search';
        $search_data_bag_name = 'search_data_' . $input_name;
        $value = (string) $app['session']->get($search_data_bag_name, '');
        return $app['twig']->render(
            '@Admin/Field/textFieldAdvancedSearchForm.twig',
            array(
                'name' => $input_name,
                'value' => $value,
                'widget_options' => $this->getOptions(),
            )
        );
    }

    public function set(Request $request, Application $app)
    {
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName();
        $source_value = $request->request->get($input_name, '');
        $this->getField()->set($source_value);
    }

    public function storePrefilledData(Request $request, Application $app)
    {
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName();
        $prefill_bag_name = 'prefill-' . $input_name;
        $value = $request->request->get($input_name, '');
        $app['session']->set($prefill_bag_name, $value);
    }

    public function removePrefilledData(Request $request, Application $app)
    {
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName();
        $prefill_bag_name = 'prefill-' . $input_name;
        $app['session']->remove($prefill_bag_name);
    }

    public function validate(Request $request, Application $app)
    {
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName();
        $invalid_field_flashbag_name = 'invalid-field-' . $input_name;
        $error_flashbag_name = 'error-' . $input_name;
        $value = $request->request->get($input_name, '');
        $options = $this->getOptions();
        if ($options['mandatory'] && trim($value) == '') {
            $app['session']->getFlashBag()->add($invalid_field_flashbag_name, $input_name);
            $app['session']->getFlashBag()->add($error_flashbag_name, 'This field is mandatory.');
            return false;
        } else {
            return true;
        }
    }

    public function getFilters(Request $request, Application $app)
    {
        $filters = array();
        $options = $this->getOptions();
        if ($options['searchable']) {
            $input_name = $this->getPrefix() . '_' . $this->getField()->getName() . '_search';
            $search_data_bag_name = 'search_data_' . $input_name;
            $q = (string) $app['session']->get($search_data_bag_name, '');
            if (trim($q) != '') {
                $filters[] = array(
                    'type' => 'text_field',
                    'field_name' => $this->getField()->getName(),
                    'value' => '%' . $q . '%',
                );
            }
        }
        return $filters;
    }

    public function storeAdvancedSearchData(Request $request, Application $app)
    {
        $options = $this->getOptions();
        if ($options['searchable']) {
            $input_name = $this->getPrefix() . '_' . $this->getField()->getName() . '_search';
            $search_data_bag_name = 'search_data_' . $input_name;
            if ($request->query->get('reset', 0) == 1) {
                $app['session']->remove($search_data_bag_name);
            }
            if ($request->query->has($input_name)) {
                $value = (string) $request->query->get($input_name, '');
                $app['session']->set($search_data_bag_name, $value);
            }
        }
    }

    public function getSortOptions()
    {
        $sort_options = array();
        $options = $this->getOptions();
        $label = $options['label'];
        if ($options['sortable']) {
            $sort_options = array(
                'field_' . $this->getField()->getName() => $label,
            );
        }
        return $sort_options;
    }

    public function getActiveFiltersDescription(Request $request, Application $app)
    {
        $options = $this->getOptions();
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName() . '_search';
        $active_filters_description = array();
        if ($options['searchable']) {
            $search_data_bag_name = 'search_data_' . $input_name;
            if ($app['session']->has($search_data_bag_name) && trim($app['session']->get($search_data_bag_name, '')) != '') {
                $value = (string) $app['session']->get($search_data_bag_name, '');
                $active_filters_description[] = array(
                    'name' => $options['label'],
                    'description' => 'contains "' . $value . '"',
                );
            }
        }
        return $active_filters_description;
    }

}

<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Admin;

class AdminTemplatingToolbox
{
    private $js;
    private $css;

    public function __construct()
    {
        $this->js = array();
        $this->css = array();
    }

    public function addJs($url, array $attributes)
    {
        $this->js[$url] = $attributes;
    }

    public function addCss($url, array $attributes)
    {
        $this->css[$url] = $attributes;
    }

    public function jsTags()
    {
        $s = '';
        foreach ($this->js as $url => $attributes) {
            $s .= '<script defer ';
            $s .= 'src="' . htmlspecialchars($url) . '" ';
            foreach ($attributes as $k => $v) {
                if ($v === null) {
                    $s .= htmlspecialchars($k) . ' ';
                } else {
                    $s .= htmlspecialchars($k) . '="' . htmlspecialchars($v) . '"';
                }
            }
            $s .= '></script>' . "\n";
        }
        return $s;
    }

    public function cssTags()
    {
        $s = '';
        foreach ($this->css as $url => $attributes) {
            $s .= '<link rel="stylesheet" ';
            $s .= 'href="' . htmlspecialchars($url) . '" ';
            foreach ($attributes as $k => $v) {
                if ($v === null) {
                    $s .= htmlspecialchars($k) . ' ';
                } else {
                    $s .= htmlspecialchars($k) . '="' . htmlspecialchars($v) . '"';
                }
            }
            $s .= '>' . "\n";
        }
        return $s;
    }

}

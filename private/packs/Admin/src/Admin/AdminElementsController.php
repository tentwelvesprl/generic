<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Admin;

use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Base\ContentUnit\ContentUnitInterface;
use Viktor\Pack\Base\I18n\TranslatableInterface;

class AdminElementsController
{
    public function dashboardContent(Request $request, Application $app)
    {
        return $app['twig']->render(
            $app['viktor.config']['admin_dashboard_view'],
            array(
                'website_title' => $app['viktor.config']['website_title'],
                'user'          => $app['viktor.user'],
                'language'      => $app['i18n']->getLanguage(),
            )
        );
    }

    public function toolbar(Request $request, Application $app)
    {
        $user = $app['viktor.user'];
        return $app['twig']->render(
            '@Admin/Admin/toolbar.twig',
            array(
                'website_title' => $app['viktor.config']['website_title'],
                'user'          => $user->getDigest(),
                'language'      => $app['i18n']->getLanguage(),
            )
        );
    }

    public function navigationMenu(Request $request, Application $app)
    {
        $markers = (array) $request->query->get('markers', array());
        $content_unit_models = array();
        foreach ($app['content_unit_factory']->getAvailableTypes() as $content_unit_type => $content_unit_class_name) {
            $content_unit_model = $app['content_unit_factory']->create($content_unit_type);
            if ($content_unit_model != null) {
                $content_unit_models[$content_unit_type] = $content_unit_model->getDigest();
            }
        }
        //echo $app['url_generator']->generate('admin-index', ['language' => 'fr'], \Symfony\Component\Routing\Generator\UrlGeneratorInterface::ABSOLUTE_PATH); exit();
        return $app['twig']->render(
            $app['viktor.config']['admin_navigation_menu_view'],
            array(
                'website_title'       => $app['viktor.config']['website_title'],
                'content_unit_models' => $content_unit_models,
                'language'            => $app['i18n']->getLanguage(),
                'markers'             => $markers,
            )
        );
    }

    public function contentUnitCard(Request $request, Application $app, $language, ContentUnitInterface $content_unit, $display_selection_checkbox = 0)
    {
        $display_selection_checkbox = (int) $display_selection_checkbox;
        if ($content_unit instanceof TranslatableInterface) {
            $content_unit->setLanguage($language);
        }
        return $app['twig']->render(
            '@Admin/Admin/contentUnitCard.twig',
            array(
                'language' => $language,
                'content_unit' => $content_unit->getDigest(),
                'display_selection_checkbox' => $display_selection_checkbox,
            )
        );
    }

    public function contentUnitReferenceCard(Request $request, Application $app, $language, $reference_type, $reference_id, $display_selection_checkbox = 0)
    {
        $display_selection_checkbox = (int) $display_selection_checkbox;
        $content_unit = $app['content_unit_factory']->load($reference_type, $reference_id);
        if ($content_unit == null) {
            return '';
        }
        if ($content_unit instanceof TranslatableInterface) {
            $content_unit->setLanguage($language);
        }
        return $app['twig']->render(
            '@Admin/Admin/contentUnitCard.twig',
            array(
                'language' => $language,
                'content_unit' => $content_unit->getDigest(),
                'display_selection_checkbox' => $display_selection_checkbox,
            )
        );
    }

}

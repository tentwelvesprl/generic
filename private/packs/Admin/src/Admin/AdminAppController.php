<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Admin;

use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Admin\Admin\AdminHtmlPage;
use Viktor\Pack\Admin\Pagination\PaginationWidget;
use Viktor\Pack\Base\FullTextIndex\SearchEngine;
use Viktor\Pack\Base\Pagination\Pagination;

class AdminAppController extends AdminHtmlPage
{
    public function homepage(Request $request, Application $app)
    {
        return $this->index($request, $app, '');
    }

    public function index(Request $request, Application $app, $language)
    {
        $app['i18n']->setLanguage($language);
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $language_urls[$language_code] = $app->path('admin-index', array(
                'language' => $language_code,
            ));
        }
        $this->addMarker('dashboard');
        $this->setTitle('Dashboard');
        return $this->render(
            $app,
            '@Admin/Admin/index.twig',
            array(
                'user' => $app['viktor.user']->getDigest(),
                'language_urls' => $language_urls,
                'language' => $app['i18n']->getLanguage(),
            )
        );
    }

    public function search(Request $request, Application $app, $language)
    {
        $app['i18n']->setLanguage($language);
        $q = (string) $request->query->get('q', '');
        $content_unit_searchable_types = array();
        foreach ($app['content_unit_factory']->getAvailableTypes() as $content_unit_type => $content_unit_class_name) {
            $content_unit_model = $app['content_unit_factory']->create($content_unit_type);
            if ($content_unit_model != null && $content_unit_model->canBeListed()) {
                $content_unit_searchable_types[] = $content_unit_model->getType();
            }
        }
        // Go!
        $search_engine = new SearchEngine($app);
        $results = $search_engine->getSearchResults(
            $q,
            'general_' . $app['i18n']->getLanguage(),
            $content_unit_searchable_types,
            array(
                'search_incomplete_tokens' => true,
                'max_number_of_results' => 50,
            )
        );
        // Apply pagination.
        $pagination = new Pagination(
            $request,
            $app,
            count($results),
            10,
            1,
            array(10, 20, 50),
            'search_' . $app['i18n']->getLanguage()
        );
        $pagination_widget = new PaginationWidget(
            $pagination,
            'search_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        $pagination_parameters = $pagination_widget->getQueryPaginationParameters($request);
        $pagination->setCurrentPage($pagination_parameters['current_page']);
        $pagination->setNbItemsPerPage($pagination_parameters['nb_items_per_page']);
        $results_to_display = array_slice(
            $results,
            ($pagination_parameters['current_page'] - 1) * $pagination_parameters['nb_items_per_page'],
            $pagination_parameters['nb_items_per_page']
        );
        $content_units = array();
        foreach ($results_to_display as $result) {
            $content_unit = $app['content_unit_factory']->load($result['type'], $result['id']);
            if ($content_unit) {
                $content_units[] = $content_unit;
            }
        }
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $language_urls[$language_code] = $app->path('admin-search', array(
                'language' => $language_code,
                'q' => $q,
            ));
        }
        $this->addMarker('search');
        $this->setTitle('Search "' . $q . '"');
        return $this->render(
            $app,
            '@Admin/Admin/search.twig',
            array(
                'language_urls' => $language_urls,
                'language' => $app['i18n']->getLanguage(),
                'search_terms' => $q,
                'content_units' => $content_units,
                'pagination' => $pagination,
                'pagination_widget' => $pagination_widget,

            )
        );
    }

    public function about(Request $request, Application $app, $language)
    {
        $app['i18n']->setLanguage($language);
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $language_urls[$language_code] = $app->path('admin-about', array(
                'language' => $language_code,
            ));
        }
        $this->addMarker('about');
        $this->setTitle('About Viktor');
        return $this->render(
            $app,
            '@Admin/Admin/about.twig',
            array(
                'language_urls' => $language_urls,
                'language' => $app['i18n']->getLanguage(),
                'version' => $app::VIKTOR_VERSION,
            )
        );
    }

    public function miscIndexForm(Request $request, Application $app, $language)
    {
        $action_type = (string) $request->request->get('action_type', '');
        $redirection_url = (string) $request->request->get('redirection_url', '/');
        $possible_action_types = array('delete');
        if (!in_array($action_type, $possible_action_types)) {
            $app->abort(404);
        }
        $selected_content_units = (array) $request->request->get('selected_content_units', array());
        $app['i18n']->setLanguage($language);
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $language_urls[$language_code] = $app->path('admin-misc-index-form', array(
                'language' => $language_code,
            ));
        }
        // Prepare variables.
        $content_units = array();
        foreach ($selected_content_units as $selected_content_unit) {
            $selected_content_unit = (string) $selected_content_unit;
            $elements = explode(':', $selected_content_unit);
            if (count($elements) == 2) {
                $content_unit = $app['content_unit_factory']->load($elements[0], $elements[1]);
                if ($content_unit != null) {
                    $content_units[] = $content_unit;
                }
            }
        }
        $complete_redirection_url = $request->getScheme() . '://' . $request->getHost() . $redirection_url;
        // Go!
        return $this->render(
            $app,
            '@Admin/Admin/miscIndexForm.twig',
            array(
                'language_urls' => $language_urls,
                'language' => $app['i18n']->getLanguage(),
                'action_type' => $action_type,
                'redirection_url' => $redirection_url,
                'content_units' => $content_units,
                'complete_redirection_url' => $complete_redirection_url,
            )
        );
    }

    public function processMiscIndexForm(Request $request, Application $app, $language)
    {
        $redirection_url = (string) $request->request->get('redirection_url', '/');
        $action_type = (string) $request->request->get('action_type', '');
        $redirection_url = filter_var($redirection_url, FILTER_SANITIZE_URL);
        $selected_content_units = (array) $request->request->get('selected_content_units', array());
        $app['i18n']->setLanguage($language);
        $content_units = array();
        foreach ($selected_content_units as $selected_content_unit) {
            $selected_content_unit = (string) $selected_content_unit;
            $elements = explode(':', $selected_content_unit);
            if (count($elements) == 2) {
                $content_unit = $app['content_unit_factory']->load($elements[0], $elements[1]);
                if ($content_unit != null) {
                    $content_units[] = $content_unit;
                }
            }
        }
        // Delete.
        if ($action_type == 'delete') {
            $nb_deleted_items = 0;
            $nb_untouched_items = 0;
            foreach ($content_units as $content_unit) {
                if ($content_unit->canBeDeleted()) {
                    if ($content_unit->delete()) {
                        $nb_deleted_items++;
                    } else {
                        $nb_untouched_items++;
                    }
                } else {
                    $nb_untouched_items++;
                }
            }
            if ($nb_deleted_items > 1) {
                $app['session']->getFlashBag()->add('success', $nb_deleted_items . ' items deleted.');
            }
            if ($nb_deleted_items == 1) {
                $app['session']->getFlashBag()->add('success', $nb_deleted_items . ' item deleted.');
            }
            if ($nb_untouched_items > 0) {
                $app['session']->getFlashBag()->add('error', $nb_untouched_items . ' item(s) could not be deleted.');
            }
        }
        return $app->redirect($request->getScheme() . '://' . $request->getHost() . $redirection_url);
    }

}

<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Admin;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Twig_SimpleFunction;

class AdminTemplatingToolboxServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['admin_templating_toolbox'] = function ($app) {
            $toolbox = new AdminTemplatingToolbox();
            return $toolbox;
        };
        $add_js_function = new Twig_SimpleFunction('admin_add_js', function ($url, $attributes) use ($app) {
            return $app['admin_templating_toolbox']->addJs($url, $attributes);
        });
        $add_css_function = new Twig_SimpleFunction('admin_add_css', function ($url, $attributes) use ($app) {
            return $app['admin_templating_toolbox']->addCss($url, $attributes);
        });
        $js_tags_function = new Twig_SimpleFunction('admin_js_tags', function () use ($app) {
            return $app['admin_templating_toolbox']->jsTags();
        });
        $css_tags_function = new Twig_SimpleFunction('admin_css_tags', function () use ($app) {
            return $app['admin_templating_toolbox']->cssTags();
        });
        $app['twig']->addFunction($add_js_function);
        $app['twig']->addFunction($add_css_function);
        $app['twig']->addFunction($js_tags_function);
        $app['twig']->addFunction($css_tags_function);
    }

}

<?php

/*
 * Copyright 2020 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Admin;

use Viktor\Application;

class AdminHtmlPage
{
    private $markers = array();
    private $title = '';

    public function getMarkers()
    {
        return $this->markers;
    }

    public function addMarker($marker)
    {
        $this->markers[] = $marker;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function render(Application $app, $view, $variables)
    {
        $default_variables = array(
            'markers' => $this->markers,
            'page_title' => $this->title,
        );
        $variables = array_merge($default_variables, $variables);
        return $app['twig']->render(
            $view,
            $variables
        );
    }
}

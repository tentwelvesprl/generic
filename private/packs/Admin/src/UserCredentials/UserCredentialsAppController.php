<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\UserCredentials;

use Exception;
use Swift_Message;
use Symfony\Component\HttpFoundation\Request;
use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;
use Viktor\Application;
use Viktor\Pack\Base\User\ViktorToSymfonyUserAdapter;
use Viktor\Pack\Base\UserCredentials\PasswordHelper;

class UserCredentialsAppController
{
    public function passwordForgotten(Request $request, Application $app)
    {
        $error_messages = array();
        foreach ($app['session']->getFlashBag()->get('error', array()) as $message) {
            $error_messages[] = $message;
        }
        $success_messages = array();
        foreach ($app['session']->getFlashBag()->get('success', array()) as $message) {
            $success_messages[] = $message;
        }
        return $app['twig']->render(
            '@Admin/UserCredentials/passwordForgotten.twig',
            array(
                'error_messages' => $error_messages,
                'success_messages' => $success_messages,
            )
        );
    }

    public function check(Request $request, Application $app)
    {
        $email = (string) $request->request->get('email', '');
        try {
            // Do we have a walid e-mail address?
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                throw new Exception('Invalid e-mail address.');
            }
            // Do we have corresponding user accounts?
            $password_helper = new PasswordHelper($app);
            $user_arrays = $password_helper->findUsersByEmail($email);
            if (empty($user_arrays)) {
                throw new Exception('Unknown e-mail address.');
            }
            // Create a token for each corresponding user account.
            foreach ($user_arrays as $k => $user_array) {
                $token = $password_helper->createToken($user_array['type'], $user_array['id']);
                if ($token == null) {
                    throw new Exception('A technical error occurred. Please try again later.');
                }
                $user_arrays[$k]['token'] = $token;
                $user = $app['content_unit_factory']->load($user_array['type'], $user_array['id']);
                if (is_a($user, 'Viktor\Pack\Base\User\UserInterface')) {
                    $user_arrays[$k]['user'] = $user;
                } else {
                    throw new Exception('This user could not be loaded.');
                }
            }
            // Send a reset link for each user.
            foreach ($user_arrays as $k => $user_array) {
                $message_txt = $app['twig']->render(
                    '@Admin/UserCredentials/confirmation-email.txt.twig',
                    array(
                        'username' => $user_array['user']->getUsername(),
                        'token' => $user_array['token'],
                        'url' =>
                            $app->path(
                                'admin-password-forgotten-reset-password',
                                array('token' => $user_array['token'])
                            ),
                    )
                );
                $message_txt = wordwrap($message_txt);
                $css_to_inline_styles = new CssToInlineStyles();
                $message_html = $app['twig']->render(
                    '@Admin/UserCredentials/confirmation-email.html.twig',
                    array(
                        'username' => $user_array['user']->getUsername(),
                        'token' => $user_array['token'],
                        'url' =>
                            $app->path(
                                'admin-password-forgotten-reset-password',
                                array('token' => $user_array['token'])
                            ),
                    )
                );
                $message_html = $css_to_inline_styles->convert($message_html);
                $message = Swift_Message::newInstance()
                    ->setSubject('Password reset')
                    ->setFrom(array($app['viktor.config']['default_email_address'] => $app['viktor.config']['website_title']))
                    ->setTo(array($email))
                    ->setBody($message_txt)
                    ->addPart($message_html, 'text/html');
                $app['mailer']->send($message);
            }
            $app['session']->getFlashBag()->add('success', 'A reset link was sent to ' . $email . '.');
        } catch (Exception $e) {
            $app['session']->getFlashBag()->add('error', $e->getMessage());
        }
        return $app->redirect($app->path('admin-password-forgotten'));
    }

    public function resetPassword(Request $request, Application $app, string $token)
    {
        $error_messages = array();
        $success_messages = array();
        $username = '';
        $new_password = '';
        $new_password_to_show = '';
        $password_helper = new PasswordHelper($app);
        try {
            $user_array = $password_helper->findUserByToken($token);
            if ($user_array == null) {
                throw new Exception('This token is not valid.');
            }
            $user = $app['content_unit_factory']->load($user_array['type'], $user_array['id']);
            if (!is_a($user, 'Viktor\Pack\Base\User\UserInterface')) {
                throw new Exception('This token does not correspond to a valid user.');
            }
            $username = $user->getUsername();
            $new_password = $password_helper->generateRandomPassword();
            $symfony_user = new ViktorToSymfonyUserAdapter($user->getUsername(), '');
            $encoder = $app['security.encoder_factory']->getEncoder($symfony_user);
            $encoded_password = $encoder->encodePassword($new_password, $symfony_user->getSalt());
            $user->setPassword($encoded_password);
            $result = $user->save($user);
            if (!$result) {
                throw new Exception('Your password could not be updated.');
            }
            $password_helper->deleteToken($token);
            $new_password_to_show = $new_password;
            $success_messages[] = 'Your password has been reset.';
        } catch (Exception $e) {
            $error_messages[] = $e->getMessage();
        }
        return $app['twig']->render(
            '@Admin/UserCredentials/resetPassword.twig',
            array(
                'error_messages' => $error_messages,
                'success_messages' => $success_messages,
                'username' => $username,
                'new_password' => $new_password_to_show,
            )
        );
    }

}

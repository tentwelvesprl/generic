<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\UserCredentials;

use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Base\UserCredentials\UserCredentials;
use Viktor\Pack\Base\User\ViktorToSymfonyUserAdapter;

class UserCredentialsWidget
{
    private $userCredentials;
    private $prefix;

    public function __construct(UserCredentials $user_credentials, $prefix)
    {
        $this->userCredentials = $user_credentials;
        $this->prefix = (string) $prefix;
        return $this;
    }

    public function getUserCredentials()
    {
        return $this->userCredentials;
    }

    public function getPrefix()
    {
        return $this->prefix;
    }

    public function set(Request $request, Application $app)
    {
        $input_name = $this->getPrefix() . '_user_credentials';
        $username = $request->request->get($input_name . '_username', '');
        $email = $request->request->get($input_name . '_email', '');
        $pw1 = $request->request->get($input_name . '_pw1', '');
        $pw2 = $request->request->get($input_name . '_pw2', '');
        $roles = $request->request->get($input_name . '_roles', array());
        $this->getUserCredentials()->setUsername($username);
        $this->getUserCredentials()->setEmail($email);
        if ($this->getUserCredentials()->getContentUnit()->getId() == 0 || $pw1 != '') {
            $symfony_user = new ViktorToSymfonyUserAdapter($username, '');
            $encoder = $app['security.encoder_factory']->getEncoder($symfony_user);
            $password = $encoder->encodePassword($pw1, $symfony_user->getSalt());
            $this->getUserCredentials()->setPassword($password);
        }
        if ($this->getUserCredentials()->getContentUnit()->canHavePermissionsModified()) {
            $this->getUserCredentials()->setRoles($roles);
        }
    }

    public function storePrefilledData(Request $request, Application $app)
    {
        $input_name = $this->getPrefix() . '_user_credentials';
        $prefill_bag_name = 'prefill-' . $input_name;
        $username = $request->request->get($input_name . '_username', '');
        $email = $request->request->get($input_name . '_email', '');
        $roles = $request->request->get($input_name . '_roles', array());
        $prefill = array(
            'username' => $username,
            'email'    => $email,
            'roles'    => $roles,
        );
        $app['session']->set($prefill_bag_name, $prefill);
    }

    public function removePrefilledData(Request $request, Application $app)
    {
        $input_name = $this->getPrefix() . '_user_credentials';
        $prefill_bag_name = 'prefill-' . $input_name;
        $app['session']->remove($prefill_bag_name);
    }

    public function validate(Request $request, Application $app)
    {
        $form_is_valid = true;
        $input_name = $this->getPrefix() . '_user_credentials';
        $invalid_field_flashbag_name = 'invalid-field-' . $input_name;
        $error_flashbag_name = 'error-' . $input_name;
        $username = $request->request->get($input_name . '_username', '');
        $email = $request->request->get($input_name . '_email', '');
        $pw1 = $request->request->get($input_name . '_pw1', '');
        $pw2 = $request->request->get($input_name . '_pw2', '');
        $roles = $request->request->get($input_name . '_roles', array());
        if ($username == '') {
            $form_is_valid = false;
            $app['session']->getFlashBag()->add($invalid_field_flashbag_name, 'username');
            $app['session']->getFlashBag()->add($error_flashbag_name . '_username', 'The username cannot be empty.');
        }
        if ($email != '' && (!filter_var($email, FILTER_VALIDATE_EMAIL))) {
            $form_is_valid = false;
            $app['session']->getFlashBag()->add($invalid_field_flashbag_name, 'email');
            $app['session']->getFlashBag()->add($error_flashbag_name . '_email', 'This address is invalid.');
        }
        if ($pw1 != '' || $pw2 != '' || $this->getUserCredentials()->getContentUnit()->getId() == 0) {
            // The password fields are not empty, or a new user is about to be
            // created, thus we have to check the password validity.
            if ($pw1 != $pw2) {
                $form_is_valid = false;
                $app['session']->getFlashBag()->add($invalid_field_flashbag_name, 'pw1');
                $app['session']->getFlashBag()->add($invalid_field_flashbag_name, 'pw2');
                $app['session']->getFlashBag()->add($error_flashbag_name . '_pw1', 'The passwords don\'t match.');
            }
            if (mb_strlen($pw1) < 8) {
                $form_is_valid = false;
                $app['session']->getFlashBag()->add($invalid_field_flashbag_name, 'pw1');
                $app['session']->getFlashBag()->add($invalid_field_flashbag_name, 'pw2');
                $app['session']->getFlashBag()->add($error_flashbag_name . '_pw1', 'Minimum 8 characters.');
            }
        }
        return $form_is_valid;
    }

    public function getSortOptions()
    {
        $sort_options = array(
            'username' => 'Username',
        );
        return $sort_options;
    }

    public function usernameIsAvailable()
    {
        return $this->getUserCredentials()->usernameIsAvailable();
    }

}

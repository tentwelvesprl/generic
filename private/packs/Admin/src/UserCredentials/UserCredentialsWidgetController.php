<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\UserCredentials;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Admin\UserCredentials\UserCredentialsWidget;
use Viktor\Pack\Base\User\UserInterface;
use Viktor\Pack\Base\UserCredentials\UserCredentials;

class UserCredentialsWidgetController
{
    public function form(
        Request $request,
        Application $app,
        UserCredentials $user_credentials,
        UserCredentialsWidget $user_credentials_widget)
    {
        $input_name = $user_credentials_widget->getPrefix() . '_user_credentials';
        $invalid_field_flashbag_name = 'invalid-field-' . $input_name;
        $error_flashbag_name = 'error-' . $input_name;
        $prefill_bag_name = 'prefill-' . $input_name;
        $username = $user_credentials->getUsername();
        $email = $user_credentials->getEmail();
        $roles = $user_credentials->getRoles();
        $prefill = $app['session']->get($prefill_bag_name);
        if ($prefill !== null) {
            $username = $prefill['username'];
            $email = $prefill['email'];
            $roles = $prefill['roles'];
        }
        $user_credentials_widget->removePrefilledData($request, $app);
        return $app['twig']->render(
            '@Admin/UserCredentials/form.twig',
            array(
                'user_digest' => $user_credentials->getContentUnit()->getDigest(),
                'name' => $input_name,
                'username' => $username,
                'email' => $email,
                'roles' => $roles,
                'available_roles' => $app['viktor.config']['security_roles'],
                'invalid_field_flashbag_name' => $invalid_field_flashbag_name,
                'error_flashbag_name' => $error_flashbag_name,
            )
        );
    }

    public function advancedSearchForm(
        Request $request,
        Application $app,
        UserCredentials $user_credentials,
        UserCredentialsWidget $user_credentials_widget)
    {
        return $app['twig']->render(
            '@Admin/UserCredentials/advancedSearchForm.twig',
            array(

            )
        );
    }

}

<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Image;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Admin\Image\UploadedImageWidget;
use Viktor\Pack\Base\Image\UploadedImage;

class UploadedImageWidgetController
{
    public function form(
        Request $request,
        Application $app,
        UploadedImage $uploaded_image,
        UploadedImageWidget $uploaded_image_widget,
        $language)
    {
        $base_name = $uploaded_image_widget->getPrefix() . '_uploaded_image';
        $prefill_bag_name = 'prefill-' . $base_name;
        $prefill = $app['session']->get($prefill_bag_name);
        $uploaded_image_digest = $uploaded_image->getDigest();
        $uploaded_image_widget->removePrefilledData($request, $app);
        return $app['twig']->render(
            '@Admin/UploadedImage/form.twig',
            array(
                'base_name' => $base_name,
                'uploaded_image' => $uploaded_image_digest,
                'options' => $uploaded_image->getOptions(),
                'language' => $language,
                'content_unit_type' => $uploaded_image->getContentUnit()->getType(),
                'content_unit_id' => $uploaded_image->getContentUnit()->getId(),
            )
        );
    }

    public function advancedSearchForm(
        Request $request,
        Application $app,
        UploadedImage $uploaded_image,
        UploadedImageWidget $uploaded_image_widget)
    {
        $base_name = $uploaded_image_widget->getPrefix() . '_uploaded_image' . '_search';
        $search_data_bag_name = 'search_data_' . $base_name;
        return $app['twig']->render(
            '@Admin/UploadedImage/advancedSearchForm.twig',
            array(
                'base_name' => $base_name,
                'options' => $uploaded_image->getOptions(),
            )
        );
    }

}

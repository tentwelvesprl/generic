<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Image;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\EventListener\AbstractSessionListener;
use Viktor\Application;
use Viktor\Pack\Base\Image\SourceImage;
use Viktor\Pack\Base\Image\ImageGenerator;

class ContentUnitImageAppController
{
    public function small(Request $request, Application $app, $language, $type, $id)
    {
        $content_unit = $app['content_unit_factory']->load($type, $id);
        if ($content_unit == null) {
            $app->abort(404, 'This content unit could not be loaded.');
        }
        if (!$content_unit->canBeViewed()) {
            $app->abort(403, 'You are not allowed to view this image.');
        }
        $source_image = $content_unit->getImage();
        if ($source_image != null) {
            $response = new Response();
            $response->headers->set(AbstractSessionListener::NO_AUTO_CACHE_CONTROL_HEADER, 'true');
            $response->setPublic();
            $response->setETag($source_image->getEtag());
            if ($response->isNotModified($request)) {
                return $response;
            }
            $image_generator = new ImageGenerator($app, $source_image);
            $image_generator->addTransformation(array(
                'name' => 'fit_to_max_dimensions',
                'options' => array(
                    'width' => 160,
                    'height' => 160,
                ),
            ));
            $image_generator->setOutputFormat('image/png');
            return $image_generator->getResponse($response);
        } else {
            $app->abort(404, 'This content unit has no image.');
        }
    }

    public function medium(Request $request, Application $app, $language, $type, $id)
    {
        $content_unit = $app['content_unit_factory']->load($type, $id);
        if ($content_unit == null) {
            $app->abort(404, 'This content unit could not be loaded.');
        }
        if (!$content_unit->canBeViewed()) {
            $app->abort(403, 'You are not allowed to view this image.');
        }
        $source_image = $content_unit->getImage();
        if ($source_image != null) {
            $response = new Response();
            $response->headers->set(AbstractSessionListener::NO_AUTO_CACHE_CONTROL_HEADER, 'true');
            $response->setPublic();
            $response->setETag($source_image->getEtag());
            if ($response->isNotModified($request)) {
                return $response;
            }
            $image_generator = new ImageGenerator($app, $source_image);
            $image_generator->addTransformation(array(
                'name' => 'fit_to_max_dimensions',
                'options' => array(
                    'width' => 288,
                    'height' => 288,
                ),
            ));
            $image_generator->setOutputFormat('image/png');
            return $image_generator->getResponse($response);
        } else {
            $app->abort(404, 'This content unit has no image.');
        }
    }

    public function medium2x(Request $request, Application $app, $language, $type, $id)
    {
        $content_unit = $app['content_unit_factory']->load($type, $id);
        if ($content_unit == null) {
            $app->abort(404, 'This content unit could not be loaded.');
        }
        if (!$content_unit->canBeViewed()) {
            $app->abort(403, 'You are not allowed to view this image.');
        }
        $source_image = $content_unit->getImage();
        if ($source_image != null) {
            $response = new Response();
            $response->headers->set(AbstractSessionListener::NO_AUTO_CACHE_CONTROL_HEADER, 'true');
            $response->setPublic();
            $response->setETag($source_image->getEtag());
            if ($response->isNotModified($request)) {
                return $response;
            }
            $image_generator = new ImageGenerator($app, $source_image);
            $image_generator->addTransformation(array(
                'name' => 'fit_to_max_dimensions',
                'options' => array(
                    'width' => 576,
                    'height' => 576,
                ),
            ));
            $image_generator->setOutputFormat('image/png');
            return $image_generator->getResponse($response);
        } else {
            $app->abort(404, 'This content unit has no image.');
        }
    }

}

<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Dataset;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Base\Dataset\Dataset;

class DatasetWidget
{
    private $sections;
    private $fieldsWidgets;
    private $prefix;

    public function __construct(array $dataset_widget_description, Dataset $dataset, $prefix)
    {
        $this->prefix = (string) $prefix;
        $this->sections = $dataset_widget_description['sections'];
        foreach ($this->sections as $k => $v) {
            $this->sections[$k]['fields'] = array();
        }
        $this->fieldsWidgets = array();
        foreach ($dataset_widget_description['fields'] as $field_name => $field_description) {
            if (!isset($field_description['class_name'])) {
                throw new Exception('Class name missing for ' . $field_name . ' controller.');
            }
            if (!isset($field_description['options'])) {
                throw new Exception('Options missing for ' . $field_name . ' controller.');
            }
            $class_name = $field_description['class_name'];
            $options = $field_description['options'];
            if (!is_a($class_name, 'Viktor\Pack\Admin\Field\AbstractFieldWidget', true)) {
                throw new Exception($class_name . ' is not a dataset field widget class.');
            }
            $fields = $dataset->getFields();
            if (!isset($fields[$field_name])) {
                throw new Exception('There is no ' . $field_name . ' field in the dataset.');
            }
            $field_widget = new $class_name($options, $fields[$field_name], $this->getPrefix());
            $section_name = (string) $field_widget->getOptions()['section'];
            if (!isset($this->sections[$section_name])) {
                throw new Exception('There is no ' . $section_name . ' section in the dataset.');
            }
            $this->sections[$section_name]['fields'][] = $field_name;
            $this->fieldsWidgets[$field_name] = $field_widget;
        }
        return $this;
    }

    public function getSections()
    {
        return $this->sections;
    }

    public function getFieldsWidgets()
    {
        return $this->fieldsWidgets;
    }

    public function getPrefix()
    {
        return $this->prefix;
    }

    public function set(Request $request, Application $app)
    {
        $fields_widgets = $this->getFieldsWidgets();
        foreach ($fields_widgets as $field_widget) {
            $field_widget->set($request, $app);
        }
    }

    public function storePrefilledData(Request $request, Application $app)
    {
        $fields_widgets = $this->getFieldsWidgets();
        foreach ($fields_widgets as $field_widget) {
            $field_widget->storePrefilledData($request, $app);
        }
    }

    public function removePrefilledData(Request $request, Application $app)
    {
        $fields_widgets = $this->getFieldsWidgets();
        foreach ($fields_widgets as $field_widget) {
            $field_widget->removePrefilledData($request, $app);
        }
    }

    public function validate(Request $request, Application $app)
    {
        $is_valid = true;
        $fields_widgets = $this->getFieldsWidgets();
        foreach ($fields_widgets as $field_widget) {
            $validation_result = $field_widget->validate($request, $app);
            if (!$validation_result) {
                $is_valid = false;
            }
        }
        return $is_valid;
    }

    public function getFilters(Request $request, Application $app)
    {
        $filters = array();
        $fields_widgets = $this->getFieldsWidgets();
        foreach ($fields_widgets as $field_widget) {
            $filters = array_merge($filters, $field_widget->getFilters($request, $app));
        }
        return $filters;
    }

    public function storeAdvancedSearchData(Request $request, Application $app)
    {
        $fields_widgets = $this->getFieldsWidgets();
        foreach ($fields_widgets as $field_widget) {
            $field_widget->storeAdvancedSearchData($request, $app);
        }
    }

    public function getSortOptions()
    {
        $sort_options = array();
        $fields_widgets = $this->getFieldsWidgets();
        foreach ($fields_widgets as $field_widget) {
            $sort_options = array_merge($sort_options, $field_widget->getSortOptions());
        }
        return $sort_options;
    }

    public function getActiveFiltersDescription(Request $request, Application $app)
    {
        $active_filters_description = array();
        $fields_widgets = $this->getFieldsWidgets();
        foreach ($fields_widgets as $field_widget) {
            $active_filters_description = array_merge($active_filters_description, $field_widget->getActiveFiltersDescription($request, $app));
        }
        return $active_filters_description;
    }

}

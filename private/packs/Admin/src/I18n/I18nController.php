<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\I18n;

use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;

class I18nController
{
    public function languageSwitcher(Request $request, Application $app, array $urls, $active_language)
    {
        $items = array();
        $available_languages = $app['viktor.config']['available_languages'];
        foreach ($available_languages as $language_code => $language_name) {
            if (isset($urls[$language_code])) {
                $url = $urls[$language_code];
            } else {
                $url = '';
            }
            $items[$language_code] = array(
                'url' => $url,
                'active' => ($language_code == $active_language),
            );
        }
        return $app['twig']->render(
            '@Admin/I18n/languageSwitcher.twig',
            array(
                'items' => $items,
            )
        );
    }

    public function languageToggle(Request $request, Application $app, $active_language)
    {
        $items = array();
        $available_languages = $app['viktor.config']['available_languages'];
        foreach ($available_languages as $language_code => $language_name) {
            $items[$language_code] = array(
                'active' => ($language_code == $active_language),
            );
        }
        return $app['twig']->render(
            '@Admin/I18n/languageToggle.twig',
            array(
                'items' => $items,
            )
        );
    }

}

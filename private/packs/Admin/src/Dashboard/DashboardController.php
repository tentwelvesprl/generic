<?php
namespace Viktor\Pack\Admin\Dashboard;

use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Base\I18n\TranslatableInterface;
use Viktor\Pack\Content\Article\AbstractArticle;

class DashboardController
{
    public function recentlyModified(Request $request, Application $app)
    {
        $language = $request->query->get('language', '');
        // Get article types that can be listed.
        $article_types = array();
        foreach ($app['content_unit_factory']->getAvailableTypes() as $content_unit_type => $content_unit_class_name) {
            $content_unit_model = $app['content_unit_factory']->create($content_unit_type);
            if ($content_unit_model != null && $content_unit_model->canBeListed() && $content_unit_model instanceof AbstractArticle) {
                $article_types[] = $content_unit_model->getType();
            }
        }
        $quoted_article_types = "'" . implode("', '", $article_types) . "'";
        $last_modified_articles = array();
        $query_builder = $app['db']->createQueryBuilder();
        $query_builder
            ->select('article.id AS aid, article.type AS type, article.modification_user_type AS modification_user_type, article.modification_user_id AS modification_user_id')
            ->from('article', 'article')
            ->innerJoin('article', 'publication_index', 'pub', 'article.type = pub.type AND article.id = pub.id')
            ->where('pub.language = :language')
            ->andWhere('article.type IN (' . $quoted_article_types . ')')
            ->orderBy('article.modification_uts', 'DESC')
            ->setParameter('language', $language)
            ->setFirstResult(0)
            ->setMaxResults(10)
        ;
        $result = $query_builder->execute();
        while ($row = $result->fetch()) {
            if ($article = $app['content_unit_factory']->load($row['type'], $row['aid'])) {
                $article->setLanguage($language);
                $article_digest = $article->getDigest();
                $last_modification_user_digest = null;
                if ($last_modification_user = $app['content_unit_factory']->load($row['modification_user_type'], $row['modification_user_id'])) {
                    if ($last_modification_user instanceof TranslatableInterface) {
                        $last_modification_user->setLanguage($language);
                    }
                    $last_modification_user_digest = $last_modification_user->getDigest();
                }
                $last_modified_articles[] = array(
                    'article' => $article_digest,
                    'last_modification_user' => $last_modification_user_digest,
                );
            }
        }
        return $app->render(
            '@Admin/Dashboard/recentlyModified.twig',
            array(
                'language' => $language,
                'last_modified_articles' => $last_modified_articles,
            )
        );
    }

    public function diskUsage(Request $request, Application $app)
    {
        $language = $request->query->get('language', '');
        $si_prefix = array('B', 'kB', 'MB', 'GB', 'TB');
        $base = 1000;
        $free_space = disk_free_space('/');
        $si_class_free_space = min((int) log($free_space, $base), count($si_prefix) - 1);
        $formatted_free_space = sprintf('%1.2f' , $free_space / pow($base, $si_class_free_space)) . ' ' . $si_prefix[$si_class_free_space];
        $total_space = disk_total_space('/');
        $si_class_total_space = min((int) log($total_space, $base), count($si_prefix) - 1);
        $formatted_total_space = sprintf('%1.2f' , $total_space / pow($base, $si_class_total_space)) . ' ' . $si_prefix[$si_class_total_space];
        $percentage_used = 100 - (($free_space / $total_space) * 100);
        return $app->render(
            '@Admin/Dashboard/diskUsage.twig',
            array(
                'language' => $language,
                'formatted_free_space' => $formatted_free_space,
                'formatted_total_space' => $formatted_total_space,
                'percentage_used' => $percentage_used,
            )
        );
    }

}
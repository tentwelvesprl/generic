<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Pagination;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Base\Pagination\Pagination;

class PaginationWidget
{
    private $pagination;
    private $prefix;

    public function __construct(Pagination $pagination, $prefix)
    {
        $this->pagination = $pagination;
        $this->prefix = (string) $prefix;
        return $this;
    }

    public function getPrefix()
    {
        return $this->prefix;
    }

    public function getPagination()
    {
        return $this->pagination;
    }

    public function getExplicitQueryPaginationParameters(Request $request)
    {
        $possible_nbs_items_per_page = $this->getPagination()->getPossibleNbsItemsPerPage();
        $value = array();
        $current_page = (int) $request->query->get($this->getPrefix() . '_current_page', 0);
        if ($current_page > 0) {
            $value['current_page'] = $current_page;
        }
        $nb_items_per_page = (int) $request->query->get($this->getPrefix() . '_nb_items_per_page', 0);
        if (in_array($nb_items_per_page, $possible_nbs_items_per_page)) {
            $value['nb_items_per_page'] = $nb_items_per_page;
        }
        return $value;
    }

    public function getQueryPaginationParameters(Request $request)
    {
        $possible_nbs_items_per_page = $this->getPagination()->getPossibleNbsItemsPerPage();
        $default_value = array(
            'current_page' => 1,
            'nb_items_per_page' => $possible_nbs_items_per_page[0],
        );
        $value = array_merge($default_value, $this->getExplicitQueryPaginationParameters($request));
        return $value;
    }

    public function storePaginationParameters(Request $request, Application $app)
    {
        $bag_name = $this->getPrefix() . '_pagination';
        if ($request->query->get('reset', 0) == 1) {
            $app['session']->remove($bag_name);
        }
        $stored_value = $app['session']->get($bag_name);
        if (!is_array($stored_value)) {
            $stored_value = array();
        }
        $new_value = $this->getExplicitQueryPaginationParameters($request);
        $value = array_merge($stored_value, $new_value);
        $app['session']->set($bag_name, $value);
    }

    public function getStoredPaginationParameters(Application $app)
    {
        $possible_nbs_items_per_page = $this->getPagination()->getPossibleNbsItemsPerPage();
        $bag_name = $this->getPrefix() . '_pagination';
        $value = $app['session']->get($bag_name);
        $current_page = 1;
        $nb_items_per_page = $possible_nbs_items_per_page[0];
        if (is_array($value)) {
            if (isset($value['current_page']) && $value['current_page'] > 0) {
                $current_page = $value['current_page'];
            }
            if (isset($value['nb_items_per_page']) && in_array($value['nb_items_per_page'], $possible_nbs_items_per_page)) {
                $nb_items_per_page = $value['nb_items_per_page'];
            }
        }
        return array(
            'current_page' => $current_page,
            'nb_items_per_page' => $nb_items_per_page,
        );
    }

}

<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\EmbedInfo;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Base\DataFile\DataFile;
use Viktor\Pack\Base\EmbedInfo\EmbedInfo;

class EmbedInfoWidget
{
    private $embedInfo;
    private $prefix;

    public function __construct(EmbedInfo $embed_info, $prefix)
    {
        $this->embedInfo = $embed_info;
        $this->prefix = (string) $prefix;
        return $this;
    }

    public function getPrefix()
    {
        return $this->prefix;
    }

    public function getEmbedInfo()
    {
        return $this->embedInfo;
    }

    public function set(Request $request, Application $app)
    {
        $base_name = $this->getPrefix() . '_embed_info';
        $value = array(
            'source_url' => (string) $request->request->get($base_name . '_source_url', ''),
            'title' => (string) $request->request->get($base_name . '_title', ''),
            'description' => (string) $request->request->get($base_name . '_description', ''),
            'image_url' => (string) $request->request->get($base_name . '_image_url', ''),
            'embed_code' => (string) $request->request->get($base_name . '_embed_code', ''),
            'width' => (int) $request->request->get($base_name . '_width', 0),
            'height' => (int) $request->request->get($base_name . '_height', 0),
            'aspect_ratio' => (float) $request->request->get($base_name . '_aspect_ratio', 0.0),
            'type' => (string) $request->request->get($base_name . '_type', ''),
            'modification_ts' => (int) $request->request->get($base_name . '_modification_ts', 0),
        );
        $this->getEmbedInfo()->setValue($value);
        if (((int) $request->request->get($base_name . '_to_be_refreshed', 0)) == 1) {
            $this->getEmbedInfo()->scrapeSourceUrl();
        }
    }

    public function storePrefilledData(Request $request, Application $app)
    {
        $base_name = $this->getPrefix() . '_embed_info';
        $prefill_bag_name = 'prefill-' . $base_name;
        $value = array(
            'source_url' => (string) $request->request->get($base_name . '_source_url', ''),
            'title' => (string) $request->request->get($base_name . '_title', ''),
            'description' => (string) $request->request->get($base_name . '_description', ''),
            'image_url' => (string) $request->request->get($base_name . '_image_url', ''),
            'embed_code' => (string) $request->request->get($base_name . '_embed_code', ''),
            'width' => (int) $request->request->get($base_name . '_width', 0),
            'height' => (int) $request->request->get($base_name . '_height', 0),
            'aspect_ratio' => (float) $request->request->get($base_name . '_aspect_ratio', 0.0),
            'type' => (string) $request->request->get($base_name . '_type', ''),
            'modification_ts' => (int) $request->request->get($base_name . '_modification_ts', 0),
        );
        $app['session']->set($prefill_bag_name, $value);
    }

    public function removePrefilledData(Request $request, Application $app)
    {
        $base_name = $this->getPrefix() . '_embed_info';
        $prefill_bag_name = 'prefill-' . $base_name;
        $app['session']->remove($prefill_bag_name);
    }

    public function getFilters(Request $request, Application $app)
    {
        $filters = array();
        $base_name = $this->getPrefix() . '_embed_info' . '_search';
        $search_data_bag_name = 'search_data_' . $base_name;
        return $filters;
    }

    public function storeAdvancedSearchData(Request $request, Application $app)
    {
        $base_name = $this->getPrefix() . '_embed_info' . '_search';
        $search_data_bag_name = 'search_data_' . $base_name;
        if ($request->query->get('reset', 0) == 1) {
            $app['session']->remove($search_data_bag_name);
        }
    }

    public function getSortOptions()
    {
        $sort_options = array();
        return $sort_options;
    }

}

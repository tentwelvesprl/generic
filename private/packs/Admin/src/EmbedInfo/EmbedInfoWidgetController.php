<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\EmbedInfo;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Admin\EmbedInfo\EmbedInfoWidget;
use Viktor\Pack\Base\EmbedInfo\EmbedInfo;

class EmbedInfoWidgetController
{
    public function form(
        Request $request,
        Application $app,
        EmbedInfo $embed_info,
        EmbedInfoWidget $embed_info_widget,
        $language)
    {
        $base_name = $embed_info_widget->getPrefix() . '_embed_info';
        $prefill_bag_name = 'prefill-' . $base_name;
        $prefill = $app['session']->get($prefill_bag_name, null);
        if (is_array($prefill)) {
            $embed_info->setValue($prefill);
        }
        $embed_info_widget->removePrefilledData($request, $app);
        return $app['twig']->render(
            '@Admin/EmbedInfo/form.twig',
            array(
                'base_name' => $base_name,
                'embed_info' => $embed_info->getValue(),
                'options' => $embed_info->getOptions(),
                'language' => $language,
                'content_unit_type' => $embed_info->getContentUnit()->getType(),
                'content_unit_id' => $embed_info->getContentUnit()->getId(),
            )
        );
    }

    public function advancedSearchForm(
        Request $request,
        Application $app,
        EmbedInfo $embed_info,
        EmbedInfoWidget $embed_info_widget)
    {
        $base_name = $embed_info_widget->getPrefix() . '_embed_info' . '_search';
        $search_data_bag_name = 'search_data_' . $base_name;
        return $app['twig']->render(
            '@Admin/EmbedInfo/advancedSearchForm.twig',
            array(
                'base_name' => $base_name,
                'options' => $embed_info->getOptions(),
            )
        );
    }

}

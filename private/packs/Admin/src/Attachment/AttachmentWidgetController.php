<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Attachment;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Admin\Attachment\AttachmentWidget;
use Viktor\Pack\Base\Attachment\Attachment;

class AttachmentWidgetController
{
    public function form(
        Request $request,
        Application $app,
        Attachment $attachment,
        AttachmentWidget $attachment_widget,
        $language)
    {
        $base_name = $attachment_widget->getPrefix() . '_attachment';
        $prefill_bag_name = 'prefill-' . $base_name;
        $prefill = $app['session']->get($prefill_bag_name);
        $attachment_array = array(
            'file_path' => $attachment->getFilePath(),
        );
        $attachment_widget->removePrefilledData($request, $app);
        return $app['twig']->render(
            '@Admin/Attachment/form.twig',
            array(
                'base_name' => $base_name,
                'attachment' => $attachment_array,
                'options' => $attachment->getOptions(),
                'language' => $language,
                'content_unit_type' => $attachment->getContentUnit()->getType(),
                'content_unit_id' => $attachment->getContentUnit()->getId(),
                'file_info' => $attachment->getFileInfoDigest(),
            )
        );
    }

    public function advancedSearchForm(
        Request $request,
        Application $app,
        Attachment $attachment,
        AttachmentWidget $attachment_widget)
    {
        $base_name = $attachment_widget->getPrefix() . '_attachment' . '_search';
        $search_data_bag_name = 'search_data_' . $base_name;
        return $app['twig']->render(
            '@Admin/Attachment/advancedSearchForm.twig',
            array(
                'base_name' => $base_name,
                'options' => $attachment->getOptions(),
            )
        );
    }

}

<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Attachment;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Base\DataFile\DataFile;
use Viktor\Pack\Base\Attachment\Attachment;

class AttachmentWidget
{
    private $attachment;
    private $prefix;

    public function __construct(Attachment $attachment, $prefix)
    {
        $this->attachment = $attachment;
        $this->prefix = (string) $prefix;
        return $this;
    }

    public function getPrefix()
    {
        return $this->prefix;
    }

    public function getAttachment()
    {
        return $this->attachment;
    }

    public function set(Request $request, Application $app)
    {
        $base_name = $this->getPrefix() . '_attachment';
        $uploaded_file = $request->files->get($base_name);
        if ($uploaded_file != null) {
            try {
                $data_file = new DataFile($app);
                $data_file->createFromUploadedFile($uploaded_file);
                $this->attachment->setFilePath($data_file->getPath());
            } catch (Exception $e) {
                // Silently fail...
            }
        }
    }

    public function storePrefilledData(Request $request, Application $app)
    {
        $base_name = $this->getPrefix() . '_attachment';
        $prefill_bag_name = 'prefill-' . $base_name;
        $value = '';
        $app['session']->set($prefill_bag_name, $value);
    }

    public function removePrefilledData(Request $request, Application $app)
    {
        $base_name = $this->getPrefix() . '_attachment';
        $prefill_bag_name = 'prefill-' . $base_name;
        $app['session']->remove($prefill_bag_name);
    }

    public function getFilters(Request $request, Application $app)
    {
        $filters = array();
        $base_name = $this->getPrefix() . '_attachment' . '_search';
        $search_data_bag_name = 'search_data_' . $base_name;
        return $filters;
    }

    public function storeAdvancedSearchData(Request $request, Application $app)
    {
        $base_name = $this->getPrefix() . '_attachment' . '_search';
        $search_data_bag_name = 'search_data_' . $base_name;
        if ($request->query->get('reset', 0) == 1) {
            $app['session']->remove($search_data_bag_name);
        }
    }

    public function getSortOptions()
    {
        $sort_options = array();
        return $sort_options;
    }

}

<?php

/*
 * Copyright 2018, 2023 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\User;

use Exception;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface as SymfonyUserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Viktor\Application;
use Viktor\Pack\Base\User\UnknownUser;
use Viktor\Pack\Base\User\ViktorToSymfonyUserAdapter;

class SymfonyUserProvider implements UserProviderInterface
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
        return $this;
    }

    public function loadUserByUsername($username)
    {
        $symfony_user = new ViktorToSymfonyUserAdapter('unknown', '');
        $result = $this->app['db']->executeQuery(
            'SELECT id, type FROM user_credentials WHERE username = ?',
            array($username)
        );
        if (!$row = $result->fetch()) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
        }
        $new_user = $this->app['content_unit_factory']->create($row['type']);
        if (!is_a($new_user, 'Viktor\Pack\Base\User\UserInterface')) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not correspond to a user.', $username));
        }
        if (!$new_user->load($row['id'])) {
            throw new UsernameNotFoundException(sprintf('User "%s" could not be loaded.', $username));
        }
        $symfony_user->setUser($new_user);
        return $symfony_user;
    }

    public function refreshUser(SymfonyUserInterface $symfony_user)
    {
        try {
            if (!$symfony_user instanceof ViktorToSymfonyUserAdapter) {
                throw new UnsupportedUserException(sprintf('Instances of %s are not supported.', get_class($symfony_user)));
            }
            $new_user = $this->app['content_unit_factory']->load($symfony_user->getUserType(), $symfony_user->getUserId());
            if (!is_a($new_user, 'Viktor\Pack\Base\User\UserInterface')) {
                throw new UsernameNotFoundException('Current user could not be loaded.');
            }
            if ($new_user->getId() == 0) {
                throw new UsernameNotFoundException('Current user could not be loaded.');
            }
        } catch (Exception $e) {
            $new_user = new UnknownUser($this->app);
        }
        $new_symfony_user = new ViktorToSymfonyUserAdapter($new_user->getUsername(), $new_user->getPassword());
        $new_symfony_user->setUser($new_user);
        return $new_symfony_user;
    }

    public function supportsClass($class)
    {
        return $class === 'Viktor\Pack\Base\User\ViktorToSymfonyUserAdapter';
    }

    public function getApp()
    {
        return $this->app;
    }

}

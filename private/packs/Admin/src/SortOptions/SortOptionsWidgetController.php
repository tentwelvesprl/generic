<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\SortOptions;

use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Admin\SortOptions\SortOptions\Widget;

class SortOptionsWidgetController
{
    public function sortOptionsSelector(
        Request $request,
        Application $app,
        SortOptionsWidget $sort_options_widget)
    {
        $base_name = $sort_options_widget->getPrefix() . '_sort_option';
        $sort_options = $sort_options_widget->getSortOptions();
        $selected_sort_option = $sort_options_widget->getSelectedSortOption($request, $app);
        $options = array();
        foreach ($sort_options as $sort_option => $sort_option_label) {
            $options[] = array(
                'sort_option' => $sort_option,
                'label' => $sort_option_label,
                'selected' => ($sort_option == $selected_sort_option['option']),
            );
        }
        $directions = array(
            array(
                'sort_direction' => 'ASC',
                'label' => 'Ascending (A → Z, 0 → 9)',
                'selected' => ($selected_sort_option['direction'] == 'ASC'),
            ),
            array(
                'sort_direction' => 'DESC',
                'label' => 'Descending (Z → A, 9 → 0)',
                'selected' => ($selected_sort_option['direction'] == 'DESC'),
            ),
        );
        return $app['twig']->render(
            '@Admin/SortOptions/sortOptionsSelector.twig',
            array(
                'form_url' => $sort_options_widget->getContentUnitModel()->getListPageUrl(),
                'base_name' => $base_name,
                'options' => $options,
                'directions' => $directions,
            )
        );
    }

}

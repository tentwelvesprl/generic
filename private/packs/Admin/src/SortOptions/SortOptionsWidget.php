<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\SortOptions;

use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Base\ContentUnit\ContentUnitInterface;

class SortOptionsWidget
{
    private $sortOptions;
    private $contentUnitModel;
    private $prefix;

    public function __construct(array $sort_options, ContentUnitInterface $content_unit_model, $prefix)
    {
        $this->sortOptions = $sort_options;
        $this->contentUnitModel = $content_unit_model;
        $this->prefix = (string) $prefix;
        return $this;
    }

    public function getSortOptions()
    {
        return $this->sortOptions;
    }

    public function getContentUnitModel()
    {
        return $this->contentUnitModel;
    }

    public function getPrefix()
    {
        return $this->prefix;
    }

    public function storeSelectedSortOption(Request $request, Application $app)
    {
        $base_name = $this->getPrefix() . '_sort_option';
        $bag_name = 'selected_sort_option_' . $base_name;
        if ($request->query->get('reset', 0) == 1) {
            $app['session']->remove($bag_name);
        }
        if ($request->query->has($base_name . '_option')) {
            $option = (string) $request->query->get($base_name . '_option', '');
            $direction = (string) $request->query->get($base_name . '_direction', '');
            if ($direction != 'DESC') {
                $direction = 'ASC';
            }
            $value = array(
                'option' => $option,
                'direction' => $direction,
            );
            $app['session']->set($bag_name, $value);
        }
    }

    public function getSelectedSortOption(Request $request, Application $app)
    {
        $default_value = array(
            'option' => '',
            'direction' => 'ASC',
        );
        $base_name = $this->getPrefix() . '_sort_option';
        $bag_name = 'selected_sort_option_' . $base_name;
        $sort_option = $app['session']->get($bag_name, '');
        if (is_array($sort_option)) {
            $sort_option = array_merge($default_value, $sort_option);
        } else {
            $sort_option = $default_value;
        }
        return $sort_option;
    }

}

<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Authentication;

use Exception;
use Monolog\Logger as Monolog_Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\HttpUtils;
use Symfony\Component\Security\Http\Firewall\UsernamePasswordFormAuthenticationListener as BaseListener;
use Symfony\Component\Security\Http\Session\SessionAuthenticationStrategyInterface;
use Viktor\Application;
use Viktor\Pack\Base\Flood\FloodHandler;

class UsernamePasswordFormAuthenticationListener extends BaseListener
{
    private $floodHandler;
    private $monolog;

    public function __construct(TokenStorageInterface $tokenStorage, AuthenticationManagerInterface $authenticationManager, SessionAuthenticationStrategyInterface $sessionStrategy, HttpUtils $httpUtils, $providerKey, AuthenticationSuccessHandlerInterface $successHandler, AuthenticationFailureHandlerInterface $failureHandler, array $options = array(), LoggerInterface $logger = null, EventDispatcherInterface $dispatcher = null, CsrfTokenManagerInterface $csrfTokenManager = null)
    {
        if (isset($options['flood_handler']) && ($options['flood_handler'] instanceof FloodHandler)) {
            $this->floodHandler = $options['flood_handler'];
        } else {
            throw new Exception('Invalid flood handler.');
        }
        if (isset($options['monolog']) && ($options['monolog'] instanceof Monolog_Logger)) {
            $this->monolog = $options['monolog'];
        } else {
            throw new Exception('Invalid Monolog logger instance.');
        }
        return parent::__construct($tokenStorage, $authenticationManager, $sessionStrategy, $httpUtils, $providerKey, $successHandler, $failureHandler, $options, $logger, $dispatcher, $csrfTokenManager);
    }

    public function getFloodHandler()
    {
        return $this->floodHandler;
    }

    public function getMonolog()
    {
        return $this->monolog;
    }

    protected function attemptAuthentication(Request $request)
    {
        $username = (string) $request->request->get('_username', '');
        $this->getFloodHandler()->setLastAttemptUsername($username);
        if (!$this->getFloodHandler()->isAllowed('authentication_failure', 5)) {
            $this->getMonolog()->warning(
                'Authentication failure: maximum number of attempts exceeded.',
                array('visitor_identifier' => $this->getFloodHandler()->getVisitorIdentifier())
            );
            throw new AuthenticationException('Maximum number of attempts exceeded.');
        }
        return parent::attemptAuthentication($request);
    }

}

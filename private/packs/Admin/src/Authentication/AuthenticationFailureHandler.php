<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Authentication;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationFailureHandler;
use Viktor\Application;

class AuthenticationFailureHandler extends DefaultAuthenticationFailureHandler
{
    private $app;

    public function __construct(Application $app, array $options = array(), LoggerInterface $logger = null)
    {
        $this->app = $app;
        return parent::__construct($app['kernel'], $app['security.http_utils'], $options, $logger);
    }

    public function getApp()
    {
        return $this->app;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $this->getApp()['flood']->register('authentication_failure');
        $this->getApp()['monolog']->warning('Failed authentication attempt.', array('exception' => $exception));
        return parent::onAuthenticationFailure($request, $exception);
    }

}

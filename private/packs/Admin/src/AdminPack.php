<?php

/*
 * Copyright 2018, 2023 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin;

use Doctrine\DBAL\Schema\Table;
use Silex\Provider\SecurityServiceProvider;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Yaml\Parser;
use Viktor\Application;
use Viktor\PackInterface;
use Viktor\Pack\Admin\Admin\AdminTemplatingToolboxServiceProvider;
use Viktor\Pack\Admin\Authentication\AuthenticationFailureHandler;
use Viktor\Pack\Admin\Authentication\AuthenticationSuccessHandler;
use Viktor\Pack\Admin\Security\GeneralFirewallRequestMatcher;
use Viktor\Pack\Admin\User\SymfonyUserProvider;
use Viktor\Pack\Base\User\UnknownUser;

class AdminPack implements PackInterface
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
        return $this;
    }

    public function getName()
    {
        return 'Admin';
    }

    public function getPath()
    {
        return realpath(__DIR__ . '/..');
    }

    public function install() {
        $app = $this->app;
        // Copy the static assets into the public folder.
        $this->copyRecursively($this->getPath() . '/assets', $this->app['viktor.web_path'] . '/assets/Admin');
        return;
    }

    public function init()
    {
        $app = $this->app;
        // Register content unit types.
        $this->app['content_unit_factory']->registerType('Viktor\Pack\Admin\User\BasicUser');
        // Define the security firewall.
        // Controllers behind the general firewall can be defined using their
        // class name or a complete controller reference (class::method).
        $this->app['viktor.controllers_behind_general_firewall'] = array(
            'Viktor\Pack\Admin\Admin\AdminAppController',
            'Viktor\Pack\Admin\Admin\AdminElementsController',
            'Viktor\Pack\Admin\Attachment\AttachmentWidgetController',
            'Viktor\Pack\Admin\Dataset\DatasetWidgetController',
            'Viktor\Pack\Admin\EmbedInfo\EmbedInfoWidgetController',
            'Viktor\Pack\Admin\Field\ContentUnitReferencesFieldAppController',
            'Viktor\Pack\Admin\Field\FieldWidgetController',
            'Viktor\Pack\Admin\Field\FileFieldAppController',
            'Viktor\Pack\Admin\Field\ImageFieldAppController',
            'Viktor\Pack\Admin\Field\MediaFieldAppController',
            'Viktor\Pack\Admin\I18n\I18nController',
            'Viktor\Pack\Admin\Image\ContentUnitImageAppController',
            'Viktor\Pack\Admin\Image\UploadedImageAppController',
            'Viktor\Pack\Admin\Image\UploadedImageWidgetController',
            'Viktor\Pack\Admin\Pagination\PaginationWidgetController',
            'Viktor\Pack\Admin\Publication\PublicationAppController',
            'Viktor\Pack\Admin\Publication\PublicationWidgetController',
            'Viktor\Pack\Admin\SortOptions\SortOptionsWidgetController',
            'Viktor\Pack\Admin\UserCredentials\UserCredentialsWidgetController',
            'Viktor\Pack\Base\Attachment\AttachmentAppController',
            'Viktor\Pack\Base\Image\ImageAppController',
        );
        $security_role_hierarchy = array();
        foreach ($this->app['viktor.config']['security_roles'] as $security_role => $security_role_description) {
            $security_role_hierarchy[$security_role] = $security_role_description['hierarchy'];
        }
        $minimal_security_role_hierarchy = array(
            'ROLE_USER' => array(),
            'ROLE_AUDITOR' => array(),
            'ROLE_WEBMASTER' => array('ROLE_AUDITOR'),
            'ROLE_ADMIN' => array('ROLE_WEBMASTER'),
        );
        $security_role_hierarchy = array_merge($minimal_security_role_hierarchy, $security_role_hierarchy);
        $this->app->register(new SecurityServiceProvider(), array(
            'security.firewalls' => array(
                'general' => array(
                    'pattern'   => new GeneralFirewallRequestMatcher($app),
                    'anonymous' => true,
                    'form' => array(
                        'login_path' => '/login',
                        'check_path' => '/login_check',
                        'listener_class' => 'Viktor\\Pack\\Admin\\Authentication\\UsernamePasswordFormAuthenticationListener',
                        'flood_handler' => $app['flood'],
                        'monolog' => $app['monolog'],
                    ),
                    'logout' => array(
                        'logout_path' => '/logout',
                    ),
                    'users' => function () use ($app) {
                        if (!isset($this->app['viktor.user_provider'])) {
                            $this->app['viktor.user_provider'] = new SymfonyUserProvider($app);
                        }
                        return $this->app['viktor.user_provider'];
                    },
                )
            ),
            'security.role_hierarchy' => $security_role_hierarchy,
        ));
        $this->app['security.access_rules'] = array(
            array('^/admin', 'ROLE_AUDITOR')
        );
        $this->app['security.authentication.failure_handler.general'] = function () use ($app) {
            return new AuthenticationFailureHandler($app);
        };
        $this->app['security.authentication.success_handler.general'] = function () use ($app) {
            $handler = new AuthenticationSuccessHandler($app);
            $handler->setProviderKey('general');
            return $handler;
        };
        $this->app['viktor.user'] = $this->app->factory(function ($app) {
            $symfony_user = $app['user'];
            if ($symfony_user === null) {
                return new UnknownUser($app);
            }
            $viktor_user = $app['content_unit_factory']->load(
                $symfony_user->getUserType(),
                $symfony_user->getUserId()
            );
            if (is_a($viktor_user, 'Viktor\Pack\Base\User\UserInterface')) {
                return $viktor_user;
            } else {
                return new UnknownUser($app);
            }
        });
        // Other services.
        $this->app->register(new AdminTemplatingToolboxServiceProvider());
        // Twig configuration.
        $this->app['twig.loader.filesystem']->addPath(__DIR__ . '/../views', 'Admin');
        return $this;
    }

    private function copyRecursively($source, $dest)
    {
        if (is_file($source)) {
            return copy($source, $dest);
        }
        if (!is_dir($dest)) {
            mkdir($dest);
        }
        $dir = dir($source);
        while (($entry = $dir->read()) !== false) {
            if ($entry[0] == '.') {
                continue;
            }
            if ($dest !== "$source/$entry") {
                $this->copyRecursively("$source/$entry", "$dest/$entry");
            }
        }
        $dir->close();
        return true;
    }

    public function addConsoleCommands() {
    }

}

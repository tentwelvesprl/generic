<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Admin\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;
use Viktor\Application;

class GeneralFirewallRequestMatcher implements RequestMatcherInterface
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function matches(Request $request)
    {
        // Check path of the authentication form.
        // It must be behind the firewall.
        if ($request->getPathInfo() == '/login_check') {
            return true;
        }
        // Same for the logout controller.
        if ($request->getPathInfo() == '/logout') {
            return true;
        }
        $controllers_behind_general_firewall = array_merge(
            $this->app['viktor.controllers_behind_general_firewall'],
            $this->app['viktor.config']['additional_controllers_behind_general_firewall']
        );
        $controller = $request->attributes->get('_controller', '');
        // TODO: fix this ugly hack.
        if (gettype($controller) != 'string') {
            $controller = '';
        }
        $controller_elements = explode('::', $controller);
        $controller_class = $controller_elements[0];
        if (in_array($controller_class, $controllers_behind_general_firewall)) {
            return true;
        }
        if (in_array($controller, $controllers_behind_general_firewall)) {
            return true;
        }
        return false;
    }

}

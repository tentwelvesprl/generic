- [Sign in](#sign-in)
  - [Viktor toolbar](#viktor-toolbar)

# Sign in {#sign-in}

Your CMS back-office is accessible by adding **/admin** after your website's base URL.
Enter your username and password to access the back-office.

You can recover your password by clicking on **Forgot your password?** and follow the instructions.

## Viktor toolbar {#viktor-toolbar}

Once you are logged in on Viktor, a toolbar a **log out** button will appear on the bottom of the web page as you browse the website. If you have the rights to modify the page you're visiting, click on the **edit this page** button to be redirected to its edition page in the back-office.
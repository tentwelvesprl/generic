- [Users](#users)
  - [Roles](#roles)
  - [Credentials](#credentials)

# Users {#users}

All the users that have access to the CMS are listed here.

We recommend to create **a personal user for each editor of the website** instead of having multiple people sharing the same account.

## Roles {#roles}

Each user has an attributed **role**, that defines what they can or cannot do in the CMS. Viktor has 3 types of roles by default:

- **Administrator**: can list, view, create, edit and delete any content in the CMS, *including* other user's information and credentials.
- **Webmaster**: can list, view, create, edit and delete any content in the CMS, *excluding* other user's information and credentials.
- **Auditor**: can only list and view the content, but cannot create, edit or delete any of it, including their own user credentials.

Roles can be modified and new roles can be created upon request.

To **deactivate a user**, it's sufficient to uncheck all the roles boxes. Deleting the user, on the contrary, would mean losing information about this user's past actions in the CMS.

## Credentials {#credentials}

Each user has a username (that must be unique), an email address, and a password.

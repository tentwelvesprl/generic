- [Libraries](#libraries)
  - [Images](#images)
  - [Media](#media)
  - [Files](#files)

# Libraries {#libraries}

The Libraries contain all the images, media and files that are uploaded to the CMS.

Just like the Articles, the listed image, media and file items are sortable in alphabetical order, by creation or last modification date, and are searchable thanks to a **Refine list** search. Each item will show you the id, title, characteristics of the item (e.g.: the size of an image), a preview image and a Quick options dropdown menu.

In addition to all the above, the items in the Libraries also show the **number of references**, that is, the number of times that specific item is used in the CMS.

If an item has *0 references*, it means that it's just uploaded in the CMS, but no content unit actually uses it, so if you delete or edit this item, this won't have any repercussion any other part of your website.

If an item has *1 or more references*, it means that 1 or more content units make use of it, so if you delete or edit this item, the changes will be reflected everywhere it's used.

You can also **sort** your items by number of references. This way, you can quickly realize if you have unused multimedia material you can delete and optimize your storage space.

## Images {#images}

By default, Viktor allows to upload images with a maximum size of 4000 x 3000 px.

We recommend to use exclusively .png and .jpg files.

You can create a **New image** directly in the Library or open an image precedently uploaded.

In the editing page of your image, you will find:

- a **preview** of your image.
- a **download** button.
- an **upload/replace file** input: use this to change the file behind this image item, while keeping all its characteristics (id, references, etc.).
- the **image information**: a list of customizable text fields and options for describing the image: adding captions, credits, alternative text for accessibility, etc.
- the **pivot**: a *crosshair* icon placed by default at the center of the image. Move the pivot and save to make the CMS recenter the image around the point of your choice. This comes in handy when the website design requires to resize the image (e.g. to make the image fit in a small box, or a portrait-oriented photo must fit in a horizontal box).

## Media {#media}

You can create a **New media** directly in the Library or open a media precedently uploaded.

If you're creating a new media, paste the URL of your media in **Source url** and check the **Scrape on next save** box, then click save. The remaining fields under **Embed info** (title, description, image url, etc.) will automatically populate.

If you edit your media (for example, changing the URL), don't forget to check the box to make sure that your media is correctly updated. Any changes you do to these fields will be overwritten with the information coming from the streaming platform.

Under **Media information**, you'll find additional fields you can edit, like caption and credits of your video.

<blockquote>Viktor is optimized for displaying content from YouTube, Vimeo, Spotify, Soundcloud or any platforms that correctly implements the latest version of [oEmbed](https://oembed.com/), which allows to directly embed a content on an external website without having to manually copy and paste the HTML code.

If you add media coming from a platform that doesn't respect the above requirement or if you encounter an error, then you will have to **copy the embed code** and paste it in the embed code field.</blockquote>

## Files {#files}

You can create a **New file** directly in the Library or open a file precedently uploaded.

In the editing page of your file, you will find:

- a **download** button.
- an **upload/replace file** input: use this to change the file behind this item, while keeping all its characteristics (id, references, etc.).
- the **file information**. By default it contains a field *title*. If you don't specify a title, the filename will be used.

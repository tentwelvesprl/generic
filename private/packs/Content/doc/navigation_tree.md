- [Navigation tree](#navigation-tree)
  - [Create a menu item](#create-a-menu-item)
  - [Create the structure of your tree](#create-the-structure-of-your-tree)

# Navigation tree {#navigation-tree}

In the **Navigation** section of the CMS you'll find a new type of element, called **tree**, that allows you to create **hierarchical structures** of items. Each item can be nested under another.

Trees can be used to create menus and submenus and lists of categories. 

## Create a menu item {#create-a-menu-item}

Create a new item by clicking on the *plus sign* on the top left.

Type the **title** of the item.

To choose the **destination** of your item link, by default you have 2 options:

- **Linked content unit**: link a preexisting article or create a new article.
- **Url**: enter the URL of a page. 
    If the page is part of your website, in which case you only need to write the **subdirectory** of your page, e.g.: if your URL is https://my-website.com/en/about-us, you only need to write "/en/about-us.
    If the page is on an external website, please enter the full URL, e.g.: https://other-organization-website.com/en/about-us.

The page linked to your menu item will by default open in the current tab. If your URL points to an external website, please choose **New tab** under **Open in**, so that the visitor doesn't "lose" your website while browsing external resources.

## Create the structure of your tree {#create-the-structure-of-your-tree}

Once you have created some elements, you can nest them to create the desired structure.

To move and reorder the elements, use the **Drag and drop** function (the *three lines* on the left side of the item).

When you nest an element (called **child**) under another (the **parent** element), the latter displays a *chevron-down* symbol and it becomes collapsable (you can open and close it like a section, showing or hiding the children).

Example: under "Navigation", you create a first item called "Main menu", which will represent the root of your menu, then nest two other items "About us" and "Contact" underneath the first; you'll obtain a menu with two links, "About us" and "Contact". If you create additional elements "Our organization" and "Team", then nest them under "About us", you will obtain a submenu.

When you perform a reordering of elements, don't forget to click on **Save structure** on the top-right of the screen.

If you want to create an element already nested under another element, you can click on the *plus sign* located on the right end of the element.



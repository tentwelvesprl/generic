- [Dashboard](#dashboard)
  - [CMS language choices](#cms-language-choices)
  - [Recently modified articles](#recently-modified-articles)
  - [User's profile button](#users-profile-button)
  - [General search engine](#general-search-engine)
  - [Content types menu](#content-types-menu)

# Dashboard {#dashboard}

The version by default of your CMS dashboard contains:

- The link to the website's homepage
- [The CMS language choices](#cms-language-choices)
- [The last 10 modified articles](#recently-modified-articles)
- [The user profile button](#users-profile-button)
- [The general search engine](#general-search-engine)
- [The content types menu](#content-types-menu)


The dashboard can be customized with additional widgets upon request.

## CMS language choices {#cms-language-choices}

Viktor allows to create multilingual websites. Therefore, you can create and view content in multiple languages.

The main CMS interface is in English, but you can specify in which language you want to see your content first while using the back-office. Click on one of the languages displayed on the top-left side of the screen, next to the
*flag* symbol.

E.g.: if your website has an English and a French version, you can choose to see first the French version of any text you write, and to land on the French page preview of the article you're writing.

## Recently modified articles {#recently-modified-articles}

In this table, you'll find the last 10 modified articles, with the following details:

- Its content type.
- Its id.
- Its title. Click on the link to be redirected to the edition page of the article.
- The last modification date.
- The last user who modified the article.

## User's profile button {#users-profile-button}

Here you can have access to your user profile and you can log out of Viktor.

## General search engine {#general-search-engine}

You can search among all the content types by clicking on the *lens* symbol.

A specific search option by type is available inside each content type section.

## Content types menu {#content-types-menu}

All the available content types are listed on the column on the left side of the screen. They are divided in sections that are customizable upon request; the default classification is:

- **Website content**: contains the content types that constitute the foundation your website, e.g. static pages, news, homepage, banners.
- **Navigation**: contains your menu and submenu tree(s).
- **Collections**: contains all the images, media and files that are uploaded to the CMS.
- **Users**: contains all the registered users.
- [Articles](#articles)
  - [Listed articles](#listed-articles)
    - [New](#new)
    - [Sorting options](#sorting-options)
    - [Refine list](#refine-list)
    - [Listed items](#listed-items)
    - [Preview image](#preview-image)
    - [Publication status](#publication-status)
    - [Quick options](#quick-options)
  - [Edition page](#edition-page)
    - [Article information](#article-information)
    - [Content sections](#content-sections)
      - [Item and SEO fields](#item-and-seo-fields)
    - [Usage](#usage)

# Articles {#articles}

The most widely used type of content in Viktor is the article. Articles can contain text, images, media, files, embeds of other articles, and can be used to create web pages with a specific URL. Common articles examples are homepages, news articles, static pages.

In this section, you'll find information about the default options common to all article types you'll find in Viktor.

Click on the content type on the menu on the left side of the screen to view the list of existing articles of that type.

## Listed articles {#listed-articles}

### New {#new}

Next to the content type name, you'll find a button **New [content type name]** to create a new instance of that content type.

### Sorting options {#sorting-options}

You can sort your articles by **title**, **creation time** or **modification time**, or in **alphabetic order** (ascending or descending). Additional specific sorting options can be implemented upon request.

### Refine list {#refine-list}

Click on **Refine list...** to search by publication status or by title in the list of articles of the same type. Other fields can be added upon request.

### Listed items {#listed-items}

Each listed article shows the following info: id, title, preview image (if applicable), publication status, quick options.

### Preview image {#preview-image}

When an image of the article is available, it is displayed as a thumbnail. For more information on how the preview image is chosen, check the **Item and SEO fields** section.

### Publication status {#publication-status}

On the right side of each listed article you'll find its publication status (when applicable) in the form of **one or more circles** depending on the number of languages your website is available in.
If e.g. your website languages are English and French, each article will show 2 circles, one of which will contain the **default language** you chose for the CMS.

These circles can be of different colors:
- **Red**: the article is not published in that language (it is not visible to the website visitors and users who don't have the permissions to view unpublished content).
- **Green**: the article is published (it is visible to anyone who browses the website).
- **Orange**: the article is scheduled for publication at a future date (it will become published on a date and time set in the future).

### Quick options {#quick-options}

A *down arrow sign* on the left of a listed article allows you to choose among a few actions:
- **Edit**: opens the edition page of the article.
- **Preview**: shows the article rendered as a web page.
- **Duplicate**: makes an identical, unpublished copy of the article.
- **Delete**: deletes the article.

## Edition page {#edition-page}

Once you enter the article's edition page, you'll find a **main area** with the article's **content sections** and an **aside** with information and options for the article.

### Article information {#article-information}

The aside contains information already available on the list as title, preview image, preview link, as well as new options:
- **Language selector**: the available languages for this article are displayed as buttons. Select or deselect them to display fields in one language or another.
- **Publication widget**: allows you to set a publication status, date and time of publication.
- **Creation and last modification info** (date, time and user).
- **Save** button.

### Content sections {#content-sections}

Sections are entirely customizable dropdown blocks that contain all your article's **fields**. By default, there is a *main content* section, followed by collapsable sections. Among the most common sections you'll find **related content**, where you can link other articles, **item**, **seo** and **usage**.

#### Item and SEO fields {#item-and-seo-fields}

Viktor is conceived to always try to have a *default content* to serve whenever necessary. An example of this can be an *image* that represents the article: one single image can be used in the header of a web page, as a thumbnail preview, and in the meta-description of the page indexed by search engines like Google.

If you only upload one image in e.g. the "main image" field of your article, the same image will be used everywhere. But if your article contains an item and/or a SEO section, you can also define a different image for each purpose.

- **Item**: by "item" we mean when the article is displayed in a smaller, limited form, e.g. when it appears as a "card" on a big index page. The item section usually contains an **item description** and an **item image**. If no SEO fields are filled, the content of this section will be used by search engines and for social media sharing.
- **SEO**: the content is specifically chosen for being used by search engines and for social media sharing.

|  | item | SEO |
|--|------|-----|
| title | N/A | replaces the article title in the `<title></title>` tag and on search engines |
| description | used in item cards and in the meta-descriptions | replaces the item description in the meta-description and when sharing the content on social media |
| image | used in item cards, in the meta-descriptions and as preview thumbnail | replaces the item image in the meta-descriptions and when sharing the content on social media |

### Usage {#usage}

Every time your article is used somewhere in another content unit (e.g.: as related content of another article, in a menu item...), a usage section will appear at the bottom of the section list. The number of content units that use the article are indicated, and you can click on the listed articles to access them directly.

If an article isn't used anywhere, it means that if you delete, edit or unpublish it, no visible change will happen on your website. Conversely, any actions taken on an article that is in use will affect the content unit that contains it.

[def]: #content-sections
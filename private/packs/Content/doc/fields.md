- [Fields](#fields)
  - [Translatable and non translatable fields](#translatable-and-non-translatable-fields)
  - [Simple text field](#simple-text-field)
  - [Rich Text field](#rich-text-field)
    - [Hyperlinks](#hyperlinks)
    - [Inline and block styles](#inline-and-block-styles)
  - [Image field](#image-field)
  - [Media field](#media-field)
  - [File field](#file-field)
  - [Other fields](#other-fields)

# Fields {#fields}

The following paragraph list the general characteristics of the main field types you'll find in Viktor. Keep in mind that custom fields and options can make the fields look different depending on the content type/project.

Fields are often accompanied by a **legend** that explains what the field's purpose is or gives details about the type of content accepted.

## Translatable and non translatable fields {#translatable-and-non-translatable-fields}

Some fields are **translatable**, meaning they allow you to encode different content for each language version on the website. E.g. the title of a page can be written in French or English, the image in a logo can have a Flemish slogan to be used on the Flemish version on the website, and a French slogan on the French version.

The translatable fields will show the language symbol next to their title.

By default, Viktor will display the fields in the default language chosen for the CMS, and all the non-translatable fields. By clicking on the language buttons in the aside on the top right of your article, you can display fields in another language, or multiple languages at the same time.

## Simple text field {#simple-text-field}

A simple text input: you cannot intervene of the size or aspect of the text, nor add html text. It can be single- or multiline and/or accept markdown language.

## Rich Text field {#rich-text-field}

A rich text editor (CKEditor) that allows you to input HTML content including advanced styling options: headings, bullet points, hyperlinks...

Each block of text in the editor displays its associated HTML tag, so that you can always know what styling is given to your text as you type. You can also visualize the actuale HTML code by clicking on *Source*.

The available options are highly customizable and allow you a certain freedom of composing your text, while remaining within the limits of the design choices established for your website. E.g.: normally, only 2 or 3 heading options are available, users cannot modify font sizes, emphasis options are limited to bold and italics, etc.

### Hyperlinks {#hyperlinks}

You can enter a hyperlink by clicking on *Link* and specify if it should open on a new tab under *target*.

### Inline and block styles {#inline-and-block-styles}

The editor offers you a predetermined set of styles that allow you to create **block-level** and **inline-level** elements. Note that according to the design of your website, both options could be available for styling an element. E.g.: for including quotes in your text, you might have a inline-level *Quote* style (in the *Styles* dropdown), which applies the quote style to the text, and a block-level *Quote* button, that surrounds the text with `<blockquote></blockquote>` tags.

## Image field {#image-field}

This field allows you to upload images from your computer or the Internet to your article.

A legend gives information about the recommended characteristics of the image (extension, width/height, quality). Note that by default, the maximum size for an image is 4000 x 3000 px.

Use **Drag files here or browse to upload** to add a new image from your computer, or **Add from url** if the image is online. The image will be stored in the Images **collection**, where you will be able to edit its details.

If the image you want to use was previously stored in the CMS, then you can just use it in the article by searching it in **Add from library**. You can search an image by its title or ID.

We encourage reusing the same image whenever possible, to save storage space, time and improving the consistency of your content (cfr. Image section).

When applicable, you'll be able to upload multiple images to create a gallery; you can reorder the images thanks to the **drag and drop** function (the three vertical lines on the side of the image item).

## Media field {#media-field}

This field allows you to add media content coming from a streaming platform like Vimeo, Youtube or Spotify.

You just need to enter the URL of your media in the **Add media from url** input. The media will be stored in the Media **collection**, where you will be able to edit its details.

If the media you want to use was previously stored in the CMS, then you can just use it in the article by searching it in **Add from library**. You can search a media by its title or ID.

If you want to upload a video file coming from your computer, please use the **file field**.

We encourage reusing the same media whenever possible, to save time and improving the consistency of your content (cfr. Media section).

When applicable, you'll be able to upload multiple media to create a gallery; you can reorder the media thanks to the **drag and drop** function (the three vertical lines on the side of the media item).

## File field {#file-field}

This field allows you to upload files (PDFs, small mp3...) from your computer to your article.

Use **Drag files here or browse to upload** to add a new file from your computer. The file will be stored in the Files **collection**, where you will be able to edit its details.

If the file you want to use was previously stored in the CMS, then you can just use it in the article by searching it in **Add from library**. You can search a file by its title or ID.

We encourage reusing the same file whenever possible, to save storage space, time and improving the consistency of your content (cfr. Files section).

When applicable, you'll be able to upload multiple files to create a gallery; you can reorder the files thanks to the **drag and drop** function (the three vertical lines on the side of the file item).

## Other fields {#other-fields}

Among the other fields you can find in Viktor, the following are the most common:

- **List** fields like checkboxes and radio buttons.
- **Linked content unit** field: allows you to link a content already created in Viktor, or to create a new content of that type which will already be linked to the article you're editing.
- **Date and time** field: the date is in `mm/dd/yyyy` format.
- **Content box**: a field that allows you to compose your page by mixing text, images, media and linked content units and reorder them thanks to the drag & drop function.
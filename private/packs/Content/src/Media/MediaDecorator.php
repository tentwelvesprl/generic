<?php

/*
 * Copyright 2021 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Content\Media;

use Viktor\Pack\Base\DbStore\AbstractDbStoreDecorator;
use Viktor\Pack\Base\DbStore\DbStoreComponentInterface;
use Viktor\Pack\Base\User\UserInterface;

class MediaDecorator extends AbstractDbStoreDecorator
{
    public function __construct(DbStoreComponentInterface $component)
    {
        parent::__construct($component);
    }

    public function getIdsQueryBuilder(array $filters = array(), array $sort_options = array())
    {
        $query_builder = $this->component->getIdsQueryBuilder($filters, $sort_options);
        foreach ($sort_options as $sort_option) {
            if (!is_array($sort_option)) {
                continue;
            }
            $option = (string) $sort_option['option'];
            $direction = (string) $sort_option['direction'];
            if ($option == 'media_number_of_references') {
                $query_builder
                    ->innerJoin(
                        'component',
                        'media_reference_count', 'media_reference_count',
                        'component.type = media_reference_count.rtype AND component.id = media_reference_count.rid'
                    )
                ;
                $query_builder->addOrderBy('media_reference_count.reference_count', $direction);
            }
        }
        return $query_builder;
    }

}

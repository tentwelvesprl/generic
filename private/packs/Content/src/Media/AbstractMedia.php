<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Content\Media;

use DateTime;
use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Base\Badge\Badge;
use Viktor\Pack\Base\FullTextIndex\FullTextIndexer;
use Viktor\Pack\Base\FullTextIndex\Tokenizer;
use Viktor\Pack\Base\EmbedInfo\HasAnEmbedInfoInterface;
use Viktor\Pack\Base\EmbedInfo\EmbedInfo;
use Viktor\Pack\Base\EmbedInfo\EmbedInfoDecorator;
use Viktor\Pack\Base\Image\SourceImage;
use Viktor\Pack\Base\User\UserInterface;
use Viktor\Pack\Content\ContentUnit\AbstractContentUnit;

abstract class AbstractMedia extends AbstractContentUnit implements HasAnEmbedInfoInterface
{
    private $embedInfo;

    public function addToIndex(FullTextIndexer $indexer)
    {
        foreach ($this->getApp()['viktor.config']['available_languages'] as $language => $language_name) {
            $this->setLanguage($language);
            $tokenizer = new Tokenizer(trim($this->getEmbedInfo()->getValue()['title']), $language);
            $tokens = $tokenizer->getTokens();
            $indexer->addTokens($tokens, 10, 'general_' . $language);
            $indexer->addTokens($tokens, 100, 'media_' . $language);
            $tokenizer = new Tokenizer(trim($this->getEmbedInfo()->getValue()['description']), $language);
            $tokens = $tokenizer->getTokens();
            $indexer->addTokens($tokens, 1, 'general_' . $language);
            $indexer->addTokens($tokens, 1, 'media_' . $language);
        }
        parent::addToIndex($indexer);
    }

    public function getTableName()
    {
        return 'media';
    }

    public function prepareNewInstance()
    {
        $this->embedInfo = new EmbedInfo($this->getApp(), $this, $this->getEmbedInfoOptions());
        $this->dbStoreComponent = new EmbedInfoDecorator($this->dbStoreComponent, $this->embedInfo);
        $this->dbStoreComponent = new MediaDecorator($this->dbStoreComponent);
    }

    public function getDigest()
    {
        $digest = parent::getDigest();
        $digest['_duplication_page_url'] = $this->getDuplicationPageUrl();
        $digest['_embed_info'] = $this->getEmbedInfo()->getValue();
        return $digest;
    }

    public function getDuplicationPageUrl()
    {
        return $this->getApp()->path('admin-media-duplication-form', array(
            'language' => $this->getLanguage(),
            'type'     => $this->getType(),
            'id'       => $this->getId(),
        ));
    }

    public function getTitle()
    {
        $title = trim($this->getEmbedInfo()->getValue()['title']);
        if ($title == '') {
            $title = parent::getTitle();
        }
        return $title;
    }

    public function getHint()
    {
        return $this->getEmbedInfo()->getValue()['source_url'];
    }

    public function getDescription()
    {
        $description = trim($this->getEmbedInfo()->getValue()['description']);
        if (strlen($description) > 160) {
            $description = mb_substr($description, 0, 156) . ' (…)';
        }
        return $description;
    }

    public function getImage()
    {
        $image_url = trim($this->getEmbedInfo()->getValue()['image_url']);
        $etag = md5(serialize($this->getModificationDateTime()));
        if ($image_url != '') {
            $source_image = new SourceImage($image_url);
            $source_image->setEtag($etag);
            return $source_image;
        } else {
            return null;
        }
    }

    public function getDatasetCombinedDescription()
    {
        $sections = array();
        $fields = array();
        return array(
            'sections' => $sections,
            'fields' => $fields
        );
    }

    public function getEmbedInfoOptions()
    {
        $options = array();
        return $options;
    }

    public function getEmbedInfo()
    {
        return $this->embedInfo;
    }

    public function renderListItem(Request $request, Application $app)
    {
        $referencing_content_units_array = $this->getDbStoreComponent()->getReferencingContentUnits();
        $referencing_content_units_count = count($referencing_content_units_array);
        $badge_digest = array();
        if ($badge_items = $this->getBadge()) {
            if ($badge_items instanceof Badge) {
                $badge_digest[] = $badge_items->getDigest();
            } elseif (is_array($badge_items)) {
                foreach ($badge_items as $badge_item) {
                    $badge_digest[] = $badge_item->getDigest();
                }
            }
        }
        return $app['twig']->render(
            '@Content/Media/listItem.twig',
            array(
                'media_digest' => $this->getDigest(),
                'language' => $this->getLanguage(),
                'referencing_content_units' => $referencing_content_units_array,
                'referencing_content_units_count' => $referencing_content_units_count,
                'badge' => $badge_digest,
            )
        );
    }

    public function getAdditionalEditionFormHeaderView()
    {
        return '';
    }

    public function getAdditionalEditionFormFooterView()
    {
        return '';
    }

    public function getAdditionalEditionFormAsideView()
    {
        return '';
    }

    public function duplicate()
    {
        $new_media = parent::duplicate();
        $new_media->getEmbedInfo()->copy($this->getEmbedInfo());
        return $new_media;
    }

}

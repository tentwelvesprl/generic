<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Content\Media;

use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Admin\Admin\AdminHtmlPage;
use Viktor\Pack\Admin\Dataset\DatasetWidget;
use Viktor\Pack\Admin\DbStore\DbStoreComponentWidget;
use Viktor\Pack\Admin\EmbedInfo\EmbedInfoWidget;
use Viktor\Pack\Admin\Pagination\PaginationWidget;
use Viktor\Pack\Admin\SortOptions\SortOptionsWidget;
use Viktor\Pack\Base\I18n\TranslatableInterface;
use Viktor\Pack\Base\Pagination\Pagination;
use Viktor\Pack\Base\User\UnknownUser;

class MediaAppController extends AdminHtmlPage
{
    public function index(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        $media_model = $app['content_unit_factory']->create($type);
        if (!is_a($media_model, 'Viktor\Pack\Content\Media\AbstractMedia')) {
            $app->abort(404, $type . ' is not a valid media type.');
        }
        $media_model->setLanguage($app['i18n']->getLanguage());
        if (!$media_model->canBeListed()) {
            $app->abort(403, 'You are not allowed to list these elements.');
        }
        // Prepare the widgets.
        $db_store_component_widget = new DbStoreComponentWidget(
            $media_model->getDbStoreComponent(),
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        $embed_info_widget = new EmbedInfoWidget(
            $media_model->getEmbedInfo(),
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        $media_widget = new MediaWidget(
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        $dataset_widget = new DatasetWidget(
            $media_model->getDatasetWidgetDescription($language),
            $media_model->getDataset(),
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        $sort_options = array();
        $sort_options = array_merge($sort_options, $dataset_widget->getSortOptions());
        $sort_options = array_merge($sort_options, $embed_info_widget->getSortOptions());
        $sort_options = array_merge($sort_options, $media_widget->getSortOptions());
        $sort_options = array_merge($sort_options, $db_store_component_widget->getSortOptions());
        $sort_options_widget = new SortOptionsWidget(
            $sort_options,
            $media_model,
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        // Store the advanced search form data in session.
        $embed_info_widget->storeAdvancedSearchData($request, $app);
        $media_widget->storeAdvancedSearchData($request, $app);
        $dataset_widget->storeAdvancedSearchData($request, $app);
        // Filters.
        $filters = array();
        $filters = array_merge($filters, $embed_info_widget->getFilters($request, $app));
        $filters = array_merge($filters, $dataset_widget->getFilters($request, $app));
        $filters[] = array(
            'type' => 'deduplicate',
            'value' => true,
        );
        // Sort options.
        $sort_options_widget->storeSelectedSortOption($request, $app);
        // Go!
        $medias_ids = $media_model->getIds(
            $filters,
            array($sort_options_widget->getSelectedSortOption($request, $app))
        );
        // Apply pagination.
        $pagination = new Pagination(
            $request,
            $app,
            count($medias_ids),
            20,
            1,
            array(20, 50, 100),
            $type . '_' . $app['i18n']->getLanguage()
        );
        $pagination_widget = new PaginationWidget(
            $pagination,
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        $pagination_widget->storePaginationParameters($request, $app);
        $pagination_parameters = $pagination_widget->getStoredPaginationParameters($app);
        $pagination->setCurrentPage($pagination_parameters['current_page']);
        $pagination->setNbItemsPerPage($pagination_parameters['nb_items_per_page']);
        $medias_ids_to_display = array_slice(
            $medias_ids,
            ($pagination_parameters['current_page'] - 1) * $pagination_parameters['nb_items_per_page'],
            $pagination_parameters['nb_items_per_page']
        );
        // Load medias.
        $medias_list = $app['content_unit_factory']->createList($type, $medias_ids_to_display);
        foreach ($medias_list as $media) {
            $media->setLanguage($app['i18n']->getLanguage());
        }
        // Active filters description.
        $active_filters_description = array();
        $active_filters_description = array_merge($active_filters_description, $dataset_widget->getActiveFiltersDescription($request, $app));
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $media_model->setLanguage($language_code);
            $language_urls[$language_code] = $media_model->getListPageUrl();
        }
        $media_model->setLanguage($app['i18n']->getLanguage());
        $this->addMarker('media-content-unit');
        $this->addMarker($media_model->getType());
        $this->setTitle(ucfirst($media_model->getPluralHumanType()));
        return $this->render(
            $app,
            '@Content/Media/index.twig',
            array(
                'media_model' => $media_model->getDigest(),
                'medias' => $medias_list,
                'embed_info' => $media_model->getEmbedInfo(),
                'embed_info_widget' => $embed_info_widget,
                'dataset' => $media_model->getDataset(),
                'dataset_widget' => $dataset_widget,
                'pagination' => $pagination,
                'pagination_widget' => $pagination_widget,
                'sort_options_widget' => $sort_options_widget,
                'active_filters_description' => $active_filters_description,
                'language_urls' => $language_urls,
                'language' => $app['i18n']->getLanguage(),
            )
        );
    }

    public function editionForm(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        $id = $request->query->get('id', 0);
        $media = $app['content_unit_factory']->create($type);
        if (!is_a($media, 'Viktor\Pack\Content\Media\AbstractMedia')) {
            $app->abort(404, 'Invalid media type.');
        }
        $media->setLanguage($app['i18n']->getLanguage());
        if ($id > 0) {
            // Existing media.
            $media->load($id);
            if ($media->getId() == 0) {
                $app->abort(404, 'Invalid media id.');
            }
            if (!$media->canBeViewed()) {
                $app->abort(403, 'You are not allowed to view this media.');
            }
        } else {
            // New media.
            if (!$media->canBeEdited()) {
                $app->abort(403, 'You are not allowed to edit this media.');
            }
        }
        $creation_user = $app['content_unit_factory']->load(
            $media->getCreationUserType(),
            $media->getCreationUserId()
        );
        if ($creation_user == null) {
            $creation_user = new UnknownUser($app);
        }
        if ($creation_user instanceof TranslatableInterface) {
            $creation_user->setLanguage($app['i18n']->getLanguage());
        }
        $modification_user = $app['content_unit_factory']->load(
            $media->getModificationUserType(),
            $media->getModificationUserId()
        );
        if ($modification_user == null) {
            $modification_user = new UnknownUser($app);
        }
        if ($modification_user instanceof TranslatableInterface) {
            $modification_user->setLanguage($app['i18n']->getLanguage());
        }
        $embed_info_widget = new EmbedInfoWidget(
            $media->getEmbedInfo(),
            'media' // Prefix for the fields names.
        );
        $dataset_widget = new DatasetWidget(
            $media->getDatasetWidgetDescription(),
            $media->getDataset(),
            'media' // Prefix for the fields names.
        );
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $media->setLanguage($language_code);
            if ($media->getId() == 0) {
                $language_urls[$language_code] = $media->getCreationPageUrl();
            } else {
                $language_urls[$language_code] = $media->getEditionPageUrl();
            }
        }
        // Content units that have a link to this content unit.
        $referencing_content_units_array = $media->getDbStoreComponent()->getReferencingContentUnits();
        $referencing_content_units_count = count($referencing_content_units_array);
        $referencing_content_units = array();
        foreach ($referencing_content_units_array as $v) {
            if (count($referencing_content_units_array) >= 20) {
                break;
            }
            $referencing_content_unit = $app['content_unit_factory']->load($v['type'], $v['id']);
            if ($referencing_content_unit != null) {
                $referencing_content_units[] = $referencing_content_unit;
            }
        }
        $media->setLanguage($app['i18n']->getLanguage());
        $this->addMarker('media-content-unit');
        $this->addMarker($media->getType());
        $this->addMarker($media->getType() . ':' . $media->getId());
        $this->setTitle($media->getTitle());
        return $this->render(
            $app,
            '@Content/Media/editionForm.twig',
            array(
                'media_digest' => $media->getDigest(),
                'dataset' => $media->getDataset(),
                'dataset_widget' => $dataset_widget,
                'embed_info' => $media->getEmbedInfo(),
                'embed_info_widget' => $embed_info_widget,
                'creation_user' => $creation_user->getDigest(),
                'modification_user' => $modification_user->getDigest(),
                'language_urls' => $language_urls,
                'language' => $app['i18n']->getLanguage(),
                'referencing_content_units' => $referencing_content_units,
                'referencing_content_units_count' => $referencing_content_units_count,
                'additional_edition_form_header_view' => $media->getAdditionalEditionFormHeaderView(),
                'additional_edition_form_footer_view' => $media->getAdditionalEditionFormFooterView(),
                'additional_edition_form_aside_view' => $media->getAdditionalEditionFormAsideView(),
            )
        );
    }

    public function editionFormProcess(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        // Check the object validity and permissions.
        $id = $request->request->get('id', 0);
        $media = $app['content_unit_factory']->create($type);
        if (!is_a($media, 'Viktor\Pack\Content\Media\AbstractMedia')) {
            $app->abort(404, 'Invalid media type.');
        }
        $media->setLanguage($app['i18n']->getLanguage());
        if ($id > 0) {
            // Existing media.
            $media->load($id);
            if ($media->getId() == 0) {
                $app->abort(404, 'Invalid media id.');
            }
            if (!$media->canBeEdited()) {
                $app->abort(403, 'You are not allowed to edit this media.');
            }
            $form_url = $media->getEditionPageUrl();
        } else {
            // New media.
            if (!$media->canBeCreated()) {
                $app->abort(403, 'You are not allowed to create this media.');
            }
            $form_url = $media->getCreationPageUrl();
        }
        // Prepare the form.
        $embed_info_widget = new EmbedInfoWidget(
            $media->getEmbedInfo(),
            'media' // Prefix for the fields names.
        );
        $dataset_widget = new DatasetWidget(
            $media->getDatasetWidgetDescription(),
            $media->getDataset(),
            'media' // Prefix for the fields names.
        );
        // Store the form data in session.
        $embed_info_widget->storePrefilledData($request, $app);
        $dataset_widget->storePrefilledData($request, $app);
        // Set the new values.
        $dataset_widget->set($request, $app);
        // Validate the form.
        $form_is_valid = true;
        $fields_widgets = $dataset_widget->getFieldsWidgets();
        foreach ($fields_widgets as $field_widget) {
            $validation_result = $field_widget->validate($request, $app);
            if (!$validation_result) {
                $form_is_valid = false;
            }
        }
        if (!$form_is_valid) {
            $app['session']->getFlashBag()->add('error', 'Some fields are invalid.');
            return $app->redirect($form_url);
        }
        // Set the uploaded media content (may copy a file the data dir).
        $embed_info_widget->set($request, $app);
        // Save the media.
        if ($media->save($app['viktor.user'])) {
            $app['session']->getFlashBag()->add(
                'success',
                ucfirst($media->getSingularHumanType()) . ' ' . $media->getTitle() . ' saved successfully.'
            );
        } else {
            $app['session']->getFlashBag()->add('error', 'The ' . $media->getSingularHumanType() . ' could not be saved.');
        }
        // Remove the form data stored in session.
        $embed_info_widget->removePrefilledData($request, $app);
        $dataset_widget->removePrefilledData($request, $app);
        // Everything is ok, back to the form.
        return $app->redirect($media->getEditionPageUrl());
    }

    public function deletionForm(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        // Check the object validity and permissions.
        $id = $request->query->get('id', 0);
        $media = $app['content_unit_factory']->load($type, $id);
        if (!is_a($media, 'Viktor\Pack\Content\Media\AbstractMedia')) {
            $app->abort(404, 'Could not load this media.');
        }
        $media->setLanguage($app['i18n']->getLanguage());
        if (!$media->canBeDeleted()) {
            $app->abort(403, 'You are not allowed to delete this media.');
        }
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $media->setLanguage($language_code);
            $language_urls[$language_code] = $media->getDeletionPageUrl();
        }
        // Content units that have a link to this content unit.
        $referencing_content_units_array = $media->getDbStoreComponent()->getReferencingContentUnits();
        $referencing_content_units_count = count($referencing_content_units_array);
        $referencing_content_units = array();
        foreach ($referencing_content_units_array as $v) {
            if (count($referencing_content_units_array) >= 20) {
                break;
            }
            $referencing_content_unit = $app['content_unit_factory']->load($v['type'], $v['id']);
            if ($referencing_content_unit != null) {
                $referencing_content_units[] = $referencing_content_unit;
            }
        }
        $media->setLanguage($app['i18n']->getLanguage());
        $this->addMarker('media-content-unit');
        $this->addMarker($media->getType());
        $this->addMarker($media->getType() . ':' . $media->getId());
        $this->setTitle('Delete "' . $media->getTitle() . '"?');
        return $this->render(
            $app,
            '@Content/Media/deletionForm.twig',
            array(
                'media_digest' => $media->getDigest(),
                'language_urls' => $language_urls,
                'language' => $app['i18n']->getLanguage(),
                'referencing_content_units' => $referencing_content_units,
                'referencing_content_units_count' => $referencing_content_units_count,
            )
        );
    }

    public function deletionFormProcess(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        // Check the object validity and permissions.
        $id = $request->request->get('id', 0);
        $media = $app['content_unit_factory']->load($type, $id);
        if (!is_a($media, 'Viktor\Pack\Content\Media\AbstractMedia')) {
            $app->abort(404, 'Could not load this media.');
        }
        $media->setLanguage($app['i18n']->getLanguage());
        if (!$media->canBeDeleted()) {
            $app->abort(403, 'You are not allowed to delete this media.');
        }
        $media_type = $media->getType();
        $media_title = $media->getTitle();
        $media_id = $media->getId();
        $media_index_url = $media->getListPageUrl();
        if ($media->delete()) {
            $app['session']->getFlashBag()->add(
                'success',
                $media_title . ' (' . $media_type . ' #' . $media_id . ') has been deleted.'
            );
        } else {
            $app['session']->getFlashBag()->add(
                'error',
                $media_title . ' (' . $media_type . ' #' . $media_id . ') could not be deleted.'
            );
        }
        return $app->redirect($media_index_url);
    }

    public function duplicationForm(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        // Check the object validity and permissions.
        $id = $request->query->get('id', 0);
        $media = $app['content_unit_factory']->load($type, $id);
        if (!is_a($media, 'Viktor\Pack\Content\Media\AbstractMedia')) {
            $app->abort(404, 'Could not load this media.');
        }
        $media->setLanguage($app['i18n']->getLanguage());
        if (!$media->canBeViewed() || !$media->canBeCreated()) {
            $app->abort(403, 'You are not allowed to duplicate this media.');
        }
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $media->setLanguage($language_code);
            $language_urls[$language_code] = $media->getDuplicationPageUrl();
        }
        // Content units that have a link to this content unit.
        $referencing_content_units_array = $media->getDbStoreComponent()->getReferencingContentUnits();
        $referencing_content_units_count = count($referencing_content_units_array);
        $referencing_content_units = array();
        foreach ($referencing_content_units_array as $v) {
            if (count($referencing_content_units_array) >= 20) {
                break;
            }
            $referencing_content_unit = $app['content_unit_factory']->load($v['type'], $v['id']);
            if ($referencing_content_unit != null) {
                $referencing_content_units[] = $referencing_content_unit;
            }
        }
        $media->setLanguage($app['i18n']->getLanguage());
        $this->addMarker('media-content-unit');
        $this->addMarker($media->getType());
        $this->addMarker($media->getType() . ':' . $media->getId());
        $this->setTitle('Duplicate "' . $media->getTitle() . '"?');
        return $this->render(
            $app,
            '@Content/Media/duplicationForm.twig',
            array(
                'media_digest' => $media->getDigest(),
                'language_urls'  => $language_urls,
                'language'       => $app['i18n']->getLanguage(),
                'referencing_content_units' => $referencing_content_units,
                'referencing_content_units_count' => $referencing_content_units_count,
            )
        );
    }

    public function duplicationFormProcess(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        // Check the object validity and permissions.
        $id = $request->request->get('id', 0);
        $media = $app['content_unit_factory']->load($type, $id);
        if (!is_a($media, 'Viktor\Pack\Content\Media\AbstractMedia')) {
            $app->abort(404, 'Could not load this media.');
        }
        $media->setLanguage($app['i18n']->getLanguage());
        if (!$media->canBeViewed() || !$media->canBeCreated()) {
            $app->abort(403, 'You are not allowed to duplicate this media.');
        }
        $new_media = $media->duplicate();
        if ($new_media->save($app['viktor.user'])) {
            $app['session']->getFlashBag()->add(
                'success',
                ucfirst($media->getSingularHumanType()) . ' ' . $media->getTitle() . ' duplicated successfully.'
            );
        } else {
            $app['session']->getFlashBag()->add('error', 'The ' . $media->getSingularHumanType() . ' could not be duplicated.');
        }
        return $app->redirect($media->getListPageUrl());
    }

}

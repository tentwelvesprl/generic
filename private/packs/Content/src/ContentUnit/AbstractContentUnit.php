<?php

/*
 * Copyright 2024 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Content\ContentUnit;

use Exception;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Throwable;
use Viktor\Application;
use Viktor\Pack\Base\Badge\HasABadgeInterface;
use Viktor\Pack\Base\ContentUnit\ContentUnitInterface;
use Viktor\Pack\Base\Dataset\HasADatasetInterface;
use Viktor\Pack\Base\DbStore\DbStoreComponent;
use Viktor\Pack\Base\DbStore\HasADbStoreComponentInterface;
use Viktor\Pack\Base\FullTextIndex\FullTextIndexableInterface;
use Viktor\Pack\Base\FullTextIndex\FullTextIndexer;
use Viktor\Pack\Base\Helper\StringHelper;
use Viktor\Pack\Base\I18n\LanguageSelector;
use Viktor\Pack\Base\I18n\TranslatableInterface;
use Viktor\Pack\Base\User\UserInterface;
use Viktor\Pack\Content\Dataset\DatasetHelper;
use Viktor\Pack\Content\Dataset\TranslatedDataset;
use Viktor\Pack\Content\Dataset\TranslatedDatasetDecorator;
use Viktor\Pack\Content\ContentUnit\HasSeoPropertiesInterface;

abstract class AbstractContentUnit implements
    ContentUnitInterface,
    FullTextIndexableInterface,
    HasABadgeInterface,
    HasADatasetInterface,
    HasADbStoreComponentInterface,
    HasSeoPropertiesInterface,
    TranslatableInterface
{    
    private $app;
    protected $datasetHelper;
    protected $dataset;
    protected $dbStoreComponent;
    protected $languageSelector;
    private $smartUrlTemporaryId = '';

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->datasetHelper = new DatasetHelper($app, $this->getDatasetCombinedDescription());
        $this->dataset = new TranslatedDataset($app, $this, $this->getDatasetDescription());
        $this->languageSelector = new LanguageSelector($this->app);
        $this->dbStoreComponent = new DbStoreComponent($app, $this->getTableName(), $this->getType());
        $this->dbStoreComponent = new TranslatedDatasetDecorator($this->dbStoreComponent, $this->dataset);
        $this->prepareNewInstance();
        $this->setLanguage($app['i18n']->getLanguage());
        return;
    }

    public function load($id)
    {
        return $this->dbStoreComponent->load($id);
    }

    public function save(UserInterface $user)
    {
        $this->getApp()['db']->beginTransaction();
        $this->preSave();
        $result = $this->dbStoreComponent->save($user);
        $this->postSave();
        if ($result) {
            $this->getApp()['db']->commit();
        } else {
            $this->getApp()['db']->rollBack();
        }
        $indexer = new FullTextIndexer($this->getApp(), $this->getType(), $this->getId());
        $indexer->addToIndexationQueue('update');
        return $result;
    }

    public function delete()
    {
        $this->getApp()['db']->beginTransaction();
        $this->preDelete();
        $result = $this->dbStoreComponent->delete();
        if ($result) {
            $this->getApp()['db']->commit();
        } else {
            $this->getApp()['db']->rollBack();
        }
        $indexer = new FullTextIndexer($this->getApp(), $this->getType(), $this->getId());
        $indexer->addToIndexationQueue('delete');
        return $result;
    }

    abstract public function getType();

    public function getId()
    {
        return $this->dbStoreComponent->getId();
    }

    public function getDigest()
    {
        $digest = $this->dbStoreComponent->getDigest();
        $digest['_language'] = $this->languageSelector->getLanguage();
        $digest['type'] = $this->getType();
        $digest['id'] = $this->getId();
        $digest['singular_human_type'] = $this->getSingularHumanType();
        $digest['plural_human_type'] = $this->getPluralHumanType();
        $digest['_title'] = $this->getTitle();
        $digest['_html_title'] = $this->getHtmlTitle();
        $digest['_hint'] = $this->getHint();
        $digest['_description'] = $this->getDescription();
        $digest['_html_description'] = $this->getHtmlDescription();
        $digest['_image'] = $this->getImage();
        $digest['_image_reference'] = $this->getImageReference();
        $digest['_can_be_created'] = $this->canBeCreated();
        $digest['_can_be_viewed'] = $this->canBeViewed();
        $digest['_can_be_edited'] = $this->canBeEdited();
        $digest['_can_be_deleted'] = $this->canBeDeleted();
        $digest['_can_be_listed'] = $this->canBeListed();
        $digest['_creation_page_url'] = $this->getCreationPageUrl();
        $digest['_view_page_url'] = $this->getViewPageUrl();
        $digest['_edition_page_url'] = $this->getEditionPageUrl();
        $digest['_deletion_page_url'] = $this->getDeletionPageUrl();
        $digest['_list_page_url'] = $this->getListPageUrl();
        $digest['_seo_title'] = $this->getSeoTitle();
        $digest['_seo_html_title'] = $this->getSeoHtmlTitle();
        $digest['_seo_description'] = $this->getSeoDescription();
        $digest['_seo_html_description'] = $this->getSeoHtmlDescription();
        $digest['_seo_image_reference'] = $this->getSeoImageReference();
        return $digest;
    }

    public function getIds(array $filters = array(), array $sort_options = array())
    {
        return $this->dbStoreComponent->getIds($filters, $sort_options);
    }

    public function getSingularHumanType()
    {
        return get_class() . ' content unit';
    }

    public function getPluralHumanType()
    {
        return get_class() . ' content units';
    }

    public function canBeCreated()
    {
        if ($this->getApp()['security.token_storage']->getToken() == null) {
            return false;
        }
        return $this->getApp()['security.authorization_checker']->isGranted('ROLE_WEBMASTER');
    }

    public function canBeViewed()
    {
        return true;
    }

    public function canBeEdited()
    {
        if ($this->getApp()['security.token_storage']->getToken() == null) {
            return false;
        }
        return $this->getApp()['security.authorization_checker']->isGranted('ROLE_WEBMASTER');
    }

    public function canBeDeleted()
    {
        if ($this->getApp()['security.token_storage']->getToken() == null) {
            return false;
        }
        return $this->getApp()['security.authorization_checker']->isGranted('ROLE_WEBMASTER');
    }

    public function canBeListed()
    {
        if ($this->getApp()['security.token_storage']->getToken() == null) {
            return false;
        }
        return $this->getApp()['security.authorization_checker']->isGranted('ROLE_AUDITOR');
    }

    public function getCreationPageUrl()
    {
        return $this->getApp()->path('admin-' . $this->getTableName() . '-edition-form', array(
            'language' => $this->getLanguage(),
            'type'     => $this->getType(),
            'id'       => 0,
        ));
    }

    public function getViewPageUrl()
    {
        $url = '';
        $fields = $this->getDataset()->getFields();
        if (isset($fields['smart_url_' . $this->getLanguage()])) {
            $url = trim($fields['smart_url_' . $this->getLanguage()]->get());
            if ($url != '') {
                $url = $this->getApp()['request_stack']->getMasterRequest()->getSchemeAndHttpHost() . $this->getApp()['request_stack']->getMasterRequest()->getBasePath() . $url;
            }

        }
        if ($url != '') {
            return $url;
        }
        try {
            $url = $this->getBaseViewPageUrl($this->getLanguage());
        } catch (RouteNotFoundException $e) {
            // Nothing
        }
        return $url;
    }

    public function getEditionPageUrl()
    {
        return $this->getApp()->path('admin-' . $this->getTableName() . '-edition-form', array(
            'language' => $this->getLanguage(),
            'type'     => $this->getType(),
            'id'       => $this->getId(),
        ));
    }

    public function getDeletionPageUrl()
    {
        return $this->getApp()->path('admin-' . $this->getTableName() . '-deletion-form', array(
            'language' => $this->getLanguage(),
            'type'     => $this->getType(),
            'id'       => $this->getId(),
        ));
    }

    public function getListPageUrl()
    {
        return $this->getApp()->path('admin-' . $this->getTableName() . '-index', array(
            'language' => $this->getLanguage(),
            'type'     => $this->getType(),
        ));
    }

    public function getTitle()
    {
        return ucfirst($this->getSingularHumanType()) . ' #' . $this->getId();
    }

    public function getHtmlTitle()
    {
        return htmlspecialchars($this->getTitle());
    }

    public function getHint()
    {
        return '';
    }

    public function getDescription()
    {
        return '';
    }

    public function getHtmlDescription()
    {
        return htmlspecialchars($this->getDescription());
    }

    public function getImage()
    {
        return null;
    }

    public function getImageReference()
    {
        return null;
    }

    public function getCreationDateTime()
    {
        return $this->dbStoreComponent->getCreationDateTime();
    }

    public function getModificationDateTime()
    {
        return $this->dbStoreComponent->getModificationDateTime();
    }

    public function addToIndex(FullTextIndexer $indexer)
    {
        $this->getDataset()->addToIndex($indexer);
    }

    public function getBadge()
    {
        return null;
    }

    public function getDataset()
    {
        return $this->dataset;
    }

    public function getDbStoreComponent()
    {
        return $this->dbStoreComponent;
    }

    public function getSeoTitle()
    {
        $fields = $this->getDataset()->getFields();
        if (!isset($fields['seo_title_' . $this->getLanguage()])) {
            return $this->getTitle();
        }
        return $fields['seo_title_' . $this->getLanguage()]->getPlainTextRepresentation();
    }

    public function getSeoHtmlTitle()
    {
        $fields = $this->getDataset()->getFields();
        if (!isset($fields['seo_title_' . $this->getLanguage()])) {
            return $this->getHtmlTitle();
        }
        return htmlspecialchars($fields['seo_title_' . $this->getLanguage()]->getPlainTextRepresentation());
    }

    public function getSeoDescription()
    {
        $fields = $this->getDataset()->getFields();
        if (!isset($fields['seo_description_' . $this->getLanguage()])) {
            return $this->getDescription();
        }
        return $fields['seo_description_' . $this->getLanguage()]->getPlainTextRepresentation();
    }

    public function getSeoHtmlDescription()
    {
        $fields = $this->getDataset()->getFields();
        if (!isset($fields['seo_description_' . $this->getLanguage()])) {
            return $this->getHtmlDescription();
        }
        return $fields['seo_description_' . $this->getLanguage()]->get();
    }

    public function getSeoImageReference()
    {
        $fields = $this->getDataset()->getFields();
        if (!isset($fields['seo_image'])) {
            return $this->getImageReference();
        }
        $image_references = $fields['seo_image']->get();
        if (!isset($image_references[0])) {
            return $this->getImageReference();
        }
        return $image_references[0];
    }
    
    public function getLanguage()
    {
        return $this->languageSelector->getLanguage();
    }

    public function setLanguage($language = '')
    {
        $this->languageSelector->setLanguage($language);
        $this->dataset->setLanguage($language);
        return $this;
    }

    public function getApp()
    {
        return $this->app;
    }

    public function getDatasetDescription()
    {
        return $this->datasetHelper->getDatasetDescription();
    }

    public function getDatasetWidgetDescription($language = '')
    {
        return $this->datasetHelper->getDatasetWidgetDescription($language);
    }

    public function getTableName()
    {
        throw new Exception('Method getTableName() must be defined in the class that extends AbstractContentUnit and return a string.');
    }

    public function prepareNewInstance()
    {
    }

    public function duplicate()
    {
        $class_name = get_called_class();
        $new_article = new $class_name($this->getApp());
        $old_fields = $this->getDataset()->getFields();
        $new_fields = $new_article->getDataset()->getFields();
        foreach ($old_fields as $field_name => $old_field) {
            $new_fields[$field_name]->set($old_field->get());
        }
        return $new_article;
    }

    public function getCreationUserType()
    {
        return $this->dbStoreComponent->getCreationUserType();
    }

    public function getCreationUserId()
    {
        return $this->dbStoreComponent->getCreationUserId();
    }

    public function getModificationUserType()
    {
        return $this->dbStoreComponent->getModificationUserType();
    }

    public function getModificationUserId()
    {
        return $this->dbStoreComponent->getModificationUserId();
    }

    public function preSave()
    {
        if ($this->getId() == 0) {
            $this->smartUrlTemporaryId = uniqid();
        }
        $fields = $this->getDataset()->getFields();
        foreach ($this->getApp()['viktor.config']['available_languages'] as $language => $language_name) {
            if (!isset($fields['smart_url_' . $language])) {
                continue;
            }
            $k = $this->getId() . ':' . $this->getType() . ':' . $language . ':view_page_url';
            if ($this->smartUrlTemporaryId != '') {
                $k = $this->smartUrlTemporaryId . ':' . $this->getType() . ':' . $language . ':view_page_url';
            }
            // Delete current smart URL.
            $query_builder = $this->getApp()['db']->createQueryBuilder();
            $query_builder
                ->delete('smart_url')
                ->where('k = :k')
                ->setParameter('k', $k)
            ;
            $query_builder->execute();
            $url_prefix = '';
            if ($this->getApp()['request_stack']->getCurrentRequest() != null) {
                $url_prefix = $this->getApp()['request_stack']->getCurrentRequest()->getBasePath();
            }
            $target = str_replace($url_prefix, '', $this->getBaseViewPageUrl($language));
            $successful_candidate = '';
            foreach ($this->getSmartUrlCandidates($language) as $c) {
                // Try to insert a smart URL until it works.
                try {
                    $query_builder = $this->getApp()['db']->createQueryBuilder();
                    $query_builder
                        ->insert('smart_url')
                        ->setValue('k', ':k')
                        ->setValue('origin', ':origin')
                        ->setValue('target', ':target')
                        ->setParameter('k', $k)
                        ->setParameter('origin', $c)
                        ->setParameter('target', $target)
                    ;
                    $query_builder->execute();
                    $successful_candidate = $c;
                    break;
                } catch (Throwable $t) {
                    // Just keep trying.
                }
            }
            // Update the field containing the smart URL.
            $fields['smart_url_' . $language]->set($successful_candidate);
        }
    }

    public function postSave()
    {
        if ($this->smartUrlTemporaryId != '') {
            $fields = $this->getDataset()->getFields();
            foreach ($this->getApp()['viktor.config']['available_languages'] as $language => $language_name) {
                if (!isset($fields['smart_url_' . $language])) {
                    continue;
                }
                $temporary_key = $this->smartUrlTemporaryId . ':' . $this->getType() . ':' . $language . ':view_page_url';
                $definitive_k = $this->getId() . ':' . $this->getType() . ':' . $language . ':view_page_url';
                $url_prefix = '';
                if ($this->getApp()['request_stack']->getCurrentRequest() != null) {
                    $url_prefix = $this->getApp()['request_stack']->getCurrentRequest()->getBasePath();
                }
                $target = str_replace($url_prefix, '', $this->getBaseViewPageUrl($language));
                $query_builder = $this->getApp()['db']->createQueryBuilder();
                $query_builder
                    ->update('smart_url')
                    ->set('k', ':def_k')
                    ->set('target', ':target')
                    ->where('k = :temp_k')
                    ->setParameter('def_k', $definitive_k)
                    ->setParameter('temp_k', $temporary_key)
                    ->setParameter('target', $target)
                ;
                $query_builder->execute();
            }
            $this->smartUrlTemporaryId = '';
        }
    }

    public function preDelete()
    {
        foreach ($this->getApp()['viktor.config']['available_languages'] as $language => $language_name) {
            $query_builder = $this->getApp()['db']->createQueryBuilder();
            $query_builder
                ->delete('smart_url')
                ->where('k = :k')
                ->setParameter('k', $this->getId() . ':' . $this->getType() . ':' . $language . ':view_page_url')
            ;
            $query_builder->execute();
        }
    }

    public function getBaseViewPageUrl($language)
    {
        $url = '';
        try {
            $url = $this->getApp()->path(
                $this->getType() . '-view',
                array(
                    'language' => $language,
                    'id' => $this->getId(),
                )
            );
        } catch (RouteNotFoundException $e) {
            // Nothing.
        }
        $url = preg_replace('@^https?://localhost@', '', $url);
        return $url;
    }

    public function getSmartUrlCandidates($language)
    {
        $candidates = array();
        $fields = $this->getDataset()->getFields();
        if (isset($fields['smart_url_' . $language])) {
            $c = $fields['smart_url_' . $language]->getPlainTextRepresentation();
            $c = preg_replace('@[^a-zA-Z0-9-_/]@', '', $c);
            $c = trim($c);
            if (strlen($c) <= 1) {
                $c = $this->generateDefaultSmartUrlCandidate($language);
                $c = preg_replace('@[^a-zA-Z0-9-_/]@', '', $c);
                $c = trim($c);
            }
            if (strlen($c) > 1) {
                if ($c[0] != '/') {
                    $c = '/' . $c;
                }
                if ($c[strlen($c) - 1] == '/') {
                    $c = substr($c, 0, strlen($c) - 1);
                }
                $candidates[] = $c;
                for ($i = 2; $i <= 9; $i++) {
                    $candidates[] = $c . '-' . $i;
                }
            }
        }
        return $candidates;
    }

    public function generateDefaultSmartUrlCandidate($language)
    {
        return '';
    }

    public function getDatasetCombinedDescription()
    {
        return array(
            'sections' => array(),
            'fields' => array(),
            'options' => array(),
        );
    }

}

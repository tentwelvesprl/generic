<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Content;

use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Schema\View;
use Exception;
use Silex\Provider\SecurityServiceProvider;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Yaml\Parser;
use Viktor\Application;
use Viktor\PackInterface;
use Viktor\Pack\Admin\User\SymfonyUserProvider;
use Viktor\Pack\Base\User\SecretaryUser;
use Viktor\Pack\Base\User\ViktorToSymfonyUserAdapter;
use Viktor\Pack\Content\User\BasicUser;

class ContentPack implements PackInterface
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
        return $this;
    }

    public function getName()
    {
        return 'Content';
    }

    public function getPath()
    {
        return realpath(__DIR__ . '/..');
    }

    public function install() {
        $app = $this->app;
        $schema = $app['db']->getSchemaManager();
        // Create 'user' table.
        if (!$schema->tablesExist('user')) {
            $table = new Table('user');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('id', 'integer', array('unsigned' => true, 'autoincrement' => true));
            $table->addColumn('type', 'string', array('length' => 64));
            $table->addColumn('creation_user_type', 'string', array('length' => 64));
            $table->addColumn('creation_user_id', 'integer', array('unsigned' => true));
            $table->addColumn('creation_uts', 'integer', array());
            $table->addColumn('modification_user_type', 'string', array('length' => 64));
            $table->addColumn('modification_user_id', 'integer', array('unsigned' => true));
            $table->addColumn('modification_uts', 'integer', array());
            $table->setPrimaryKey(array('type', 'id'));
            $table->addIndex(array('id'));
            $table->addIndex(array('type'));
            $schema->createTable($table);
        }
        // Create 'article' table.
        if (!$schema->tablesExist('article')) {
            $table = new Table('article');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('id', 'integer', array('unsigned' => true, 'autoincrement' => true));
            $table->addColumn('type', 'string', array('length' => 64));
            $table->addColumn('creation_user_type', 'string', array('length' => 64));
            $table->addColumn('creation_user_id', 'integer', array('unsigned' => true));
            $table->addColumn('creation_uts', 'integer', array());
            $table->addColumn('modification_user_type', 'string', array('length' => 64));
            $table->addColumn('modification_user_id', 'integer', array('unsigned' => true));
            $table->addColumn('modification_uts', 'integer', array());
            $table->setPrimaryKey(array('type', 'id'));
            $table->addIndex(array('id'));
            $table->addIndex(array('type'));
            $schema->createTable($table);
        }
        // Create 'node' table.
        if (!$schema->tablesExist('node')) {
            $table = new Table('node');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('id', 'integer', array('unsigned' => true, 'autoincrement' => true));
            $table->addColumn('type', 'string', array('length' => 64));
            $table->addColumn('creation_user_type', 'string', array('length' => 64));
            $table->addColumn('creation_user_id', 'integer', array('unsigned' => true));
            $table->addColumn('creation_uts', 'integer', array());
            $table->addColumn('modification_user_type', 'string', array('length' => 64));
            $table->addColumn('modification_user_id', 'integer', array('unsigned' => true));
            $table->addColumn('modification_uts', 'integer', array());
            $table->setPrimaryKey(array('type', 'id'));
            $table->addIndex(array('id'));
            $table->addIndex(array('type'));
            $schema->createTable($table);
        }
        // Create 'image' table.
        if (!$schema->tablesExist('image')) {
            $table = new Table('image');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('id', 'integer', array('unsigned' => true, 'autoincrement' => true));
            $table->addColumn('type', 'string', array('length' => 64));
            $table->addColumn('creation_user_type', 'string', array('length' => 64));
            $table->addColumn('creation_user_id', 'integer', array('unsigned' => true));
            $table->addColumn('creation_uts', 'integer', array());
            $table->addColumn('modification_user_type', 'string', array('length' => 64));
            $table->addColumn('modification_user_id', 'integer', array('unsigned' => true));
            $table->addColumn('modification_uts', 'integer', array());
            $table->setPrimaryKey(array('type', 'id'));
            $table->addIndex(array('id'));
            $table->addIndex(array('type'));
            $schema->createTable($table);
        }
        // Create image references count view.
        $view = new View('image_reference_count', '
            SELECT 
                image.type AS rtype, 
                image.id AS rid, 
                CASE WHEN content_unit_references_index.reference_id IS NULL THEN 0 ELSE COUNT(image.id) END AS reference_count
            FROM `image` 
            LEFT JOIN content_unit_references_index ON (
                image.type = content_unit_references_index.reference_type 
                AND image.id = content_unit_references_index.reference_id) 
            GROUP BY rtype, rid
        ');
        $schema->dropAndCreateView($view);
        // Create 'file' table.
        if (!$schema->tablesExist('file')) {
            $table = new Table('file');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('id', 'integer', array('unsigned' => true, 'autoincrement' => true));
            $table->addColumn('type', 'string', array('length' => 64));
            $table->addColumn('creation_user_type', 'string', array('length' => 64));
            $table->addColumn('creation_user_id', 'integer', array('unsigned' => true));
            $table->addColumn('creation_uts', 'integer', array());
            $table->addColumn('modification_user_type', 'string', array('length' => 64));
            $table->addColumn('modification_user_id', 'integer', array('unsigned' => true));
            $table->addColumn('modification_uts', 'integer', array());
            $table->setPrimaryKey(array('type', 'id'));
            $table->addIndex(array('id'));
            $table->addIndex(array('type'));
            $schema->createTable($table);
        }
        // Create file references count view.
        $view = new View('file_reference_count', '
            SELECT 
                file.type AS rtype, 
                file.id AS rid, 
                CASE WHEN content_unit_references_index.reference_id IS NULL THEN 0 ELSE COUNT(file.id) END AS reference_count
            FROM `file` 
            LEFT JOIN content_unit_references_index ON (
                file.type = content_unit_references_index.reference_type 
                AND file.id = content_unit_references_index.reference_id) 
            GROUP BY rtype, rid
        ');
        $schema->dropAndCreateView($view);
        // Create 'media' table.
        if (!$schema->tablesExist('media')) {
            $table = new Table('media');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('id', 'integer', array('unsigned' => true, 'autoincrement' => true));
            $table->addColumn('type', 'string', array('length' => 64));
            $table->addColumn('creation_user_type', 'string', array('length' => 64));
            $table->addColumn('creation_user_id', 'integer', array('unsigned' => true));
            $table->addColumn('creation_uts', 'integer', array());
            $table->addColumn('modification_user_type', 'string', array('length' => 64));
            $table->addColumn('modification_user_id', 'integer', array('unsigned' => true));
            $table->addColumn('modification_uts', 'integer', array());
            $table->setPrimaryKey(array('type', 'id'));
            $table->addIndex(array('id'));
            $table->addIndex(array('type'));
            $schema->createTable($table);
        }
        // Create media references count view.
        $view = new View('media_reference_count', '
            SELECT 
                media.type AS rtype, 
                media.id AS rid, 
                CASE WHEN content_unit_references_index.reference_id IS NULL THEN 0 ELSE COUNT(media.id) END AS reference_count
            FROM `media` 
            LEFT JOIN content_unit_references_index ON (
                media.type = content_unit_references_index.reference_type 
                AND media.id = content_unit_references_index.reference_id) 
            GROUP BY rtype, rid
        ');
        $schema->dropAndCreateView($view);
        // Create the admin user.
        try {
            $secretary_user = new SecretaryUser($app);
            $admin_user = new BasicUser($app);
            $symfony_admin_user = new ViktorToSymfonyUserAdapter('admin', '');
            $admin_user->setUsername('admin');
            $admin_user->setEmail('');
            $encoded_password = $app['security.encoder_factory']->getEncoder($symfony_admin_user)->encodePassword('123456789', '');
            $admin_user->setPassword($encoded_password);
            $admin_user->setRoles(array('ROLE_ADMIN'));
            $admin_user->save($secretary_user);
        } catch (Exception $e) {
            // Fail silently...
        }
        return;
    }

    public function init()
    {
        $app = $this->app;
        // Register content unit types.
        $this->app['content_unit_factory']->registerType('Viktor\Pack\Content\User\BasicUser');
        // Twig configuration.
        $this->app['twig.loader.filesystem']->addPath(__DIR__ . '/../views', 'Content');
        // Firewall.
        $pack_controllers_behind_general_firewall = array(
            'Viktor\Pack\Content\Article\ArticleAppController',
            'Viktor\Pack\Content\Article\ArticleController',
            'Viktor\Pack\Content\FileContentUnit\FileContentUnitAppController',
            'Viktor\Pack\Content\FileContentUnit\FileContentUnitController',
            'Viktor\Pack\Content\Image\ImageAppController',
            'Viktor\Pack\Content\Image\ImageController',
            'Viktor\Pack\Content\Media\MediaAppController',
            'Viktor\Pack\Content\Media\MediaController',
            'Viktor\Pack\Content\Node\NodeAppController',
            'Viktor\Pack\Content\Node\NodeController',
            'Viktor\Pack\Content\User\UserAppController',
            'Viktor\Pack\Content\User\UserController',
            'Viktor\Pack\Content\AdminDoc\AdminDocAppController',
        );
        $this->app['viktor.controllers_behind_general_firewall'] = array_merge(
            $this->app['viktor.controllers_behind_general_firewall'],
            $pack_controllers_behind_general_firewall
        );
        return $this;
    }

    public function addConsoleCommands() {
    }

}

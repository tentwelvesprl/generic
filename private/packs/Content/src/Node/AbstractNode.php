<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Content\Node;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Viktor\Application;
use Viktor\Pack\Base\Badge\Badge;
use Viktor\Pack\Base\Badge\HasABadgeInterface;
use Viktor\Pack\Base\Helper\StringHelper;
use Viktor\Pack\Base\Publication\Publication;
use Viktor\Pack\Base\Publication\PublicationDecorator;
use Viktor\Pack\Base\Publication\PublishableInterface;
use Viktor\Pack\Base\Tree\NodeInterface;
use Viktor\Pack\Base\User\UserInterface;
use Viktor\Pack\Content\ContentUnit\AbstractContentUnit;

abstract class AbstractNode extends AbstractContentUnit implements 
    NodeInterface,
    PublishableInterface
{
    private $publication;

    public function getTableName()
    {
        return 'node';
    }

    public function prepareNewInstance()
    {
        $this->publication = new Publication($this->getApp(), $this, $this->getPublicationOptions());
        $this->dbStoreComponent = new PublicationDecorator($this->dbStoreComponent, $this->publication);
    }

    public function canBeViewed()
    {
        if ($this->getPublication()->isPublishedNow()) {
            return true;
        }
        if ($this->getApp()['security.token_storage']->getToken() == null) {
            return false;
        }
        return $this->getApp()['security.authorization_checker']->isGranted('ROLE_AUDITOR');
    }

    public function getCreationPageUrl()
    {
        return $this->getApp()->path('admin-' . $this->getTableName() . '-edition-form', array(
            'language'  => $this->getLanguage(),
            'type'      => $this->getType(),
            'id'        => 0,
            'parent_id' => $this->getId(),
        ));
    }

    public function getDatasetCombinedDescription()
    {
        $sections = array();
        $fields = array(
            'title' => array(
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\TextField',
                    'options'    => array(
                        'source_type' => 'plain_text',
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\TextFieldWidget',
                    'options'    => array(
                        'label' => 'Title',
                    ),
                ),
            ),
        );
        return array(
            'sections' => $sections,
            'fields' => $fields
        );
    }

    public function getPublicationOptions()
    {
        $options = array(
            'use_published' => true,
            'use_start_datetime' => false,
            'use_end_datetime' => false,
        );
        return $options;
    }

    public function getPublication()
    {
        return $this->publication;
    }

    public function setLanguage($language = '')
    {
        parent::setLanguage($language);
        $this->publication->setLanguage($language);
        return $this;
    }

    public function renderListItem(Request $request, Application $app)
    {
        $publication_statuses = array();
        foreach ($this->getPublication()->getContent() as $language => $publication_content) {
            $publication_status = 'not_published';
            if ($publication_content['published']) {
                if ($this->getPublication()->isPublishedNow($language)) {
                    $publication_status = 'published';
                } else {
                    $publication_status = 'scheduled';
                }
            }
            $publication_statuses[$language] = $publication_status;
        }
        $badge_digest = array();
        if ($badge_items = $this->getBadge()) {
            if ($badge_items instanceof Badge) {
                $badge_digest[] = $badge_items->getDigest();
            } elseif (is_array($badge_items)) {
                foreach ($badge_items as $badge_item) {
                    $badge_digest[] = $badge_item->getDigest();
                }
            }
        }
        return $app['twig']->render(
            '@Content/Node/listItem.twig',
            array(
                'node_digest' => $this->getDigest(),
                'language' => $this->getLanguage(),
                'use_published' => $this->getPublication()->getOptions()['use_published'],
                'publication_statuses' => $publication_statuses,
                'badge' => $badge_digest,
            )
        );
    }

    public function getAdditionalEditionFormHeaderView()
    {
        return '';
    }

    public function getAdditionalEditionFormFooterView()
    {
        return '';
    }

    public function getAdditionalEditionFormAsideView()
    {
        return '';
    }

}

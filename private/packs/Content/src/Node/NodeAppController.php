<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Content\Node;

use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Admin\Admin\AdminHtmlPage;
use Viktor\Pack\Admin\Dataset\DatasetWidget;
use Viktor\Pack\Base\I18n\TranslatableInterface;
use Viktor\Pack\Admin\Publication\PublicationWidget;
use Viktor\Pack\Base\Tree\TreeStructure;
use Viktor\Pack\Base\User\UnknownUser;

class NodeAppController extends AdminHtmlPage
{
    public function index(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        $node_model = $app['content_unit_factory']->create($type);
        if (!is_a($node_model, 'Viktor\Pack\Content\Node\AbstractNode')) {
            $app->abort(404, $type . ' is not a valid node type.');
        }
        $node_model->setLanguage($app['i18n']->getLanguage());
        if (!$node_model->canBeListed()) {
            $app->abort(403, 'You are not allowed to list these elements.');
        }
        $nodes_ids = $node_model->getIds();
        $nodes_list = $app['content_unit_factory']->createList($type, $nodes_ids);
        foreach ($nodes_list as $node) {
            $node->setLanguage($app['i18n']->getLanguage());
        }
        $nodes_list_indexed_by_id = array();
        foreach ($nodes_list as $node) {
            $nodes_list_indexed_by_id[$node->getId()] = $node;
        }
        $nodes_list = $nodes_list_indexed_by_id;
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $node_model->setLanguage($language_code);
            $language_urls[$language_code] = $node_model->getListPageUrl();
        }
        $node_model->setLanguage($app['i18n']->getLanguage());
        $tree_structure = new TreeStructure($app, $type);
        $tree_structure->load();
        $this->addMarker('node-content-unit');
        $this->addMarker($node_model->getType());
        $this->setTitle(ucfirst($node_model->getPluralHumanType()));
        return $this->render(
            $app,
            '@Content/Node/index.twig',
            array(
                'node_model'      => $node_model->getDigest(),
                'nodes'           => $nodes_list,
                'tree_structure'  => $tree_structure->getStructure(),
                'language_urls'   => $language_urls,
                'language'        => $app['i18n']->getLanguage(),
            )
        );
    }

    public function structureProcess(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        $node = $app['content_unit_factory']->create($type);
        if (!is_a($node, 'Viktor\Pack\Content\Node\AbstractNode')) {
            $app->abort(404, 'Invalid node type.');
        }
        $node->setLanguage($app['i18n']->getLanguage());
        if (!$node->canBeEdited()) {
            $app->abort(403, 'You are not allowed to edit this node.');
        }
        $input = json_decode($request->request->get('structure', ''));
        $tree_structure = new TreeStructure($app, $type);
        $success = true;
        if ($input !== null) {
            foreach ($input as $item) {
                $parent_id = (int) $item->id;
                $children = $item->children;
                foreach ($children as $child_id) {
                    $tree_structure->addItem($child_id, $parent_id);
                }
            }
            $success = $tree_structure->save();
        }
        if ($success) {
            $app['session']->getFlashBag()->add('success', 'Structure saved successfully.');
        } else {
            $app['session']->getFlashBag()->add('error', 'The structure could not be saved.');
        }
        return $app->redirect($node->getListPageUrl());
    }

    public function editionForm(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        $id = $request->query->get('id', 0);
        $node = $app['content_unit_factory']->create($type);
        if (!is_a($node, 'Viktor\Pack\Content\Node\AbstractNode')) {
            $app->abort(404, 'Invalid node type.');
        }
        $node->setLanguage($app['i18n']->getLanguage());
        if ($id > 0) {
            // Existing node.
            $node->load($id);
            if ($node->getId() == 0) {
                $app->abort(404, 'Invalid node id.');
            }
            if (!$node->canBeViewed()) {
                $app->abort(403, 'You are not allowed to view this node.');
            }
        } else {
            // New user.
            if (!$node->canBeCreated()) {
                $app->abort(403, 'You are not allowed to create this node.');
            }
        }
        $creation_user = $app['content_unit_factory']->load(
            $node->getCreationUserType(),
            $node->getCreationUserId()
        );
        if ($creation_user == null) {
            $creation_user = new UnknownUser($app);
        }
        if ($creation_user instanceof TranslatableInterface) {
            $creation_user->setLanguage($app['i18n']->getLanguage());
        }
        $modification_user = $app['content_unit_factory']->load(
            $node->getModificationUserType(),
            $node->getModificationUserId()
        );
        if ($modification_user == null) {
            $modification_user = new UnknownUser($app);
        }
        if ($modification_user instanceof TranslatableInterface) {
            $modification_user->setLanguage($app['i18n']->getLanguage());
        }
        // Prepare the widgets.
        $publication_widget = new PublicationWidget(
            $node->getPublication(),
            'node' // Prefix for the fields names.
        );
        $dataset_widget = new DatasetWidget(
            $node->getDatasetWidgetDescription(),
            $node->getDataset(),
            'node' // Prefix for the fields names.
        );
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $node->setLanguage($language_code);
            if ($node->getId() == 0) {
                $language_urls[$language_code] = $node->getCreationPageUrl();
            } else {
                $language_urls[$language_code] = $node->getEditionPageUrl();
            }
        }
        // Content units that have a link to this content unit.
        $referencing_content_units_array = $node->getDbStoreComponent()->getReferencingContentUnits();
        $referencing_content_units_count = count($referencing_content_units_array);
        $referencing_content_units = array();
        foreach ($referencing_content_units_array as $v) {
            if (count($referencing_content_units_array) >= 20) {
                break;
            }
            $referencing_content_unit = $app['content_unit_factory']->load($v['type'], $v['id']);
            if ($referencing_content_unit != null) {
                $referencing_content_units[] = $referencing_content_unit;
            }
        }
        $node->setLanguage($app['i18n']->getLanguage());
        $this->addMarker('node-content-unit');
        $this->addMarker($node->getType());
        $this->addMarker($node->getType() . ':' . $node->getId());
        $this->setTitle($node->getTitle());
        return $this->render(
            $app,
            '@Content/Node/editionForm.twig',
            array(
                'parent_id' => (int) $request->query->get('parent_id', 0),
                'node_digest' => $node->getDigest(),
                'dataset' => $node->getDataset(),
                'dataset_widget' => $dataset_widget,
                'publication' => $node->getPublication(),
                'publication_widget' => $publication_widget,
                'creation_user' => $creation_user->getDigest(),
                'modification_user' => $modification_user->getDigest(),
                'language_urls' => $language_urls,
                'language' => $app['i18n']->getLanguage(),
                'referencing_content_units' => $referencing_content_units,
                'referencing_content_units_count' => $referencing_content_units_count,
                'additional_edition_form_header_view' => $node->getAdditionalEditionFormHeaderView(),
                'additional_edition_form_footer_view' => $node->getAdditionalEditionFormFooterView(),
                'additional_edition_form_aside_view' => $node->getAdditionalEditionFormAsideView(),
            )
        );
    }

    public function editionFormProcess(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        // Check the object validity and permissions.
        $id = $request->request->get('id', 0);
        $node = $app['content_unit_factory']->create($type);
        if (!is_a($node, 'Viktor\Pack\Content\Node\AbstractNode')) {
            $app->abort(404, 'Invalid node type.');
        }
        $node->setLanguage($app['i18n']->getLanguage());
        if ($id > 0) {
            // Existing node.
            $node->load($id);
            if ($node->getId() == 0) {
                $app->abort(404, 'Invalid node id.');
            }
            if (!$node->canBeEdited()) {
                $app->abort(403, 'You are not allowed to edit this node.');
            }
            $form_url = $node->getEditionPageUrl();
        } else {
            // New node.
            if (!$node->canBeCreated()) {
                $app->abort(403, 'You are not allowed to create this node.');
            }
            $form_url = $node->getCreationPageUrl();
        }
        // Prepare the form.
        $dataset_widget = new DatasetWidget(
            $node->getDatasetWidgetDescription(),
            $node->getDataset(),
            'node' // Prefix for the fields names.
        );
        $publication_widget = new PublicationWidget(
            $node->getPublication(),
            'node' // Prefix for the fields names.
        );
        // Store the form data in session.
        $dataset_widget->storePrefilledData($request, $app);
        $publication_widget->storePrefilledData($request, $app);
        // Set the new values.
        $publication_widget->set($request, $app);
        $dataset_widget->set($request, $app);
        // Validate the form.
        $form_is_valid = true;
        $fields_widgets = $dataset_widget->getFieldsWidgets();
        foreach ($fields_widgets as $field_widget) {
            $field_language = $field_widget->getField()->getMetadata('language');
            // Language independant fields are always validated.
            // Translated fields are only validated for the published languages.
            if ($field_language == '' || $publication_widget->getPublication()->getPublished($field_language)) {
                $validation_result = $field_widget->validate($request, $app);
                if (!$validation_result) {
                    $form_is_valid = false;
                }
            }
        }
        if (!$form_is_valid) {
            $app['session']->getFlashBag()->add('error', 'Some fields are invalid.');
            return $app->redirect($form_url);
        }
        // Save the node.
        if ($node->save($app['viktor.user'])) {
            // Modify the parent node id.
            $parent_id = (int) $request->request->get('parent_id', 0);
            if ($parent_id > 0) {
                $tree_structure = new TreeStructure($app, $type);
                $tree_structure->load();
                $tree_structure->setParentId($node->getId(), $parent_id);
                $tree_structure->save();
            }
            $app['session']->getFlashBag()->add(
                'success',
                ucfirst($node->getSingularHumanType()) . ' ' . $node->getTitle() . ' saved successfully.'
        );
        } else {
            $app['session']->getFlashBag()->add('error', 'The ' . $node->getSingularHumanType() . ' could not be saved.');
        }
        // Remove the form data stored in session.
        $publication_widget->removePrefilledData($request, $app);
        $dataset_widget->removePrefilledData($request, $app);
        // Everything is ok, back to the form.
        return $app->redirect($node->getEditionPageUrl());
    }

    public function deletionForm(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        // Check the object validity and permissions.
        $id = $request->query->get('id', 0);
        $node = $app['content_unit_factory']->load($type, $id);
        if (!is_a($node, 'Viktor\Pack\Content\Node\AbstractNode')) {
            $app->abort(404, 'Could not load this node.');
        }
        $node->setLanguage($app['i18n']->getLanguage());
        if (!$node->canBeDeleted()) {
            $app->abort(403, 'You are not allowed to delete this node.');
        }
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $node->setLanguage($language_code);
            $language_urls[$language_code] = $node->getDeletionPageUrl();
        }
        $node->setLanguage($app['i18n']->getLanguage());
        $this->addMarker('node-content-unit');
        $this->addMarker($node->getType());
        $this->addMarker($node->getType() . ':' . $node->getId());
        $this->setTitle('Delete "' . $node->getTitle() . '"?');
        return $this->render(
            $app,
            '@Content/Node/deletionForm.twig',
            array(
                'node_digest'   => $node->getDigest(),
                'language_urls' => $language_urls,
                'language'      => $app['i18n']->getLanguage(),
            )
        );
    }

    public function deletionFormProcess(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        // Check the object validity and permissions.
        $id = $request->request->get('id', 0);
        $node = $app['content_unit_factory']->load($type, $id);
        if (!is_a($node, 'Viktor\Pack\Content\Node\AbstractNode')) {
            $app->abort(404, 'Could not load this node.');
        }
        $node->setLanguage($app['i18n']->getLanguage());
        if (!$node->canBeDeleted()) {
            $app->abort(403, 'You are not allowed to delete this node.');
        }
        $node_type = $node->getType();
        $node_title = $node->getTitle();
        $node_id = $node->getId();
        $nodes_list_url = $node->getListPageUrl();
        if ($node->delete()) {
            $app['session']->getFlashBag()->add(
                'success',
                $node_title . ' (' . $node_type . ' #' . $node_id . ') has been deleted.'
            );
        } else {
            $app['session']->getFlashBag()->add(
                'error',
                $node_title . ' (' . $node_type . ' #' . $node_id . ') could not be deleted.'
            );
        }
        return $app->redirect($nodes_list_url);
    }

}

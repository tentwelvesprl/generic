<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Content\User;

use Viktor\Pack\Base\Badge\Badge;

class BasicUser extends AbstractUser
{
    public function getType()
    {
        return 'basic-user';
    }

    public function getSingularHumanType()
    {
        return 'user';
    }

    public function getPluralHumanType()
    {
        return 'users';
    }

    public function getBadge()
    {
        $badge_items = array();
        foreach ($this->getUserCredentials()->getRoles() as $role) {
            $role_title = $this->getApp()['viktor.config']['security_roles'][$role]['title'];
            $badge_items[] = new Badge(52, 57, 87, $role_title);
        }
        return $badge_items;
    }

    public function canBeCreated()
    {
        if ($this->getApp()['security.token_storage']->getToken() == null) {
            return false;
        }
        return $this->getApp()['security.authorization_checker']->isGranted('ROLE_ADMIN');
    }

    public function canBeViewed()
    {
        return true;
    }

    public function canBeEdited()
    {
        if ($this->getApp()['security.token_storage']->getToken() == null) {
            return false;
        }
        if (
            $this->getType() == $this->getApp()['viktor.user']->getType()
            && $this->getId() == $this->getApp()['viktor.user']->getId()
        ) {
            return true;
        } else {
            return $this->getApp()['security.authorization_checker']->isGranted('ROLE_ADMIN');
        }
    }

    public function canBeDeleted()
    {
        if ($this->getApp()['security.token_storage']->getToken() == null) {
            return false;
        }
        if (
            $this->getType() == $this->getApp()['viktor.user']->getType()
            && $this->getId() == $this->getApp()['viktor.user']->getId()
        ) {
            return false;
        }
        return $this->getApp()['security.authorization_checker']->isGranted('ROLE_ADMIN');
    }

    public function canBeListed()
    {
        if ($this->getApp()['security.token_storage']->getToken() == null) {
            return false;
        }
        return $this->getApp()['security.authorization_checker']->isGranted('ROLE_AUDITOR');
    }

}

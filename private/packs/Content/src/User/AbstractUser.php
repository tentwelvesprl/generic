<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Content\User;

use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Base\Badge\Badge;
use Viktor\Pack\Base\FullTextIndex\FullTextIndexableInterface;
use Viktor\Pack\Base\User\UserInterface;
use Viktor\Pack\Base\UserCredentials\UserCredentials;
use Viktor\Pack\Base\UserCredentials\UserCredentialsDecorator;
use Viktor\Pack\Content\ContentUnit\AbstractContentUnit;

abstract class AbstractUser extends AbstractContentUnit implements 
    UserInterface,
    FullTextIndexableInterface
{
    private $userCredentials;

    public function getTableName()
    {
        return 'user';
    }

    public function prepareNewInstance()
    {
        $this->userCredentials = new UserCredentials($this->getApp(), $this);
        $this->dbStoreComponent = new UserCredentialsDecorator($this->dbStoreComponent, $this->userCredentials);
    }

    public function getDigest()
    {
        $digest = parent::getDigest();
        $digest['_can_have_permissions_modified'] = $this->canHavePermissionsModified();
        return $digest;
    }

    public function getTitle()
    {
        $fields = $this->getDataset()->getFields();
        $title = $fields['first_name']->getPlainTextRepresentation() . ' ' . $fields['last_name']->getPlainTextRepresentation();
        if (trim($title) == '') {
            $title = $this->getUsername();
        }
        $title = $title;
        return $title;
    }

    public function getUsername()
    {
        return $this->userCredentials->getUsername();
    }

    public function getPassword()
    {
        return $this->userCredentials->getPassword();
    }

    public function getRoles()
    {
        $roles = $this->userCredentials->getRoles();
        $roles[] = 'ROLE_USER';
        return $roles;
    }

    public function getEmail()
    {
        return $this->userCredentials->getEmail();
    }

    public function setUsername($username)
    {
        return $this->userCredentials->setUsername($username);
    }

    public function setPassword($password)
    {
        return $this->userCredentials->setPassword($password);
    }

    public function setRoles(array $roles)
    {
        return $this->userCredentials->setRoles($roles);
    }

    public function setEmail($email)
    {
        return $this->userCredentials->setEmail($email);
    }

    public function canHavePermissionsModified()
    {
        if ($this->getApp()['security.token_storage']->getToken() == null) {
            return false;
        }
        return $this->getApp()['security.authorization_checker']->isGranted('ROLE_ADMIN');
    }

    public function usernameIsAvailable()
    {
        return $this->userCredentials->usernameIsAvailable();
    }

    public function getUserCredentials()
    {
        return $this->userCredentials;
    }

    public function getDatasetCombinedDescription()
    {
        $sections = array();
        $fields = array(
            'first_name' => array(
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\TextField',
                    'options'    => array(
                        'source_type' => 'plain_text',
                    ),
                    'full_text_index_weights' => array(
                        'general' => 5,
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\TextFieldWidget',
                    'options'    => array(
                        'label' => 'First name',
                        'searchable' => true,
                    ),
                ),
            ),
            'last_name' => array(
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\TextField',
                    'options'    => array(
                        'source_type' => 'plain_text',
                    ),
                    'full_text_index_weights' => array(
                        'general' => 5,
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\TextFieldWidget',
                    'options'    => array(
                        'label' => 'Last name',
                        'searchable' => true,
                    ),
                ),
            ),
        );
        return array(
            'sections' => $sections,
            'fields' => $fields
        );
    }

    public function renderListItem(Request $request, Application $app)
    {
        $badge_digest = array();
        if ($badge_items = $this->getBadge()) {
            if ($badge_items instanceof Badge) {
                $badge_digest[] = $badge_items->getDigest();
            } elseif (is_array($badge_items)) {
                foreach ($badge_items as $badge_item) {
                    $badge_digest[] = $badge_item->getDigest();
                }
            }
        }
        return $app['twig']->render(
            '@Content/User/listItem.twig',
            array(
                'user_digest' => $this->getDigest(),
                'badge' => $badge_digest,
            )
        );
    }

    public function getAdditionalEditionFormHeaderView()
    {
        return '';
    }

    public function getAdditionalEditionFormFooterView()
    {
        return '';
    }

    public function getAdditionalEditionFormAsideView()
    {
        return '';
    }

}

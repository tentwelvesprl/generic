<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Content\User;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Admin\Admin\AdminHtmlPage;
use Viktor\Pack\Admin\Dataset\DatasetWidget;
use Viktor\Pack\Admin\DbStore\DbStoreComponentWidget;
use Viktor\Pack\Admin\Pagination\PaginationWidget;
use Viktor\Pack\Admin\SortOptions\SortOptionsWidget;
use Viktor\Pack\Admin\UserCredentials\UserCredentialsWidget;
use Viktor\Pack\Base\I18n\TranslatableInterface;
use Viktor\Pack\Base\Pagination\Pagination;
use Viktor\Pack\Base\User\UnknownUser;

class UserAppController extends AdminHtmlPage
{
    public function index(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        $user_model = $app['content_unit_factory']->create($type);
        if (!is_a($user_model, 'Viktor\Pack\Content\User\AbstractUser')) {
            $app->abort(404, $type . ' is not a valid user type.');
        }
        $user_model->setLanguage($app['i18n']->getLanguage());
        if (!$user_model->canBeListed()) {
            $app->abort(403, 'You are not allowed to list these elements.');
        }
        // Prepare the widgets.
        $db_store_component_widget = new DbStoreComponentWidget(
            $user_model->getDbStoreComponent(),
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        $dataset_widget = new DatasetWidget(
            $user_model->getDatasetWidgetDescription($language),
            $user_model->getDataset(),
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        $user_credentials_widget = new UserCredentialsWidget(
            $user_model->getUserCredentials(),
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        $sort_options = array();
        $sort_options = array_merge($sort_options, $dataset_widget->getSortOptions());
        $sort_options = array_merge($sort_options, $user_credentials_widget->getSortOptions());
        $sort_options = array_merge($sort_options, $db_store_component_widget->getSortOptions());
        $sort_options_widget = new SortOptionsWidget(
            $sort_options,
            $user_model,
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        // Store the advanced search form data in session.
        $dataset_widget->storeAdvancedSearchData($request, $app);
        // Filters.
        $filters = array();
        $filters = array_merge($filters, $dataset_widget->getFilters($request, $app));
        $filters[] = array(
            'type' => 'deduplicate',
            'value' => true,
        );
        // Sort options.
        $sort_options_widget->storeSelectedSortOption($request, $app);
        // Go!
        $users_ids = $user_model->getIds(
            $filters,
            array($sort_options_widget->getSelectedSortOption($request, $app))
        );
        // Apply pagination.
        $pagination = new Pagination(
            $request,
            $app,
            count($users_ids),
            10,
            1,
            array(10, 20, 50),
            $type . '_' . $app['i18n']->getLanguage()
        );
        $pagination_widget = new PaginationWidget(
            $pagination,
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        $pagination_widget->storePaginationParameters($request, $app);
        $pagination_parameters = $pagination_widget->getStoredPaginationParameters($app);
        $pagination->setCurrentPage($pagination_parameters['current_page']);
        $pagination->setNbItemsPerPage($pagination_parameters['nb_items_per_page']);
        $users_ids_to_display = array_slice(
            $users_ids,
            ($pagination_parameters['current_page'] - 1) * $pagination_parameters['nb_items_per_page'],
            $pagination_parameters['nb_items_per_page']
        );
        $users_list = $app['content_unit_factory']->createList($type, $users_ids_to_display);
        foreach ($users_list as $user) {
            $user->setLanguage($app['i18n']->getLanguage());
        }
        // Active filters description.
        $active_filters_description = array();
        $active_filters_description = array_merge($active_filters_description, $dataset_widget->getActiveFiltersDescription($request, $app));
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $user_model->setLanguage($language_code);
            $language_urls[$language_code] = $user_model->getListPageUrl();
        }
        $user_model->setLanguage($app['i18n']->getLanguage());
        $this->addMarker('user-content-unit');
        $this->addMarker($user_model->getType());
        $this->setTitle(ucfirst($user_model->getPluralHumanType()));
        return $this->render(
            $app,
            '@Content/User/index.twig',
            array(
                'user_model' => $user_model->getDigest(),
                'users' => $users_list,
                'dataset' => $user_model->getDataset(),
                'dataset_widget' => $dataset_widget,
                'pagination' => $pagination,
                'pagination_widget' => $pagination_widget,
                'sort_options_widget' => $sort_options_widget,
                'active_filters_description' => $active_filters_description,
                'language_urls' => $language_urls,
                'language' => $app['i18n']->getLanguage(),
            )
        );
    }

    public function editionForm(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        $id = $request->query->get('id', 0);
        $user = $app['content_unit_factory']->create($type);
        if (!is_a($user, 'Viktor\Pack\Content\User\AbstractUser')) {
            $app->abort(404, 'Invalid user type.');
        }
        $user->setLanguage($app['i18n']->getLanguage());
        if ($id > 0) {
            // Existing user.
            $user->load($id);
            if ($user->getId() == 0) {
                $app->abort(404, 'Invalid user id.');
            }
            if (!$user->canBeViewed()) {
                $app->abort(403, 'You are not allowed to view this user.');
            }
        } else {
            // New user.
            if (!$user->canBeCreated()) {
                $app->abort(403, 'You are not allowed to create this user.');
            }
        }
        $creation_user = $app['content_unit_factory']->load(
            $user->getCreationUserType(),
            $user->getCreationUserId()
        );
        if ($creation_user == null) {
            $creation_user = new UnknownUser($app);
        }
        if ($creation_user instanceof TranslatableInterface) {
            $creation_user->setLanguage($app['i18n']->getLanguage());
        }
        $modification_user = $app['content_unit_factory']->load(
            $user->getModificationUserType(),
            $user->getModificationUserId()
        );
        if ($modification_user == null) {
            $modification_user = new UnknownUser($app);
        }
        if ($modification_user instanceof TranslatableInterface) {
            $modification_user->setLanguage($app['i18n']->getLanguage());
        }
        $dataset_widget = new DatasetWidget(
            $user->getDatasetWidgetDescription(),
            $user->getDataset(),
            'user' // Prefix for the fields names.
        );
        $user_credentials_widget = new UserCredentialsWidget(
            $user->getUserCredentials(),
            'user' // Prefix for the fields names.
        );
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $user->setLanguage($language_code);
            if ($user->getId() == 0) {
                $language_urls[$language_code] = $user->getCreationPageUrl();
            } else {
                $language_urls[$language_code] = $user->getEditionPageUrl();
            }
        }
        // Content units that have a link to this content unit.
        $referencing_content_units_array = $user->getDbStoreComponent()->getReferencingContentUnits();
        $referencing_content_units_count = count($referencing_content_units_array);
        $referencing_content_units = array();
        foreach ($referencing_content_units_array as $v) {
            if (count($referencing_content_units_array) >= 20) {
                break;
            }
            $referencing_content_unit = $app['content_unit_factory']->load($v['type'], $v['id']);
            if ($referencing_content_unit != null) {
                $referencing_content_units[] = $referencing_content_unit;
            }
        }
        $user->setLanguage($app['i18n']->getLanguage());
        $this->addMarker('user-content-unit');
        $this->addMarker($user->getType());
        $this->addMarker($user->getType() . ':' . $user->getId());
        $this->setTitle($user->getTitle());
        return $this->render(
            $app,
            '@Content/User/editionForm.twig',
            array(
                'user_digest' => $user->getDigest(),
                'dataset' => $user->getDataset(),
                'dataset_widget' => $dataset_widget,
                'user_credentials' => $user->getUserCredentials(),
                'user_credentials_widget' => $user_credentials_widget,
                'creation_user' => $creation_user->getDigest(),
                'modification_user' => $modification_user->getDigest(),
                'language_urls' => $language_urls,
                'language' => $app['i18n']->getLanguage(),
                'referencing_content_units' => $referencing_content_units,
                'referencing_content_units_count' => $referencing_content_units_count,
                'additional_edition_form_header_view' => $user->getAdditionalEditionFormHeaderView(),
                'additional_edition_form_footer_view' => $user->getAdditionalEditionFormFooterView(),
                'additional_edition_form_aside_view' => $user->getAdditionalEditionFormAsideView(),
            )
        );
    }

    public function editionFormProcess(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        // Check the object validity and permissions.
        $id = $request->request->get('id', 0);
        $user = $app['content_unit_factory']->create($type);
        if (!is_a($user, 'Viktor\Pack\Content\User\AbstractUser')) {
            $app->abort(404, 'Invalid user type.');
        }
        $user->setLanguage($app['i18n']->getLanguage());
        if ($id > 0) {
            // Existing user.
            $user->load($id);
            if ($user->getId() == 0) {
                $app->abort(404, 'Invalid user id.');
            }
            if (!$user->canBeEdited()) {
                $app->abort(403, 'You are not allowed to edit this user.');
            }
            $form_url = $user->getEditionPageUrl();
        } else {
            // New user.
            if (!$user->canBeCreated()) {
                $app->abort(403, 'You are not allowed to create this user.');
            }
            $form_url = $user->getCreationPageUrl();
        }
        // Prepare the form.
        $dataset_widget = new DatasetWidget(
            $user->getDatasetWidgetDescription(),
            $user->getDataset(),
            'user' // Prefix for the fields names.
        );
        $user_credentials_widget = new UserCredentialsWidget(
            $user->getUserCredentials(),
            'user' // Prefix for the fields names.
        );
        // Store the form data in session.
        $user_credentials_widget->storePrefilledData($request, $app);
        $dataset_widget->storePrefilledData($request, $app);
        // Set the new values.
        try {
            $user_credentials_widget->set($request, $app);
            $dataset_widget->set($request, $app);
        } catch (Exception $e) {
            $app['session']->getFlashBag()->add('error', $e->getMessage());
            return $app->redirect($form_url);
        }
        // Validate the form.
        $form_is_valid = true;
        $fields_widgets = $dataset_widget->getFieldsWidgets();
        foreach ($fields_widgets as $field_widget) {
            $field_language = $field_widget->getField()->getMetadata('language');
            // Language independant fields are always validated.
            // Translated fields are only validated for the published languages.
            if ($field_language == '' || $publication_widget->getPublication()->getPublished($field_language)) {
                $validation_result = $field_widget->validate($request, $app);
                if (!$validation_result) {
                    $form_is_valid = false;
                }
            }
        }
        $user_credentials_validation_result = $user_credentials_widget->validate($request, $app);
        if (!$user_credentials_validation_result) {
            $form_is_valid = false;
        }
        if (!$form_is_valid) {
            $app['session']->getFlashBag()->add('error', 'Some fields are invalid.');
            return $app->redirect($form_url);
        }
        // Check if the username is available.
        if (!$user_credentials_widget->usernameIsAvailable()) {
            $app['session']->getFlashBag()->add('error', 'This username is not available.');
            return $app->redirect($form_url);
        }
        // Save the user.
        if ($user->save($app['viktor.user'])) {
            $app['session']->getFlashBag()->add(
                'success',
                ucfirst($user->getSingularHumanType()) . ' ' . $user->getTitle() . ' saved successfully.'
            );
        } else {
            $app['session']->getFlashBag()->add('error', 'The ' . $user->getSingularHumanType() . ' could not be saved.');
        }
        // Remove the form data stored in session.
        $user_credentials_widget->removePrefilledData($request, $app);
        $dataset_widget->removePrefilledData($request, $app);
        // Everything is ok, back to the form.
        return $app->redirect($user->getEditionPageUrl());
    }

    public function deletionForm(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        // Check the object validity and permissions.
        $id = $request->query->get('id', 0);
        $user = $app['content_unit_factory']->load($type, $id);
        if (!is_a($user, 'Viktor\Pack\Content\User\AbstractUser')) {
            $app->abort(404, 'Could not load this user.');
        }
        $user->setLanguage($app['i18n']->getLanguage());
        if (!$user->canBeDeleted()) {
            $app->abort(403, 'You are not allowed to delete this user.');
        }
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $user->setLanguage($language_code);
            $language_urls[$language_code] = $user->getDeletionPageUrl();
        }
        $user->setLanguage($app['i18n']->getLanguage());
        $this->addMarker('user-content-unit');
        $this->addMarker($user->getType());
        $this->addMarker($user->getType() . ':' . $user->getId());
        $this->setTitle('Delete "' . $user->getTitle() . '"?');
        return $this->render(
            $app,
            '@Content/User/deletionForm.twig',
            array(
                'language'      => $app['i18n']->getLanguage(),
                'language_urls' => $language_urls,
                'user_digest'   => $user->getDigest(),
            )
        );
    }

    public function deletionFormProcess(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        // Check the object validity and permissions.
        $id = $request->request->get('id', 0);
        $user = $app['content_unit_factory']->load($type, $id);
        if (!is_a($user, 'Viktor\Pack\Content\User\AbstractUser')) {
            $app->abort(404, 'Could not load this user.');
        }
        $user->setLanguage($app['i18n']->getLanguage());
        if (!$user->canBeDeleted()) {
            $app->abort(403, 'You are not allowed to delete this user.');
        }
        $user_type = $user->getType();
        $user_title = $user->getTitle();
        $user_id = $user->getId();
        $user_index_url = $user->getListPageUrl();
        if ($user->delete()) {
            $app['session']->getFlashBag()->add(
                'success',
                $user_title . ' (' . $user_type . ' #' . $user_id . ') has been deleted.'
            );
        } else {
            $app['session']->getFlashBag()->add(
                'error',
                $user_title . ' (' . $user_type . ' #' . $user_id . ') could not be deleted.'
            );
        }
        return $app->redirect($user_index_url);
    }

}

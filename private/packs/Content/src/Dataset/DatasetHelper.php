<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Content\Dataset;

use Viktor\Application;

class DatasetHelper
{
    private $app;
    private $combinedDescription;

    public function __construct(Application $app, array $combined_description)
    {
        $this->app = $app;
        $this->combinedDescription = $combined_description;
        $this->combinedDescription = $this->prepareSeoFields($this->combinedDescription);
        $this->combinedDescription = $this->prepareSmartUrlFields($this->combinedDescription);
    }

    public function prepareSeoFields($combined_description)
    {
        if (!isset($combined_description['options']) || !is_array($combined_description['options'])) {
            return $combined_description;
        }
        if (!isset($combined_description['options']['add_seo_fields'])) {
            return $combined_description;
        }
        if (!$combined_description['options']['add_seo_fields']) {
            return $combined_description;
        }
        $combined_description['sections']['seo'] = array(
            'label' => 'SEO',
            'collapse' => true,
        );
        $combined_description['fields']['seo_title'] = array(
            'translatable' => true,
            'model' => array(
                'class_name' => 'Viktor\Pack\Base\Field\TextField',
                'options'    => array(
                    'indexed' => false,
                    'source_type' => 'plain_text',
                ),
                'full_text_index_weights' => array(
                    'general' => 1,
                ),
            ),
            'widget' => array(
                'class_name' => 'Viktor\Pack\Admin\Field\TextFieldWidget',
                'options'    => array(
                    'label' => 'SEO title',
                    'searchable' => false,
                    'sortable' => false,
                    'legend' => 'This title will be used for search engines and social medias only.',
                    'section' => 'seo',
                ),
            ),
        );
        $combined_description['fields']['seo_description'] = array(
            'translatable' => true,
            'model' => array(
                'class_name' => 'Viktor\Pack\Base\Field\TextField',
                'options'    => array(
                    'indexed' => false,
                    'authorized_tags' => '<p><strong><em><sub><sup><br>',
                    'source_type' => 'html',
                ),
                'full_text_index_weights' => array(
                    'general' => 2,
                ),
            ),
            'widget' => array(
                'class_name' => 'Viktor\Pack\Admin\Field\TextFieldWidget',
                'options'    => array(
                    'label' => 'SEO description',
                    'legend' => 'This description will be used for search engines and social medias only.',
                    'searchable' => false,
                    'sortable' => false,
                    'section' => 'seo',
                    'ckeditor_config_file_url' => '/assets/Admin/cke/teaser-cke-config.js'
                ),
            )
        );
        $combined_description['fields']['seo_image'] = array(
            'translatable' => false,
            'model' => array(
                'class_name' => 'Viktor\Pack\Base\Field\ImageField',
                'options'    => array(
                    'max_number' => 1,
                ),
            ),
            'widget' => array(
                'class_name' => 'Viktor\Pack\Admin\Field\ImageFieldWidget',
                'options'    => array(
                    'label' => 'SEO preview image',
                    'legend' => 'This image will be used for search engines and social medias only.',
                    'section' => 'seo',
                ),
            ),
        );
        return $combined_description;
    }

    public function prepareSmartUrlFields($combined_description)
    {
        if (!isset($combined_description['options']) || !is_array($combined_description['options'])) {
            return $combined_description;
        }
        if (!isset($combined_description['options']['add_smart_url_fields'])) {
            return $combined_description;
        }
        if (!$combined_description['options']['add_smart_url_fields']) {
            return $combined_description;
        }
        $combined_description['sections']['smart_url'] = array(
            'label' => 'Smart URL',
            'collapse' => true,
        );
        $combined_description['fields']['smart_url'] = array(
            'translatable' => true,
            'model' => array(
                'class_name' => 'Viktor\Pack\Base\Field\TextField',
                'options'    => array(
                    'indexed' => false,
                    'source_type' => 'plain_text',
                ),
            ),
            'widget' => array(
                'class_name' => 'Viktor\Pack\Admin\Field\TextFieldWidget',
                'options'    => array(
                    'label' => 'Smart URL',
                    'searchable' => false,
                    'sortable' => false,
                    'section' => 'smart_url',
                ),
            ),
        );
        return $combined_description;
    }

    public function getDatasetDescription()
    {
        $dataset_description = array();
        foreach ($this->combinedDescription['fields'] as $field_name => $field_description) {
            if (isset($field_description['translatable']) && $field_description['translatable']) {
                foreach ($this->app['viktor.config']['available_languages'] as $language_code => $language_name) {
                    $metadata = array();
                    if (isset($field_description['model']['metadata'])) {
                        $metadata = $field_description['model']['metadata'];
                    }
                    $metadata['language'] = $language_code;
                    $metadata['base_name'] = $field_name;
                    $metadata['translatable'] = true;
                    $full_text_index_weights = array();
                    if (isset($field_description['model']['full_text_index_weights'])) {
                        foreach ($field_description['model']['full_text_index_weights'] as $index_id => $weight) {
                            $full_text_index_weights[$index_id . '_' . $language_code] = $weight;
                        }
                    }
                    $dataset_description[$field_name . '_' . $language_code] = array(
                        'class_name' => $field_description['model']['class_name'],
                        'options' => $field_description['model']['options'],
                        'metadata' => $metadata,
                        'full_text_index_weights' => $full_text_index_weights,
                    );
                }
            } else {
                $metadata = array();
                if (isset($field_description['model']['metadata'])) {
                    $metadata = $field_description['model']['metadata'];
                }
                $metadata['language'] = '';
                $metadata['base_name'] = $field_name;
                $metadata['translatable'] = false;
                $full_text_index_weights = array();
                if (isset($field_description['model']['full_text_index_weights'])) {
                    foreach ($field_description['model']['full_text_index_weights'] as $index_id => $weight) {
                        foreach ($this->app['viktor.config']['available_languages'] as $language_code => $language_name) {
                            $full_text_index_weights[$index_id . '_' . $language_code] = $weight;
                        }
                    }
                }
                $dataset_description[$field_name] = array(
                    'class_name' => $field_description['model']['class_name'],
                    'options' => $field_description['model']['options'],
                    'metadata' => $metadata,
                    'full_text_index_weights' => $full_text_index_weights,
                );
            }
        }
        return $dataset_description;
    }

    public function getDatasetWidgetDescription($language = '')
    {
        $dataset_widget_description = array();
        $dataset_widget_description['sections'] = array();
        $dataset_widget_description['sections'][''] = array(
            'label' => 'Main',
            'collapse' => false,
        );
        if (!empty($this->combinedDescription['sections'])) {
            foreach ($this->combinedDescription['sections'] as $section_name => $section_description) {
                $dataset_widget_description['sections'][$section_name] = $section_description;
            }
        }
        $dataset_widget_description['fields'] = array();
        foreach ($this->combinedDescription['fields'] as $field_name => $field_description) {
            if (isset($field_description['translatable']) && $field_description['translatable']) {
                foreach ($this->app['viktor.config']['available_languages'] as $language_code => $language_name) {
                    if ($language == '' || $language == $language_code) {
                        $options = $field_description['widget']['options'];
                        if (count($this->app['viktor.config']['available_languages']) > 1) {
                            $options['label'] = $options['label'] . ' (' . $language_code . ')';
                        }
                        $dataset_widget_description['fields'][$field_name . '_' . $language_code] = array(
                            'class_name' => $field_description['widget']['class_name'],
                            'options'    => $options,
                        );
                    }
                }
            } else {
                $dataset_widget_description['fields'][$field_name] = array(
                    'class_name' => $field_description['widget']['class_name'],
                    'options'    => $field_description['widget']['options'],
                );
            }
        }
        return $dataset_widget_description;
    }

}

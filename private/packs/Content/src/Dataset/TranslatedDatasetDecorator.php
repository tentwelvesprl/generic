<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Content\Dataset;

use Viktor\Application;
use Viktor\Pack\Base\Dataset\DatasetDecorator;
use Viktor\Pack\Base\DbStore\DbStoreComponentInterface;
use Viktor\Pack\Content\Dataset\TranslatedDataset;

class TranslatedDatasetDecorator extends DatasetDecorator
{
    public function __construct(DbStoreComponentInterface $component, TranslatedDataset $dataset)
    {
        parent::__construct($component, $dataset);
    }

    public function getDigest()
    {
        $digest = $this->getComponent()->getDigest();
        $fields = $this->getDataset()->getFields();
        foreach ($fields as $field_name => $field) {
            $base_name = $field->getMetadata('base_name');
            if ($field->getMetadata('language') == '' || $field->getMetadata('language') == $this->getDataset()->getLanguage()) {
                $digest[$base_name] = $field->getDigest();
            }
        }
        return $digest;
    }

}

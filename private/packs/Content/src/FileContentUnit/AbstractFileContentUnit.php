<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Content\FileContentUnit;

use DateTime;
use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Base\Badge\Badge;
use Viktor\Pack\Base\FullTextIndex\Tokenizer;
use Viktor\Pack\Base\Attachment\HasAnAttachmentInterface;
use Viktor\Pack\Base\Attachment\Attachment;
use Viktor\Pack\Base\Attachment\AttachmentDecorator;
use Viktor\Pack\Base\FullTextIndex\FullTextIndexer;
use Viktor\Pack\Base\User\UserInterface;
use Viktor\Pack\Content\ContentUnit\AbstractContentUnit;

abstract class AbstractFileContentUnit extends AbstractContentUnit implements HasAnAttachmentInterface
{
    private $attachment;

    public function addToIndex(FullTextIndexer $indexer)
    {
        foreach ($this->getApp()['viktor.config']['available_languages'] as $language => $language_name) {
            $this->setLanguage($language);
            $s = basename($this->getAttachmentFilePath());
            $s = str_replace('_', ' ', $s);
            $s = str_replace('-', ' ', $s);
            $tokenizer = new Tokenizer($this->getTitle(), $language);
            $tokens = $tokenizer->getTokens();
            $indexer->addTokens($tokens, 1, 'general_' . $language);
            $indexer->addTokens($tokens, 100, 'file_' . $language);
        }
        parent::addToIndex($indexer);
    }

    public function getTableName()
    {
        return 'file';
    }

    public function prepareNewInstance()
    {
        $this->attachment = new Attachment($this->getApp(), $this, $this->getAttachmentOptions());
        $this->dbStoreComponent = new AttachmentDecorator($this->dbStoreComponent, $this->attachment);
        $this->dbStoreComponent = new FileContentUnitDecorator($this->dbStoreComponent);
    }

    public function getDigest()
    {
        $digest = parent::getDigest();
        $digest['_duplication_page_url'] = $this->getDuplicationPageUrl();
        return $digest;
    }

    public function getViewPageUrl()
    {
        if ($this->getId() > 0 && $this->getAttachment()->fileCanBeRead()) {
            return $this->getApp()->path('attachment-from-content-unit', array(
                'language'    => $this->getLanguage(),
                'type'        => $this->getType(),
                'id'          => $this->getId(),
                'disposition' => 'inline',
            ));
        } else {
            return false;
        }
    }

    public function getDuplicationPageUrl()
    {
        return $this->getApp()->path('admin-file-duplication-form', array(
            'language' => $this->getLanguage(),
            'type'     => $this->getType(),
            'id'       => $this->getId(),
        ));
    }

    public function getTitle()
    {
        $title = basename($this->getAttachment()->getFilePath());
        return $title;
    }

    public function getImage()
    {
        return $this->getAttachment()->getSourceImage();
    }

    public function getDatasetCombinedDescription()
    {
        $sections = array(
            '' => array(
                'label' => 'File information',
                'collapse' => false,
            )
        );
        $fields = array(
            'title' => array(
                'translatable' => true,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\TextField',
                    'options'    => array(
                        'indexed' => true,
                        'source_type' => 'plain_text',
                    ),
                    'full_text_index_weights' => array(
                        'general' => 1,
                        'file' => 1,
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\TextFieldWidget',
                    'options'    => array(
                        'label' => 'Title',
                        'searchable' => true,
                        'sortable' => false,
                        'section' => '',
                    ),
                ),
            ),
        );
        return array(
            'sections' => $sections,
            'fields' => $fields
        );
    }

    public function getAttachmentOptions()
    {
        $options = array();
        return $options;
    }

    public function getAttachment()
    {
        return $this->attachment;
    }

    public function getAttachmentFilePath()
    {
        return $this->attachment->getFilePath();
    }

    public function setAttachmentFilePath($file_path)
    {
        $this->attachment->setFilePath($file_path);
    }

    public function renderListItem(Request $request, Application $app)
    {
        $referencing_content_units_array = $this->getDbStoreComponent()->getReferencingContentUnits();
        $referencing_content_units_count = count($referencing_content_units_array);
        return $app['twig']->render(
            '@Content/FileContentUnit/listItem.twig',
            array(
                'file_content_unit_digest' => $this->getDigest(),
                'language' => $this->getLanguage(),
                'referencing_content_units' => $referencing_content_units_array,
                'referencing_content_units_count' => $referencing_content_units_count,
            )
        );
    }

    public function getAdditionalEditionFormHeaderView()
    {
        return '';
    }

    public function getAdditionalEditionFormFooterView()
    {
        return '';
    }

    public function getAdditionalEditionFormAsideView()
    {
        return '';
    }

    public function duplicate()
    {
        $new_file_content_unit = parent::duplicate();
        $new_file_content_unit->getAttachment()->copy($this->getAttachment());
        return $new_file_content_unit;
    }

}

<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Content\FileContentUnit;

use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Admin\Admin\AdminHtmlPage;
use Viktor\Pack\Admin\Dataset\DatasetWidget;
use Viktor\Pack\Admin\DbStore\DbStoreComponentWidget;
use Viktor\Pack\Admin\Attachment\AttachmentWidget;
use Viktor\Pack\Admin\Pagination\PaginationWidget;
use Viktor\Pack\Admin\SortOptions\SortOptionsWidget;
use Viktor\Pack\Base\I18n\TranslatableInterface;
use Viktor\Pack\Base\Pagination\Pagination;
use Viktor\Pack\Base\User\UnknownUser;

class FileContentUnitAppController extends AdminHtmlPage
{
    public function index(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        $file_content_unit_model = $app['content_unit_factory']->create($type);
        if (!is_a($file_content_unit_model, 'Viktor\Pack\Content\FileContentUnit\AbstractFileContentUnit')) {
            $app->abort(404, $type . ' is not a valid file content unit type.');
        }
        $file_content_unit_model->setLanguage($app['i18n']->getLanguage());
        if (!$file_content_unit_model->canBeListed()) {
            $app->abort(403, 'You are not allowed to list these elements.');
        }
        // Prepare the widgets.
        $db_store_component_widget = new DbStoreComponentWidget(
            $file_content_unit_model->getDbStoreComponent(),
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        $attachment_widget = new AttachmentWidget(
            $file_content_unit_model->getAttachment(),
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        $file_content_unit_widget = new FileContentUnitWidget(
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        $dataset_widget = new DatasetWidget(
            $file_content_unit_model->getDatasetWidgetDescription($language),
            $file_content_unit_model->getDataset(),
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        $sort_options = array();
        $sort_options = array_merge($sort_options, $dataset_widget->getSortOptions());
        $sort_options = array_merge($sort_options, $attachment_widget->getSortOptions());
        $sort_options = array_merge($sort_options, $file_content_unit_widget->getSortOptions());
        $sort_options = array_merge($sort_options, $db_store_component_widget->getSortOptions());
        $sort_options_widget = new SortOptionsWidget(
            $sort_options,
            $file_content_unit_model,
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        // Store the advanced search form data in session.
        $attachment_widget->storeAdvancedSearchData($request, $app);
        $file_content_unit_widget->storeAdvancedSearchData($request, $app);
        $dataset_widget->storeAdvancedSearchData($request, $app);
        // Filters.
        $filters = array();
        $filters = array_merge($filters, $attachment_widget->getFilters($request, $app));
        $filters = array_merge($filters, $dataset_widget->getFilters($request, $app));
        $filters[] = array(
            'type' => 'deduplicate',
            'value' => true,
        );
        // Sort options.
        $sort_options_widget->storeSelectedSortOption($request, $app);
        // Go!
        $file_content_units_ids = $file_content_unit_model->getIds(
            $filters,
            array($sort_options_widget->getSelectedSortOption($request, $app))
        );
        // Apply pagination.
        $pagination = new Pagination(
            $request,
            $app,
            count($file_content_units_ids),
            20,
            1,
            array(20, 50, 100),
            $type . '_' . $app['i18n']->getLanguage()
        );
        $pagination_widget = new PaginationWidget(
            $pagination,
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        $pagination_widget->storePaginationParameters($request, $app);
        $pagination_parameters = $pagination_widget->getStoredPaginationParameters($app);
        $pagination->setCurrentPage($pagination_parameters['current_page']);
        $pagination->setNbItemsPerPage($pagination_parameters['nb_items_per_page']);
        $file_content_units_ids_to_display = array_slice(
            $file_content_units_ids,
            ($pagination_parameters['current_page'] - 1) * $pagination_parameters['nb_items_per_page'],
            $pagination_parameters['nb_items_per_page']
        );
        // Load file content units.
        $file_content_units_list = $app['content_unit_factory']->createList($type, $file_content_units_ids_to_display);
        foreach ($file_content_units_list as $file_content_unit) {
            $file_content_unit->setLanguage($app['i18n']->getLanguage());
        }
        // Active filters description.
        $active_filters_description = array();
        $active_filters_description = array_merge($active_filters_description, $dataset_widget->getActiveFiltersDescription($request, $app));
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $file_content_unit_model->setLanguage($language_code);
            $language_urls[$language_code] = $file_content_unit_model->getListPageUrl();
        }
        $file_content_unit_model->setLanguage($app['i18n']->getLanguage());
        $this->addMarker('file-content-unit');
        $this->addMarker($file_content_unit_model->getType());
        $this->setTitle(ucfirst($file_content_unit_model->getPluralHumanType()));
        return $this->render(
            $app,
            '@Content/FileContentUnit/index.twig',
            array(
                'file_content_unit_model' => $file_content_unit_model->getDigest(),
                'file_content_units'  => $file_content_units_list,
                'attachment' => $file_content_unit_model->getAttachment(),
                'attachment_widget' => $attachment_widget,
                'dataset' => $file_content_unit_model->getDataset(),
                'dataset_widget' => $dataset_widget,
                'pagination' => $pagination,
                'pagination_widget' => $pagination_widget,
                'sort_options_widget' => $sort_options_widget,
                'active_filters_description' => $active_filters_description,
                'language_urls' => $language_urls,
                'language' => $app['i18n']->getLanguage(),
            )
        );
    }

    public function editionForm(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        $id = $request->query->get('id', 0);
        $file_content_unit = $app['content_unit_factory']->create($type);
        if (!is_a($file_content_unit, 'Viktor\Pack\Content\FileContentUnit\AbstractFileContentUnit')) {
            $app->abort(404, 'Invalid file content unit type.');
        }
        $file_content_unit->setLanguage($app['i18n']->getLanguage());
        if ($id > 0) {
            // Existing file content unit.
            $file_content_unit->load($id);
            if ($file_content_unit->getId() == 0) {
                $app->abort(404, 'Invalid file content unit id.');
            }
            if (!$file_content_unit->canBeViewed()) {
                $app->abort(403, 'You are not allowed to view this file content unit.');
            }
        } else {
            // New file content unit.
            if (!$file_content_unit->canBeCreated()) {
                $app->abort(403, 'You are not allowed to create this file content unit.');
            }
        }
        $creation_user = $app['content_unit_factory']->load(
            $file_content_unit->getCreationUserType(),
            $file_content_unit->getCreationUserId()
        );
        if ($creation_user == null) {
            $creation_user = new UnknownUser($app);
        }
        if ($creation_user instanceof TranslatableInterface) {
            $creation_user->setLanguage($app['i18n']->getLanguage());
        }
        $modification_user = $app['content_unit_factory']->load(
            $file_content_unit->getModificationUserType(),
            $file_content_unit->getModificationUserId()
        );
        if ($modification_user == null) {
            $modification_user = new UnknownUser($app);
        }
        if ($modification_user instanceof TranslatableInterface) {
            $modification_user->setLanguage($app['i18n']->getLanguage());
        }
        $attachment_widget = new AttachmentWidget(
            $file_content_unit->getAttachment(),
            'file_content_unit' // Prefix for the fields names.
        );
        $dataset_widget = new DatasetWidget(
            $file_content_unit->getDatasetWidgetDescription(),
            $file_content_unit->getDataset(),
            'file_content_unit' // Prefix for the fields names.
        );
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $file_content_unit->setLanguage($language_code);
            if ($file_content_unit->getId() == 0) {
                $language_urls[$language_code] = $file_content_unit->getCreationPageUrl();
            } else {
                $language_urls[$language_code] = $file_content_unit->getEditionPageUrl();
            }
        }
        // Content units that have a link to this content unit.
        $referencing_content_units_array = $file_content_unit->getDbStoreComponent()->getReferencingContentUnits();
        $referencing_content_units_count = count($referencing_content_units_array);
        $referencing_content_units = array();
        foreach ($referencing_content_units_array as $v) {
            if (count($referencing_content_units_array) >= 20) {
                break;
            }
            $referencing_content_unit = $app['content_unit_factory']->load($v['type'], $v['id']);
            if ($referencing_content_unit != null) {
                $referencing_content_units[] = $referencing_content_unit;
            }
        }
        $file_content_unit->setLanguage($app['i18n']->getLanguage());
        $this->addMarker('file-content-unit');
        $this->addMarker($file_content_unit->getType());
        $this->addMarker($file_content_unit->getType() . ':' . $file_content_unit->getId());
        $this->setTitle($file_content_unit->getTitle());
        return $this->render(
            $app,
            '@Content/FileContentUnit/editionForm.twig',
            array(
                'file_content_unit_digest' => $file_content_unit->getDigest(),
                'dataset' => $file_content_unit->getDataset(),
                'dataset_widget' => $dataset_widget,
                'attachment' => $file_content_unit->getAttachment(),
                'attachment_widget' => $attachment_widget,
                'creation_user' => $creation_user->getDigest(),
                'modification_user' => $modification_user->getDigest(),
                'language_urls' => $language_urls,
                'language' => $app['i18n']->getLanguage(),
                'referencing_content_units' => $referencing_content_units,
                'referencing_content_units_count' => $referencing_content_units_count,
                'additional_edition_form_header_view' => $file_content_unit->getAdditionalEditionFormHeaderView(),
                'additional_edition_form_footer_view' => $file_content_unit->getAdditionalEditionFormFooterView(),
                'additional_edition_form_aside_view' => $file_content_unit->getAdditionalEditionFormAsideView(),
            )
        );
    }

    public function editionFormProcess(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        // Check the object validity and permissions.
        $id = $request->request->get('id', 0);
        $file_content_unit = $app['content_unit_factory']->create($type);
        if (!is_a($file_content_unit, 'Viktor\Pack\Content\FileContentUnit\AbstractFileContentUnit')) {
            $app->abort(404, 'Invalid file content unit type.');
        }
        $file_content_unit->setLanguage($app['i18n']->getLanguage());
        if ($id > 0) {
            // Existing file content unit.
            $file_content_unit->load($id);
            if ($file_content_unit->getId() == 0) {
                $app->abort(404, 'Invalid file content unit id.');
            }
            if (!$file_content_unit->canBeEdited()) {
                $app->abort(403, 'You are not allowed to edit this file content unit.');
            }
            $form_url = $file_content_unit->getEditionPageUrl();
        } else {
            // New file content unit.
            if (!$file_content_unit->canBeCreated()) {
                $app->abort(403, 'You are not allowed to create this file content unit.');
            }
            $form_url = $file_content_unit->getCreationPageUrl();
        }
        // Prepare the form.
        $attachment_widget = new AttachmentWidget(
            $file_content_unit->getAttachment(),
            'file_content_unit' // Prefix for the fields names.
        );
        $dataset_widget = new DatasetWidget(
            $file_content_unit->getDatasetWidgetDescription(),
            $file_content_unit->getDataset(),
            'file_content_unit' // Prefix for the fields names.
        );
        // Store the form data in session.
        $attachment_widget->storePrefilledData($request, $app);
        $dataset_widget->storePrefilledData($request, $app);
        // Set the new values.
        $dataset_widget->set($request, $app);
        // Validate the form.
        $form_is_valid = true;
        $fields_widgets = $dataset_widget->getFieldsWidgets();
        foreach ($fields_widgets as $field_widget) {
            $validation_result = $field_widget->validate($request, $app);
            if (!$validation_result) {
                $form_is_valid = false;
            }
        }
        if (!$form_is_valid) {
            $app['session']->getFlashBag()->add('error', 'Some fields are invalid.');
            return $app->redirect($form_url);
        }
        // Set the uploaded file content unit content (may copy a file the data dir).
        $attachment_widget->set($request, $app);
        // Save the file content unit.
        if ($file_content_unit->save($app['viktor.user'])) {
            $app['session']->getFlashBag()->add(
                'success',
                ucfirst($file_content_unit->getSingularHumanType()) . ' ' . $file_content_unit->getTitle() . ' saved successfully.'
            );
        } else {
            $app['session']->getFlashBag()->add('error', 'The ' . $file_content_unit->getSingularHumanType() . ' could not be saved.');
        }
        // Remove the form data stored in session.
        $attachment_widget->removePrefilledData($request, $app);
        $dataset_widget->removePrefilledData($request, $app);
        // Everything is ok, back to the form.
        return $app->redirect($file_content_unit->getEditionPageUrl());
    }

    public function deletionForm(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        // Check the object validity and permissions.
        $id = $request->query->get('id', 0);
        $file_content_unit = $app['content_unit_factory']->load($type, $id);
        if (!is_a($file_content_unit, 'Viktor\Pack\Content\FileContentUnit\AbstractFileContentUnit')) {
            $app->abort(404, 'Could not load this file content unit.');
        }
        $file_content_unit->setLanguage($app['i18n']->getLanguage());
        if (!$file_content_unit->canBeDeleted()) {
            $app->abort(403, 'You are not allowed to delete this file content unit.');
        }
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $file_content_unit->setLanguage($language_code);
            $language_urls[$language_code] = $file_content_unit->getDeletionPageUrl();
        }
        // Content units that have a link to this content unit.
        $referencing_content_units_array = $file_content_unit->getDbStoreComponent()->getReferencingContentUnits();
        $referencing_content_units_count = count($referencing_content_units_array);
        $referencing_content_units = array();
        foreach ($referencing_content_units_array as $v) {
            if (count($referencing_content_units_array) >= 20) {
                break;
            }
            $referencing_content_unit = $app['content_unit_factory']->load($v['type'], $v['id']);
            if ($referencing_content_unit != null) {
                $referencing_content_units[] = $referencing_content_unit;
            }
        }
        $file_content_unit->setLanguage($app['i18n']->getLanguage());
        $this->addMarker('file-content-unit');
        $this->addMarker($file_content_unit->getType());
        $this->addMarker($file_content_unit->getType() . ':' . $file_content_unit->getId());
        $this->setTitle('Delete "' . $file_content_unit->getTitle() . '"?');
        return $this->render(
            $app,
            '@Content/FileContentUnit/deletionForm.twig',
            array(
                'file_content_unit_digest' => $file_content_unit->getDigest(),
                'language_urls' => $language_urls,
                'language' => $app['i18n']->getLanguage(),
                'referencing_content_units' => $referencing_content_units,
                'referencing_content_units_count' => $referencing_content_units_count,
            )
        );
    }

    public function deletionFormProcess(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        // Check the object validity and permissions.
        $id = $request->request->get('id', 0);
        $file_content_unit = $app['content_unit_factory']->load($type, $id);
        if (!is_a($file_content_unit, 'Viktor\Pack\Content\FileContentUnit\AbstractFileContentUnit')) {
            $app->abort(404, 'Could not load this file content unit.');
        }
        $file_content_unit->setLanguage($app['i18n']->getLanguage());
        if (!$file_content_unit->canBeDeleted()) {
            $app->abort(403, 'You are not allowed to delete this file content unit.');
        }
        $file_content_unit_type = $file_content_unit->getType();
        $file_content_unit_title = $file_content_unit->getTitle();
        $file_content_unit_id = $file_content_unit->getId();
        $file_content_unit_index_url = $file_content_unit->getListPageUrl();
        if ($file_content_unit->delete()) {
            $app['session']->getFlashBag()->add(
                'success',
                $file_content_unit_title . ' (' . $file_content_unit_type . ' #' . $file_content_unit_id . ') has been deleted.'
            );
        } else {
            $app['session']->getFlashBag()->add(
                'error',
                $file_content_unit_title . ' (' . $file_content_unit_type . ' #' . $file_content_unit_id . ') could not be deleted.'
            );
        }
        return $app->redirect($file_content_unit_index_url);
    }

    public function duplicationForm(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        // Check the object validity and permissions.
        $id = $request->query->get('id', 0);
        $file_content_unit = $app['content_unit_factory']->load($type, $id);
        if (!is_a($file_content_unit, 'Viktor\Pack\Content\FileContentUnit\AbstractFileContentUnit')) {
            $app->abort(404, 'Could not load this file content unit.');
        }
        $file_content_unit->setLanguage($app['i18n']->getLanguage());
        if (!$file_content_unit->canBeViewed() || !$file_content_unit->canBeCreated()) {
            $app->abort(403, 'You are not allowed to duplicate this file content unit.');
        }
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $file_content_unit->setLanguage($language_code);
            $language_urls[$language_code] = $file_content_unit->getDuplicationPageUrl();
        }
        // Content units that have a link to this content unit.
        $referencing_content_units_array = $file_content_unit->getDbStoreComponent()->getReferencingContentUnits();
        $referencing_content_units_count = count($referencing_content_units_array);
        $referencing_content_units = array();
        foreach ($referencing_content_units_array as $v) {
            if (count($referencing_content_units_array) >= 20) {
                break;
            }
            $referencing_content_unit = $app['content_unit_factory']->load($v['type'], $v['id']);
            if ($referencing_content_unit != null) {
                $referencing_content_units[] = $referencing_content_unit;
            }
        }
        $file_content_unit->setLanguage($app['i18n']->getLanguage());
        $this->addMarker('file-content-unit');
        $this->addMarker($file_content_unit->getType());
        $this->addMarker($file_content_unit->getType() . ':' . $file_content_unit->getId());
        $this->setTitle('Duplicate "' . $file_content_unit->getTitle() . '"?');
        return $this->render(
            $app,
            '@Content/FileContentUnit/duplicationForm.twig',
            array(
                'file_content_unit_digest' => $file_content_unit->getDigest(),
                'language_urls'  => $language_urls,
                'language'       => $app['i18n']->getLanguage(),
                'referencing_content_units' => $referencing_content_units,
                'referencing_content_units_count' => $referencing_content_units_count,
            )
        );
    }

    public function duplicationFormProcess(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        // Check the object validity and permissions.
        $id = $request->request->get('id', 0);
        $file_content_unit = $app['content_unit_factory']->load($type, $id);
        if (!is_a($file_content_unit, 'Viktor\Pack\Content\FileContentUnit\AbstractFileContentUnit')) {
            $app->abort(404, 'Could not load this file content unit.');
        }
        $file_content_unit->setLanguage($app['i18n']->getLanguage());
        if (!$file_content_unit->canBeViewed() || !$file_content_unit->canBeCreated()) {
            $app->abort(403, 'You are not allowed to duplicate this file content unit.');
        }
        $new_file_content_unit = $file_content_unit->duplicate();
        if ($new_file_content_unit->save($app['viktor.user'])) {
            $app['session']->getFlashBag()->add(
                'success',
                ucfirst($file_content_unit->getSingularHumanType()) . ' ' . $file_content_unit->getTitle() . ' duplicated successfully.'
            );
        } else {
            $app['session']->getFlashBag()->add('error', 'The ' . $file_content_unit->getSingularHumanType() . ' could not be duplicated.');
        }
        return $app->redirect($file_content_unit->getListPageUrl());
    }

}

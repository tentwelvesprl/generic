<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Content\Controller;

use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Viktor\Application;
use Viktor\Pack\Base\I18n\TranslatableInterface;

class HtmlPage
{
    private $app;
    private $request;
    private $language;
    private $siteName;
    private $title;
    private $type;
    private $description;
    private $image;
    private $url;
    private $alternateUrls;
    private $preferredLanguage;

    public function __construct()
    {
        $this->language = '';
        $this->siteName = '';
        $this->title = '';
        $this->type = 'website';
        $this->description = '';
        $this->image = '';
        $this->url = '';
        $this->alternateUrls = array();
        $this->preferredLanguage = '';
    }

    public function init(Request $request, Application $app)
    {
        $this->request = $request;
        $this->app = $app;
        $available_languages = array_keys($app['viktor.config']['available_languages']);
        $default_language = $available_languages[0];
        $this->preferredLanguage = $request->cookies->get('preferred_language', $default_language);
        $this->language = $this->preferredLanguage;
    }

    public function getApp()
    {
        return $this->app;
    }

    public function getRequest()
    {
        return $this->request;
    }

    public function getLanguage()
    {
        return $this->language;
    }

    public function setLanguage($language)
    {
        $this->language = (string) $language;
        $this->setPreferredLanguage($language);
    }

    public function getSiteName()
    {
        return $this->siteName;
    }

    public function setSiteName($site_name)
    {
        $this->siteName = (string) $site_name;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = (string) $type;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = (string) $title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = (string) $description;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = (string) $image;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = (string) $url;
    }

    public function getAlternateUrls()
    {
        return $this->alternateUrls;
    }

    public function setAlternateUrl($language, $url)
    {
        $this->alternateUrls[(string) $language] = (string) $url;
    }

    public function getPreferredLanguage()
    {
        return $this->preferredLanguage;
    }

    public function setPreferredLanguage($language)
    {
        $this->preferredLanguage = $language;
    }

    public function render($view, array $variables, Response $response = null)
    {
        if ($response === null) {
            $response = new Response();
        }
        $cookie = new Cookie('preferred_language', $this->getPreferredLanguage(), time() + 3600 * 24 * 30);
        $response->headers->setCookie($cookie);
        if ($this->getSiteName() == '') {
            $this->setSiteName($this->getApp()['viktor.config']['website_title']);
        }
        $main_content_unit = $this->getApp()['main_content_unit']->get();
        $main_content_unit_digest = null;
        if ($main_content_unit != null) {
            if ($this->getTitle() == '') {
                $this->setTitle($main_content_unit->getTitle());
            }
            if ($this->getDescription() == '') {
                $this->setDescription($main_content_unit->getDescription());
            }
            if ($this->getImage() == '' && $main_content_unit->getImage() != null) {
                $this->setImage($this->getApp()->url(
                    'image-from-content-unit',
                    array(
                        'language' => $main_content_unit->getLanguage(),
                        'format' => 'og',
                        'type' => $main_content_unit->getType(),
                        'id' => $main_content_unit->getId(),
                    )
                ));
            }
            if ($this->getUrl() == '') {
                $this->setUrl($main_content_unit->getViewPageUrl());
            }
            if ($main_content_unit instanceof TranslatableInterface) {
                $alternate_urls = $this->getAlternateUrls();
                $main_content_unit_language = $main_content_unit->getLanguage();
                foreach ($this->getApp()['viktor.config']['available_languages'] as $language => $language_name) {
                    if (!isset($alternate_urls[$language]) && $language != $this->getLanguage()) {
                        $main_content_unit->setLanguage($language);
                        if ($main_content_unit->canBeViewed()) {
                            $this->setAlternateUrl($language, $main_content_unit->getViewPageUrl());
                        }
                    }
                }
                $main_content_unit->setLanguage($main_content_unit_language);
            }
            $main_content_unit_digest = $main_content_unit->getDigest();
        }
        // Set page common variables.
        $default_variables = array(
            'language' => $this->getLanguage(),
            'site_name' => $this->getSiteName(),
            'type' => $this->getType(),
            'title' => $this->getTitle(),
            'description' => $this->getDescription(),
            'image' => $this->getImage(),
            'url' => $this->getUrl(),
            'alternate_urls' => $this->getAlternateUrls(),
            'main_content_unit' => $main_content_unit_digest,
        );
        $variables = array_merge($default_variables, $variables);
        return $this->getApp()->render(
            $view,
            $variables,
            $response
        );
    }

}

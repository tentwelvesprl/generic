<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Content\Article;

use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Admin\Admin\AdminHtmlPage;
use Viktor\Pack\Admin\Dataset\DatasetWidget;
use Viktor\Pack\Admin\DbStore\DbStoreComponentWidget;
use Viktor\Pack\Admin\Publication\PublicationWidget;
use Viktor\Pack\Admin\Pagination\PaginationWidget;
use Viktor\Pack\Admin\SortOptions\SortOptionsWidget;
use Viktor\Pack\Base\I18n\TranslatableInterface;
use Viktor\Pack\Base\Pagination\Pagination;
use Viktor\Pack\Base\User\UnknownUser;

class ArticleAppController extends AdminHtmlPage
{
    public function index(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        $article_model = $app['content_unit_factory']->create($type);
        if (!is_a($article_model, 'Viktor\Pack\Content\Article\AbstractArticle')) {
            $app->abort(404, $type . ' is not a valid article type.');
        }
        $article_model->setLanguage($app['i18n']->getLanguage());
        if (!$article_model->canBeListed()) {
            $app->abort(403, 'You are not allowed to list these elements.');
        }
        // Prepare the widgets.
        $db_store_component_widget = new DbStoreComponentWidget(
            $article_model->getDbStoreComponent(),
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        $dataset_widget = new DatasetWidget(
            $article_model->getDatasetWidgetDescription($language),
            $article_model->getDataset(),
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        $publication_widget = new PublicationWidget(
            $article_model->getPublication(),
            $language,
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        $sort_options = array();
        $sort_options = array_merge($sort_options, $dataset_widget->getSortOptions());
        $sort_options = array_merge($sort_options, $publication_widget->getSortOptions());
        $sort_options = array_merge($sort_options, $db_store_component_widget->getSortOptions());
        $sort_options_widget = new SortOptionsWidget(
            $sort_options,
            $article_model,
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        // Store the advanced search form data in session.
        $dataset_widget->storeAdvancedSearchData($request, $app);
        $publication_widget->storeAdvancedSearchData($request, $app);
        // Filters.
        $filters = array();
        $filters = array_merge($filters, $dataset_widget->getFilters($request, $app));
        $filters = array_merge($filters, $publication_widget->getFilters($request, $app));
        $filters[] = array(
            'type' => 'deduplicate',
            'value' => true,
        );
        // Sort options.
        $sort_options_widget->storeSelectedSortOption($request, $app);
        // Go!
        $articles_ids = $article_model->getIds(
            $filters,
            array($sort_options_widget->getSelectedSortOption($request, $app))
        );
        // Apply pagination.
        $pagination = new Pagination(
            $request,
            $app,
            count($articles_ids),
            10,
            1,
            array(10, 20, 50),
            $type . '_' . $app['i18n']->getLanguage()
        );
        $pagination_widget = new PaginationWidget(
            $pagination,
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        $pagination_widget->storePaginationParameters($request, $app);
        $pagination_parameters = $pagination_widget->getStoredPaginationParameters($app);
        $pagination->setCurrentPage($pagination_parameters['current_page']);
        $pagination->setNbItemsPerPage($pagination_parameters['nb_items_per_page']);
        $articles_ids_to_display = array_slice(
            $articles_ids,
            ($pagination_parameters['current_page'] - 1) * $pagination_parameters['nb_items_per_page'],
            $pagination_parameters['nb_items_per_page']
        );
        // Load articles.
        $articles_list = $app['content_unit_factory']->createList($type, $articles_ids_to_display);
        foreach ($articles_list as $article) {
            $article->setLanguage($app['i18n']->getLanguage());
        }
        // Active filters description.
        $active_filters_description = array();
        $active_filters_description = array_merge($active_filters_description, $dataset_widget->getActiveFiltersDescription($request, $app));
        $active_filters_description = array_merge($active_filters_description, $publication_widget->getActiveFiltersDescription($request, $app));
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $article_model->setLanguage($language_code);
            $language_urls[$language_code] = $article_model->getListPageUrl();
        }
        $article_model->setLanguage($app['i18n']->getLanguage());
        $this->addMarker('article-content-unit');
        $this->addMarker($article_model->getType());
        $this->setTitle(ucfirst($article_model->getPluralHumanType()));
        return $this->render(
            $app,
            '@Content/Article/index.twig',
            array(
                'article_model' => $article_model->getDigest(),
                'articles' => $articles_list,
                'dataset' => $article_model->getDataset(),
                'dataset_widget' => $dataset_widget,
                'publication' => $article_model->getPublication(),
                'publication_widget' => $publication_widget,
                'pagination' => $pagination,
                'pagination_widget' => $pagination_widget,
                'sort_options_widget' => $sort_options_widget,
                'active_filters_description' => $active_filters_description,
                'language_urls' => $language_urls,
                'language' => $app['i18n']->getLanguage(),
            )
        );
    }

    public function editionForm(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        $id = $request->query->get('id', 0);
        $article = $app['content_unit_factory']->create($type);
        if (!is_a($article, 'Viktor\Pack\Content\Article\AbstractArticle')) {
            $app->abort(404, 'Invalid article type.');
        }
        $article->setLanguage($app['i18n']->getLanguage());
        if ($id > 0) {
            // Existing article.
            $article->load($id);
            if ($article->getId() == 0) {
                $app->abort(404, 'Invalid article id.');
            }
            if (!$article->canBeViewed()) {
                $app->abort(403, 'You are not allowed to view this article.');
            }
        } else {
            // New article.
            if (!$article->canBeCreated()) {
                $app->abort(403, 'You are not allowed to create this article.');
            }
        }
        $creation_user = $app['content_unit_factory']->load(
            $article->getCreationUserType(),
            $article->getCreationUserId()
        );
        if ($creation_user == null) {
            $creation_user = new UnknownUser($app);
        }
        if ($creation_user instanceof TranslatableInterface) {
            $creation_user->setLanguage($app['i18n']->getLanguage());
        }
        $modification_user = $app['content_unit_factory']->load(
            $article->getModificationUserType(),
            $article->getModificationUserId()
        );
        if ($modification_user == null) {
            $modification_user = new UnknownUser($app);
        }
        if ($modification_user instanceof TranslatableInterface) {
            $modification_user->setLanguage($app['i18n']->getLanguage());
        }
        $dataset_widget = new DatasetWidget(
            $article->getDatasetWidgetDescription(),
            $article->getDataset(),
            'article' // Prefix for the fields names.
        );
        $publication_widget = new PublicationWidget(
            $article->getPublication(),
            'article' // Prefix for the fields names.
        );
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $article->setLanguage($language_code);
            if ($article->getId() == 0) {
                $language_urls[$language_code] = $article->getCreationPageUrl();
            } else {
                $language_urls[$language_code] = $article->getEditionPageUrl();
            }
        }
        // Content units that have a link to this content unit.
        $referencing_content_units_array = $article->getDbStoreComponent()->getReferencingContentUnits();
        $referencing_content_units_count = count($referencing_content_units_array);
        $referencing_content_units = array();
        foreach ($referencing_content_units_array as $v) {
            if (count($referencing_content_units_array) >= 20) {
                break;
            }
            $referencing_content_unit = $app['content_unit_factory']->load($v['type'], $v['id']);
            if ($referencing_content_unit != null) {
                $referencing_content_units[] = $referencing_content_unit;
            }
        }
        $article->setLanguage($app['i18n']->getLanguage());
        $this->addMarker('article-content-unit');
        $this->addMarker($article->getType());
        $this->addMarker($article->getType() . ':' . $article->getId());
        $this->setTitle($article->getTitle());
        return $this->render(
            $app,
            '@Content/Article/editionForm.twig',
            array(
                'article_digest' => $article->getDigest(),
                'dataset' => $article->getDataset(),
                'dataset_widget' => $dataset_widget,
                'publication' => $article->getPublication(),
                'publication_widget' => $publication_widget,
                'creation_user' => $creation_user->getDigest(),
                'modification_user' => $modification_user->getDigest(),
                'language_urls' => $language_urls,
                'language' => $app['i18n']->getLanguage(),
                'referencing_content_units' => $referencing_content_units,
                'referencing_content_units_count' => $referencing_content_units_count,
                'additional_edition_form_header_view' => $article->getAdditionalEditionFormHeaderView(),
                'additional_edition_form_footer_view' => $article->getAdditionalEditionFormFooterView(),
                'additional_edition_form_aside_view' => $article->getAdditionalEditionFormAsideView(),
            )
        );
    }

    public function editionFormProcess(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        // Check the object validity and permissions.
        $id = $request->request->get('id', 0);
        $article = $app['content_unit_factory']->create($type);
        if (!is_a($article, 'Viktor\Pack\Content\Article\AbstractArticle')) {
            $app->abort(404, 'Invalid article type.');
        }
        $article->setLanguage($app['i18n']->getLanguage());
        if ($id > 0) {
            // Existing article.
            $article->load($id);
            if ($article->getId() == 0) {
                $app->abort(404, 'Invalid article id.');
            }
            if (!$article->canBeEdited()) {
                $app->abort(403, 'You are not allowed to edit this article.');
            }
            $form_url = $article->getEditionPageUrl();
        } else {
            // New article.
            if (!$article->canBeCreated()) {
                $app->abort(403, 'You are not allowed to create this article.');
            }
            $form_url = $article->getCreationPageUrl();
        }
        // Prepare the form.
        $dataset_widget = new DatasetWidget(
            $article->getDatasetWidgetDescription(),
            $article->getDataset(),
            'article' // Prefix for the fields names.
        );
        $publication_widget = new PublicationWidget(
            $article->getPublication(),
            'article' // Prefix for the fields names.
        );
        // Store the form data in session.
        $dataset_widget->storePrefilledData($request, $app);
        $publication_widget->storePrefilledData($request, $app);
        // Set the new values.
        $publication_widget->set($request, $app);
        $dataset_widget->set($request, $app);
        // Validate the form.
        $form_is_valid = true;
        $fields_widgets = $dataset_widget->getFieldsWidgets();
        foreach ($fields_widgets as $field_widget) {
            $field_language = $field_widget->getField()->getMetadata('language');
            // Language independant fields are always validated.
            // Translated fields are only validated for the published languages.
            if ($field_language == '' || $publication_widget->getPublication()->getPublished($field_language)) {
                $validation_result = $field_widget->validate($request, $app);
                if (!$validation_result) {
                    $form_is_valid = false;
                }
            }
        }
        if (!$form_is_valid) {
            $app['session']->getFlashBag()->add('error', 'Some fields are invalid.');
            return $app->redirect($form_url);
        }
        // Save the article.
        if ($article->save($app['viktor.user'])) {
            $app['session']->getFlashBag()->add(
                'success',
                ucfirst($article->getSingularHumanType()) . ' ' . $article->getTitle() . ' saved successfully.'
            );
        } else {
            $app['session']->getFlashBag()->add('error', 'The ' . $article->getSingularHumanType() . ' could not be saved.');
        }
        // Remove the form data stored in session.
        $dataset_widget->removePrefilledData($request, $app);
        // Everything is ok, back to the form.
        return $app->redirect($article->getEditionPageUrl());
    }

    public function deletionForm(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        // Check the object validity and permissions.
        $id = $request->query->get('id', 0);
        $article = $app['content_unit_factory']->load($type, $id);
        if (!is_a($article, 'Viktor\Pack\Content\Article\AbstractArticle')) {
            $app->abort(404, 'Could not load this article.');
        }
        $article->setLanguage($app['i18n']->getLanguage());
        if (!$article->canBeDeleted()) {
            $app->abort(403, 'You are not allowed to delete this article.');
        }
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $article->setLanguage($language_code);
            $language_urls[$language_code] = $article->getDeletionPageUrl();
        }
        $article->setLanguage($app['i18n']->getLanguage());
        $this->addMarker('article-content-unit');
        $this->addMarker($article->getType());
        $this->addMarker($article->getType() . ':' . $article->getId());
        $this->setTitle('Delete "' . $article->getTitle() . '"?');
        return $this->render(
            $app,
            '@Content/Article/deletionForm.twig',
            array(
                'article_digest' => $article->getDigest(),
                'language_urls'  => $language_urls,
                'language'       => $app['i18n']->getLanguage(),
            )
        );
    }

    public function deletionFormProcess(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        // Check the object validity and permissions.
        $id = $request->request->get('id', 0);
        $article = $app['content_unit_factory']->load($type, $id);
        if (!is_a($article, 'Viktor\Pack\Content\Article\AbstractArticle')) {
            $app->abort(404, 'Could not load this article.');
        }
        $article->setLanguage($app['i18n']->getLanguage());
        if (!$article->canBeDeleted()) {
            $app->abort(403, 'You are not allowed to delete this article.');
        }
        $article_type = $article->getType();
        $article_title = $article->getTitle();
        $article_id = $article->getId();
        $article_index_url = $article->getListPageUrl();
        if ($article->delete()) {
            $app['session']->getFlashBag()->add(
                'success',
                $article_title . ' (' . $article_type . ' #' . $article_id . ') has been deleted.'
            );
        } else {
            $app['session']->getFlashBag()->add(
                'error',
                $article_title . ' (' . $article_type . ' #' . $article_id . ') could not be deleted.'
            );
        }
        return $app->redirect($article_index_url);
    }

    public function duplicationForm(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        // Check the object validity and permissions.
        $id = $request->query->get('id', 0);
        $article = $app['content_unit_factory']->load($type, $id);
        if (!is_a($article, 'Viktor\Pack\Content\Article\AbstractArticle')) {
            $app->abort(404, 'Could not load this article.');
        }
        $article->setLanguage($app['i18n']->getLanguage());
        if (!$article->canBeViewed() || !$article->canBeCreated()) {
            $app->abort(403, 'You are not allowed to duplicate this article.');
        }
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $article->setLanguage($language_code);
            $language_urls[$language_code] = $article->getDuplicationPageUrl();
        }
        $article->setLanguage($app['i18n']->getLanguage());
        $this->addMarker('article-content-unit');
        $this->addMarker($article->getType());
        $this->addMarker($article->getType() . ':' . $article->getId());
        $this->setTitle('Duplicate "' . $article->getTitle() . '"?');
        return $this->render(
            $app,
            '@Content/Article/duplicationForm.twig',
            array(
                'article_digest' => $article->getDigest(),
                'language_urls'  => $language_urls,
                'language'       => $app['i18n']->getLanguage(),
            )
        );
    }

    public function duplicationFormProcess(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        // Check the object validity and permissions.
        $id = $request->request->get('id', 0);
        $article = $app['content_unit_factory']->load($type, $id);
        if (!is_a($article, 'Viktor\Pack\Content\Article\AbstractArticle')) {
            $app->abort(404, 'Could not load this article.');
        }
        $article->setLanguage($app['i18n']->getLanguage());
        if (!$article->canBeViewed() || !$article->canBeCreated()) {
            $app->abort(403, 'You are not allowed to duplicate this article.');
        }
        $new_article = $article->duplicate();
        if ($new_article->save($app['viktor.user'])) {
            $app['session']->getFlashBag()->add(
                'success',
                ucfirst($article->getSingularHumanType()) . ' ' . $article->getTitle() . ' duplicated successfully.'
            );
        } else {
            $app['session']->getFlashBag()->add('error', 'The ' . $article->getSingularHumanType() . ' could not be duplicated.');
        }
        return $app->redirect($article->getListPageUrl());
    }

}

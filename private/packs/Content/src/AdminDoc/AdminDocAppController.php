<?php

namespace Viktor\Pack\Content\AdminDoc;

use ParsedownExtra;
use Viktor\Application;
use Viktor\Pack\Admin\Admin\AdminHtmlPage;

class AdminDocAppController extends AdminHtmlPage
{
    public function md(Application $app, $language, $docpool, $file)
    {
        $app['i18n']->setLanguage($language);
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $language_urls[$language_code] = $app->path('admin-doc', array(
                'language' => $language_code,
                'docpool' => $docpool,
                'file' => $file,
            ));
        }
        // Determine what doc are we consulting.
        $doc_folder_path = '';
        if ($docpool == 'app') {
            $doc_folder_path = realpath($app['viktor.app_path'] . '/doc');
        } elseif ($docpool == 'content') {
            $doc_folder_path = realpath($app['viktor.packs_path'] . '/Content/doc');
        } else {
            return $app->abort(404);
        }
        // Check if file path is correct.
        $doc_file_path = realpath($doc_folder_path);
        $md_file_path = realpath($doc_folder_path . '/' . $file);
        if (substr($file, -2) != 'md') {
            return $app->abort(404);
        }
        if (substr($md_file_path, 0, strlen($doc_file_path)) != $doc_file_path) {
            return $app->abort(404);
        }
        // Import and parse doc.
        $content = file_get_contents($md_file_path);
        $parsedown = new ParsedownExtra();
        $parsedown
            ->setBreaksEnabled(true)
            ->setMarkupEscaped(true)
            ->setUrlsLinked(false);
        $processed_html = $parsedown->text($content);
        $this->addMarker('doc');
        $this->setTitle('Viktor 4 doc');
        return $this->render(
            $app,
            '@Content/AdminDoc/adminDoc.twig',
            array(
                'language_urls' => $language_urls,
                'language' => $app['i18n']->getLanguage(),
                'processed_html' => $processed_html,
                'current_file' => $file,
            )
        );
    }

}
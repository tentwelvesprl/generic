<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Content\Image;

use DateTime;
use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Base\Badge\Badge;
use Viktor\Pack\Base\FullTextIndex\FullTextIndexer;
use Viktor\Pack\Base\FullTextIndex\Tokenizer;
use Viktor\Pack\Base\Image\HasAnUploadedImageInterface;
use Viktor\Pack\Base\Image\ImageDocumentInterface;
use Viktor\Pack\Base\Image\UploadedImage;
use Viktor\Pack\Base\Image\UploadedImageDecorator;
use Viktor\Pack\Base\User\UserInterface;
use Viktor\Pack\Content\ContentUnit\AbstractContentUnit;

abstract class AbstractImage extends AbstractContentUnit implements
    HasAnUploadedImageInterface,
    ImageDocumentInterface
{
    private $uploadedImage;

    public function addToIndex(FullTextIndexer $indexer)
    {
        foreach ($this->getApp()['viktor.config']['available_languages'] as $language => $language_name) {
            $this->setLanguage($language);
            $s = basename($this->getUploadedImage()->getFilePath());
            $s = str_replace('_', ' ', $s);
            $s = str_replace('-', ' ', $s);
            $tokenizer = new Tokenizer($this->getTitle(), $language);
            $tokens = $tokenizer->getTokens();
            $indexer->addTokens($tokens, 1, 'general_' . $language);
            $indexer->addTokens($tokens, 20, 'image_' . $language);
        }
        parent::addToIndex($indexer);
    }

    public function getTableName()
    {
        return 'image';
    }

    public function prepareNewInstance()
    {
        $this->uploadedImage = new UploadedImage($this->getApp(), $this, $this->getUploadedImageOptions());
        $this->dbStoreComponent = new UploadedImageDecorator($this->dbStoreComponent, $this->uploadedImage);
        $this->dbStoreComponent = new ImageDecorator($this->dbStoreComponent);
    }

    public function getDigest()
    {
        $digest = parent::getDigest();
        $digest['width'] = $this->uploadedImage->getWidth();
        $digest['height'] = $this->uploadedImage->getHeight();
        $digest['area'] = $this->uploadedImage->getArea();
        $digest['ratio'] = $this->uploadedImage->getRatio();
        $digest['_caption'] = $this->getCaption();
        $digest['_html_caption'] = $this->getHtmlCaption();
        $digest['_credits'] = $this->getCredits();
        $digest['_html_credits'] = $this->getHtmlCredits();
        $digest['_alt_text'] = $this->getAltText();
        $digest['_html_alt_text'] = $this->getHtmlAltText();
        $digest['_duplication_page_url'] = $this->getDuplicationPageUrl();
        return $digest;
    }

    public function getDuplicationPageUrl()
    {
        return $this->getApp()->path('admin-image-duplication-form', array(
            'language' => $this->getLanguage(),
            'type'     => $this->getType(),
            'id'       => $this->getId(),
        ));
    }

    public function getTitle()
    {
        $title = basename($this->getUploadedImage()->getFilePath());
        return $title;
    }

    public function getImage()
    {
        return $this->uploadedImage->getSourceImage();
    }

    public function getImageReference()
    {
        if ($this->getId() == 0) {
            return null;
        }
        return array(
            'type' => $this->getType(),
            'id' => $this->getId(),
        );
    }

    public function getDatasetCombinedDescription()
    {
        $sections = array(
            '' => array(
                'label' => 'Image information',
                'collapse' => false,
            )
        );
        $fields = array(
            'caption' => array(
                'translatable' => true,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\TextField',
                    'options'    => array(
                        'indexed' => true,
                        'source_type' => 'markdown',
                    ),
                    'full_text_index_weights' => array(
                        'general' => 1,
                        'image' => 1,
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\TextFieldWidget',
                    'options'    => array(
                        'label' => 'Caption',
                        'searchable' => true,
                        'sortable' => false,
                        'legend' => 'This field accepts Markdown syntax, e.g.: *<em>italic</em>*, **<strong>bold</strong>**.',
                        'section' => '',
                    ),
                ),
            ),
            'credits' => array(
                'translatable' => false,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\TextField',
                    'options'    => array(
                        'indexed' => true,
                        'source_type' => 'markdown',
                    ),
                    'full_text_index_weights' => array(
                        'general' => 1,
                        'image' => 1,
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\TextFieldWidget',
                    'options'    => array(
                        'label' => 'Credits',
                        'searchable' => true,
                        'sortable' => false,
                        'legend' => 'This field accepts Markdown syntax, e.g.: *<em>italic</em>*, **<strong>bold</strong>**.',
                        'section' => '',
                    ),
                ),
            ),
            'alt_text' => array(
                'translatable' => true,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\TextField',
                    'options'    => array(
                        'indexed' => true,
                        'source_type' => 'markdown',
                    ),
                    'full_text_index_weights' => array(
                        'general' => 1,
                        'image' => 1,
                    ),
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\TextFieldWidget',
                    'options'    => array(
                        'label' => 'Alternative text',
                        'legend' => 'This text describes the appearance or function of the image and improves accessibility for the visually impaired.',
                        'searchable' => true,
                        'sortable' => false,
                        'section' => '',
                    ),
                ),
            ),
            'keywords' => array(
                'translatable' => false,
                'model' => array(
                    'class_name' => 'Viktor\Pack\Base\Field\TextField',
                    'options'    => array(
                        'indexed' => true,
                        'source_type' => 'plain_text',
                    ),
                    'full_text_index_weights' => array(
                        'general' => 1,
                        'image' => 100,
                    )
                ),
                'widget' => array(
                    'class_name' => 'Viktor\Pack\Admin\Field\TextFieldWidget',
                    'options'    => array(
                        'label' => 'Keywords',
                        'legend' => 'They\'ll allow you to search for the image within the admin interface.',
                        'searchable' => true,
                        'sortable' => false,
                        'section' => '',
                    ),
                ),
            ),
        );
        return array(
            'sections' => $sections,
            'fields' => $fields
        );
    }

    public function getUploadedImageOptions()
    {
        $options = array(
        );
        return $options;
    }

    public function getUploadedImage()
    {
        return $this->uploadedImage;
    }

    public function getCaption()
    {
        $fields = $this->getDataset()->getFields();
        $caption = trim($fields['caption_' . $this->getLanguage()]->getPlainTextRepresentation());
        return $caption;
    }

    public function getHtmlCaption()
    {
        $fields = $this->getDataset()->getFields();
        $caption = trim($fields['caption_' . $this->getLanguage()]->getProcessedValue());
        return $caption;
    }

    public function getCredits()
    {
        $fields = $this->getDataset()->getFields();
        $credits = trim($fields['credits']->getPlainTextRepresentation());
        return $credits;
    }

    public function getHtmlCredits()
    {
        $fields = $this->getDataset()->getFields();
        $credits = trim($fields['credits']->getProcessedValue());
        return $credits;
    }

    public function getAltText()
    {
        $fields = $this->getDataset()->getFields();
        $alt_text = trim($fields['alt_text_' . $this->getLanguage()]->getPlainTextRepresentation());
        return $alt_text;
    }

    public function getHtmlAltText()
    {
        $fields = $this->getDataset()->getFields();
        $alt_text = trim($fields['alt_text_' . $this->getLanguage()]->getProcessedValue());
        return $alt_text;
    }

    public function setUploadedImageFilePath($file_path)
    {
        $this->uploadedImage->setFilePath($file_path);
    }

    public function renderListItem(Request $request, Application $app)
    {
        $referencing_content_units_array = $this->getDbStoreComponent()->getReferencingContentUnits();
        $referencing_content_units_count = count($referencing_content_units_array);
        $badge_digest = array();
        if ($badge_items = $this->getBadge()) {
            if ($badge_items instanceof Badge) {
                $badge_digest[] = $badge_items->getDigest();
            } elseif (is_array($badge_items)) {
                foreach ($badge_items as $badge_item) {
                    $badge_digest[] = $badge_item->getDigest();
                }
            }
        }
        return $app['twig']->render(
            '@Content/Image/listItem.twig',
            array(
                'image_digest' => $this->getDigest(),
                'language' => $this->getLanguage(),
                'referencing_content_units' => $referencing_content_units_array,
                'referencing_content_units_count' => $referencing_content_units_count,
                'badge' => $badge_digest,
            )
        );
    }

    public function getAdditionalEditionFormHeaderView()
    {
        return '';
    }

    public function getAdditionalEditionFormFooterView()
    {
        return '';
    }

    public function getAdditionalEditionFormAsideView()
    {
        return '';
    }

    public function duplicate()
    {
        $new_image = parent::duplicate();
        $new_image->getUploadedImage()->copy($this->getUploadedImage());
        return $new_image;
    }

}

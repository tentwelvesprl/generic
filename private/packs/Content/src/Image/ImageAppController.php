<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Content\Image;

use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Admin\Admin\AdminHtmlPage;
use Viktor\Pack\Admin\Dataset\DatasetWidget;
use Viktor\Pack\Admin\DbStore\DbStoreComponentWidget;
use Viktor\Pack\Admin\Image\UploadedImageWidget;
use Viktor\Pack\Admin\Pagination\PaginationWidget;
use Viktor\Pack\Admin\SortOptions\SortOptionsWidget;
use Viktor\Pack\Base\I18n\TranslatableInterface;
use Viktor\Pack\Base\Pagination\Pagination;
use Viktor\Pack\Base\User\UnknownUser;

class ImageAppController extends AdminHtmlPage
{
    public function index(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        $image_model = $app['content_unit_factory']->create($type);
        if (!is_a($image_model, 'Viktor\Pack\Content\Image\AbstractImage')) {
            $app->abort(404, $type . ' is not a valid image type.');
        }
        $image_model->setLanguage($app['i18n']->getLanguage());
        if (!$image_model->canBeListed()) {
            $app->abort(403, 'You are not allowed to list these elements.');
        }
        // Prepare the widgets.
        $db_store_component_widget = new DbStoreComponentWidget(
            $image_model->getDbStoreComponent(),
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        $uploaded_image_widget = new UploadedImageWidget(
            $image_model->getUploadedImage(),
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        $image_widget = new ImageWidget(
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        $dataset_widget = new DatasetWidget(
            $image_model->getDatasetWidgetDescription($language),
            $image_model->getDataset(),
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        $sort_options = array();
        $sort_options = array_merge($sort_options, $dataset_widget->getSortOptions());
        $sort_options = array_merge($sort_options, $uploaded_image_widget->getSortOptions());
        $sort_options = array_merge($sort_options, $image_widget->getSortOptions());
        $sort_options = array_merge($sort_options, $db_store_component_widget->getSortOptions());
        $sort_options_widget = new SortOptionsWidget(
            $sort_options,
            $image_model,
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        // Store the advanced search form data in session.
        $uploaded_image_widget->storeAdvancedSearchData($request, $app);
        $image_widget->storeAdvancedSearchData($request, $app);
        $dataset_widget->storeAdvancedSearchData($request, $app);
        // Filters.
        $filters = array();
        $filters = array_merge($filters, $uploaded_image_widget->getFilters($request, $app));
        $filters = array_merge($filters, $dataset_widget->getFilters($request, $app));
        $filters[] = array(
            'type' => 'deduplicate',
            'value' => true,
        );
        // Sort options.
        $sort_options_widget->storeSelectedSortOption($request, $app);
        // Go!
        $images_ids = $image_model->getIds(
            $filters,
            array($sort_options_widget->getSelectedSortOption($request, $app))
        );
        // Apply pagination.
        $pagination = new Pagination(
            $request,
            $app,
            count($images_ids),
            20,
            1,
            array(20, 50, 100),
            $type . '_' . $app['i18n']->getLanguage()
        );
        $pagination_widget = new PaginationWidget(
            $pagination,
            $type . '_' . $app['i18n']->getLanguage() // Prefix for the fields names.
        );
        $pagination_widget->storePaginationParameters($request, $app);
        $pagination_parameters = $pagination_widget->getStoredPaginationParameters($app);
        $pagination->setCurrentPage($pagination_parameters['current_page']);
        $pagination->setNbItemsPerPage($pagination_parameters['nb_items_per_page']);
        $images_ids_to_display = array_slice(
            $images_ids,
            ($pagination_parameters['current_page'] - 1) * $pagination_parameters['nb_items_per_page'],
            $pagination_parameters['nb_items_per_page']
        );
        // Load images.
        $images_list = $app['content_unit_factory']->createList($type, $images_ids_to_display);
        foreach ($images_list as $image) {
            $image->setLanguage($app['i18n']->getLanguage());
        }
        // Active filters description.
        $active_filters_description = array();
        $active_filters_description = array_merge($active_filters_description, $dataset_widget->getActiveFiltersDescription($request, $app));
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $image_model->setLanguage($language_code);
            $language_urls[$language_code] = $image_model->getListPageUrl();
        }
        $image_model->setLanguage($app['i18n']->getLanguage());
        $this->addMarker('image-content-unit');
        $this->addMarker($image_model->getType());
        $this->setTitle(ucfirst($image_model->getPluralHumanType()));
        return $this->render(
            $app,
            '@Content/Image/index.twig',
            array(
                'image_model' => $image_model->getDigest(),
                'images' => $images_list,
                'uploaded_image' => $image_model->getUploadedImage(),
                'uploaded_image_widget' => $uploaded_image_widget,
                'dataset' => $image_model->getDataset(),
                'dataset_widget' => $dataset_widget,
                'pagination' => $pagination,
                'pagination_widget' => $pagination_widget,
                'sort_options_widget' => $sort_options_widget,
                'active_filters_description' => $active_filters_description,
                'language_urls' => $language_urls,
                'language' => $app['i18n']->getLanguage(),
            )
        );
    }

    public function editionForm(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        $id = $request->query->get('id', 0);
        $image = $app['content_unit_factory']->create($type);
        if (!is_a($image, 'Viktor\Pack\Content\Image\AbstractImage')) {
            $app->abort(404, 'Invalid image type.');
        }
        $image->setLanguage($app['i18n']->getLanguage());
        if ($id > 0) {
            // Existing image.
            $image->load($id);
            if ($image->getId() == 0) {
                $app->abort(404, 'Invalid image id.');
            }
            if (!$image->canBeViewed()) {
                $app->abort(403, 'You are not allowed to view this image.');
            }
        } else {
            // New image.
            if (!$image->canBeCreated()) {
                $app->abort(403, 'You are not allowed to create this image.');
            }
        }
        $creation_user = $app['content_unit_factory']->load(
            $image->getCreationUserType(),
            $image->getCreationUserId()
        );
        if ($creation_user == null) {
            $creation_user = new UnknownUser($app);
        }
        if ($creation_user instanceof TranslatableInterface) {
            $creation_user->setLanguage($app['i18n']->getLanguage());
        }
        $modification_user = $app['content_unit_factory']->load(
            $image->getModificationUserType(),
            $image->getModificationUserId()
        );
        if ($modification_user == null) {
            $modification_user = new UnknownUser($app);
        }
        if ($modification_user instanceof TranslatableInterface) {
            $modification_user->setLanguage($app['i18n']->getLanguage());
        }
        $uploaded_image_widget = new UploadedImageWidget(
            $image->getUploadedImage(),
            'image' // Prefix for the fields names.
        );
        $dataset_widget = new DatasetWidget(
            $image->getDatasetWidgetDescription(),
            $image->getDataset(),
            'image' // Prefix for the fields names.
        );
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $image->setLanguage($language_code);
            if ($image->getId() == 0) {
                $language_urls[$language_code] = $image->getCreationPageUrl();
            } else {
                $language_urls[$language_code] = $image->getEditionPageUrl();
            }
        }
        $image->setLanguage($app['i18n']->getLanguage());
        // Content units that have a link to this content unit.
        $referencing_content_units_array = $image->getDbStoreComponent()->getReferencingContentUnits();
        $referencing_content_units_count = count($referencing_content_units_array);
        $referencing_content_units = array();
        foreach ($referencing_content_units_array as $v) {
            if (count($referencing_content_units_array) >= 20) {
                break;
            }
            $referencing_content_unit = $app['content_unit_factory']->load($v['type'], $v['id']);
            if ($referencing_content_unit != null) {
                $referencing_content_units[] = $referencing_content_unit;
            }
        }
        $this->addMarker('image-content-unit');
        $this->addMarker($image->getType());
        $this->addMarker($image->getType() . ':' . $image->getId());
        $this->setTitle($image->getTitle());
        return $this->render(
            $app,
            '@Content/Image/editionForm.twig',
            array(
                'image_digest' => $image->getDigest(),
                'dataset' => $image->getDataset(),
                'dataset_widget' => $dataset_widget,
                'uploaded_image' => $image->getUploadedImage(),
                'uploaded_image_widget' => $uploaded_image_widget,
                'creation_user' => $creation_user->getDigest(),
                'modification_user' => $modification_user->getDigest(),
                'language_urls' => $language_urls,
                'language' => $app['i18n']->getLanguage(),
                'referencing_content_units' => $referencing_content_units,
                'referencing_content_units_count' => $referencing_content_units_count,
                'additional_edition_form_header_view' => $image->getAdditionalEditionFormHeaderView(),
                'additional_edition_form_footer_view' => $image->getAdditionalEditionFormFooterView(),
                'additional_edition_form_aside_view' => $image->getAdditionalEditionFormAsideView(),
            )
        );
    }

    public function editionFormProcess(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        // Check the object validity and permissions.
        $id = $request->request->get('id', 0);
        $image = $app['content_unit_factory']->create($type);
        if (!is_a($image, 'Viktor\Pack\Content\Image\AbstractImage')) {
            $app->abort(404, 'Invalid image type.');
        }
        $image->setLanguage($app['i18n']->getLanguage());
        if ($id > 0) {
            // Existing image.
            $image->load($id);
            if ($image->getId() == 0) {
                $app->abort(404, 'Invalid image id.');
            }
            if (!$image->canBeEdited()) {
                $app->abort(403, 'You are not allowed to edit this image.');
            }
            $form_url = $image->getEditionPageUrl();
        } else {
            // New image.
            if (!$image->canBeCreated()) {
                $app->abort(403, 'You are not allowed to create this image.');
            }
            $form_url = $image->getCreationPageUrl();
        }
        // Prepare the form.
        $uploaded_image_widget = new UploadedImageWidget(
            $image->getUploadedImage(),
            'image' // Prefix for the fields names.
        );
        $dataset_widget = new DatasetWidget(
            $image->getDatasetWidgetDescription(),
            $image->getDataset(),
            'image' // Prefix for the fields names.
        );
        // Store the form data in session.
        $uploaded_image_widget->storePrefilledData($request, $app);
        $dataset_widget->storePrefilledData($request, $app);
        // Set the new values.
        $dataset_widget->set($request, $app);
        // Validate the form.
        $form_is_valid = true;
        $fields_widgets = $dataset_widget->getFieldsWidgets();
        foreach ($fields_widgets as $field_widget) {
            $validation_result = $field_widget->validate($request, $app);
            if (!$validation_result) {
                $form_is_valid = false;
            }
        }
        if (!$form_is_valid) {
            $app['session']->getFlashBag()->add('error', 'Some fields are invalid.');
            return $app->redirect($form_url);
        }
        // Validate the uploaded image
        $validation_result = $uploaded_image_widget->validate($request, $app);
        if ($validation_result['status'] == 'error') {
            $app['session']->getFlashBag()->add('error', $validation_result['reason']);
            return $app->redirect($form_url);
        }
        // Set the uploaded image content (may copy a file the data dir).
        $uploaded_image_widget->set($request, $app);
        // Save the image.
        if ($image->save($app['viktor.user'])) {
            $app['session']->getFlashBag()->add(
                'success',
                ucfirst($image->getSingularHumanType()) . ' ' . $image->getTitle() . ' saved successfully.'
            );
        } else {
            $app['session']->getFlashBag()->add('error', 'The ' . $image->getSingularHumanType() . ' could not be saved.');
        }
        // Remove the form data stored in session.
        $uploaded_image_widget->removePrefilledData($request, $app);
        $dataset_widget->removePrefilledData($request, $app);
        // Everything is ok, back to the form.
        return $app->redirect($image->getEditionPageUrl());
    }

    public function deletionForm(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        // Check the object validity and permissions.
        $id = $request->query->get('id', 0);
        $image = $app['content_unit_factory']->load($type, $id);
        if (!is_a($image, 'Viktor\Pack\Content\Image\AbstractImage')) {
            $app->abort(404, 'Could not load this image.');
        }
        $image->setLanguage($app['i18n']->getLanguage());
        if (!$image->canBeDeleted()) {
            $app->abort(403, 'You are not allowed to delete this image.');
        }
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $image->setLanguage($language_code);
            $language_urls[$language_code] = $image->getDeletionPageUrl();
        }
        $image->setLanguage($app['i18n']->getLanguage());
        // Content units that have a link to this content unit.
        $referencing_content_units_array = $image->getDbStoreComponent()->getReferencingContentUnits();
        $referencing_content_units_count = count($referencing_content_units_array);
        $referencing_content_units = array();
        foreach ($referencing_content_units_array as $v) {
            if (count($referencing_content_units_array) >= 20) {
                break;
            }
            $referencing_content_unit = $app['content_unit_factory']->load($v['type'], $v['id']);
            if ($referencing_content_unit != null) {
                $referencing_content_units[] = $referencing_content_unit;
            }
        }
        $this->addMarker('image-content-unit');
        $this->addMarker($image->getType());
        $this->addMarker($image->getType() . ':' . $image->getId());
        $this->setTitle('Delete "' . $image->getTitle() . '"?');
        return $this->render(
            $app,
            '@Content/Image/deletionForm.twig',
            array(
                'image_digest' => $image->getDigest(),
                'language_urls'  => $language_urls,
                'language'       => $app['i18n']->getLanguage(),
                'referencing_content_units' => $referencing_content_units,
                'referencing_content_units_count' => $referencing_content_units_count,
            )
        );
    }

    public function deletionFormProcess(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        // Check the object validity and permissions.
        $id = $request->request->get('id', 0);
        $image = $app['content_unit_factory']->load($type, $id);
        if (!is_a($image, 'Viktor\Pack\Content\Image\AbstractImage')) {
            $app->abort(404, 'Could not load this image.');
        }
        $image->setLanguage($app['i18n']->getLanguage());
        if (!$image->canBeDeleted()) {
            $app->abort(403, 'You are not allowed to delete this image.');
        }
        $image_type = $image->getType();
        $image_title = $image->getTitle();
        $image_id = $image->getId();
        $image_index_url = $image->getListPageUrl();
        if ($image->delete()) {
            $app['session']->getFlashBag()->add(
                'success',
                $image_title . ' (' . $image_type . ' #' . $image_id . ') has been deleted.'
            );
        } else {
            $app['session']->getFlashBag()->add(
                'error',
                $image_title . ' (' . $image_type . ' #' . $image_id . ') could not be deleted.'
            );
        }
        return $app->redirect($image_index_url);
    }

    public function duplicationForm(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        // Check the object validity and permissions.
        $id = $request->query->get('id', 0);
        $image = $app['content_unit_factory']->load($type, $id);
        if (!is_a($image, 'Viktor\Pack\Content\Image\AbstractImage')) {
            $app->abort(404, 'Could not load this image.');
        }
        $image->setLanguage($app['i18n']->getLanguage());
        if (!$image->canBeViewed() || !$image->canBeCreated()) {
            $app->abort(403, 'You are not allowed to duplicate this image.');
        }
        // Language switcher.
        $language_urls = array();
        foreach ($app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $image->setLanguage($language_code);
            $language_urls[$language_code] = $image->getDuplicationPageUrl();
        }
        $image->setLanguage($app['i18n']->getLanguage());
        // Content units that have a link to this content unit.
        $referencing_content_units_array = $image->getDbStoreComponent()->getReferencingContentUnits();
        $referencing_content_units_count = count($referencing_content_units_array);
        $referencing_content_units = array();
        foreach ($referencing_content_units_array as $v) {
            if (count($referencing_content_units_array) >= 20) {
                break;
            }
            $referencing_content_unit = $app['content_unit_factory']->load($v['type'], $v['id']);
            if ($referencing_content_unit != null) {
                $referencing_content_units[] = $referencing_content_unit;
            }
        }
        $this->addMarker('image-content-unit');
        $this->addMarker($image->getType());
        $this->addMarker($image->getType() . ':' . $image->getId());
        $this->setTitle('Duplicate "' . $image->getTitle() . '"?');
        return $this->render(
            $app,
            '@Content/Image/duplicationForm.twig',
            array(
                'image_digest' => $image->getDigest(),
                'language_urls'  => $language_urls,
                'language'       => $app['i18n']->getLanguage(),
                'referencing_content_units' => $referencing_content_units,
                'referencing_content_units_count' => $referencing_content_units_count,
            )
        );
    }

    public function duplicationFormProcess(Request $request, Application $app, $language, $type)
    {
        $app['i18n']->setLanguage($language);
        // Check the object validity and permissions.
        $id = $request->request->get('id', 0);
        $image = $app['content_unit_factory']->load($type, $id);
        if (!is_a($image, 'Viktor\Pack\Content\Image\AbstractImage')) {
            $app->abort(404, 'Could not load this image.');
        }
        $image->setLanguage($app['i18n']->getLanguage());
        if (!$image->canBeViewed() || !$image->canBeCreated()) {
            $app->abort(403, 'You are not allowed to duplicate this image.');
        }
        $new_image = $image->duplicate();
        if ($new_image->save($app['viktor.user'])) {
            $app['session']->getFlashBag()->add(
                'success',
                ucfirst($image->getSingularHumanType()) . ' ' . $image->getTitle() . ' duplicated successfully.'
            );
        } else {
            $app['session']->getFlashBag()->add('error', 'The ' . $image->getSingularHumanType() . ' could not be duplicated.');
        }
        return $app->redirect($image->getListPageUrl());
    }

}

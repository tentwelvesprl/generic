<?php

/*
 * Copyright 2022 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\ContentBox;

use Doctrine\DBAL\Schema\Table;
use Exception;
use Viktor\Application;
use Viktor\PackInterface;
use Viktor\Pack\Base\User\SecretaryUser;

class ContentBoxPack implements PackInterface
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
        return $this;
    }

    public function getName()
    {
        return 'ContentBox';
    }

    public function getPath()
    {
        return realpath(__DIR__ . '/..');
    }

    public function install() {
        $app = $this->app;
        // Copy the static assets into the public folder.
        $this->copyRecursively($this->getPath() . '/assets', $this->app['viktor.web_path'] . '/assets/ContentBox');
        return;
    }

    public function init()
    {
        $app = $this->app;
        // Twig configuration.
        $this->app['twig.loader.filesystem']->addPath(__DIR__ . '/../views', 'ContentBox');
        // Firewall.
        $pack_controllers_behind_general_firewall = array(
            'Viktor\Pack\ContentBox\Field\ContentBoxFieldAppController',
        );
        $this->app['viktor.controllers_behind_general_firewall'] = array_merge(
            $this->app['viktor.controllers_behind_general_firewall'],
            $pack_controllers_behind_general_firewall
        );
        return $this;
    }

    private function copyRecursively($source, $dest)
    {
        if (is_file($source)) {
            return copy($source, $dest);
        }
        if (!is_dir($dest)) {
            mkdir($dest);
        }
        $dir = dir($source);
        while (($entry = $dir->read()) !== false) {
            if ($entry[0] == '.') {
                continue;
            }
            if ($dest !== "$source/$entry") {
                $this->copyRecursively("$source/$entry", "$dest/$entry");
            }
        }
        $dir->close();
        return true;
    }

    public function addConsoleCommands() {
    }

}

<?php

/*
 * Copyright 2022 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\ContentBox\Field;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Viktor\Application;
use Viktor\Pack\Base\DataFile\UploadHandler;
use Viktor\Pack\Base\DataFile\DataFile;
use Viktor\Pack\Base\EmbedInfo\HasAnEmbedInfoInterface;
use Viktor\Pack\Base\FullTextIndex\SearchEngine;
use Viktor\Pack\Base\I18n\TranslatableInterface;
use Viktor\Pack\Base\Image\ImageGenerator;
use Viktor\Pack\Base\Image\SourceImage;

class ContentBoxFieldAppController
{
    public function item(Request $request, Application $app, $language)
    {
        $base_name = (string) $request->query->get('base_name', '');
        $type = (string) $request->query->get('type', '');
        $reference_type = (string) $request->query->get('reference_type', '');
        $reference_id = (int) $request->query->get('reference_id', 0);
        $disabled = (bool) $request->query->get('disabled', false);
        if ($type == 'separator') {
            return $app['twig']->render(
                '@ContentBox/Field/contentBoxFieldItem.twig',
                array(
                    'language' => $language,
                    'type' => 'separator',
                    'content' => '',
                    'reference_type' => '',
                    'reference_id' => 0,
                    'base_name' => $base_name,
                    'valid' => true,
                    'disabled' => $disabled,
                    'error_message' => '',
                )
            );
        } elseif ($type == 'html') {
            return $app['twig']->render(
                '@ContentBox/Field/contentBoxFieldItem.twig',
                array(
                    'language' => $language,
                    'type' => 'html',
                    'content' => '',
                    'reference_type' => '',
                    'reference_id' => 0,
                    'base_name' => $base_name,
                    'valid' => true,
                    'disabled' => $disabled,
                    'error_message' => '',
                )
            );
        } elseif ($type == 'image') {
            $image = $app['content_unit_factory']->load($reference_type, $reference_id);
            if ($image != null) {
                $image->setLanguage($language);
                return $app['twig']->render(
                    '@ContentBox/Field/contentBoxFieldItem.twig',
                    array(
                        'language' => $language,
                        'type' => 'image',
                        'content' => '',
                        'reference_type' => $reference_type,
                        'reference_id' => $reference_id,
                        'base_name' => $base_name,
                        'valid' => true,
                        'disabled' => $disabled,
                        'error_message' => '',
                    )
                );
            }
        } elseif ($type == 'media') {
            $image = $app['content_unit_factory']->load($reference_type, $reference_id);
            if ($image != null) {
                $image->setLanguage($language);
                return $app['twig']->render(
                    '@ContentBox/Field/contentBoxFieldItem.twig',
                    array(
                        'language' => $language,
                        'type' => 'media',
                        'content' => '',
                        'reference_type' => $reference_type,
                        'reference_id' => $reference_id,
                        'base_name' => $base_name,
                        'valid' => true,
                        'disabled' => $disabled,
                        'error_message' => '',
                    )
                );
            }
        } elseif ($type == 'content_unit') {
            $content_unit = $app['content_unit_factory']->load($reference_type, $reference_id);
            if ($content_unit != null) {
                $content_unit->setLanguage($language);
                return $app['twig']->render(
                    '@ContentBox/Field/contentBoxFieldItem.twig',
                    array(
                        'language' => $language,
                        'type' => 'content_unit',
                        'content' => '',
                        'reference_type' => $reference_type,
                        'reference_id' => $reference_id,
                        'base_name' => $base_name,
                        'valid' => true,
                        'disabled' => $disabled,
                        'error_message' => '',
                    )
                );
            }
        }
        $app->abort(404);
    }

    public function createItemFromTmpFile(Request $request, Application $app, $language, $type, $base_name)
    {
        $data_file = null;
        try {
            $file_name = (string) $request->request->get('file_name', '');
            $path = $app['viktor.tmp_path'] . '/' . $file_name;
            $source_image = new SourceImage($path);
            $image_generator = new ImageGenerator($app, $source_image);
            $image_generator->load();
            $validation_result = $image_generator->validate();
            if ($validation_result['status'] == 'error') {
                throw new Exception($validation_result['reason']);
            }
            $content_unit = $app['content_unit_factory']->create($type);
            if ($content_unit == null || !$content_unit->canBeCreated()) {
                throw new Exception('The image could not be created.');
            }
            $data_file = new DataFile($app);
            if (!$data_file->createFromTmpFile($file_name)) {
                throw new Exception('The file could not be created.');
            }
            $content_unit->setUploadedImageFilePath($data_file->getPath());
            $content_unit->save($app['viktor.user']);
            if ($content_unit->getId() == 0) {
                throw new Exception('The image could not be saved.');
            }
            return $app['twig']->render(
                '@ContentBox/Field/contentBoxFieldItem.twig',
                array(
                    'language' => $language,
                    'type' => 'image',
                    'content' => '',
                    'reference_type' => $content_unit->getType(),
                    'reference_id' => $content_unit->getId(),
                    'base_name' => $base_name,
                    'valid' => true,
                    'disabled' => false,
                    'error_message' => '',
                )
            );
        } catch (Exception $e) {
            if ($data_file != null) {
                $data_file->delete();
            }
            return $app['twig']->render(
                '@ContentBox/Field/contentBoxFieldItem.twig',
                array(
                    'language' => $language,
                    'type' => 'image',
                    'content' => '',
                    'reference_type' => '',
                    'reference_id' => 0,
                    'base_name' => $base_name,
                    'valid' => false,
                    'disabled' => false,
                    'error_message' => $e->getMessage(),
                )
            );
        }
    }

    public function uploadHandler(Request $request, Application $app, $language, $type, $base_name)
    {
        try {
            $content_unit = $app['content_unit_factory']->create($type);
            if ($content_unit == null || !$content_unit->canBeCreated()) {
                throw new Exception('The image could not be created.');
            }
            $options = array(
                'upload_dir' => $app['viktor.tmp_path'] . '/',
                'param_name' => $base_name . '_files',
                'accept_file_types' => '//',
            );
            $upload_handler = new UploadHandler($options);
            exit();
        } catch (Exception $e) {
            echo $e->getmessage(); exit();
            $app->abort(404);
        }
    }

    public function createItemFromUrl(Request $request, Application $app, $language, $type, $base_name)
    {
        try {
            $url = (string) $request->request->get('url', '');
            $content_unit = $app['content_unit_factory']->create($type);
            if ($content_unit == null || !$content_unit->canBeCreated() || !($content_unit instanceof HasAnEmbedInfoInterface)) {
                throw new Exception('The media could not be created.');
            }
            $content_unit->getEmbedInfo()->setValue(array('source_url' => $url));
            $content_unit->getEmbedInfo()->scrapeSourceUrl();
            $content_unit->save($app['viktor.user']);
            if ($content_unit->getId() == 0) {
                throw new Exception('The media could not be saved.');
            }
            return $app['twig']->render(
                '@ContentBox/Field/contentBoxFieldItem.twig',
                array(
                    'language' => $language,
                    'type' => 'media',
                    'content' => '',
                    'reference_type' => $content_unit->getType(),
                    'reference_id' => $content_unit->getId(),
                    'base_name' => $base_name,
                    'valid' => true,
                    'disabled' => false,
                    'error_message' => '',
                )
            );
        } catch (Exception $e) {
            return '';
        }
    }

}

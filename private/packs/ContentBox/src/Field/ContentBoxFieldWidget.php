<?php

/*
 * Copyright 2022 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\ContentBox\Field;

use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;
use Viktor\Pack\Admin\Field\AbstractFieldWidget;
use Viktor\Pack\Base\Field\AbstractField;
use Viktor\Pack\Base\I18n\TranslatableInterface;
use Viktor\Pack\Base\Tree\NodeInterface;
use Viktor\Pack\Base\Tree\TreeStructure;

class ContentBoxFieldWidget extends AbstractFieldWidget
{
    public function __construct(array $options, AbstractField $field, $prefix)
    {
        parent::__construct($options, $field, $prefix);
        $this->options = array_merge($this->getDefaultOptions(), $options);
    }

    public function getDefaultOptions()
    {
        $parent_default_options = parent::getDefaultOptions();
        $default_options = array(
            'ckeditor_config_file_url' => '/assets/Admin/js/admin-rich-text-field-ckeditor-config.js',
        );
        return array_merge($parent_default_options, $default_options);
    }

    public function form(Request $request, Application $app, $language)
    {
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName();
        $invalid_field_flashbag_name = 'invalid-field-' . $input_name;
        $error_flashbag_name = 'error-' . $input_name;
        $prefill_bag_name = 'prefill-' . $input_name;
        $value = $this->getField()->get();
        $prefill = $app['session']->get($prefill_bag_name);
        if ($prefill !== null) {
            $value = $prefill;
        }
        $this->removePrefilledData($request, $app);
        $content_unit_type_labels = array();
        foreach ($this->getField()->getOptions()['content_unit_types'] as $t) {
            $model = $app['content_unit_factory']->create($t);
            if ($model != null) {
                $content_unit_type_labels[] = $model->getSingularHumanType();
            }
        }
        return $app['twig']->render(
            '@ContentBox/Field/contentBoxFieldForm.twig',
            array(
                'name' => $input_name,
                'value' => $value,
                'options' => $this->getField()->getOptions(),
                'widget_options' => $this->getOptions(),
                'metadata' => $this->getField()->getAllMetadata(),
                'invalid_field_flashbag_name' => $invalid_field_flashbag_name,
                'error_flashbag_name' => $error_flashbag_name,
                'language' => $language,
                'field_language' => $this->getField()->getMetadata('language'),
                'translatable' => $this->getField()->getMetadata('translatable'),
                'disabled' => !$this->getField()->getContentUnit()->canBeEdited(),
                'content_unit_type_labels' => $content_unit_type_labels,
            )
        );
    }

    public function advancedSearchForm(Request $request, Application $app, $language)
    {
        return '';
    }

    public function set(Request $request, Application $app)
    {
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName();
        $new_value = $this->getValueFromPost($request, $input_name);
        $this->getField()->set($new_value);
    }

    public function storePrefilledData(Request $request, Application $app)
    {
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName();
        $prefill_bag_name = 'prefill-' . $input_name;
        $value = $this->getValueFromPost($request, $input_name);
        $app['session']->set($prefill_bag_name, $value);
    }

    public function removePrefilledData(Request $request, Application $app)
    {
        $input_name = $this->getPrefix() . '_' . $this->getField()->getName();
        $prefill_bag_name = 'prefill-' . $input_name;
        $app['session']->remove($prefill_bag_name);
    }

    public function validate(Request $request, Application $app)
    {
        return true;
    }

    public function getFilters(Request $request, Application $app)
    {
        $filters = array();
        return $filters;
    }

    public function storeAdvancedSearchData(Request $request, Application $app)
    {
        return;
    }

    public function getSortOptions()
    {
        $sort_options = array();
        return $sort_options;
    }

    public function getActiveFiltersDescription(Request $request, Application $app)
    {
        $active_filters_description = array();
        return $active_filters_description;
    }

    public function getValueFromPost($request, $input_name)
    {
        $value_type = $request->request->get($input_name . '_type', array());
        $value_content = $request->request->get($input_name . '_content', array());
        $value_reference_type = $request->request->get($input_name . '_reference_type', array());
        $value_reference_id = $request->request->get($input_name . '_reference_id', array());
        $new_value = array();
        if (is_array($value_type)) {
            foreach ($value_type as $k => $type) {
                $new_value[] = array(
                    'type' => $type,
                    'content' => $value_content[$k],
                    'reference_type' => $value_reference_type[$k],
                    'reference_id' => $value_reference_id[$k],
                );
            }
        }
        return $new_value;
    }

}

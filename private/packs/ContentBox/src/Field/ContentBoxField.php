<?php

/*
 * Copyright 2022 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\ContentBox\Field;

use Doctrine\DBAL\Query\QueryBuilder;
use Normalizer;
use Viktor\Pack\Base\Dataset\Dataset;
use Viktor\Pack\Base\Field\AbstractField;
use Viktor\Pack\Base\Field\ContentUnitReferencesIndex;

class ContentBoxField extends AbstractField
{
    private $value;

    public function __construct(Dataset $dataset, array $options, $field_name, array $metadata, array $full_text_index_weights)
    {
        parent::__construct($dataset, $options, $field_name, $metadata, $full_text_index_weights);
        $this->value = array();
        return;
    }

    public function getDefaultOptions()
    {
        return array(
            'indexed' => false,
            'max_number' => 999,
            'authorized_tags' => '<p><em><strong><ol><ul><li><hr>',
            'image_type' => $this->getApp()['viktor.config']['default_image_content_unit_type'],
            'media_type' => '',
            'content_unit_types' => array(),
        );
    }

    public function get()
    {
        return $this->value;
    }

    public function set($value = null)
    {
        $options = $this->getOptions();
        $filtered_value = array();
        if (!is_array($value)) {
            $value = array();
        }
        foreach ($value as $v) {
            if (!isset($v['type'])) {
                $v['type'] = 'separator';
            }
            if (!isset($v['content'])) {
                $v['content'] = '';
            }
            if (!isset($v['reference_type'])) {
                $v['reference_type'] = '';
            }
            if (!isset($v['reference_id'])) {
                $v['reference_id'] = 0;
            }
            if ($v['type'] == 'html') {
                $v['content'] = Normalizer::normalize((string) $v['content'], Normalizer::FORM_C);
                $v['content'] = strip_tags($v['content'], $options['authorized_tags']);
            }
            if ($v['type'] == 'image') {
                if ($v['reference_type'] != $options['image_type'] || $v['reference_id'] <= 0) {
                    continue;
                }
            }
            if ($v['type'] == 'media') {
                if ($v['reference_type'] != $options['media_type'] || $v['reference_id'] <= 0) {
                    continue;
                }
            }
            if ($v['type'] == 'content_unit') {
                if ((!in_array($v['reference_type'], $options['content_unit_types'])) || $v['reference_id'] <= 0) {
                    continue;
                }
            }
            $filtered_value[] = $v;
        }
        $this->value = array_slice($filtered_value, 0, $options['max_number']);
        return $this;
    }

    public function getDigest()
    {
        return $this->value;
    }

    public function updateIndex()
    {
        $references = array();
        foreach ($this->get() as $item) {
            if ($item['reference_id'] != 0) {
                $references[] = array(
                    'type' => $item['reference_type'],
                    'id' => $item['reference_id'],
                );
            }
        }
        $content_unit_references_index = new ContentUnitReferencesIndex(
            $this->getApp(),
            $this->getContentUnit()->getType(),
            $this->getContentUnit()->getId(),
            $this->getName()
        );
        $content_unit_references_index->set($references);
        $result = $content_unit_references_index->updateIndex();
        return $result;
    }

    public function deleteFromIndex()
    {
        $content_unit_references_index = new ContentUnitReferencesIndex(
            $this->getApp(),
            $this->getContentUnit()->getType(),
            $this->getContentUnit()->getId(),
            $this->getName()
        );
        $content_unit_references_index->set(array());
        $result = $content_unit_references_index->deleteFromIndex();
        return $result;
    }

    public function getPlainTextRepresentation()
    {
        $s = '';
        foreach ($this->value as $v) {
            $plain_text = preg_replace('#<br\s*/?-->#i', "\n", $v['content']);
            $plain_text = html_entity_decode(strip_tags($plain_text), ENT_QUOTES | ENT_HTML5);
            $plain_text = trim($plain_text);
            $s .= $plain_text . ' ';
        }
        return $s;
    }

    public function getDecoratedIdsQueryBuilder(QueryBuilder $query_builder, array $filters, array $sort_options)
    {
        return $query_builder;
    }

    public function getRegroupedDigest($group_images_and_medias_together = false)
    {
        $digest = array();
        $image_accumulator = array();
        $media_accumulator = array();
        $common_accumulator = array();
        $content_unit_accumulator = array();
        reset($this->value);
        $item = current($this->value);
        while ($item != false) {
            while ($item != false && $item['type'] == 'separator') {
                $digest[] = $item;
                $item = next($this->value);
            }
            while ($item != false && $item['type'] == 'html') {
                $digest[] = $item;
                $item = next($this->value);
            }
            if ($group_images_and_medias_together) {
                while ($item != false && ($item['type'] == 'image' || $item['type'] == 'media')) {
                    $common_accumulator[] = array(
                        'type' => $item['reference_type'],
                        'id' => $item['reference_id'],
                    );
                    $item = next($this->value);
                }
                if (!empty($common_accumulator)) {
                    $digest[] = array(
                        'type' => 'image_or_media',
                        'content_units' => $common_accumulator,
                    );
                    $common_accumulator = array();
                }
            } else {
                while ($item != false && $item['type'] == 'image') {
                    $image_accumulator[] = array(
                        'type' => $item['reference_type'],
                        'id' => $item['reference_id'],
                    );
                    $item = next($this->value);
                }
                if (!empty($image_accumulator)) {
                    $digest[] = array(
                        'type' => 'image',
                        'content_units' => $image_accumulator,
                    );
                    $image_accumulator = array();
                }
                while ($item != false && $item['type'] == 'media') {
                    $media_accumulator[] = array(
                        'type' => $item['reference_type'],
                        'id' => $item['reference_id'],
                    );
                    $item = next($this->value);
                }
                if (!empty($media_accumulator)) {
                    $digest[] = array(
                        'type' => 'media',
                        'content_units' => $media_accumulator,
                    );
                    $media_accumulator = array();
                }
            }
            while ($item != false && ($item['type'] == 'content_unit')) {
                $content_unit_accumulator[] = array(
                    'type' => $item['reference_type'],
                    'id' => $item['reference_id'],
                );
                $item = next($this->value);
            }
            if (!empty($content_unit_accumulator)) {
                $digest[] = array(
                    'type' => 'content_unit',
                    'content_units' => $content_unit_accumulator,
                );
                $content_unit_accumulator = array();
            }
            while ($item != false && !in_array($item['type'], array('separator', 'html', 'image', 'media', 'content_unit'))) {
                $item = next($this->value);
            }
        }
        return $digest;
    }

}

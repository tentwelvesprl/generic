<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Tree;

use Exception;
use Symfony\Component\Yaml\Yaml;
use Viktor\Application;

class TreeStructure
{
    private $app;
    private $type;
    private $items;

    public function __construct(Application $app, $type)
    {
        $this->app = $app;
        $this->type = (string) $type;
        $this->items = array();
    }

    public function getItems()
    {
        return $this->items;
    }

    public function load()
    {
        $this->items = array();
        $query_builder = $this->app['db']->createQueryBuilder();
        $query_builder = $query_builder
            ->select('content')
            ->from('tree_structure')
            ->where('type = :type')
            ->setParameter('type', $this->type)
        ;
        $result = $query_builder->execute();
        $content = array();
        if ($row = $result->fetch()) {
            try {
                $content = Yaml::parse($row['content']);
            } catch (Exception $e) {
                $content = array();
            }
        }
        foreach ($content as $id => $parent_id) {
            $this->addItem($id, $parent_id);
        }
        // Make sure that:
        // - every existing node is present into the structure;
        // - the structure does not reference any non-existing node.
        $node_model = $this->app['content_unit_factory']->create($this->type);
        if ($node_model == null) {
            throw new Exception($this->type . ' is not a valid content unit type.');
        }
        $nodes_ids = $node_model->getIds();
        sort($nodes_ids);
        foreach ($nodes_ids as $node_id) {
            if (!$this->itemIsPresent($node_id)) {
                $this->addItem($node_id, 0);
            }
        }
        $items_array_copy = $this->items;
        foreach ($items_array_copy as $item_id => $parent_id) {
            if (!in_array($item_id, $nodes_ids)) {
                $this->removeItem($item_id);
            }
        }
        // Reorder items.
        $ordered_items = $this->getChildrenItems(0);
        $this->items = $ordered_items;
        return $this;
    }

    public function getChildrenItems($id, $recursion_level = 0) {
        $children_items = array();
        if ($recursion_level > 50) {
            return $children_items;
        }
        foreach ($this->items as $k => $parent_k) {
            if ($parent_k == $id) {
                $children_items[$k] = $parent_k;
                $children_items = $children_items + $this->getChildrenItems($k, $recursion_level + 1);
            }
        }
        return $children_items;
    }

    public function getParentIds($id, $recursion_level = 0)
    {
        $parent_ids = array();
        if ($recursion_level > 50) {
            return $parent_ids;
        }
        if (!isset($this->items[$id])) {
            return $parent_ids;
        }
        if ($this->items[$id] == 0) {
            $parent_ids = array();
        } else {
            $parent_ids = $this->getParentIds($this->items[$id]);
            $parent_ids[] = $this->items[$id];
        }
        return $parent_ids;
    }

    public function save()
    {
        $this->delete();
        $query_builder = $this->app['db']->createQueryBuilder();
        $query_builder
            ->insert('tree_structure')
            ->setValue('type', ':type')
            ->setValue('content', ':content')
            ->setParameter('type', $this->type)
            ->setParameter('content', Yaml::dump($this->items));
        if ($query_builder->execute() == 1) {
            $query_successful = true;
        } else {
            $query_successful = false;
        }
        return $query_successful;
    }

    public function delete()
    {
        $query_builder = $this->app['db']->createQueryBuilder();
        $query_builder
            ->delete('tree_structure')
            ->where('type = :type')
            ->setParameter('type', $this->type);
        if ($query_builder->execute() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function processStructure()
    {
        // First make sure that items are ordered.
        $ordered_items = $this->getChildrenItems(0);
        $this->items = $ordered_items;
        // Process.
        $flat_list = array(0 => array());
        $structure = array(0 => &$flat_list[0]);
        $levels = array(0 => 0);
        foreach ($this->items as $id => $parent_id) {
            $flat_list[$id] = array();
            if (isset($flat_list[$parent_id])) {
                $flat_list[$parent_id][$id] = &$flat_list[$id];
                $levels[$id] = $levels[$parent_id] + 1;
            } else {
                $flat_list[0][$id] = &$flat_list[$id];
                $levels[$id] = 1;
            }
        }
        return array(
            'flat_list' => $flat_list,
            'structure' => $structure[0],
            'levels' => $levels,
        );
    }

    public function getFlatList()
    {
        $process_result = $this->processStructure();
        return $process_result['flat_list'];
    }

    public function getStructure()
    {
        $process_result = $this->processStructure();
        return $process_result['structure'];
    }

    public function getLevels()
    {
        $process_result = $this->processStructure();
        return $process_result['levels'];
    }

    public function itemIsPresent($id)
    {
        $id = (int) $id;
        if (isset($this->items[$id])) {
            return true;
        } else {
            return false;
        }
    }

    public function addItem($id, $parent_id)
    {
        $id = (int) $id;
        $parent_id = (int) $parent_id;
        if ($id > 0) { // The new item id must be valid.
            if ($parent_id < 0) {
                $parent_id = 0;
            }
            if (!isset($this->items[$id])) { // The new item must not be already present.
                if (isset($this->items[$parent_id])) { // The parent item must exist.
                    $this->items[$id] = $parent_id;
                } else {
                    $this->items[$id] = 0;
                }
            }
        }
        return $this;
    }

    public function removeItem($id)
    {
        $id = (int) $id;
        if (isset($this->items[$id])) {
            unset($this->items[$id]);
        }
        foreach ($this->items as $item_id => $parent_id) {
            if ($parent_id == $id) { // No node can be a child of the removed item.
                $this->items[$item_id] = 0;
            }
        }
        return $this;
    }

    public function setParentId($id, $parent_id)
    {
        if (isset($this->items[$id]) && isset($this->items[$parent_id])) {
            if ($this->items[$id] != $parent_id) {
                $this->removeItem($id);
                $this->addItem($id, $parent_id);
            }
        }
    }

    public function getParentId($id)
    {
        if (isset($this->items[$id])) {
            return $this->items[$id];
        }
        return 0;
    }

}

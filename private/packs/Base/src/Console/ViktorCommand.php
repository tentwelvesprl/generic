<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Console;

use Knp\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Viktor\Pack\Base\Console\ViktorStyle;

class ViktorCommand extends Command
{
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $comment_style = new OutputFormatterStyle('blue');
        $output->getFormatter()->setStyle('comment', $comment_style);
        $info_style = new OutputFormatterStyle('cyan');
        $output->getFormatter()->setStyle('info', $info_style);
        $io = new ViktorStyle($input, $output);
        return (int) $this->executeWithStyle($io, $input, $output);
    }

    protected function executeWithStyle(ViktorStyle $io, InputInterface $input, OutputInterface $output)
    {
    }

}

<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\ContentUnit;

use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Viktor\Pack\Base\Console\ViktorCommand;
use Viktor\Pack\Base\Console\ViktorStyle;
use Viktor\Pack\Base\User\SecretaryUser;

class ContentUnitCommand extends ViktorCommand
{
    protected function configure()
    {
        $this
            ->setName('viktor:cu')
            ->setDescription('Performs various tasks on content units')
            ->setHelp('This command can save all content units.')
            ->setDefinition(
                new InputDefinition(array(
                    new InputOption('save-all', 's', InputOption::VALUE_NONE, 'Load and save all the content units'),
                    new InputOption('list-zero-reference', 'z', InputOption::VALUE_NONE, 'List all content units which have zero reference'),
                    new InputOption('type', 't', InputOption::VALUE_OPTIONAL, 'Restrict actions to this content unit type'),
                    new InputOption('delete', 'd', InputOption::VALUE_REQUIRED, 'Delete a content unit'),
                ))
            );
        return;
    }

    protected function executeWithStyle(ViktorStyle $io, InputInterface $input, OutputInterface $output)
    {
        $app = $this->getSilexApplication();
        $user = new SecretaryUser($app);
        $type_option = $input->getOption('type');
        $delete_option = $input->getOption('delete');
        // Save all content units.
        if ($input->getOption('save-all')) {
            foreach ($app['content_unit_factory']->getAvailableTypes() as $content_unit_type => $content_unit_class_name) {
                if ($type_option != null && $type_option != $content_unit_type) {
                    continue;
                }
                $content_unit_model = $app['content_unit_factory']->create($content_unit_type);
                $io->section($content_unit_model->getPluralHumanType());
                $ids = $content_unit_model->getIds();
                foreach ($ids as $id) {
                    $content_unit = $app['content_unit_factory']->create($content_unit_type);
                    $content_unit->load($id);
                    if ($content_unit->getId() == 0) {
                        $io->text(''
                            . '<error>#' . $id . '</error> '
                            . 'This unit could not be loaded.'
                        );
                    } else {
                        $io->text(''
                            . '<info>#' . $id . '</info> '
                            . $content_unit->getTitle()
                        );
                        $content_unit->save($user);
                    }
                    unset($content_unit);
                }
                $output->writeln('');
            }
        }
        // List all content units with zero reference.
        if ($input->getOption('list-zero-reference')) {
            foreach ($app['content_unit_factory']->getAvailableTypes() as $content_unit_type => $content_unit_class_name) {
                if ($type_option != null && $type_option != $content_unit_type) {
                    continue;
                }
                $content_unit_model = $app['content_unit_factory']->create($content_unit_type);
                $ids = $content_unit_model->getIds();
                foreach ($ids as $id) {
                    $content_unit = $app['content_unit_factory']->create($content_unit_type);
                    $content_unit->load($id);
                    if ($content_unit->getId() != 0) {
                        $referencing_content_units_array = $content_unit->getDbStoreComponent()->getReferencingContentUnits();
                        if (count($referencing_content_units_array) == 0) {
                            $io->text($content_unit->getType() . ':' . $content_unit->getId());
                        }
                    }
                    unset($content_unit);
                }
                $output->writeln('');
            }
        }
        // Delete a content unit.
        if ($delete_option != null) {
            $content_unit_reference = explode(':', $delete_option);
            if (count($content_unit_reference) != 2) {
                $io->error('Invalid content unit reference for the delete operation.');
            } elseif (!$content_unit = $app['content_unit_factory']->load((string) $content_unit_reference[0], (int) $content_unit_reference[1])) {
                $io->error('This content unit cannot be loaded.');
            } else {
                $content_unit->delete();
                $io->text('Content unit deleted.');
            }
        }
        return;
    }

}

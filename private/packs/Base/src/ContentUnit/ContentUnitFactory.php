<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\ContentUnit;

use Viktor\Application;

class ContentUnitFactory
{
    private $app;
    private $availableTypes;
    private $registry;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->availableTypes = array();
        foreach ($this->app['viktor.config']['content_unit_types'] as $type => $class_name) {
            $this->registerType($class_name);
        }
        $this->registry = array();
        return;
    }

    public function registerType($class_name)
    {
        if (
            class_exists($class_name)
            && is_a($class_name, 'Viktor\Pack\Base\ContentUnit\ContentUnitInterface', true)
        ) {
            $class_model = new $class_name($this->app);
            $this->availableTypes[$class_model->getType()] = $class_name;
        }
        return $this;
    }

    public function getAvailableTypes()
    {
        return $this->availableTypes;
    }

    public function create($type)
    {
        if (!isset($this->availableTypes[$type])) {
            return null;
        } else {
            $class_name = $this->availableTypes[$type];
        }
        if (
            class_exists($class_name)
            && is_a($class_name, 'Viktor\Pack\Base\ContentUnit\ContentUnitInterface', true)
        ) {
            return new $class_name($this->app);
        }
        return null;
    }

    public function load($type, $id)
    {
        $type = (string) $type;
        $id = (int) $id;
        $key = $type . '$' . $id;
        if (isset($this->registry[$key])) {
            return $this->registry[$key];
        } else {
            $content_unit = $this->create($type);
            if ($content_unit !== null) {
                $load_result = $content_unit->load($id);
                if ($load_result) {
                    $this->registry[$key] = $content_unit;
                    return $content_unit;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }
    }

    public function createList($type, array $ids)
    {
        $l = new ContentUnitList();
        foreach ($ids as $id) {
            $content_unit = $this->load($type, $id);
            if ($content_unit !== null) {
                $l->append($content_unit);
            }
        }
        return $l;
    }

    public function getTypeFromClassName($class_name)
    {
        $key = array_search($class_name, $this->availableTypes);
        return $key;
    }

}

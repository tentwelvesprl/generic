<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\ContentUnit;

use Viktor\Application;
use Viktor\Pack\Base\User\UserInterface;

interface ContentUnitInterface
{
    public function __construct(Application $app);

    public function load($id);

    public function save(UserInterface $user);

    public function delete();

    public function getType();

    public function getId();

    public function getDigest();

    public function getIds(array $filters = array());

    public function getSingularHumanType();

    public function getPluralHumanType();

    public function canBeCreated();

    public function canBeViewed();

    public function canBeEdited();

    public function canBeDeleted();

    public function canBeListed();

    public function getCreationPageUrl();

    public function getViewPageUrl();

    public function getEditionPageUrl();

    public function getDeletionPageUrl();

    public function getListPageUrl();

    public function getTitle();

    public function getHtmlTitle();

    public function getHint();

    public function getDescription();

    public function getHtmlDescription();

    public function getImage();

    public function getImageReference();

    public function getCreationDateTime();

    public function getModificationDateTime();

}

<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\ContentUnit;

use ArrayObject;

class ContentUnitList extends ArrayObject
{
    public function __construct()
    {
        return parent::__construct();
    }

    public function append(mixed $value): void
    {
        if ($value instanceof ContentUnitInterface) {
            parent::append($value);
        }
    }

    public function exchangeArray(object|array $array): array
    {
        if ($input instanceof ContentUnitList) {
            parent::exchangeArray($array);
        }
    }

    public function offsetSet(mixed $key, mixed $value): void
    {
        if ($value instanceof ContentUnitInterface) {
            parent::offsetSet($key, $value);
        }
    }

    public function getDigest()
    {
        $digest = array();
        $iterator = $this->getIterator();
        while ($iterator->valid()) {
            $digest[] = $iterator->current()->getDigest();
            $iterator->next();
        }
        return $digest;
    }

}

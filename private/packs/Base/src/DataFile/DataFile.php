<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\DataFile;

use DateTime;
use Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Viktor\Application;

class DataFile
{
    private $app;
    private $path;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->path = '';
    }

    public function getPath()
    {
        return $this->path;
    }

    public function load($path)
    {
        $this->path = (string) $path;
    }

    public function getFilename()
    {
        return basename($this->path);
    }

    public function generateSubdirPath()
    {
        $dt = new DateTime();
        $subdir = $dt->format('/Y/m/d/H/i');
        return $subdir;
    }

    public function generateUniqueDestinationName($subdir, $original_name)
    {
        $path_parts = pathinfo($original_name);
        $destination_name = $original_name;
        if (file_exists($this->app['viktor.data_path'] . $subdir . '/' . $destination_name)) {
            $i = 1;
            while (file_exists($this->app['viktor.data_path'] . $subdir . '/' . $destination_name)) {
                $destination_name = ''
                    . $path_parts['filename']
                    . '-' . $i
                    . ($path_parts['extension'] != '' ? '.' : '') . $path_parts['extension'];
                $i++;
            }
        }
        return $destination_name;
    }

    public function createFromUploadedFile(UploadedFile $uploadedFile)
    {
        try {
            $subdir = $this->generateSubdirPath();
            $original_name = $uploadedFile->getClientOriginalName();
            $destination_name = $this->generateUniqueDestinationName($subdir, $original_name);
            $uploadedFile->move($this->app['viktor.data_path'] . $subdir, $destination_name);
            $this->path = $subdir . '/' . $destination_name;
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function createFromUrl($url)
    {
        try {
            $subdir = $this->generateSubdirPath();
            @mkdir($this->app['viktor.data_path'] . $subdir, 0777, true);
            $original_name = basename((string) parse_url($url, PHP_URL_PATH));
            if (trim($original_name) == '') {
                $original_name = 'data';
            }
            $destination_name = $this->generateUniqueDestinationName($subdir, $original_name);
            $new_file_path = $this->app['viktor.data_path'] . $subdir . '/' . $destination_name;
            $handle = fopen($new_file_path, 'w');
            $curl_handle = curl_init($url);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 20);
            curl_setopt($curl_handle, CURLOPT_FILE, $handle);
            // Limit upload size.
            curl_setopt($curl_handle, CURLOPT_BUFFERSIZE, 128);
            curl_setopt($curl_handle, CURLOPT_NOPROGRESS, false);
            curl_setopt(
                $curl_handle,
                CURLOPT_PROGRESSFUNCTION,
                function ($download_size, $downloaded, $upload_size, $uploaded) {
                    // If $downloaded exceeds 20 MB, returning non-0 breaks the connection.
                    return ($downloaded > (20 * 1024 * 1024)) ? 1 : 0;
                }
            );
            if (!curl_exec($curl_handle)) {
                throw new Exception('The file could not be downloaded.');
            }
            $this->path = $subdir . '/' . $destination_name;
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function createFromTmpFile($file_name)
    {
        try {
            $subdir = $this->generateSubdirPath();
            @mkdir($this->app['viktor.data_path'] . $subdir, 0777, true);
            $original_name = $file_name;
            if (trim($original_name) == '') {
                $original_name = 'data';
            }
            $destination_name = $this->generateUniqueDestinationName($subdir, $original_name);
            $new_file_path = $this->app['viktor.data_path'] . $subdir . '/' . $destination_name;
            if (!@rename($this->app['viktor.tmp_path'] . '/' . $file_name, $new_file_path)) {
                throw new Exception('The file could not be copied.');
            }
            $this->path = $subdir . '/' . $destination_name;
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function createFromFile($file_path)
    {
        try {
            $subdir = $this->generateSubdirPath();
            @mkdir($this->app['viktor.data_path'] . $subdir, 0777, true);
            $original_name = basename($file_path);
            if (trim($original_name) == '') {
                $original_name = 'data';
            }
            $destination_name = $this->generateUniqueDestinationName($subdir, $original_name);
            $new_file_path = $this->app['viktor.data_path'] . $subdir . '/' . $destination_name;
            if (!@copy($file_path, $new_file_path)) {
                throw new Exception('The file could not be copied.');
            }
            $this->path = $subdir . '/' . $destination_name;
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function delete()
    {
        if ($this->path != '') {
            @unlink($this->app['viktor.data_path'] . $this->getPath());
        }
    }

}

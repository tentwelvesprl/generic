<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\UserCredentials;

use Viktor\Application;

class PasswordHelper
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function getApp()
    {
        return $this->app;
    }

    public function getValidityDuration()
    {
        // Seven days, in seconds.
        return 7 * 24 * 3600;
    }

    public function findUsersByEmail(string $email)
    {
        $users = array();
        $query_builder = $this->getApp()['db']->createQueryBuilder();
        $query_builder
            ->select(
                'user_credentials.type AS user_credentials$type',
                'user_credentials.id AS user_credentials$id'
            )
            ->from('user_credentials')
            ->where('user_credentials.email = :email')
            ->setParameter('email', $email)
        ;
        $result = $query_builder->execute();
        while ($row = $result->fetch()) {
            $users[] = array(
                'type' => $row['user_credentials$type'],
                'id' => $row['user_credentials$id'],
            );
        }
        return $users;
    }

    public function findUserByToken(string $token)
    {
        $users = array();
        $query_builder = $this->getApp()['db']->createQueryBuilder();
        $query_builder
            ->select(
                'password_forgotten.type AS password_forgotten$type',
                'password_forgotten.id AS password_forgotten$id'
            )
            ->from('password_forgotten')
            ->where('password_forgotten.token = :token')
            ->andWhere('password_forgotten.uts > :mintime')
            ->setParameter('token', $token)
            ->setParameter('mintime', time() - $this->getValidityDuration())
        ;
        $result = $query_builder->execute();
        $user = null;
        if ($row = $result->fetch()) {
            $user = array(
                'type' => $row['password_forgotten$type'],
                'id' => $row['password_forgotten$id'],
            );
        }
        return $user;
    }

    public function createToken(string $user_type, int $user_id)
    {
        $token = md5($user_type . $user_id . time() . rand());
        $query_builder = $this->getApp()['db']->createQueryBuilder();
        $query_builder
            ->insert('password_forgotten')
            ->setValue('type', ':type')
            ->setValue('id', ':id')
            ->setValue('uts', ':uts')
            ->setValue('token', ':token')
            ->setParameter('type', $user_type)
            ->setParameter('id', $user_id)
            ->setParameter('uts', time())
            ->setParameter('token', $token)
        ;
        $result = $query_builder->execute();
        if ($result == 1) {
            return $token;
        } else {
            return null;
        }
    }

    public function deleteToken(string $token)
    {
        $users = array();
        $query_builder = $this->getApp()['db']->createQueryBuilder();
        $query_builder
            ->delete('password_forgotten')
            ->where('password_forgotten.token = :token')
            ->setParameter('token', $token)
        ;
        $result = $query_builder->execute();
        return $result;
    }

    public function generateRandomPassword()
    {
        // Only use characters that are easy to distinguish.
        $alphabet = 'abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789';
        $password = '';
        for ($i = 0; $i < 16; $i++) {
            $password .= substr($alphabet, rand(0, strlen($alphabet) - 1), 1);
        }
        return $password;
    }

}

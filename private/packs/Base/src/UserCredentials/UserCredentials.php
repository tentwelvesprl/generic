<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\UserCredentials;

use Doctrine\DBAL\Query\QueryBuilder;
use Viktor\Application;
use Viktor\Pack\Base\ContentUnit\ContentUnitInterface;

class UserCredentials
{
    private $username;
    private $password;
    private $roles;
    private $email;
    private $app;
    private $contentUnit;

    public function __construct(Application $app, ContentUnitInterface $content_unit)
    {
        $this->username = '';
        $this->password = '';
        $this->roles = array();
        $this->email = '';
        $this->app = $app;
        $this->contentUnit = $content_unit;
        return;
    }

    public function getContentUnit()
    {
        return $this->contentUnit;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setUsername($username)
    {
        $this->username = (string) $username;
        return $this;
    }

    public function setPassword($password)
    {
        $this->password = (string) $password;
        return $this;
    }

    public function setRoles(array $roles)
    {
        $this->roles = $roles;
        return $this;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function getDecoratedRowQueryBuilder(QueryBuilder $query_builder)
    {
        $query_builder
            ->addSelect(
                'user_credentials.username AS user_credentials$username',
                'user_credentials.password AS user_credentials$password',
                'user_credentials.roles AS user_credentials$roles',
                'user_credentials.email AS user_credentials$email'
            )
            ->innerJoin(
                'component',
                'user_credentials', 'user_credentials',
                'component.type = user_credentials.type AND component.id = user_credentials.id'
            )
        ;
        return $query_builder;
    }

    public function getDecoratedIdsQueryBuilder(QueryBuilder $query_builder, array $filters, array $sort_options)
    {
        foreach ($sort_options as $sort_option) {
            if (!is_array($sort_option)) {
                continue;
            }
            $option = (string) $sort_option['option'];
            $direction = (string) $sort_option['direction'];
            switch ($option) {
                case 'username':
                    $field_alias = 'f' . md5($option);
                    $query_builder
                        ->innerJoin(
                            'component',
                            'user_credentials', $field_alias,
                            'component.type = ' . $field_alias . '.type AND component.id = ' . $field_alias . '.id'
                        )
                        ->addOrderBy($field_alias . '.username', $direction);
                    break;
            }
        }
        return $query_builder;
    }

    public function setFromRow(array $row)
    {
        $this->setUsername($row['user_credentials$username']);
        $this->setPassword($row['user_credentials$password']);
        $this->setRoles(explode(',', $row['user_credentials$roles']));
        $this->setEmail($row['user_credentials$email']);
        return;
    }

    public function usernameIsAvailable()
    {
        $result = $this->app['db']->executeQuery(
            '
                SELECT
                    username
                FROM
                    user_credentials
                WHERE
                    username = ?
                    AND (type != ? OR id != ?)
            ',
            array(
                $this->contentUnit->getUsername(),
                $this->contentUnit->getType(),
                $this->contentUnit->getId()
            )
        );
        if ($result->fetch()) {
            return false;
        } else {
            return true;
        }
    }

    public function save()
    {
        if ($this->contentUnit->getId() <= 0) {
            return false;
        }
        $result = $this->app['db']->executeQuery(
            '
                SELECT username
                FROM user_credentials
                WHERE
                    type = ?
                    AND id = ?
            ',
            array(
                $this->contentUnit->getType(),
                $this->contentUnit->getId()
            )
        );
        if ($row = $result->fetch()) {
            // Update.
            if (!$this->usernameIsAvailable())
            {
                return false;
            }
            $query_builder = $this->app['db']->createQueryBuilder();
            $query_builder
                ->update('user_credentials')
                ->set('username', ':username')
                ->set('password', ':password')
                ->set('roles', ':roles')
                ->set('email', ':email')
                ->where('type = :type AND id = :id')
                ->setParameter('username', $this->getUsername())
                ->setParameter('password', $this->getPassword())
                ->setParameter('roles', implode(',', $this->getRoles()))
                ->setParameter('email', $this->getEmail())
                ->setParameter('type', $this->contentUnit->getType())
                ->setParameter('id', $this->contentUnit->getId());
            $query_builder->execute();
            return true;
        } else {
            // Insert.
            $query_builder = $this->app['db']->createQueryBuilder();
            $query_builder
                ->insert('user_credentials')
                ->setValue('type', ':type')
                ->setValue('id', ':id')
                ->setValue('username', ':username')
                ->setValue('password', ':password')
                ->setValue('roles', ':roles')
                ->setValue('email', ':email')
                ->setParameter('type', $this->contentUnit->getType())
                ->setParameter('id', $this->contentUnit->getId())
                ->setParameter('username', $this->getUsername())
                ->setParameter('password', $this->getPassword())
                ->setParameter('roles', implode(',', $this->getRoles()))
                ->setParameter('email', $this->getEmail());
            if ($query_builder->execute() == 1) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function delete()
    {
        $query_builder = $this->app['db']->createQueryBuilder();
        $query_builder
            ->delete('user_credentials')
            ->where('type = :type AND id = :id')
            ->setParameter('type', $this->contentUnit->getType())
            ->setParameter('id', $this->contentUnit->getId());
        if ($query_builder->execute() == 1) {
            return true;
        } else {
            return false;
        }
    }

}

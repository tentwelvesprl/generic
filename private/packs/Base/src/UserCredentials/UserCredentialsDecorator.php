<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\UserCredentials;

use Viktor\Pack\Base\DbStore\AbstractDbStoreDecorator;
use Viktor\Pack\Base\DbStore\DbStoreComponentInterface;
use Viktor\Pack\Base\User\UserInterface;

class UserCredentialsDecorator extends AbstractDbStoreDecorator
{
    private $userCredentials;

    public function __construct(DbStoreComponentInterface $component, UserCredentials $user_credentials)
    {
        parent::__construct($component);
        $this->userCredentials = $user_credentials;
        return;
    }

    public function getRowQueryBuilder($id)
    {
        $query_builder = $this->component->getRowQueryBuilder($id);
        $query_builder = $this->userCredentials->getDecoratedRowQueryBuilder($query_builder);
        return $query_builder;
    }

    public function setFromRow(array $row)
    {
        $this->component->setFromRow($row);
        $this->userCredentials->setFromRow($row);
        return;
    }

    public function save(UserInterface $user)
    {
        if (!$this->component->save($user)) {
            return false;
        } else {
            return $this->userCredentials->save();
        }
    }

    public function delete()
    {
        if (!$this->userCredentials->delete()) {
            return false;
        } else {
            return $this->component->delete();
        }
    }

    public function getIdsQueryBuilder(array $filters = array(), array $sort_options = array())
    {
        $query_builder = $this->component->getIdsQueryBuilder($filters, $sort_options);
        return $this->userCredentials->getDecoratedIdsQueryBuilder($query_builder, $filters, $sort_options);
    }

    public function getDigest()
    {
        $digest = $this->component->getDigest();
        $digest['username'] = $this->userCredentials->getUsername();
        $digest['password'] = $this->userCredentials->getPassword();
        $digest['roles'] = $this->userCredentials->getRoles();
        $digest['email'] = $this->userCredentials->getEmail();
        return $digest;
    }

}

<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\FullTextIndex;

use Viktor\Application;
use Viktor\Pack\Base\ContentUnit\ContentUnitInterface;

class FullTextIndexer
{
    private $app;
    private $contentUnitType;
    private $contentUnitId;
    private $indexes;

    public function __construct(Application $app, $content_unit_type, $content_unit_id)
    {
        $this->app = $app;
        $this->contentUnitType = (string) $content_unit_type;
        $this->contentUnitId = (int) $content_unit_id;
        $this->indexes = array();
    }

    public function addTokens(array $tokens, $weight, $index_id)
    {
        $index_id = (string) $index_id;
        if (!isset($this->indexes[$index_id])) {
            $this->indexes[$index_id] = array();
        }
        foreach ($tokens as $token) {
            if (mb_strlen($token) >= 2) {
                if (!isset($this->indexes[$index_id][$token])) {
                    $this->indexes[$index_id][$token] = 0;
                }
                $this->indexes[$index_id][$token] += $weight;
            }
        }
    }

    public function update()
    {
        $this->delete();
        foreach ($this->indexes as $index_id => $tokens_scores) {
            foreach ($tokens_scores as $token => $score) {
                $token = mb_substr($token, 0, 25);
                $query_builder = $this->app['db']->createQueryBuilder();
                $query_builder
                    ->insert('full_text_index')
                    ->setValue('type', ':type')
                    ->setValue('id', ':id')
                    ->setValue('index_id', ':index_id')
                    ->setValue('token', ':token')
                    ->setValue('score', ':score')
                    ->setParameter('type', $this->contentUnitType)
                    ->setParameter('id', $this->contentUnitId)
                    ->setParameter('index_id', $index_id)
                    ->setParameter('token', $token)
                    ->setParameter('score', $score);
                $query_builder->execute();
            }
        }
    }

    public function delete()
    {
        $query_builder = $this->app['db']->createQueryBuilder();
        $query_builder
            ->delete('full_text_index')
            ->where('type = :type AND id = :id')
            ->setParameter('type', $this->contentUnitType)
            ->setParameter('id', $this->contentUnitId);
        $query_builder->execute();
    }

    public function addToIndexationQueue($action)
    {
        $query_builder = $this->app['db']->createQueryBuilder();
        $query_builder
            ->insert('full_text_indexation_queue')
            ->setValue('content_unit_type', ':type')
            ->setValue('content_unit_id', ':id')
            ->setValue('action', ':action')
            ->setParameter('type', $this->contentUnitType)
            ->setParameter('id', $this->contentUnitId)
            ->setParameter('action', (string) $action);
        $query_builder->execute();
    }

}

<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\FullTextIndex;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Viktor\Application;
use Viktor\Pack\Base\Console\ViktorStyle;
use Viktor\Pack\Base\Cron\AbstractCronTask;

class FullTextIndexCronTask extends AbstractCronTask
{
    public function getTitle()
    {
        return 'Full text indexation';
    }

    public function execute(ViktorStyle $io, InputInterface $input, OutputInterface $output)
    {
        $queue = new FullTextIndexationQueue($this->getApp());
        for ($i = 0; $i < 10; $i++) {
            $result = $queue->processNextItem();
            $io->text($result['message']);
        }
    }

}

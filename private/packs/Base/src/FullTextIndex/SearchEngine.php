<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\FullTextIndex;

use PDO;
use Viktor\Application;
use Viktor\Pack\Base\FullTextIndex\Tokenizer;

class SearchEngine
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function getSearchResults($q, $index_id, $types = null, array $options = array(), $language = '')
    {
        $tokenizer = new Tokenizer((string) $q, (string) $language);
        $tokens = $tokenizer->getTokens();
        $sanitized_tokens = array();
        foreach ($tokens as $token) {
            $token = trim($token);
            if (mb_strlen($token) >= 2) {
                $sanitized_tokens[] = $token;
            }
        }
        if (count($sanitized_tokens) == 0) {
            return array();
        }
        if (count($sanitized_tokens) > 10) {
            $sanitized_tokens = array_slice($sanitized_tokens, 0, 10);
        }
        $index_id = (string) $index_id;
        if (!is_array($types)) {
            $types = array();
        }
        $default_options = array(
            'search_incomplete_tokens' => false,
            'require_every_token' => true,
            'max_number_of_results' => 100,
        );
        $options = array_merge($default_options, $options);
        $result_sets = array();
        foreach ($sanitized_tokens as $token) {
            $query_builder = $this->app['db']->createQueryBuilder();
            $query_builder
                ->select(
                    'full_text_index.type AS full_text_index$type',
                    'full_text_index.id AS full_text_index$id',
                    'SUM(full_text_index.score) AS total_score'
                )
                ->from('full_text_index', 'full_text_index')
                ->where('full_text_index.index_id = :index_id')
                ->groupBy('full_text_index$type', 'full_text_index$id')
                ->orderBy('total_score', 'DESC')
                ->setFirstResult(0)
                ->setMaxResults($options['max_number_of_results'])
                ->setParameter(':index_id', $index_id);
            if (count($types) > 0) {
                $escaped_types = array();
                foreach ($types as $type) {
                    $escaped_types[] = $this->app['db']->quote($type, PDO::PARAM_STR);
                }
                $query_builder->andWhere('full_text_index.type IN (' . implode(', ', $escaped_types) . ')');
            }
            if ($options['search_incomplete_tokens']) {
                $query_builder->andWhere('full_text_index.token LIKE \'' . addcslashes($token, '%_') . '%\'');
            } else {
                $query_builder->andWhere('full_text_index.token = \'' . addcslashes($token, '%_') . '\'');
            }
            $result = $query_builder->execute();
            $results = array();
            while ($row = $result->fetch()) {
                $k = $row['full_text_index$type'] . ':' . $row['full_text_index$id'];
                $results[$k] = array(
                    'type' => (string) $row['full_text_index$type'],
                    'id' => (int) $row['full_text_index$id'],
                    'score' => (int) $row['total_score'],
                );
            }
            $result_sets[] = $results;
        }
        $first_result_set = array_shift($result_sets);
        $final_result_set = array();
        foreach ($first_result_set as $k => $result) {
            $score = $result['score'];
            foreach ($result_sets as $result_set) {
                if ($options['require_every_token'] && !isset($result_set[$k])) {
                    continue 2; // One of the searched tokens is not present.
                }
                $score += $result_set[$k]['score'];
            }
            $k = sprintf('%07d', $score) . ':' . $k;
            $final_result_set[$k] = array(
                'type' => $result['type'],
                'id' => $result['id'],
                'score' => $score,
            );
        } 
        krsort($final_result_set);
        return $final_result_set;
    }

}

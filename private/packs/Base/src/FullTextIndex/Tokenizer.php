<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\FullTextIndex;

class Tokenizer
{
    private $text;
    private $tokens;
    private $language;
    private $excludedWords;

    public function __construct($text, $language = '')
    {
        $this->text = (string) $text;
        $this->tokens = null;
        $this->language = (string) $language;
    }

    public function removePunctuationFromText()
    {
        $text = $this->text;
        // Use the 'u' modifier to turn UTF-8 on.
        $pattern = '@[^[:alnum:]]@u';
        $text = preg_replace($pattern, ' ', $text);
        $text = str_replace("\n", " ", $text);
        $text = str_replace("\r", " ", $text);
        $text = str_replace("\t", " ", $text);
        $this->text = $text;
        return $this;
    }

    public function normalizeText()
    {
        $text = $this->text;
        $text = mb_strtolower($text);
        $this->text = $text;
        return $this;
    }

    public function generateTokens()
    {
        $this->removePunctuationFromText()->normalizeText();
        $words = explode(' ', $this->text);
        $tokens = array();
        foreach ($words as $word) {
            $word = trim($word);
            if ($word == '') {
                continue;
            }
            if ($this->isExcluded($word)) {
                continue;
            }
            $tokens[] = $word;
        }
        $this->tokens = $tokens;
        return $this;
    }

    public function getTokens()
    {
        if ($this->tokens === null) {
            $this->generateTokens();
        }
        return $this->tokens;
    }

    public function isExcluded($word)
    {
        if ($this->excludedWords == null)  {
            $this->excludedWords = $this->getExcludedWordsList();
        }
        return in_array($word, $this->excludedWords);
    }

    public function getExcludedWordsList()
    {
        if ($this->language == 'fr') {
            return array(
                'le',
                'la',
                'les',
                'l',
                'un',
                'une',
                'des',
                'd',
                'du',
                'à',
                'au',
                'aux',
                'que',
                'qu',
            );
        }
        if ($this->language == 'nl') {
            return array(
                'de',
                'het',
                't',
                'een',
                'van',
                'dat',
                'te',
            );
        }
        if ($this->language == 'de') {
            return array(
                'der',
                'die',
                'das',
                'den',
                'dem',
                'des',
                'ein',
                'eine',
                'einen',
                'einem',
                'einer',
                'eines',
                'von',
            );
        }
        if ($this->language == 'en') {
            return array(
                'the',
                'a',
                'an',
                's',
                'to',
                'that',
            );
        }
        return array();
    }

}

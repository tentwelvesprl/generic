<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\FullTextIndex;

use Viktor\Application;
use Viktor\Pack\Base\ContentUnit\ContentUnitInterface;

class FullTextIndexationQueue
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function processNextItem()
    {
        $return_array = array(
            'status'  => 'error',
            'message' => '?',
        );
        $query_builder = $this->app['db']->createQueryBuilder();
        $query_builder
            ->select('id', 'content_unit_type', 'content_unit_id', 'action')
            ->from('full_text_indexation_queue', 'full_text_indexation_queue')
            ->orderBy('id', 'ASC')
            ->setFirstResult(0)
            ->setMaxResults(1);
        $result = $query_builder->execute();
        if ($row = $result->fetch()) {
            $indexer = new FullTextIndexer($this->app, $row['content_unit_type'], $row['content_unit_id']);
            switch ($row['action']) {
                case 'update':
                    $content_unit = $this->app['content_unit_factory']->load($row['content_unit_type'], $row['content_unit_id']);
                    if ($content_unit != null && $content_unit instanceof FullTextIndexableInterface) {
                        $content_unit->addToIndex($indexer);
                        $indexer->update();
                        $return_array['status'] = 'ok';
                        $return_array['message'] = ''
                            . $content_unit->getTitle()
                            . ' (' . $content_unit->getSingularHumanType() . ' #' . $content_unit->getId() . ')'
                            . ' indexed';
                    } else {
                        $return_array['message'] = ''
                            . 'Reference'
                            . ' ' . $row['content_unit_type'] . ' #' . $row['content_unit_id']
                            . ' could not be indexed';
                    }
                    break;
                case 'delete':
                    $indexer->delete();
                    $return_array['status'] = 'ok';
                    $return_array['message'] = ''
                        . 'Reference'
                        . ' ' . $row['content_unit_type'] . ' #' . $row['content_unit_id']
                        . ' deleted from full text index';
                    break;
            }
            $deletion_query_builder = $this->app['db']->createQueryBuilder();
            $deletion_query_builder
                ->delete('full_text_indexation_queue')
                ->where('content_unit_type = :content_unit_type AND content_unit_id = :content_unit_id AND action = :action')
                ->setParameter('content_unit_type', $row['content_unit_type'])
                ->setParameter('content_unit_id', $row['content_unit_id'])
                ->setParameter('action', $row['action']);
            $deletion_query_builder->execute();
        } else {
            $return_array['status'] = 'ok';
            $return_array['message'] = 'Nothing left to do: the full text indexation queue is empty.';
        }
        return $return_array;
    }

}

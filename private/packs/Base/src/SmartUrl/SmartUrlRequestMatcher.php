<?php

/*
 * Copyright 2024 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\SmartUrl;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Matcher\RequestMatcherInterface;
use Symfony\Component\Routing\Matcher\UrlMatcherInterface;
use Viktor\Application;

class SmartUrlRequestMatcher implements RequestMatcherInterface
{
    private $app;
    private $requestMatcher;

    public function __construct(Application $app, RequestMatcherInterface $request_matcher)
    {
        $this->app = $app;
        $this->requestMatcher = $request_matcher;
    }

    public function matchRequest(Request $request)
    {
        try {
            return $this->requestMatcher->matchRequest($request);
        } catch (ResourceNotFoundException $e) {
            if (!($this->requestMatcher instanceof UrlMatcherInterface)) {
                throw $e;
            }
            $origin = $request->getPathInfo();
            if (substr($origin, -1) == '/') {
                $origin = substr($origin, 0, strlen($origin) - 1);
            }
            $query_builder = $this->app['db']->createQueryBuilder();
            $query_builder
                ->select("target")
                ->from("smart_url", "su")
                ->where("origin = :origin")
                ->setParameter('origin', $origin);
            $result = $query_builder->execute();
            if ($row = $result->fetch()) {
                $target_without_base_path = str_replace($request->getBasePath(), '', $row['target']);
                return $this->requestMatcher->match($target_without_base_path);
            }
            throw $e;
        }
    }

}

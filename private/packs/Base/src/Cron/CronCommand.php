<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Cron;

use Exception;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Viktor\Pack\Base\Console\ViktorCommand;
use Viktor\Pack\Base\Console\ViktorStyle;
use Viktor\Pack\Base\User\SecretaryUser;

class CronCommand extends ViktorCommand
{
    protected function configure()
    {
        $this
            ->setName('viktor:cron')
            ->setDescription('Executes scheduled task')
            ->setHelp('This command executes scheduled tasks at regular intervals.');
    }

    protected function executeWithStyle(ViktorStyle $io, InputInterface $input, OutputInterface $output)
    {
        $app = $this->getSilexApplication();
        $current_execution_time = time();
        $crontab = $app['cron']->get();
        foreach ($crontab as $task) {
            $last_execution_time = (int) $app['registry']->get('cron_last_execution_uts_' . $task['class_name']);
            if ($last_execution_time + $task['interval'] <= $current_execution_time) {
                try {
                    $task_object = new $task['class_name']($app);
                    $io->title($task_object->getTitle());
                    $task_object->execute($io, $input, $output);
                    $app['registry']->set('cron_last_execution_uts_' . $task['class_name'], $current_execution_time);
                } catch (Exception $e) {
                    $io->error($e->getMessage());
                }
            }
        }
    }

}

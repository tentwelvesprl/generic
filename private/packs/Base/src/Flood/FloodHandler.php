<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Flood;

use Viktor\Application;

class FloodHandler
{
    private $app;
    private $lastAttemptUsername;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->lastAttemptUsername = '';
    }

    public function getApp()
    {
        return $this->app;
    }

    public function getVisitorIdentifier($visitor_identifier = '')
    {
        $visitor_identifier = (string) $visitor_identifier;
        if (trim($visitor_identifier) == '') {
            $visitor_identifier = $this->getApp()['request_stack']->getCurrentRequest()->getClientIp();
            $last_attempt_username = $this->getLastAttemptUsername();
            if ($last_attempt_username != '') {
                $visitor_identifier .= ';' . $last_attempt_username;
            }
        }
        return $visitor_identifier;
    }

    public function getLastAttemptUsername()
    {
        return $this->lastAttemptUsername;
    }

    public function setLastAttemptUsername($username)
    {
        $this->lastAttemptUsername = $username;
    }

    public function register($event_name, $window = 0, $visitor_identifier = '')
    {
        $event_name = (string) $event_name;
        $window = (int) $window;
        if ($window <= 0) {
            $window = 3600;
        }
        $visitor_identifier = $this->getVisitorIdentifier($visitor_identifier);
        $query_builder = $this->getApp()['db']->createQueryBuilder();
        $query_builder
            ->insert('flood')
            ->setValue('event', ':event_name')
            ->setValue('visitor_identifier', ':visitor_identifier')
            ->setValue('ts', ':ts')
            ->setValue('expiration_ts', ':expiration_ts')
            ->setParameter('event_name', $event_name)
            ->setParameter('visitor_identifier', $visitor_identifier)
            ->setParameter('ts', time())
            ->setParameter('expiration_ts', time() + $window);
        if ($query_builder->execute() != 1) {
            throw new Exception('Could not register the event ' . $event_name . ' for flood handling.');
        }
    }

    public function isAllowed($event_name, $threshold, $window = 0, $visitor_identifier = '')
    {
        $event_name = (string) $event_name;
        $threshold = (int) $threshold;
        $window = (int) $window;
        if ($window <= 0) {
            $window = 3600;
        }
        $visitor_identifier = $this->getVisitorIdentifier($visitor_identifier);
        $query_builder = $this->getApp()['db']->createQueryBuilder();
        $query_builder
            ->select('COUNT(id) AS cid')
            ->from('flood')
            ->andWhere('event = :event_name')
            ->andWhere('visitor_identifier = :visitor_identifier')
            ->andWhere('ts > :ts')
            ->setParameter('event_name', $event_name)
            ->setParameter('visitor_identifier', $visitor_identifier)
            ->setParameter('ts', time() - $window);
        $result = $query_builder->execute();
        $nb = 0;
        if ($row = $result->fetch()) {
            $nb = $row['cid'];
        }
        return ($nb < $threshold);
    }

    public function garbageCollection()
    {
        $query_builder = $this->getApp()['db']->createQueryBuilder();
        $query_builder
            ->delete('flood')
            ->where('expiration_ts < :ts')
            ->setParameter('ts', time());
        return $query_builder->execute();
    }

}

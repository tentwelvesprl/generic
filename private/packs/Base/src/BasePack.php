<?php

/*
 * Copyright 2023 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base;

use Doctrine\DBAL\Logging\DebugStack;
use Doctrine\DBAL\Schema\Table;
use Exception;
use Monolog\Logger;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Silex\Provider\MonologServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\SwiftmailerServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\Matcher\RequestMatcherInterface;
use Viktor\Application;
use Viktor\Pack\Base\ContentUnit\ContentUnitCommand;
use Viktor\Pack\Base\ContentUnit\ContentUnitServiceProvider;
use Viktor\Pack\Base\Cron\CronCommand;
use Viktor\Pack\Base\Cron\CronServiceProvider;
use Viktor\Pack\Base\DbStore\DbStoreCommand;
use Viktor\Pack\Base\DebugBar\DebugBar;
use Viktor\Pack\Base\Flood\FloodServiceProvider;
use Viktor\Pack\Base\I18n\I18nServiceProvider;
use Viktor\Pack\Base\Image\ImageGeneratorServiceProvider;
use Viktor\Pack\Base\Registry\RegistryServiceProvider;
use Viktor\Pack\Base\SmartUrl\SmartUrlRequestMatcher;
use Viktor\PackInterface;

class BasePack implements PackInterface
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
        return $this;
    }

    public function getName()
    {
        return 'Base';
    }

    public function getPath()
    {
        return realpath(__DIR__ . '/..');
    }

    public function install() {
        $app = $this->app;
        $schema = $app['db']->getSchemaManager();
        // Create 'user_credentials' table.
        if (!$schema->tablesExist('user_credentials')) {
            $table = new Table('user_credentials');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('id', 'integer', array('unsigned' => true));
            $table->addColumn('type', 'string', array('length' => 64));
            $table->addColumn('username', 'string', array('length' => 128));
            $table->addColumn('password', 'string', array('length' => 255));
            $table->addColumn('roles', 'string', array('length' => 255));
            $table->addColumn('email', 'string', array('length' => 64));
            $table->setPrimaryKey(array('type', 'id'));
            $table->addIndex(array('email'));
            $table->addUniqueIndex(array('username'));
            $schema->createTable($table);
        }
        // Create 'password_forgotten' table.
        if (!$schema->tablesExist('password_forgotten')) {
            $table = new Table('password_forgotten');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('id', 'integer', array('unsigned' => true));
            $table->addColumn('type', 'string', array('length' => 64));
            $table->addColumn('uts', 'integer', array());
            $table->addColumn('token', 'string', array('length' => 32));
            $table->setPrimaryKey(array('token'));
            $schema->createTable($table);
        }
        // Create 'dataset' table.
        if (!$schema->tablesExist('dataset')) {
            $table = new Table('dataset');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('id', 'integer', array('unsigned' => true));
            $table->addColumn('type', 'string', array('length' => 64));
            $table->addColumn('content', 'text');
            $table->setPrimaryKey(array('type', 'id'));
            $schema->createTable($table);
        }
        // Create 'simple_text_field_index' table.
        if (!$schema->tablesExist('simple_text_field_index')) {
            $table = new Table('simple_text_field_index');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('id', 'integer', array('unsigned' => true));
            $table->addColumn('type', 'string', array('length' => 64));
            $table->addColumn('field_name', 'string', array('length' => 64));
            $table->addColumn('value', 'string', array('length' => 256));
            $table->setPrimaryKey(array('type', 'field_name', 'id'));
            $table->addIndex(array('value'));
            $schema->createTable($table);
        }
        // Create 'rich_text_field_index' table.
        if (!$schema->tablesExist('rich_text_field_index')) {
            $table = new Table('rich_text_field_index');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('id', 'integer', array('unsigned' => true));
            $table->addColumn('type', 'string', array('length' => 64));
            $table->addColumn('field_name', 'string', array('length' => 64));
            $table->addColumn('value', 'string', array('length' => 256));
            $table->setPrimaryKey(array('type', 'field_name', 'id'));
            $table->addIndex(array('value'));
            $schema->createTable($table);
        }
        // Create 'text_field_index' table.
        if (!$schema->tablesExist('text_field_index')) {
            $table = new Table('text_field_index');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('id', 'integer', array('unsigned' => true));
            $table->addColumn('type', 'string', array('length' => 64));
            $table->addColumn('field_name', 'string', array('length' => 64));
            $table->addColumn('value', 'string', array('length' => 256));
            $table->setPrimaryKey(array('type', 'field_name', 'id'));
            $table->addIndex(array('value'));
            $schema->createTable($table);
        }
        // Create 'integer_field_index' table.
        if (!$schema->tablesExist('integer_field_index')) {
            $table = new Table('integer_field_index');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('id', 'integer', array('unsigned' => true));
            $table->addColumn('type', 'string', array('length' => 64));
            $table->addColumn('field_name', 'string', array('length' => 64));
            $table->addColumn('value', 'integer', array('unsigned' => false));
            $table->setPrimaryKey(array('type', 'field_name', 'id'));
            $table->addIndex(array('value'));
            $schema->createTable($table);
        }
        // Create 'list_field_index' table.
        if (!$schema->tablesExist('list_field_index')) {
            $table = new Table('list_field_index');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('id', 'integer', array('unsigned' => true));
            $table->addColumn('type', 'string', array('length' => 64));
            $table->addColumn('field_name', 'string', array('length' => 64));
            $table->addColumn('item', 'string', array('length' => 64));
            $table->addColumn('selected', 'boolean', array());
            $table->setPrimaryKey(array('type', 'field_name', 'id', 'item'));
            $table->addIndex(array('item', 'selected'));
            $schema->createTable($table);
        }
        // Create 'content_unit_references_index' table.
        if (!$schema->tablesExist('content_unit_references_index')) {
            $table = new Table('content_unit_references_index');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('id', 'integer', array('unsigned' => true));
            $table->addColumn('type', 'string', array('length' => 64));
            $table->addColumn('field_name', 'string', array('length' => 64));
            $table->addColumn('k', 'integer', array('unsigned' => true));
            $table->addColumn('reference_id', 'integer', array('unsigned' => true));
            $table->addColumn('reference_type', 'string', array('length' => 64));
            $table->setPrimaryKey(array('type', 'field_name', 'id', 'k'));
            $table->addIndex(array('reference_type', 'reference_id'));
            $schema->createTable($table);
        }
        // Create 'address_field_index' table.
        if (!$schema->tablesExist('address_field_index')) {
            $table = new Table('address_field_index');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('id', 'integer', array('unsigned' => true));
            $table->addColumn('type', 'string', array('length' => 64));
            $table->addColumn('field_name', 'string', array('length' => 64));
            $table->addColumn('postal_code', 'string', array('length' => 64));
            $table->addColumn('country', 'string', array('length' => 2));
            $table->addColumn('latitude', 'float');
            $table->addColumn('longitude', 'float');
            $table->setPrimaryKey(array('type', 'field_name', 'id'));
            $table->addIndex(array('postal_code'));
            $table->addIndex(array('country'));
            $table->addIndex(array('latitude'));
            $table->addIndex(array('longitude'));
            $schema->createTable($table);
        }
        // Create 'date_time_field_index' table.
        if (!$schema->tablesExist('date_time_field_index')) {
            $table = new Table('date_time_field_index');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('id', 'integer', array('unsigned' => true));
            $table->addColumn('type', 'string', array('length' => 64));
            $table->addColumn('field_name', 'string', array('length' => 64));
            $table->addColumn('value', 'integer', array('unsigned' => false));
            $table->addColumn('date_only', 'string', array('length' => 10));
            $table->addColumn('time_only', 'string', array('length' => 8));
            $table->setPrimaryKey(array('type', 'field_name', 'id'));
            $table->addIndex(array('value'));
            $table->addIndex(array('date_only'));
            $table->addIndex(array('time_only'));
            $schema->createTable($table);
        }
        // Create 'publication' table.
        if (!$schema->tablesExist('publication')) {
            $table = new Table('publication');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('id', 'integer', array('unsigned' => true));
            $table->addColumn('type', 'string', array('length' => 64));
            $table->addColumn('content', 'text');
            $table->setPrimaryKey(array('type', 'id'));
            $schema->createTable($table);
        }
        // Create 'publication_index' table.
        if (!$schema->tablesExist('publication_index')) {
            $table = new Table('publication_index');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('id', 'integer', array('unsigned' => true));
            $table->addColumn('type', 'string', array('length' => 64));
            $table->addColumn('language', 'string', array('length' => 64));
            $table->addColumn('published', 'boolean');
            $table->addColumn('start_uts', 'integer', array());
            $table->addColumn('end_uts', 'integer', array());
            $table->setPrimaryKey(array('type', 'id', 'language'));
            $table->addIndex(array('published'));
            $table->addIndex(array('start_uts'));
            $table->addIndex(array('end_uts'));
            $schema->createTable($table);
        }
        // Create 'full_text_index' table.
        if (!$schema->tablesExist('full_text_index')) {
            $table = new Table('full_text_index');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('id', 'integer', array('unsigned' => true));
            $table->addColumn('type', 'string', array('length' => 64));
            $table->addColumn('index_id', 'string', array('length' => 64));
            $table->addColumn('token', 'string', array('length' => 128));
            $table->addColumn('score', 'integer', array());
            $table->addIndex(array('type', 'id'));
            $table->addIndex(array('token', 'index_id', 'type'));
            $schema->createTable($table);
        }
        // Create 'full_text_indexation_queue' table.
        if (!$schema->tablesExist('full_text_indexation_queue')) {
            $table = new Table('full_text_indexation_queue');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('id', 'bigint', array('unsigned' => true, 'autoincrement' => true));
            $table->addColumn('content_unit_id', 'integer', array('unsigned' => true));
            $table->addColumn('content_unit_type', 'string', array('length' => 64));
            $table->addColumn('action', 'string', array('length' => 64));
            $table->setPrimaryKey(array('id'));
            $schema->createTable($table);
        }
        // Create 'registry' table.
        if (!$schema->tablesExist('registry')) {
            $table = new Table('registry');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('key', 'string', array('length' => 128));
            $table->addColumn('value', 'text');
            $table->setPrimaryKey(array('key'));
            $schema->createTable($table);
        }
        // Create 'tree_structure' table.
        if (!$schema->tablesExist('tree_structure')) {
            $table = new Table('tree_structure');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('type', 'string', array('length' => 64));
            $table->addColumn('content', 'text');
            $table->setPrimaryKey(array('type'));
            $schema->createTable($table);
        }
        // Create 'uploaded_image' table.
        if (!$schema->tablesExist('uploaded_image')) {
            $table = new Table('uploaded_image');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('id', 'integer', array('unsigned' => true));
            $table->addColumn('type', 'string', array('length' => 64));
            $table->addColumn('file_path', 'text');
            $table->addColumn('pivot_x', 'float');
            $table->addColumn('pivot_y', 'float');
            $table->addColumn('etag', 'string', array('length' => 256));
            $table->setPrimaryKey(array('type', 'id'));
            $schema->createTable($table);
        }
        // Create 'uploaded_image_index' table.
        if (!$schema->tablesExist('uploaded_image_index')) {
            $table = new Table('uploaded_image_index');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('id', 'integer', array('unsigned' => true));
            $table->addColumn('type', 'string', array('length' => 64));
            $table->addColumn('width', 'integer', array('unsigned' => true));
            $table->addColumn('height', 'integer', array('unsigned' => true));
            $table->addColumn('area', 'integer', array('unsigned' => true));
            $table->addColumn('ratio', 'float', array('unsigned' => true));
            $table->setPrimaryKey(array('type', 'id'));
            $table->addIndex(array('area'));
            $table->addIndex(array('ratio'));
            $schema->createTable($table);
        }
        // Create 'attachment' table.
        if (!$schema->tablesExist('attachment')) {
            $table = new Table('attachment');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('id', 'integer', array('unsigned' => true));
            $table->addColumn('type', 'string', array('length' => 64));
            $table->addColumn('file_path', 'text');
            $table->addColumn('etag', 'string', array('length' => 256));
            $table->setPrimaryKey(array('type', 'id'));
            $schema->createTable($table);
        }
        // Create 'embed_info' table.
        if (!$schema->tablesExist('embed_info')) {
            $table = new Table('embed_info');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('id', 'integer', array('unsigned' => true));
            $table->addColumn('type', 'string', array('length' => 64));
            $table->addColumn('value', 'text');
            $table->addColumn('embed_raw_output', 'text');
            $table->setPrimaryKey(array('type', 'id'));
            $schema->createTable($table);
        }
        // Create 'flood' table.
        if (!$schema->tablesExist('flood')) {
            $table = new Table('flood');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('id', 'integer', array('unsigned' => true, 'autoincrement' => true));
            $table->addColumn('event', 'string', array('length' => 64));
            $table->addColumn('visitor_identifier', 'string', array('length' => 128));
            $table->addColumn('ts', 'integer', array('unsigned' => true));
            $table->addColumn('expiration_ts', 'integer', array('unsigned' => true));
            $table->setPrimaryKey(array('id'));
            $table->addIndex(array('expiration_ts'));
            $table->addIndex(array('event', 'visitor_identifier', 'ts'));
            $schema->createTable($table);
        }
        // Create 'smart_url' table.
        if (!$schema->tablesExist('smart_url')) {
            $table = new Table('smart_url');
            $table->addOption('collate', 'utf8mb4_unicode_ci');
            $table->addOption('charset', 'utf8mb4');
            $table->addColumn('k', 'string', array('length' => 128));
            $table->addColumn('origin', 'string', array('length' => 256));
            $table->addColumn('target', 'string', array('length' => 256));
            $table->setPrimaryKey(array('k'));
            $table->addUniqueIndex(array('origin'));
            $schema->createTable($table);
        }
    }

    public function init()
    {
        $app = $this->app;
        // Exceptions handling.
        // In debug mode, use Silex default error pages, which carry extra info.
        // Otherwise, use Viktor fancy pages.
        if (!$app['debug']) {
            $app->error(function (Exception $e, $code) use ($app) {
                // Explicitely log the error, since Silex won't do it.
                if ($code >= 500 and $code < 600) {
                    error_log($e->getTraceAsString());
                }
                $response = new Response();
                // Note: Silex automatically sets the appropriate status code.
                if (isset(Response::$statusTexts[$code])) {
                    $text = Response::$statusTexts[$code];
                } else {
                    $text = 'Error';
                }
                $response->setContent($app['twig']->render(
                    $app['viktor.config']['error_page_view'],
                    array(
                        'code' => $code,
                        'text' => $text,
                    ),
                    $response
                ));
                return $response;
            });
        }
        // Services.
        $this->app->register(new HttpFragmentServiceProvider());
        $dbs_options = array(
            'viktor' => array(
                'driver'   => $this->app['viktor.config']['db']['driver'],
                'host'     => $this->app['viktor.config']['db']['host'],
                'dbname'   => $this->app['viktor.config']['db']['dbname'],
                'user'     => $this->app['viktor.config']['db']['user'],
                'password' => $this->app['viktor.config']['db']['password'],
                'charset'  => $this->app['viktor.config']['db']['charset'],
            ),
        );
        foreach ($this->app['viktor.config']['additional_dbs'] as $k => $additional_db) {
            $dbs_options[$k] = array(
                'driver'   => $additional_db['driver'],
                'host'     => $additional_db['host'],
                'dbname'   => $additional_db['dbname'],
                'user'     => $additional_db['user'],
                'password' => $additional_db['password'],
                'charset'  => $additional_db['charset'],
            );
        }
        $this->app->register(new DoctrineServiceProvider(), array(
            'dbs.options' => $dbs_options,
        ));
        $this->app->register(new FloodServiceProvider());
        if ($this->app['debug']) {
            $logger = new DebugStack();
            $this->app['db.config']->setSQLLogger($logger);
        }
        // Debug bar.
        if ($app['debug'] && $app['viktor.config']['enable_debug_bar']) {
            $app['kernel'] = $app->extend('kernel', function (HttpKernelInterface $kernel, Application $app) {
                $kernel = new DebugBar($app, $kernel);
                return $kernel;
            });
        }
        $this->app->register(new TwigServiceProvider());
        $this->app->register(new SessionServiceProvider(), array(
            'session.storage.options' => array(
                'name' => 'VIKTORSESSID',
            ),
        ));
        $this->app->register(new SwiftmailerServiceProvider(), array(
            'swiftmailer.use_spool' => $this->app['viktor.config']['swiftmailer']['use_spool'],
            'swiftmailer.options' => array(
                'host' => $this->app['viktor.config']['swiftmailer']['options']['host'],
                'port' => $this->app['viktor.config']['swiftmailer']['options']['port'],
                'username' => $this->app['viktor.config']['swiftmailer']['options']['username'],
                'password' => $this->app['viktor.config']['swiftmailer']['options']['password'],
                'encryption' => $this->app['viktor.config']['swiftmailer']['options']['encryption'],
                'auth_mode' => $this->app['viktor.config']['swiftmailer']['options']['auth_mode'],
            ),
        ));
        switch ($this->app['viktor.config']['log_level']) {
            case 'debug': $log_level = Logger::DEBUG; break;
            case 'info': $log_level = Logger::INFO; break;
            case 'warning': $log_level = Logger::WARNING; break;
            case 'error': $log_level = Logger::ERROR; break;
            default: $log_level = Logger::WARNING;
        }
        $this->app->register(new MonologServiceProvider(), array(
            'monolog.logfile' => $this->app['viktor.log_path'] . '/viktor_' . date('Ymd') . '.log',
            'monolog.level' => $log_level,
        ));
        $this->app->register(new I18nServiceProvider());
        $this->app->register(new ContentUnitServiceProvider());
        $this->app->register(new CronServiceProvider());
        $this->app->register(new RegistryServiceProvider());
        $this->app->register(new ImageGeneratorServiceProvider());
        $this->app['request_matcher'] = $app->extend('request_matcher', function (RequestMatcherInterface $request_matcher, Application $app) {
            $request_matcher = new SmartUrlRequestMatcher($app, $request_matcher);
            return $request_matcher;
        });
        // Twig configuration.
        $this->app['twig.loader.filesystem']->addPath(__DIR__ . '/../views', 'Base');
        $this->app['twig.loader.filesystem']->addPath($this->app['viktor.app_path'] . '/views');
        // Register content unit types.
        $this->app['content_unit_factory']->registerType('Viktor\Pack\Base\User\UnknownUser');
        $this->app['content_unit_factory']->registerType('Viktor\Pack\Base\User\SecretaryUser');
        // Cron tasks.
        $this->app['cron']->add('Viktor\Pack\Base\Flood\FloodHandlerCronTask', 300);
        $this->app['cron']->add('Viktor\Pack\Base\FullTextIndex\FullTextIndexCronTask', 60);
        // Default image formats.
        $this->app['image_generator.formats']->registerFormat('og', array(
            'transformations' => array(
                array(
                    'name' => 'resize_around_pivot_point',
                    'options' => array(
                        'width' => 1200,
                        'height' => 630,
                    ),
                ),
            ),
            'output_format' => array(
                'mime_type' => 'image/jpeg',
                'options' => array('jpeg_quality' => 90),
            ),
            'max_age' => 0,
        ));
        return $this;
    }

    public function addConsoleCommands()
    {
        $this->app['console']->add(new ContentUnitCommand());
        $this->app['console']->add(new DbStoreCommand());
        $this->app['console']->add(new CronCommand());
    }

}

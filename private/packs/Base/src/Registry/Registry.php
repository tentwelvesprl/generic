<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Registry;

use Viktor\Application;

class Registry
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function get($key)
    {
        $query_builder = $this->app['db']->createQueryBuilder();
        $query_builder = $query_builder
            ->select('`value`')
            ->from('registry', 'registry')
            ->where('registry.`key` = :k')
            ->setParameter('k', (string) $key)
        ;
        $result = $query_builder->execute();
        if ($row = $result->fetch()) {
            return $row['value'];
        } else {
            return null;
        }
    }

    public function set($key, $value)
    {
        $query_builder = $this->app['db']->createQueryBuilder();
        $query_builder = $query_builder
            ->delete('registry')
            ->where('registry.`key` = :k')
            ->setParameter('k', (string) $key)
        ;
        $query_builder->execute();
        $query_builder = $this->app['db']->createQueryBuilder();
        $query_builder = $query_builder
            ->insert('registry')
            ->setValue('`key`', ':k')
            ->setValue('`value`', ':v')
            ->setParameter('k', (string) $key)
            ->setParameter('v', (string) $value)
        ;
        $query_builder->execute();
    }

}

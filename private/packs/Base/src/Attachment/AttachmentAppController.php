<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Attachment;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Viktor\Application;
use Viktor\Pack\Base\I18n\TranslatableInterface;

class AttachmentAppController
{
    public function attachmentFromContentUnit(Request $request, Application $app, $language, $type, $id, $disposition, $filename)
    {
        $attachment_content_unit = $app['content_unit_factory']->load($type, $id);
        if ($attachment_content_unit == null) {
            $app->abort(404, 'This attachment could not be loaded.');
        }
        if (!$attachment_content_unit instanceof HasAnAttachmentInterface) {
            $app->abort(404, 'This content unit has no attachment.');
        }
        if ($attachment_content_unit instanceof TranslatableInterface) {
            $attachment_content_unit->setLanguage($language);
        }
        if (!$attachment_content_unit->canBeViewed()) {
            $app->abort(404, 'This attachment cannot be displayed.');
        }
        $attachment = $attachment_content_unit->getAttachment();
        $attachment_content_unit_digest = $attachment_content_unit->getDigest();
        if ($attachment == null) {
            $app->abort(404, 'This content unit has no attachment.');
        }
        $canonical_url = $app->path('attachment-from-content-unit', array(
            'language' => $language,
            'type' => $type,
            'id' => $id,
            'disposition' => $disposition,
            'filename' => $attachment_content_unit_digest['_original_filename'],
        ));
        $file_path = $app['viktor.data_path'] . $attachment->getFilePath();
        if (!file_exists($file_path)) {
            $app->abort(404, 'The attached file is missing.');
        }
        $response = new BinaryFileResponse($file_path);
        $fallback_name = basename($file_path);
        $fallback_name = @iconv('UTF-8', 'ASCII//IGNORE', $fallback_name);
        if (trim($fallback_name) == '') {
            $fallback_name = 'file';
        }
        if ($disposition == 'attachment') {
            $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $fallback_name);
        } else {
            $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_INLINE, $fallback_name);
        }
        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Content-Type', mime_content_type($file_path));
        return $response;
    }

}

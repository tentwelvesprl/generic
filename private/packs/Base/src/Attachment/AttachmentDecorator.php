<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Attachment;

use Viktor\Pack\Base\DbStore\AbstractDbStoreDecorator;
use Viktor\Pack\Base\DbStore\DbStoreComponentInterface;
use Viktor\Pack\Base\User\UserInterface;

class AttachmentDecorator extends AbstractDbStoreDecorator
{
    private $attachment;

    public function __construct(DbStoreComponentInterface $component, Attachment $attachment)
    {
        parent::__construct($component);
        $this->attachment = $attachment;
    }

    public function getAttachment()
    {
        return $this->attachment;
    }

    public function getRowQueryBuilder($id)
    {
        $query_builder = $this->component->getRowQueryBuilder($id);
        $query_builder = $this->attachment->getDecoratedRowQueryBuilder($query_builder);
        return $query_builder;
    }

    public function setFromRow(array $row)
    {
        $this->component->setFromRow($row);
        $this->attachment->setFromRow($row);
        return;
    }

    public function save(UserInterface $user)
    {
        if (!$this->component->save($user)) {
            return false;
        } else {
            return $this->attachment->save();
        }
    }

    public function delete()
    {
        if (!$this->attachment->delete()) {
            return false;
        } else {
            return $this->component->delete();
        }
    }

    public function getIdsQueryBuilder(array $filters = array(), array $sort_options = array())
    {
        $query_builder = $this->component->getIdsQueryBuilder($filters, $sort_options);
        return $this->attachment->getDecoratedIdsQueryBuilder($query_builder, $filters, $sort_options);
    }

    public function getDigest()
    {
        $digest = $this->component->getDigest();
        $digest = array_merge($digest, $this->attachment->getFileInfoDigest());
        return $digest;
    }

}

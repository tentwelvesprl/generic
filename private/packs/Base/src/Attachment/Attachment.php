<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Attachment;

use Doctrine\DBAL\Query\QueryBuilder;
use Exception;
use finfo;
use Symfony\Component\Yaml\Yaml;
use Viktor\Application;
use Viktor\Pack\Base\ContentUnit\ContentUnitInterface;
use Viktor\Pack\Base\Image\ImageGenerator;
use Viktor\Pack\Base\Image\SourceImage;

class Attachment
{
    private $app;
    private $contentUnit;
    private $options;
    private $filePath; // File path, relative to Viktor data path.
    private $etag;

    public function __construct(Application $app, ContentUnitInterface $content_unit, array $options)
    {
        $this->app = $app;
        $this->contentUnit = $content_unit;
        $default_options = array(
        );
        $this->options = array_merge($default_options, $options);
        $this->filePath = '';
        $this->etag = '';
        return;
    }

    public function getApp()
    {
        return $this->app;
    }

    public function getContentUnit()
    {
        return $this->contentUnit;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function getFilePath()
    {
        return $this->filePath;
    }

    public function setFilePath($file_path)
    {
        $this->filePath = (string) $file_path;
        return $this;
    }

    public function getEtag()
    {
        return $this->etag;
    }

    public function setEtag($etag)
    {
        $this->etag = (string) $etag;
        return $this;
    }

    public function getDecoratedRowQueryBuilder(QueryBuilder $query_builder)
    {
        $query_builder
            ->addSelect('attachment.file_path AS attachment$file_path')
            ->addSelect('attachment.etag AS attachment$etag')
            ->innerJoin(
                'component',
                'attachment', 'attachment',
                'component.type = attachment.type AND component.id = attachment.id'
            )
        ;
        return $query_builder;
    }

    public function setFromRow(array $row)
    {
        $this->setFilePath($row['attachment$file_path']);
        $this->setEtag($row['attachment$etag']);
    }

    public function save()
    {
        if ($this->contentUnit->getId() <= 0) {
            return false;
        }
        $this->setEtag(md5(time() . $this->getFilePath()));
        $result = $this->app['db']->executeQuery(
            '
                SELECT id
                FROM attachment
                WHERE
                    type = ?
                    AND id = ?
            ',
            array(
                $this->contentUnit->getType(),
                $this->contentUnit->getId()
            )
        );
        $query_successful = false;
        if ($row = $result->fetch()) {
            // Update.
            $query_builder = $this->app['db']->createQueryBuilder();
            $query_builder
                ->update('attachment')
                ->set('file_path', ':file_path')
                ->set('etag', ':etag')
                ->where('type = :type AND id = :id')
                ->setParameter('file_path', $this->getFilePath())
                ->setParameter('etag', $this->getEtag())
                ->setParameter('type', $this->contentUnit->getType())
                ->setParameter('id', $this->contentUnit->getId());
            $query_builder->execute();
            $query_successful = true;
        } else {
            // Insert.
            $query_builder = $this->app['db']->createQueryBuilder();
            $query_builder
                ->insert('attachment')
                ->setValue('type', ':type')
                ->setValue('id', ':id')
                ->setValue('file_path', ':file_path')
                ->setValue('etag', ':etag')
                ->setParameter('type', $this->contentUnit->getType())
                ->setParameter('id', $this->contentUnit->getId())
                ->setParameter('file_path', $this->getFilePath())
                ->setParameter('etag', $this->getEtag());
            if ($query_builder->execute() == 1) {
                $query_successful = true;
            } else {
                $query_successful = false;
            }
        }
        if (!$query_successful) {
            return false;
        } else {
            $this->updateIndex();
            return true;
        }
    }

    public function delete()
    {
        $query_builder = $this->app['db']->createQueryBuilder();
        $query_builder
            ->delete('attachment')
            ->where('type = :type AND id = :id')
            ->setParameter('type', $this->contentUnit->getType())
            ->setParameter('id', $this->contentUnit->getId());
        if ($query_builder->execute() == 1) {
            $this->deleteFromIndex();
            return true;
        } else {
            return false;
        }
    }

    public function updateIndex()
    {
        $this->deleteFromIndex();
        return true;
    }

    public function deleteFromIndex()
    {
        return true;
    }

    public function getDecoratedIdsQueryBuilder(QueryBuilder $query_builder, array $filters, array $sort_options)
    {
        return $query_builder;
    }

    public function copy(Attachment $source)
    {
        $this->filePath = $source->getFilePath();
    }

    public function fileCanBeRead()
    {
        $app = $this->getApp();
        $complete_file_path = $app['viktor.data_path'] . $this->getFilePath();
        if (@is_readable($complete_file_path)) {
            return true;
        } else {
            return false;
        }
    }

    public function getFileInfoDigest()
    {
        $a = array();
        $a['_file_can_be_read'] = false;
        $a['_file_size'] = 0;
        $a['_human_file_size'] = '0 B';
        $a['_file_description'] = '';
        $a['_mime_type'] = '';
        $a['_original_filename'] = '';
        $a['_original_extension'] = '';
        $app = $this->getApp();
        if ($this->getFilePath() != '') {
            $complete_file_path = $app['viktor.data_path'] . $this->getFilePath();
            if ($this->fileCanBeRead()) {
                $a['_file_can_be_read'] = true;
                $bytes = filesize($complete_file_path);
                $a['_file_size'] = $bytes;
                $size = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
                $factor = floor((strlen($bytes) - 1) / 3);
                $a['_human_file_size'] = sprintf("%.1f", $bytes / pow(1000, $factor)) . ' ' . $size[$factor];
                $finfo = new finfo(FILEINFO_NONE);
                $a['_file_description'] = (string) $finfo->file($complete_file_path);
                $finfo = new finfo(FILEINFO_MIME_TYPE);
                $a['_mime_type'] = (string) $finfo->file($complete_file_path);
                $path_parts = pathinfo($complete_file_path);
                if (isset($path_parts['extension'])) {
                    $a['_original_filename'] = $path_parts['filename'] . '.' . $path_parts['extension'];
                    $a['_original_extension'] = $path_parts['extension'];
                } else {
                    $a['_original_filename'] = $path_parts['filename'];
                    $a['_original_extension'] = '';
                }
            }
        }
        return $a;
    }

    public function getSourceImage()
    {
        $source_image = null;
        if ($this->filePath != '') {
            $source_image = new SourceImage($this->app['viktor.data_path'] . $this->filePath);
            $source_image->setEtag($this->getEtag());
            $image_generator = new ImageGenerator($this->getApp(), $source_image);
            $file_info_digest = $this->getFileInfoDigest();
            $mime_type = $file_info_digest['_mime_type'];
            if (!in_array($mime_type, $image_generator->getImageProcessor()->getAvailableInputFormats())) {
                $source_image = null;
            }
        }
        return $source_image;
    }

}

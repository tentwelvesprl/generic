<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\I18n;

use Viktor\Application;

class BabelFish
{
    private $app;
    private $dictionary;
    private $languageSelector;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->dictionary = array();
        $this->languageSelector = new LanguageSelector($app);
        return;
    }

    public function loadDictionary($path)
    {
        if (($f = fopen($path, 'r')) !== false) {
            // Header line.
            $languages_order = array();
            if (!feof($f)) {
                $line = fgets($f);
                $languages_list = explode("\t", trim($line));
                foreach ($languages_list as $language) {
                    $languages_order[] = $language;
                }
            }
            // Content lines.
            while (!feof($f)) {
                $line = fgets($f);
                if (trim($line) != '' && substr(trim($line), 0, 1) != '#') {
                    $line = str_replace("\n", '', $line);
                    $terms = explode("\t", $line);
                    $reference = '';
                    foreach ($terms as $k => $term) {
                        if (isset($languages_order[$k]) && $languages_order[$k] == 'reference') {
                            $reference = $term;
                            $this->dictionary[$reference] = array();
                        }
                    }
                    foreach ($terms as $k => $term) {
                        $this->dictionary[$reference][$languages_order[$k]] = $term;
                    }
                }
            }
            fclose($f);
        }
        return;
    }

    public function getLanguage($language = '')
    {
        return $this->languageSelector->getLanguage();
    }

    public function setLanguage($language = '')
    {
        $this->languageSelector->setLanguage($language);
        return;
    }

    public function getTranslation($s, $language = '')
    {
        if ($language == '') {
            $language = $this->languageSelector->getLanguage();
        }
        if (isset($this->dictionary[$s]) && isset($this->dictionary[$s][$language])) {
            return $this->dictionary[$s][$language];
        } else {
            return $s;
        }
    }

}

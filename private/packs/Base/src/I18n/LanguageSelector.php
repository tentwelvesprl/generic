<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\I18n;

use Viktor\Application;

class LanguageSelector
{
    private $app;
    private $currentLanguage;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->currentLanguage = '';
        $this->setLanguage(); // Use the default language.
        return;
    }

    public function getAvailableLanguages()
    {
        $available_languages = $this->app['viktor.config']['available_languages'];
        if (!is_array($available_languages)) {
            return array('en' => 'English');
        }
        return $available_languages;
    }

    public function getLanguage()
    {
        return $this->currentLanguage;
    }

    public function setLanguage($language = '')
    {
        $available_languages = $this->getAvailableLanguages();
        $available_languages_codes = array_keys($available_languages);
        if (!isset($available_languages[$language])) {
            $this->currentLanguage = $available_languages_codes[0];
        } else {
            $this->currentLanguage = $language;
        }
        return;
    }

}

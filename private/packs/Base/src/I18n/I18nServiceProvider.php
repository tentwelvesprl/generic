<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\I18n;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Twig_SimpleFunction;

class I18nServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['i18n'] = function ($app) {
            $babel_fish = new BabelFish($app);
            return $babel_fish;
        };
        $get_translation_function = new Twig_SimpleFunction('t', function ($s) use ($app) {
            return $app['i18n']->getTranslation($s);
        });
        $app['twig']->addFunction($get_translation_function);
        return;
    }

}

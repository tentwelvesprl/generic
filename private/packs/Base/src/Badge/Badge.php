<?php

/*
 * Copyright 2020 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Badge;

class Badge
{
    private $red;
    private $green;
    private $blue;
    private $text;

    public function __construct(int $red, int $green, int $blue, string $text)
    {
        $this->setRed($red);
        $this->setGreen($green);
        $this->setBlue($blue);
        $this->setText($text);
    }

    public function getRed()
    {
        return $this->red;
    }

    public function getGreen()
    {
        return $this->green;
    }

    public function getBlue()
    {
        return $this->blue;
    }

    public function setRed(int $red)
    {
        $this->red = max(0, min(255, $red));
    }

    public function setGreen(int $green)
    {
        $this->green = max(0, min(255, $green));
    }

    public function setBlue(int $blue)
    {
        $this->blue = max(0, min(255, $blue));
    }

    public function setText(string $text)
    {
        $this->text = $text;
    }

    public function getDigest()
    {
        return array(
            'red' => $this->red,
            'green' => $this->green,
            'blue' => $this->blue,
            'text' => $this->text,
        );
    }

}

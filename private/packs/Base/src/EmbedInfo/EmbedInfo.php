<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\EmbedInfo;

use Doctrine\DBAL\Query\QueryBuilder;
use Embed\Embed;
use Embed\Http\Crawler;
use Embed\Http\CurlClient;
use Exception;
use finfo;
use Symfony\Component\Yaml\Yaml;
use Viktor\Application;
use Viktor\Pack\Base\ContentUnit\ContentUnitInterface;

class EmbedInfo
{
    private $app;
    private $contentUnit;
    private $options;
    private $value;
    private $embedRawOutput;

    public function __construct(Application $app, ContentUnitInterface $content_unit, array $options)
    {
        $this->app = $app;
        $this->contentUnit = $content_unit;
        $default_options = array(
        );
        $this->options = array_merge($default_options, $options);
        $this->value = array(
            'source_url' => '',
            'title' => '',
            'description' => '',
            'image_url' => '',
            'embed_code' => '',
            'width' => 0,
            'height' => 0,
            'aspect_ratio' => 0.0,
            'type' => '',
            'modification_ts' => 0,
        );
        $this->embedRawOutput = array();
        return;
    }

    public function getApp()
    {
        return $this->app;
    }

    public function getContentUnit()
    {
        return $this->contentUnit;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValue(array $value)
    {
        $this->value = array(
            'source_url' => (isset($value['source_url']) ? (string) $value['source_url'] : ''),
            'title' => (isset($value['title']) ? (string) $value['title'] : ''),
            'description' => (isset($value['description']) ? (string) $value['description'] : ''),
            'image_url' => (isset($value['image_url']) ? (string) $value['image_url'] : ''),
            'embed_code' => (isset($value['embed_code']) ? (string) $value['embed_code'] : ''),
            'width' => (isset($value['width']) ? (int) $value['width'] : 0),
            'height' => (isset($value['height']) ? (int) $value['height'] : 0),
            'aspect_ratio' => (isset($value['aspect_ratio']) ? (float) $value['aspect_ratio'] : 0.0),
            'type' => (isset($value['type']) ? (string) $value['type'] : ''),
            'modification_ts' => (isset($value['modification_ts']) ? (int) $value['modification_ts'] : 0),
        );
    }

    public function getEmbedRawOutput()
    {
        return $this->embedRawOutput;
    }

    public function setEmbedRawOutput(array $a)
    {
        $this->embedRawOutput = $a;
    }

    public function getDecoratedRowQueryBuilder(QueryBuilder $query_builder)
    {
        $query_builder
            ->addSelect('embed_info.value AS embed_info$value')
            ->addSelect('embed_info.embed_raw_output AS embed_info$embed_raw_output')
            ->innerJoin(
                'component',
                'embed_info', 'embed_info',
                'component.type = embed_info.type AND component.id = embed_info.id'
            )
        ;
        return $query_builder;
    }

    public function setFromRow(array $row)
    {
        try {
            $value = Yaml::parse($row['embed_info$value']);
            if (is_array($value)) {
                $this->value = array_merge($this->value, $value);
            }
            $embed_raw_output = Yaml::parse($row['embed_info$embed_raw_output']);
            if (is_array($embed_raw_output)) {
                $this->embedRawOutput = array_merge($this->embedRawOutput, $embed_raw_output);
            }
        } catch (Exception $e) {
            // Silently fail...
        }
    }

    public function save()
    {
        if ($this->contentUnit->getId() <= 0) {
            return false;
        }
        $result = $this->app['db']->executeQuery(
            '
                SELECT id
                FROM embed_info
                WHERE
                    type = ?
                    AND id = ?
            ',
            array(
                $this->contentUnit->getType(),
                $this->contentUnit->getId()
            )
        );
        $query_successful = false;
        if ($row = $result->fetch()) {
            // Update.
            $query_builder = $this->app['db']->createQueryBuilder();
            $query_builder
                ->update('embed_info')
                ->set('value', ':value')
                ->set('embed_raw_output', ':embed_raw_output')
                ->where('type = :type AND id = :id')
                ->setParameter('type', $this->contentUnit->getType())
                ->setParameter('id', $this->contentUnit->getId())
                ->setParameter('value', Yaml::dump($this->getValue()))
                ->setParameter('embed_raw_output', Yaml::dump($this->getEmbedRawOutput()));
            $query_builder->execute();
            $query_successful = true;
        } else {
            // Insert.
            $query_builder = $this->app['db']->createQueryBuilder();
            $query_builder
                ->insert('embed_info')
                ->setValue('type', ':type')
                ->setValue('id', ':id')
                ->setValue('value', ':value')
                ->setValue('embed_raw_output', ':embed_raw_output')
                ->setParameter('type', $this->contentUnit->getType())
                ->setParameter('id', $this->contentUnit->getId())
                ->setParameter('value', Yaml::dump($this->getValue()))
                ->setParameter('embed_raw_output', Yaml::dump($this->getEmbedRawOutput()));
            if ($query_builder->execute() == 1) {
                $query_successful = true;
            } else {
                $query_successful = false;
            }
        }
        if (!$query_successful) {
            return false;
        } else {
            $this->updateIndex();
            return true;
        }
    }

    public function delete()
    {
        $query_builder = $this->app['db']->createQueryBuilder();
        $query_builder
            ->delete('embed_info')
            ->where('type = :type AND id = :id')
            ->setParameter('type', $this->contentUnit->getType())
            ->setParameter('id', $this->contentUnit->getId());
        if ($query_builder->execute() == 1) {
            $this->deleteFromIndex();
            return true;
        } else {
            return false;
        }
    }

    public function updateIndex()
    {
        $this->deleteFromIndex();
        return true;
    }

    public function deleteFromIndex()
    {
        return true;
    }

    public function getDecoratedIdsQueryBuilder(QueryBuilder $query_builder, array $filters, array $sort_options)
    {
        return $query_builder;
    }

    public function copy(EmbedInfo $source)
    {
        $this->value = $source->getValue();
        $this->embedRawOutput = $source->getEmbedRawOutput();
    }

    public function scrapeSourceUrl()
    {
        $value = $this->getValue();
        $source_url = trim($value['source_url']);
        try {

            $client = new CurlClient();
            $client->setSettings(array(
                'cookies_path' => $this->getApp()['viktor.tmp_path'] . '/' . 'embed-cookies.' . uniqid(),
            ));
            $embed = new Embed(new Crawler($client));
            $info = $embed->get($source_url);
            $value['source_url'] = (string) $info->url;
            $value['title'] = (string) $info->title;
            $value['description'] = (string) $info->description;
            $value['image_url'] = (string) $info->image;
            $value['embed_code'] = trim((string) $info->code->html);
            $value['width'] = (string) $info->code->width;
            $value['height'] = (string) $info->code->height;
            $value['aspect_ratio'] = (string) $info->code->ratio;
            $value['type'] = '';
            $embed_raw_output = $value;
            $value['modification_ts'] = time();
            $this->setValue($value);
            $this->setEmbedRawOutput($embed_raw_output);
        } catch (Exception $e) {
            // Silently fail...
        }
    }

}

<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Publication;

use Viktor\Pack\Base\DbStore\AbstractDbStoreDecorator;
use Viktor\Pack\Base\DbStore\DbStoreComponentInterface;
use Viktor\Pack\Base\User\UserInterface;

class PublicationDecorator extends AbstractDbStoreDecorator
{
    private $publication;

    public function __construct(DbStoreComponentInterface $component, Publication $publication)
    {
        parent::__construct($component);
        $this->publication = $publication;
    }

    public function getPublication()
    {
        return $this->publication;
    }

    public function getRowQueryBuilder($id)
    {
        $query_builder = $this->component->getRowQueryBuilder($id);
        $query_builder = $this->publication->getDecoratedRowQueryBuilder($query_builder);
        return $query_builder;
    }

    public function setFromRow(array $row)
    {
        $this->component->setFromRow($row);
        $this->publication->setFromRow($row);
        return;
    }

    public function save(UserInterface $user)
    {
        if (!$this->component->save($user)) {
            return false;
        } else {
            return $this->publication->save();
        }
    }

    public function delete()
    {
        if (!$this->publication->delete()) {
            return false;
        } else {
            return $this->component->delete();
        }
    }

    public function getIdsQueryBuilder(array $filters = array(), array $sort_options = array())
    {
        $query_builder = $this->component->getIdsQueryBuilder($filters, $sort_options);
        return $this->publication->getDecoratedIdsQueryBuilder($query_builder, $filters, $sort_options);
    }

    public function getDigest()
    {
        $digest = $this->component->getDigest();
        $publication = $this->getPublication()->getContent();
        $publication_options = $this->getPublication()->getOptions();
        $digest['published'] = $publication[$this->publication->getLanguage()]['published'];
        if ($publication_options['use_start_datetime']) {
            $digest['start_datetime'] = $publication[$this->publication->getLanguage()]['start_datetime'];
        }
        if ($publication_options['use_end_datetime']) {
            $digest['end_datetime'] = $publication[$this->publication->getLanguage()]['end_datetime'];
        }
        $digest['_is_published_now'] = $this->getPublication()->isPublishedNow();
        return $digest;
    }

}

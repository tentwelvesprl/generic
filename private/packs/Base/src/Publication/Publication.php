<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Publication;

use DateTime;
use Exception;
use Doctrine\DBAL\Query\QueryBuilder;
use Symfony\Component\Yaml\Yaml;
use Viktor\Application;
use Viktor\Pack\Base\ContentUnit\ContentUnitInterface;
use Viktor\Pack\Base\I18n\LanguageSelector;
use Viktor\Pack\Base\I18n\TranslatableInterface;

class Publication implements TranslatableInterface
{
    private $app;
    private $contentUnit;
    private $options;
    private $content;
    private $languageSelector;

    public function __construct(Application $app, ContentUnitInterface $content_unit, array $options)
    {
        $this->app = $app;
        $this->contentUnit = $content_unit;
        $default_options = array(
            'use_published' => true,
            'use_start_datetime' => false,
            'use_end_datetime' => false,
        );
        $this->options = array_merge($default_options, $options);
        $this->content = array();
        foreach ($this->app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $this->content[$language_code] = array(
                'published' => ($this->options['use_published'] ? false : true),
                'start_datetime' => new DateTime(),
                'end_datetime' => new DateTime(),
            );
            $this->content[$language_code]['end_datetime']->modify('+1 year');
        }
        $this->languageSelector = new LanguageSelector($this->app);
        return;
    }

    public function getApp()
    {
        return $this->app;
    }

    public function getContentUnit()
    {
        return $this->contentUnit;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function getPublished($language)
    {
        if ($this->options['use_published'] && isset($this->content[$language])) {
            return $this->content[$language]['published'];
        } else {
            return true;
        }
    }

    public function setPublished($language, $value)
    {
        if ($this->options['use_published'] && isset($this->content[$language])) {
            $this->content[$language]['published'] = (bool) $value;
        }
    }

    public function setStartDateTime($language, $uts)
    {
        if ($this->options['use_start_datetime'] && isset($this->content[$language])) {
            $this->content[$language]['start_datetime']->setTimestamp($uts);
        }
    }

    public function setEndDateTime($language, $uts)
    {
        if ($this->options['use_end_datetime'] && isset($this->content[$language])) {
            $this->content[$language]['end_datetime']->setTimestamp($uts);
        }
    }

    public function isPublishedNow($language = '')
    {
        if (!isset($this->content[$language])) {
            $language = $this->getLanguage();
        }
        $published = true;
        if ($this->options['use_published'] && !$this->content[$language]['published']) {
            $published = false;
        }
        if ($this->options['use_start_datetime'] && $this->content[$language]['start_datetime']->format('U') > time()) {
            $published = false;
        }
        if ($this->options['use_end_datetime'] && $this->content[$language]['end_datetime']->format('U') < time()) {
            $published = false;
        }
        return $published;
    }

    public function getDecoratedRowQueryBuilder(QueryBuilder $query_builder)
    {
        $query_builder
            ->addSelect('publication.content AS publication$content')
            ->innerJoin(
                'component',
                'publication', 'publication',
                'component.type = publication.type AND component.id = publication.id'
            )
        ;
        return $query_builder;
    }

    public function setFromRow(array $row)
    {
        try {
            $content = Yaml::parse($row['publication$content']);
        } catch (Exception $e) {
            $content = array();
        }
        if (!is_array($content)) {
            $content = array();
        }
        foreach ($this->content as $language => $v) {
            if (isset($content[$language])) {
                if ($this->options['use_published'] && isset($content[$language]['published'])) {
                    $this->content[$language]['published'] = (bool) $content[$language]['published'];
                }
                if ($this->options['use_start_datetime'] && isset($content[$language]['start_uts'])) {
                    $this->content[$language]['start_datetime']->setTimestamp($content[$language]['start_uts']);
                }
                if ($this->options['use_end_datetime'] && isset($content[$language]['end_uts'])) {
                    $this->content[$language]['end_datetime']->setTimestamp($content[$language]['end_uts']);
                }
            }
        }
    }

    public function save()
    {
        if ($this->contentUnit->getId() <= 0) {
            return false;
        }
        $reformatted_content = array();
        foreach ($this->content as $language => $publication) {
            $reformatted_content[$language] = array(
                'published' => $publication['published'],
                'start_uts' => (int) $publication['start_datetime']->format('U'),
                'end_uts' => (int) $publication['end_datetime']->format('U'),
            );
        }
        $result = $this->app['db']->executeQuery(
            '
                SELECT id
                FROM publication
                WHERE
                    type = ?
                    AND id = ?
            ',
            array(
                $this->contentUnit->getType(),
                $this->contentUnit->getId()
            )
        );
        $query_successful = false;
        if ($row = $result->fetch()) {
            // Update.
            $query_builder = $this->app['db']->createQueryBuilder();
            $query_builder
                ->update('publication')
                ->set('content', ':content')
                ->where('type = :type AND id = :id')
                ->setParameter('content', Yaml::dump($reformatted_content))
                ->setParameter('type', $this->contentUnit->getType())
                ->setParameter('id', $this->contentUnit->getId());
            $query_builder->execute();
            $query_successful = true;
        } else {
            // Insert.
            $query_builder = $this->app['db']->createQueryBuilder();
            $query_builder
                ->insert('publication')
                ->setValue('type', ':type')
                ->setValue('id', ':id')
                ->setValue('content', ':content')
                ->setParameter('type', $this->contentUnit->getType())
                ->setParameter('id', $this->contentUnit->getId())
                ->setParameter('content', Yaml::dump($reformatted_content));
            if ($query_builder->execute() == 1) {
                $query_successful = true;
            } else {
                $query_successful = false;
            }
        }
        if (!$query_successful) {
            return false;
        } else {
            $this->updateIndex();
            return true;
        }
    }

    public function delete()
    {
        $query_builder = $this->app['db']->createQueryBuilder();
        $query_builder
            ->delete('publication')
            ->where('type = :type AND id = :id')
            ->setParameter('type', $this->contentUnit->getType())
            ->setParameter('id', $this->contentUnit->getId());
        if ($query_builder->execute() == 1) {
            $this->deleteFromIndex();
            return true;
        } else {
            return false;
        }
    }

    public function updateIndex()
    {
        $this->deleteFromIndex();
        foreach ($this->app['viktor.config']['available_languages'] as $language_code => $language_name) {
            $query_builder = $this->app['db']->createQueryBuilder();
            $query_builder
                ->insert('publication_index')
                ->setValue('type', ':type')
                ->setValue('id', ':id')
                ->setValue('language', ':language')
                ->setValue('published', ':published')
                ->setValue('start_uts', ':start_uts')
                ->setValue('end_uts', ':end_uts')
                ->setParameter('type', $this->getContentUnit()->getType())
                ->setParameter('id', $this->getContentUnit()->getId())
                ->setParameter('language', $language_code)
                ->setParameter('published', (int) $this->content[$language_code]['published'])
                ->setParameter('start_uts', (int) $this->content[$language_code]['start_datetime']->format('U'))
                ->setParameter('end_uts', (int) $this->content[$language_code]['end_datetime']->format('U'));
            $query_builder->execute();
        }
        return true;
    }

    public function deleteFromIndex()
    {
        $query_builder = $this->getApp()['db']->createQueryBuilder();
        $query_builder
            ->delete('publication_index')
            ->where('type = :type AND id = :id')
            ->setParameter('type', $this->getContentUnit()->getType())
            ->setParameter('id', $this->getContentUnit()->getId());
        $query_builder->execute();
        return true;
    }

    public function getDecoratedIdsQueryBuilder(QueryBuilder $query_builder, array $filters, array $sort_options)
    {
        static $counter = 0;
        foreach ($filters as $filter) {
            $counter++;
            if ($this->options['use_published'] && $filter['type'] == 'publication_status' && $filter['value'] != '') {
                $field_alias = 'f' . md5('publication_index_publication_status');
                $search_value = ($filter['value'] == 'published_only' ? 1 : 0);
                $query_builder
                    ->innerJoin(
                        'component',
                        'publication_index', $field_alias,
                        'component.type = ' . $field_alias . '.type AND component.id = ' . $field_alias . '.id'
                    )
                    ->andWhere($field_alias . '.language = \'' . $this->getLanguage() . '\'')
                    ->andWhere($field_alias . '.published = ' . $search_value)
                ;
            }
            if ($this->options['use_published'] && $filter['type'] == 'published_now') {
                $field_alias = 'f' . md5('publication_index_published_now');
                $query_builder
                    ->innerJoin(
                        'component',
                        'publication_index', $field_alias,
                        'component.type = ' . $field_alias . '.type AND component.id = ' . $field_alias . '.id'
                    )
                    ->andWhere($field_alias . '.language = \'' . $this->getLanguage() . '\'')
                    ->andWhere($field_alias . '.published = 1');
                ;
                if ($this->options['use_start_datetime']) {
                    $query_builder->andWhere($field_alias . '.start_uts <= UNIX_TIMESTAMP()');
                }
                if ($this->options['use_end_datetime']) {
                    $query_builder->andWhere($field_alias . '.end_uts > UNIX_TIMESTAMP()');
                }
            }
            if ($this->options['use_start_datetime'] && $filter['type'] == 'publication_start_datetime') {
                $value = (int) $filter['value'];
                $operator = trim((string) $filter['operator']);
                switch ($operator) {
                    case 'less_than':
                        $sql_operator = '<';
                        break;
                    case 'less_than_or_equal_to':
                        $sql_operator = '<=';
                        break;
                    case 'equal_to':
                        $sql_operator = '=';
                        break;
                    case 'greater_than_or_equal_to':
                        $sql_operator = '>=';
                        break;
                    case 'greater_than':
                        $sql_operator = '>';
                        break;
                    default:
                        $sql_operator = '=';
                }
                $field_alias = 'f' . md5('publication_index_start_datetime_' . $counter);
                $value_alias = 'v' . md5('publication_index_start_datetime_value_' . $counter);
                // Note that the aliases are prefixed with a letter, so that
                // Doctrine consider them as named parameters.
                $query_builder
                    ->innerJoin(
                        'component',
                        'publication_index', $field_alias,
                        'component.type = ' . $field_alias . '.type AND component.id = ' . $field_alias . '.id'
                    )
                    ->andWhere($field_alias . '.language = \'' . $this->getLanguage() . '\'')
                    ->andWhere($field_alias . '.start_uts ' . $sql_operator . ' :' . $value_alias)
                    ->setParameter($value_alias, $value)
                    ->setParameter($field_name_alias, $field_name)
                ;
            }
        }
        foreach ($sort_options as $sort_option) {
            if (!is_array($sort_option)) {
                continue;
            }
            $option = (string) $sort_option['option'];
            $direction = (string) $sort_option['direction'];
            switch ($option) {
                case 'publication_time':
                    if ($this->options['use_start_datetime']) {
                        $field_alias = 'f' . md5($option);
                        $query_builder
                            ->innerJoin(
                                'component',
                                'publication_index', $field_alias,
                                'component.type = ' . $field_alias . '.type AND component.id = ' . $field_alias . '.id'
                            )
                            ->andWhere($field_alias . '.language = \'' . $this->getLanguage() . '\'')
                            ->addOrderBy($field_alias . '.start_uts', $direction);
                    }
                    break;
            }
        }
        return $query_builder;
    }

    public function getLanguage()
    {
        return $this->languageSelector->getLanguage();
    }

    public function setLanguage($language = '')
    {
        $this->languageSelector->setLanguage($language);
        return $this;
    }

}

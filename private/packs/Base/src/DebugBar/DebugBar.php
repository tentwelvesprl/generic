<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\DebugBar;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\TerminableInterface;
use Viktor\Application;
use Viktor\Pack\Base\I18n\TranslatableInterface;

// Silex expects the application kernel to implement TerminableInterface.
class DebugBar implements HttpKernelInterface, TerminableInterface
{
    private $app;
    private $kernel;

    public function __construct(Application $app, HttpKernelInterface $kernel)
    {
        $this->app = $app;
        $this->kernel = $kernel;
        return $this;
    }

    public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = true)
    {
        $time_start = microtime(true);
        $response = $this->kernel->handle($request, $type, $catch);
        if ($type != self::MASTER_REQUEST) {
            return $response;
        }
        if (!preg_match('@^text/html@', $response->headers->get('Content-Type'))) {
            return $response;
        }
        $content = $response->getContent();
        $number_of_queries = 0;
        if ($sql_logger = $this->app['db.config']->getSQLLogger()) {
            $number_of_queries = count($sql_logger->queries);
        }
        $memory_peak_usage = memory_get_peak_usage();
        $size = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        $factor = floor((strlen($memory_peak_usage) - 1) / 3);
        $formatted_memory_peak_usage = sprintf("%.1f", $memory_peak_usage / pow(1024, $factor)) . ' ' . $size[$factor];
        $time_end = microtime(true);
        $execution_time = $time_end - $time_start;
        $user = $this->app['viktor.user'];
        if ($user instanceof TranslatableInterface) {
            $user->setLanguage($this->app['i18n']->getLanguage());
        }
        $vdb = $this->app['twig']->render(
            '@Base/DebugBar/debug-bar.twig',
            array(
                'status_code'       => $response->getStatusCode(),
                'user'              => $user->getDigest(),
                'number_of_queries' => $number_of_queries,
                'memory_peak_usage' => $formatted_memory_peak_usage,
                'execution_time'    => $execution_time,
            )
        );
        $content = str_replace(
            '</body>',
            '<iframe src="data:text/html;base64,' . base64_encode($vdb) . '" style="display: block; position: fixed; left: 0; bottom: 0; width: 100%; height: 32px; margin: 0; padding: 0; border: none; z-index: 1000;"></iframe></body>',
            $content
        );
        $response->setContent($content);
        return $response;
    }

    public function terminate(Request $request, Response $response)
    {
        if ($this->kernel instanceof TerminableInterface) {
            $this->kernel->terminate($request, $response);
        }
    }

}

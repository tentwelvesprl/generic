<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Dataset;

use Doctrine\DBAL\Query\QueryBuilder;
use Exception;
use Symfony\Component\Yaml\Yaml;
use Viktor\Application;
use Viktor\Pack\Base\ContentUnit\ContentUnitInterface;
use Viktor\Pack\Base\FullTextIndex\FullTextIndexer;
use Viktor\Pack\Base\FullTextIndex\Tokenizer;

class Dataset
{
    private $app;
    private $contentUnit;
    private $description;
    private $fields;

    public function __construct(Application $app, ContentUnitInterface $content_unit, array $description)
    {
        $this->app = $app;
        $this->contentUnit = $content_unit;
        $this->description = $description;
        $this->fields = $this->initFields();
        return;
    }

    private function initFields()
    {
        $this->fields = array();
        foreach ($this->description as $field_name => $field_description) {
            if (!isset($field_description['class_name'])) {
                throw new Exception('Class name missing for ' . $field_name . '.');
            }
            if (!isset($field_description['options'])) {
                throw new Exception('Options missing for ' . $field_name . '.');
            }
            $metadata = array();
            if (isset($field_description['metadata'])) {
                $metadata = $field_description['metadata'];
            }
            $full_text_index_weights = array();
            if (isset($field_description['full_text_index_weights'])) {
                $full_text_index_weights = $field_description['full_text_index_weights'];
            }
            $class_name = $field_description['class_name'];
            $options = $field_description['options'];
            if (!is_a($class_name, 'Viktor\Pack\Base\Field\AbstractField', true)) {
                throw new Exception($class_name . ' is not a dataset field class.');
            }
            $this->fields[$field_name] = new $class_name($this, $options, $field_name, $metadata, $full_text_index_weights);
        }
        return $this->fields;
    }

    public function getApp()
    {
        return $this->app;
    }

    public function getContentUnit()
    {
        return $this->contentUnit;
    }

    public function getFields()
    {
        return $this->fields;
    }

    public function getDecoratedRowQueryBuilder(QueryBuilder $query_builder)
    {
        $query_builder
            ->addSelect(
                'dataset.content AS dataset$content'
            )
            ->innerJoin(
                'component',
                'dataset', 'dataset',
                'component.type = dataset.type AND component.id = dataset.id'
            )
        ;
        return $query_builder;
    }

    public function setFromRow(array $row)
    {
        try {
            $content = Yaml::parse($row['dataset$content']);
        } catch (Exception $e) {
            $content = array();
        }
        if (!is_array($content)) {
            $content = array();
        }
        $fields = $this->getFields();
        foreach ($fields as $field_name => $field) {
            if (isset($content[$field_name])) {
                $field->set($content[$field_name]);
            } else {
                $field->set(); // Reset.
            }
        }
        return;
    }

    public function save()
    {
        if ($this->contentUnit->getId() <= 0) {
            return false;
        }
        $content = array();
        $fields = $this->getFields();
        foreach ($fields as $field_name => $field) {
            $content[$field_name] = $field->get();
        }
        $result = $this->app['db']->executeQuery(
            '
                SELECT id
                FROM dataset
                WHERE
                    type = ?
                    AND id = ?
            ',
            array(
                $this->contentUnit->getType(),
                $this->contentUnit->getId()
            )
        );
        $query_successful = false;
        if ($row = $result->fetch()) {
            // Update.
            $query_builder = $this->app['db']->createQueryBuilder();
            $query_builder
                ->update('dataset')
                ->set('content', ':content')
                ->where('type = :type AND id = :id')
                ->setParameter('content', Yaml::dump($content))
                ->setParameter('type', $this->contentUnit->getType())
                ->setParameter('id', $this->contentUnit->getId());
            $query_builder->execute();
            $query_successful = true;
        } else {
            // Insert.
            $query_builder = $this->app['db']->createQueryBuilder();
            $query_builder
                ->insert('dataset')
                ->setValue('type', ':type')
                ->setValue('id', ':id')
                ->setValue('content', ':content')
                ->setParameter('type', $this->contentUnit->getType())
                ->setParameter('id', $this->contentUnit->getId())
                ->setParameter('content', Yaml::dump($content));
            if ($query_builder->execute() == 1) {
                $query_successful = true;
            } else {
                $query_successful = false;
            }
        }
        if (!$query_successful) {
            return false;
        } else {
            foreach ($fields as $field_name => $field) {
                $field->updateIndex();
            }
            return true;
        }
    }

    public function delete()
    {
        $fields = $this->getFields();
        foreach ($fields as $field_name => $field) {
            $field->deleteFromIndex();
        }
        $query_builder = $this->app['db']->createQueryBuilder();
        $query_builder
            ->delete('dataset')
            ->where('type = :type AND id = :id')
            ->setParameter('type', $this->contentUnit->getType())
            ->setParameter('id', $this->contentUnit->getId());
        if ($query_builder->execute() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function addToIndex(FullTextIndexer $indexer)
    {
        $fields = $this->getFields();
        foreach ($fields as $field_name => $field) {
            $text = $field->getPlainTextRepresentation();
            $tokenizer = new Tokenizer($text, $field->getMetadata('language'));
            $tokens = $tokenizer->getTokens();
            foreach ($field->getFullTextIndexWeights() as $index_id => $weight) {
                $indexer->addTokens($tokens, $weight, $index_id);
            }
        }
    }

    public function getDecoratedIdsQueryBuilder(QueryBuilder $query_builder, array $filters, array $sort_options)
    {
        foreach ($this->fields as $field) {
            $query_builder = $field->getDecoratedIdsQueryBuilder($query_builder, $filters, $sort_options);
        }
        return $query_builder;
    }

}

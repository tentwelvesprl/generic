<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Helper;

class StringHelper
{
    public static function slugify($s)
    {
        $s = str_replace("'", ' ', $s);
        $s = str_replace("’", ' ', $s);
        // Workaround for some bogus iconv implementations.
        // See <https://bugs.php.net/bug.php?id=61484>.
        $test_s = iconv('UTF-8', 'ASCII//TRANSLIT', $s);
        if ($test_s != '') {
            $s = $test_s;
        }
        $s = preg_replace('/[^[:alnum:]\\s\\-]/', '', $s);
        $s = preg_replace('/\\s+/', '-', trim($s));
        $s = preg_replace('/\\-+/', '-', $s);
        $s = strtolower($s);
        return $s;
    }
}

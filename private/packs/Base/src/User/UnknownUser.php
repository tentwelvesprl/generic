<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\User;

use DateTime;
use Viktor\Application;

class UnknownUser implements UserInterface
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
        return;
    }

    public function load($id)
    {
        return true;
    }

    public function save(UserInterface $user)
    {
        return false;
    }

    public function delete()
    {
        return false;
    }

    public function getType()
    {
        return 'unknown-user';
    }

    public function getId()
    {
        return 1;
    }

    public function getDigest()
    {
        $digest = array();
        $digest['type'] = $this->getType();
        $digest['id'] = $this->getId();
        $digest['singular_human_type'] = $this->getSingularHumanType();
        $digest['plural_human_type'] = $this->getPluralHumanType();
        $digest['_title'] = $this->getTitle();
        $digest['_html_title'] = $this->getHtmlTitle();
        $digest['_description'] = $this->getDescription();
        $digest['_html_description'] = $this->getHtmlDescription();
        $digest['_image'] = $this->getImage();
        $digest['_image_reference'] = $this->getImageReference();
        $digest['_can_be_created'] = $this->canBeCreated();
        $digest['_can_be_viewed'] = $this->canBeViewed();
        $digest['_can_be_edited'] = $this->canBeEdited();
        $digest['_can_be_deleted'] = $this->canBeDeleted();
        $digest['_can_be_listed'] = $this->canBeListed();
        $digest['_creation_page_url'] = $this->getCreationPageUrl();
        $digest['_view_page_url'] = $this->getViewPageUrl();
        $digest['_edition_page_url'] = $this->getEditionPageUrl();
        $digest['_deletion_page_url'] = $this->getDeletionPageUrl();
        $digest['_list_page_url'] = $this->getListPageUrl();
        $digest['_can_have_permissions_modified'] = $this->canHavePermissionsModified();
        return $digest;
    }

    public function getIds(array $filters = array())
    {
        $ids = array(1);
        return $ids;
    }

    public function getSingularHumanType()
    {
        return 'unknown user';
    }

    public function getPluralHumanType()
    {
        return 'unknown users';
    }

    public function canBeCreated()
    {
        return false;
    }

    public function canBeViewed()
    {
        return true;
    }

    public function canBeEdited()
    {
        return false;
    }

    public function canBeDeleted()
    {
        return false;
    }

    public function canBeListed()
    {
        return false;
    }

    public function getCreationPageUrl()
    {
        return '';
    }

    public function getViewPageUrl()
    {
        return '';
    }

    public function getEditionPageUrl()
    {
        return '';
    }

    public function getDeletionPageUrl()
    {
        return '';
    }

    public function getListPageUrl()
    {
        return '';
    }

    public function canHavePermissionsModified()
    {
        return false;
    }

    public function getTitle()
    {
        return 'Unknown user';
    }

    public function getHtmlTitle()
    {
        return htmlspecialchars($this->getTitle());
    }

    public function getHint()
    {
        return '';
    }

    public function getDescription()
    {
        return '';
    }

    public function getHtmlDescription()
    {
        return htmlspecialchars($this->getDescription());
    }

    public function getImage()
    {
        return null;
    }

    public function getImageReference()
    {
        return null;
    }

    public function getCreationDateTime()
    {
        return new DateTime();
    }

    public function getModificationDateTime()
    {
        return new DateTime();
    }

    public function getUsername()
    {
        return '';
    }

    public function getPassword()
    {
        return '';
    }

    public function getRoles()
    {
        return array('ROLE_USER');
    }

    public function setUsername($username)
    {
        return $this;
    }

    public function setPassword($password)
    {
        return $this;
    }

    public function setRoles(array $roles)
    {
        return $this;
    }

}

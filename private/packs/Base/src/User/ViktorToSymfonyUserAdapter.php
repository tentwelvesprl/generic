<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\User;

use InvalidArgumentException;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface as Symfony_UserInterface;
use Viktor\Application;
use Viktor\Pack\Base\ContentUnit\ContentUnitInterface;

class ViktorToSymfonyUserAdapter implements Symfony_UserInterface, EquatableInterface
{
    private $userType;
    private $userId;
    private $userModificationTs;
    private $username;
    private $password;
    private $roles;
    private $enabled;
    private $accountNonExpired;
    private $credentialsNonExpired;
    private $accountNonLocked;

    public function __construct($username, $password)
    {
        if ($username === '' || $username === null) {
            throw new InvalidArgumentException('The username cannot be empty.');
        }
        $this->userType = 'unknown-user';
        $this->userId = 0;
        $this->userModificationTs = 0;
        $this->username = (string) $username;
        $this->password = (string) $password;
        $this->roles = array('ROLE_USER');
        $this->enabled = true;
        $this->accountNonExpired = true;
        $this->credentialsNonExpired = true;
        $this->accountNonLocked = true;
        return;
    }

    public function __toString()
    {
        return $this->username;
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * {@inheritdoc}
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * {@inheritdoc}
     */
    public function getSalt()
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
        return;
    }

    public function isEqualTo(Symfony_UserInterface $user)
    {
        if ($user instanceof ViktorToSymfonyUserAdapter) {
            $is_equal =
                $this->getUserType() === $user->getUserType()
                && $this->getUserId() === $user->getUserId()
                && empty(array_diff($this->getRoles(), $user->getRoles()));
            return $is_equal;
        }
        return false;
    }

    public function getUserType()
    {
        return $this->userType;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function getUserModificationTs()
    {
        return $this->userModificationTs;
    }

    public function setUser(UserInterface $user)
    {
        $this->userType = $user->getType();
        $this->userId = $user->getId();
        $this->userModificationTs = (int) $user->getModificationDateTime()->format('U');
        $this->username = $user->getUsername();
        $this->password = $user->getPassword();
        $this->roles = $user->getRoles();
        return $this;
    }

}

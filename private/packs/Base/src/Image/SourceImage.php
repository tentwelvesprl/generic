<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Image;

class SourceImage
{
    private $path; // Can be a local path or a url.
    private $pivotX; // Percentage.
    private $pivotY; // Percentage.
    private $etag;

    public function __construct($path)
    {
        $this->path = '';
        $this->setPath($path);
        $this->pivotX = 50.0;
        $this->pivotY = 50.0;
        $this->etag = '';
    }

    public function getPath()
    {
        return $this->path;
    }

    public function setPath($path)
    {
        $this->path = (string) $path;
        return $this;
    }

    public function getPivotX()
    {
        return $this->pivotX;
    }

    public function getPivotY()
    {
        return $this->pivotY;
    }

    public function setPivot($x, $y)
    {
        $x = (float) $x;
        $y = (float) $y;
        if ($x >= 0.0 && $x <= 100.0) {
            $this->pivotX = $x;
        }
        if ($y >= 0.0 && $y <= 100.0) {
            $this->pivotY = $y;
        }
        return $this;
    }

    public function getEtag()
    {
        return $this->etag;
    }

    public function setEtag($etag)
    {
        $this->etag = (string) $etag;
        return $this;
    }

}

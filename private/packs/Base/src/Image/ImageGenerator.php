<?php

/*
 * Copyright 2019 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Image;

use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Viktor\Application;

class ImageGenerator
{
    private $imageProcessor;
    private $app;

    public function __construct(Application $app, SourceImage $source_image)
    {
        $this->app = $app;
        switch ($app['viktor.config']['image_generator_backend']) {
            case 'gd':
                $this->imageProcessor = new ImageProcessorGd($source_image);
                break;
            case 'imagick':
                $this->imageProcessor = new ImageProcessorImagick($source_image);
                break;
            case 'vips':
                $this->imageProcessor = new ImageProcessorVips($source_image);
                break;
            default:
                throw new Exception('Unknown image generator backend (\'' . (string) $app['viktor.config']['image_generator_backend'] . '\').');
        }
    }

    public function getSourceImage()
    {
        return $this->getImageProcessor()->getSourceImage();
    }

    public function getImageProcessor()
    {
        return $this->imageProcessor;
    }

    public function getWidth()
    {
        return $this->getImageProcessor()->getWidth();
    }

    public function getHeight()
    {
        return $this->getImageProcessor()->getHeight();
    }

    public function load()
    {
        $this->getImageProcessor()->load();
        return $this;
    }

    public function validate()
    {
        $image_area = $this->getWidth() * $this->getHeight();
        if ($image_area > $this->app['viktor.config']['max_image_area']) {
            $max_image_side_length = floor(sqrt($this->app['viktor.config']['max_image_area']));
            return array(
                'status' => 'error',
                'reason' => 'Image size cannot exceed ' . $max_image_side_length . ' by ' . $max_image_side_length . ' pixels.',
            );
        }
        return array(
            'status' => 'success',
            'reason' => 'Ok.',
        );
    }

    public function getAvailableOutputFormats()
    {
        return $this->getImageProcessor()->getAvailableOutputFormats();
    }

    public function getOutputFormat() {
        return $this->getImageProcessor()->getOutputFormat();
    }

    public function setOutputFormat($mime_type, array $options = array())
    {
        $this->getImageProcessor()->setOutputFormat($mime_type, $options);
        return $this;
    }

    public function addTransformation(array $transformation)
    {
        $this->getImageProcessor()->addTransformation($transformation);
        return $this;
    }

    public function addTransformations(array $transformations)
    {
        $this->getImageProcessor()->addTransformations($transformations);
        return $this;
    }

    public function applyTransformations()
    {
        $this->getImageProcessor()->applyTransformations();
        return $this;
    }

    public function getProcessedImageContent()
    {
        return $this->getImageProcessor()->getProcessedImageContent();
    }

    public function getResponse($response = null)
    {
        try {
            if (!($response instanceof Response)) {
                $response = new Response();
            }
            $this->load();
            $this->applyTransformations();
            $new_extension = $this->getOutputFormat()['extension'];
            $complete_file_path = $this->getSourceImage()->getPath();
            $info = pathinfo($complete_file_path);
            $fallback_name = basename($complete_file_path, '.' . $info['extension']);
            $fallback_name = @iconv('UTF-8', 'ASCII//IGNORE', $fallback_name);
            $fallback_name = preg_replace('#[^a-zA-Z0-9_\-\ ]#', '', $fallback_name);
            if (trim($fallback_name) == '') {
                $fallback_name = 'image';
            }
            $fallback_name .= '.' . $new_extension;
            $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_INLINE, $fallback_name);
            $response->headers->set('Content-Disposition', $disposition);
            $response->headers->set('Content-Type', $this->getImageProcessor()->getOutputFormatMimeType());
            $response->setContent($this->getProcessedImageContent());
        } catch (Exception $e) {
            $response = new Response($e->getMessage(), 404);
        }
        return $response;
    }

}

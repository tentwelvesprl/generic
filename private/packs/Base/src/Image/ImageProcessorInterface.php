<?php

/*
 * Copyright 2019 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Image;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

interface ImageProcessorInterface
{
    public function __construct(SourceImage $source_image);

    public function getSourceImage();

    public function getWidth();

    public function getHeight();

    public function load();

    public function getAvailableInputFormats();

    public function getAvailableOutputFormats();

    public function getOutputFormat();

    public function getOutputFormatMimeType();

    public function setOutputFormat($mime_type, array $options = array());

    public function addTransformation(array $transformation);

    public function addTransformations(array $transformations);

    public function applyTransformations();

    public function getProcessedImageContent();

}

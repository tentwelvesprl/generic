<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Image;

use Exception;
use Doctrine\DBAL\Query\QueryBuilder;
use Symfony\Component\Yaml\Yaml;
use Viktor\Application;
use Viktor\Pack\Base\ContentUnit\ContentUnitInterface;

class UploadedImage
{
    private $app;
    private $contentUnit;
    private $options;
    private $filePath; // File path, relative to Viktor data path.
    private $pivotX; // Percentage.
    private $pivotY; // Percentage.
    private $width;
    private $height;
    private $area;
    private $ratio;
    private $etag;

    public function __construct(Application $app, ContentUnitInterface $content_unit, array $options)
    {
        $this->app = $app;
        $this->contentUnit = $content_unit;
        $default_options = array(
        );
        $this->options = array_merge($default_options, $options);
        $this->filePath = '';
        $this->pivotX = 50.0;
        $this->pivotY = 50.0;
        $this->width = 0;
        $this->height = 0;
        $this->area = 0;
        $this->ratio = 1.0;
        $this->etag = '';
        return;
    }

    public function getApp()
    {
        return $this->app;
    }

    public function getContentUnit()
    {
        return $this->contentUnit;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function getFilePath()
    {
        return $this->filePath;
    }

    public function setFilePath($file_path)
    {
        $this->filePath = (string) $file_path;
        return $this;
    }

    public function getPivotX()
    {
        return $this->pivotX;
    }

    public function getPivotY()
    {
        return $this->pivotY;
    }

    public function setPivot($x, $y)
    {
        $x = (float) $x;
        $y = (float) $y;
        if ($x >= 0.0 && $x <= 100.0) {
            $this->pivotX = $x;
        }
        if ($y >= 0.0 && $y <= 100.0) {
            $this->pivotY = $y;
        }
        return $this;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function setWidth($width)
    {
        $this->width = $width;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function setHeight($height)
    {
        $this->height = $height;
    }

    public function getArea()
    {
        return $this->area;
    }

    public function setArea($area)
    {
        $this->area = $area;
    }

    public function getRatio()
    {
        return $this->ratio;
    }

    public function setRatio($ratio)
    {
        $this->ratio = $ratio;
    }

    public function getEtag()
    {
        return $this->etag;
    }

    public function setEtag($etag)
    {
        $this->etag = (string) $etag;
        return $this;
    }

    public function getDecoratedRowQueryBuilder(QueryBuilder $query_builder)
    {
        $query_builder
            ->addSelect('uploaded_image.file_path AS uploaded_image$file_path')
            ->addSelect('uploaded_image.pivot_x AS uploaded_image$pivot_x')
            ->addSelect('uploaded_image.pivot_y AS uploaded_image$pivot_y')
            ->addSelect('uploaded_image.etag AS uploaded_image$etag')
            ->addSelect('uploaded_image_index.width AS uploaded_image_index$width')
            ->addSelect('uploaded_image_index.height AS uploaded_image_index$height')
            ->addSelect('uploaded_image_index.area AS uploaded_image_index$area')
            ->addSelect('uploaded_image_index.ratio AS uploaded_image_index$ratio')
            ->innerJoin(
                'component',
                'uploaded_image', 'uploaded_image',
                'component.type = uploaded_image.type AND component.id = uploaded_image.id'
            )
            ->innerJoin(
                'component',
                'uploaded_image_index', 'uploaded_image_index',
                'component.type = uploaded_image_index.type AND component.id = uploaded_image_index.id'
            )
        ;
        return $query_builder;
    }

    public function setFromRow(array $row)
    {
        $this->setFilePath($row['uploaded_image$file_path']);
        $this->setPivot($row['uploaded_image$pivot_x'], $row['uploaded_image$pivot_y']);
        $this->setWidth($row['uploaded_image_index$width']);
        $this->setHeight($row['uploaded_image_index$height']);
        $this->setArea($row['uploaded_image_index$area']);
        $this->setRatio($row['uploaded_image_index$ratio']);
        $this->setEtag($row['uploaded_image$etag']);
    }

    public function save()
    {
        if ($this->contentUnit->getId() <= 0) {
            return false;
        }
        $this->setEtag(md5(time() . $this->getFilePath() . $this->getPivotX() . $this->getPivotY()));
        $result = $this->app['db']->executeQuery(
            '
                SELECT id
                FROM uploaded_image
                WHERE
                    type = ?
                    AND id = ?
            ',
            array(
                $this->contentUnit->getType(),
                $this->contentUnit->getId()
            )
        );
        $query_successful = false;
        if ($row = $result->fetch()) {
            // Update.
            $query_builder = $this->app['db']->createQueryBuilder();
            $query_builder
                ->update('uploaded_image')
                ->set('file_path', ':file_path')
                ->set('pivot_x', ':pivot_x')
                ->set('pivot_y', ':pivot_y')
                ->set('etag', ':etag')
                ->where('type = :type AND id = :id')
                ->setParameter('file_path', $this->getFilePath())
                ->setParameter('pivot_x', $this->getPivotX())
                ->setParameter('pivot_y', $this->getPivotY())
                ->setParameter('etag', $this->getEtag())
                ->setParameter('type', $this->contentUnit->getType())
                ->setParameter('id', $this->contentUnit->getId());
            $query_builder->execute();
            $query_successful = true;
        } else {
            // Insert.
            $query_builder = $this->app['db']->createQueryBuilder();
            $query_builder
                ->insert('uploaded_image')
                ->setValue('type', ':type')
                ->setValue('id', ':id')
                ->setValue('file_path', ':file_path')
                ->setValue('pivot_x', ':pivot_x')
                ->setValue('pivot_y', ':pivot_y')
                ->setValue('etag', ':etag')
                ->setParameter('type', $this->contentUnit->getType())
                ->setParameter('id', $this->contentUnit->getId())
                ->setParameter('file_path', $this->getFilePath())
                ->setParameter('pivot_x', $this->getPivotX())
                ->setParameter('pivot_y', $this->getPivotY())
                ->setParameter('etag', $this->getEtag());
            if ($query_builder->execute() == 1) {
                $query_successful = true;
            } else {
                $query_successful = false;
            }
        }
        if (!$query_successful) {
            return false;
        } else {
            $this->updateIndex();
            return true;
        }
    }

    public function delete()
    {
        $query_builder = $this->app['db']->createQueryBuilder();
        $query_builder
            ->delete('uploaded_image')
            ->where('type = :type AND id = :id')
            ->setParameter('type', $this->contentUnit->getType())
            ->setParameter('id', $this->contentUnit->getId());
        if ($query_builder->execute() == 1) {
            $this->deleteFromIndex();
            return true;
        } else {
            return false;
        }
    }

    public function updateIndex()
    {
        $this->deleteFromIndex();
        $extended_info = $this->getExtendedInfo();
        $width = $extended_info['width'];
        $height = $extended_info['height'];
        $area = $extended_info['area'];
        $ratio = $extended_info['ratio'];
        $query_builder = $this->app['db']->createQueryBuilder();
        $query_builder
            ->insert('uploaded_image_index')
            ->setValue('type', ':type')
            ->setValue('id', ':id')
            ->setValue('width', ':width')
            ->setValue('height', ':height')
            ->setValue('area', ':area')
            ->setValue('ratio', ':ratio')
            ->setParameter('type', $this->getContentUnit()->getType())
            ->setParameter('id', $this->getContentUnit()->getId())
            ->setParameter('width', $width)
            ->setParameter('height', $height)
            ->setParameter('area', $area)
            ->setParameter('ratio', $ratio);
        $query_builder->execute();
        return true;
    }

    public function deleteFromIndex()
    {
        $query_builder = $this->getApp()['db']->createQueryBuilder();
        $query_builder
            ->delete('uploaded_image_index')
            ->where('type = :type AND id = :id')
            ->setParameter('type', $this->getContentUnit()->getType())
            ->setParameter('id', $this->getContentUnit()->getId());
        $query_builder->execute();

        return true;
    }

    public function getDecoratedIdsQueryBuilder(QueryBuilder $query_builder, array $filters, array $sort_options)
    {
        return $query_builder;
    }

    public function copy(UploadedImage $source)
    {
        $this->filePath = $source->getFilePath();
    }

    public function getSourceImage()
    {
        if ($this->filePath != '') {
            $source_image = new SourceImage($this->app['viktor.data_path'] . $this->filePath);
            $source_image->setPivot($this->getPivotX(), $this->getPivotY());
            $source_image->setEtag($this->getEtag());
            return $source_image;
        } else {
            return null;
        }
    }

    public function getDigest()
    {
        $digest = array(
            'file_path' => $this->getFilePath(),
            'pivot_x' => $this->getPivotX(),
            'pivot_y' => $this->getPivotY(),
            'width' => $this->getWidth(),
            'height' => $this->getHeight(),
            'area' => $this->getArea(),
            'ratio' => $this->getRatio(),
            'etag' => $this->getEtag(),
        );
        return $digest;
    }

    public function getExtendedInfo()
    {
        $width = 0;
        $height = 0;
        $area = 0;
        $ratio = 1.0;
        try {
            $source_image = new SourceImage($this->getApp()['viktor.data_path'] . $this->getFilePath());
            $image_generator = new ImageGenerator($this->getApp(), $source_image);
            $image_generator->load();
            $width = $image_generator->getWidth();
            $height = $image_generator->getHeight();
            $area = $width * $height;
            if ($height > 0) {
                $ratio = $width / $height;
            }
        } catch (Exception $e) {
            // Fail silently.
        }
        $a = array(
            'width' => $width,
            'height' => $height,
            'ratio' => $ratio,
            'area' => $area,
        );
        return $a;
    }

}

<?php

/*
 * Copyright 2019 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Image;

use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class ImageProcessorGd implements ImageProcessorInterface
{
    private $sourceImage;
    private $outputFormatMimeType;
    private $outputFormatOptions;
    private $transformations;
    private $imageResource;
    private $width;
    private $height;

    public function __construct(SourceImage $source_image)
    {
        $this->sourceImage = $source_image;
        $this->outputFormatMimeType = 'image/png';
        $this->outputFormatOptions = array();
        $this->transformations = array();
        $this->imageResource = null;
        $this->width = 0;
        $this->height = 0;
    }

    public function __destruct()
    {
        if ($this->imageResource != null) {
            imagedestroy($this->imageResource);
        }
    }

    public function getSourceImage()
    {
        return $this->sourceImage;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function load()
    {
        $complete_file_path = $this->getSourceImage()->getPath();
        if ($complete_file_path == '') {
            throw new Exception('The source image is not specified.');
        }
        $this->imageResource = @imagecreatefromstring(@file_get_contents($complete_file_path));
        if ($this->imageResource == false) {
            throw new Exception('Invalid source image file.');
        }
        $this->width = (int) imagesx($this->imageResource);
        $this->height = (int) imagesy($this->imageResource);
        return $this;
    }

    public function getAvailableInputFormats()
    {
        return array(
            'image/png',
            'image/jpeg',
            'image/gif',
        );
    }

    public function getAvailableOutputFormats()
    {
        return array(
            'image/png' => array(
                'extension' => 'png',
                'default_options' => array(),
            ),
            'image/jpeg' => array(
                'extension' => 'jpeg',
                'default_options' => array(
                    'jpeg_quality' => 85,
                ),
            ),
        );
    }

    public function getOutputFormat()
    {
        $available_output_formats = $this->getAvailableOutputFormats();
        return $available_output_formats[$this->outputFormatMimeType];
    }

    public function getOutputFormatMimeType()
    {
        return $this->outputFormatMimeType;
    }

    public function setOutputFormat($mime_type, array $options = array())
    {
        $mime_type = (string) $mime_type;
        $options = (array) $options;
        $available_output_formats = $this->getAvailableOutputFormats();
        if (isset($available_output_formats[$mime_type])) {
            $this->outputFormatMimeType = $mime_type;
            $this->outputFormatOptions = array_merge($available_output_formats[$mime_type]['default_options'], $options);
        }
        return $this;
    }

    public function addTransformation(array $transformation)
    {
        $this->transformations[] = $transformation;
        return $this;
    }

    public function addTransformations(array $transformations)
    {
        foreach ($transformations as $transformation) {
            $this->addTransformation($transformation);
        }
        return $this;
    }

    public function applyTransformationFitToWidth(array $options)
    {
        $default_options = array(
            'width' => 200,
        );
        $options = array_merge($default_options, $options);
        $new_width = (int) $options['width'];
        $new_height = round($new_width * $this->height / $this->width);
        $new_im = imagecreatetruecolor($new_width, $new_height);
        imagealphablending($new_im, false);
        $transparent_color = imagecolorallocatealpha($new_im, 255, 255, 255, 127);
        imagefilledrectangle($new_im, 0, 0, $new_width, $new_height, $transparent_color);
        imagealphablending($new_im, true);
        imagecopyresampled($new_im, $this->imageResource, 0, 0, 0, 0, $new_width, $new_height, $this->width, $this->height);
        $this->imageResource = $new_im;
        $this->width = $new_width;
        $this->height = $new_height;
    }

    public function applyTransformationFitToHeight(array $options)
    {
        $default_options = array(
            'height' => 200,
        );
        $options = array_merge($default_options, $options);
        $new_height = (int) $options['height'];
        $new_width = round($new_height * $this->width / $this->height);
        $new_im = imagecreatetruecolor($new_width, $new_height);
        imagealphablending($new_im, false);
        $transparent_color = imagecolorallocatealpha($new_im, 255, 255, 255, 127);
        imagefilledrectangle($new_im, 0, 0, $new_width, $new_height, $transparent_color);
        imagealphablending($new_im, true);
        imagecopyresampled($new_im, $this->imageResource, 0, 0, 0, 0, $new_width, $new_height, $this->width, $this->height);
        $this->imageResource = $new_im;
        $this->width = $new_width;
        $this->height = $new_height;
    }

    public function applyTransformationFitToMinDimensions(array $options)
    {
        $default_options = array(
            'width' => 200,
            'height' => 200,
        );
        $options = array_merge($default_options, $options);
        $min_width = (int) $options['width'];
        $min_height = (int) $options['height'];
        $new_width = round($min_height * $this->width / $this->height);
        $new_height = round($min_width * $this->height / $this->width);
        if ($new_height < $min_height) {
            $new_height = $min_height;
        } else {
            $new_width = $min_width;
        }
        $new_im = imagecreatetruecolor($new_width, $new_height);
        imagealphablending($new_im, false);
        $transparent_color = imagecolorallocatealpha($new_im, 255, 255, 255, 127);
        imagefilledrectangle($new_im, 0, 0, $new_width, $new_height, $transparent_color);
        imagealphablending($new_im, true);
        imagecopyresampled($new_im, $this->imageResource, 0, 0, 0, 0, $new_width, $new_height, $this->width, $this->height);
        $this->imageResource = $new_im;
        $this->width = $new_width;
        $this->height = $new_height;
    }

    public function applyTransformationFitToMaxDimensions(array $options)
    {
        $default_options = array(
            'width' => 200,
            'height' => 200,
        );
        $options = array_merge($default_options, $options);
        $max_width = (int) $options['width'];
        $max_height = (int) $options['height'];
        $new_width = round($max_height * $this->width / $this->height);
        $new_height = round($max_width * $this->height / $this->width);
        if ($new_height > $max_height) {
            $new_height = $max_height;
        } else {
            $new_width = $max_width;
        }
        $new_im = imagecreatetruecolor($new_width, $new_height);
        imagealphablending($new_im, false);
        $transparent_color = imagecolorallocatealpha($new_im, 255, 255, 255, 127);
        imagefilledrectangle($new_im, 0, 0, $new_width, $new_height, $transparent_color);
        imagealphablending($new_im, true);
        imagecopyresampled($new_im, $this->imageResource, 0, 0, 0, 0, $new_width, $new_height, $this->width, $this->height);
        $this->imageResource = $new_im;
        $this->width = $new_width;
        $this->height = $new_height;
    }

    public function applyTransformationCropCenter(array $options)
    {
        $default_options = array(
            'width' => 200,
            'height' => 200,
        );
        $options = array_merge($default_options, $options);
        $new_width = (int) $options['width'];
        $new_height = (int) $options['height'];
        $new_im = imagecreatetruecolor($new_width, $new_height);
        imagealphablending($new_im, false);
        $transparent_color = imagecolorallocatealpha($new_im, 255, 255, 255, 127);
        imagefilledrectangle($new_im, 0, 0, $new_width, $new_height, $transparent_color);
        imagealphablending($new_im, true);
        if ($new_width <= $this->width) {
            $x = round(($this->width - $new_width) / 2);
            $src_width = $new_width;
            $dst_x = 0;
        } else {
            $x = 0;
            $src_width = $this->width;
            $dst_x = round(($new_width - $this->width) / 2);
        }
        if ($new_height <= $this->height) {
            $y = round(($this->height - $new_height) / 2);
            $src_height = $new_height;
            $dst_y = 0;
        } else {
            $y = 0;
            $src_height = $this->height;
            $dst_y = round(($new_height - $this->height) / 2);
        }
        imagecopy($new_im, $this->imageResource, $dst_x, $dst_y, $x, $y, $src_width, $src_height);
        $this->imageResource = $new_im;
        $this->width = $new_width;
        $this->height = $new_height;
    }

    public function applyTransformationResizeAroundPivotPoint(array $options)
    {
        $default_options = array(
            'width' => 200,
            'height' => 200,
        );
        $options = array_merge($default_options, $options);
        $new_width = (int) $options['width'];
        $new_height = (int) $options['height'];
        $pivot_x_pc = (float) $this->getSourceImage()->getPivotX();
        $pivot_y_pc = (float) $this->getSourceImage()->getPivotY();
        // Get pivot point coordinates.
        $pivot_x = (int) floor($this->width * $pivot_x_pc / 100.0);
        $pivot_y = (int) floor($this->height * $pivot_y_pc / 100.0);
        // Take a slice of the original image, centered around the pivot.
        $new_ratio = $new_width / $new_height;
        $old_ratio = $this->width / $this->height;
        if ($old_ratio > $new_ratio) {
            // We take a vertical slice of the original image.
            $slice_width = floor($this->height * $new_ratio);
            $slice_height = $this->height;
        } else {
            // We take an horizontal slice of the original image.
            $slice_width = $this->width;
            $slice_height = floor($this->width / $new_ratio);
        }
        $slice_x = floor($pivot_x - ($slice_width / 2));
        $slice_y = floor($pivot_y - ($slice_height / 2));
        if ($slice_x < 0) {
            $slice_x = 0;
        }
        if ($slice_y < 0) {
            $slice_y = 0;
        }
        if ($slice_x + $slice_width > $this->width) {
            $slice_x = $this->width - $slice_width;
        }
        if ($slice_y + $slice_height > $this->height) {
            $slice_y = $this->height - $slice_height;
        }
        $slice_im = imagecreatetruecolor($slice_width, $slice_height);
        imagealphablending($slice_im, false);
        $transparent_color = imagecolorallocatealpha($slice_im, 255, 255, 255, 127);
        imagefilledrectangle($slice_im, 0, 0, $slice_width, $slice_height, $transparent_color);
        imagealphablending($slice_im, true);
        imagecopy($slice_im, $this->imageResource, 0, 0, $slice_x, $slice_y, $slice_width, $slice_height);
        // Create a new image.
        $new_im = imagecreatetruecolor($new_width, $new_height);
        imagealphablending($new_im, false);
        $transparent_color = imagecolorallocatealpha($new_im, 255, 255, 255, 127);
        imagefilledrectangle($new_im, 0, 0, $new_width, $new_height, $transparent_color);
        imagealphablending($new_im, true);
        imagecopyresampled($new_im, $slice_im, 0, 0, 0, 0, $new_width, $new_height, $slice_width, $slice_height);
        $this->imageResource = $new_im;
        $this->width = $new_width;
        $this->height = $new_height;
    }

    public function applyTransformationWatermark(array $options)
    {
        $default_options = array(
            'hpos' => 'right',
            'vpos' => 'bottom',
            'path' => '',
        );
        $options = array_merge($default_options, $options);
        $hpos = (string) $options['hpos'];
        $vpos = (string) $options['vpos'];
        $path = (string) $options['path'];
        if (($watermark = @imagecreatefromstring(@file_get_contents($path))) != false) {
            // Make sure we have a transparent true color source.
            $im_tc = imagecreatetruecolor($this->width, $this->height);
            imagealphablending($im_tc, false);
            $transparent_color = imagecolorallocatealpha($im_tc, 255, 255, 255, 127);
            imagefilledrectangle($im_tc, 0, 0, $this->width, $this->height, $transparent_color);
            imagealphablending($im_tc, true);
            imagealphablending($this->imageResource, true);
            imagecopy($im_tc, $this->imageResource, 0, 0, 0, 0, $this->width, $this->height);
            imagealphablending($watermark, true);
            $this->imageResource = $im_tc;
            switch ($hpos) {
                case 'left':
                    $x = 0;
                    break;
                case 'right':
                    $x = imagesx($this->imageResource) - imagesx($watermark);
                    break;
                default:
                    $x = round((imagesx($this->imageResource) - imagesx($watermark)) / 2);
                    break;
            }
            switch ($vpos) {
                case 'top':
                    $y = 0;
                    break;
                case 'bottom':
                    $y = imagesy($this->imageResource) - imagesy($watermark);
                    break;
                default:
                    $y = round((imagesy($this->imageResource) - imagesy($watermark)) / 2);
                    break;
            }
            imagecopy($this->imageResource, $watermark, $x, $y, 0, 0, imagesx($watermark), imagesy($watermark));
        }
    }

    public function applyTransformationDesaturate(array $options)
    {
        imagefilter($this->imageResource, IMG_FILTER_GRAYSCALE);
    }

    public function applyTransformationBlur(array $options)
    {
        imagefilter($this->imageResource, IMG_FILTER_GAUSSIAN_BLUR);
    }

    public function applyTransformations()
    {
        foreach ($this->transformations as $transformation) {
            switch ($transformation['name']) {
                case 'fit_to_width':
                    $this->applyTransformationFitToWidth($transformation['options']);
                    break;
                case 'fit_to_height':
                    $this->applyTransformationFitToHeight($transformation['options']);
                    break;
                case 'fit_to_min_dimensions':
                    $this->applyTransformationFitToMinDimensions($transformation['options']);
                    break;
                case 'fit_to_max_dimensions':
                    $this->applyTransformationFitToMaxDimensions($transformation['options']);
                    break;
                case 'crop_center':
                    $this->applyTransformationCropCenter($transformation['options']);
                    break;
                case 'resize_around_pivot_point':
                    $this->applyTransformationResizeAroundPivotPoint($transformation['options']);
                    break;
                case 'watermark':
                    $this->applyTransformationWatermark($transformation['options']);
                    break;
                case 'desaturate':
                    $this->applyTransformationDesaturate($transformation['options']);
                    break;
                case 'blur':
                    $this->applyTransformationBlur($transformation['options']);
                    break;
                default:
                    throw new Exception('Unknown transformation.');
            }
        }
        return $this;
    }

    public function getProcessedImageContent()
    {
        ob_start();
        switch($this->outputFormatMimeType) {
            case 'image/png';
                imagealphablending($this->imageResource, false);
                imagesavealpha($this->imageResource, true);
                imagepng($this->imageResource);
                break;
            case 'image/jpeg';
                imageinterlace($this->imageResource, true);
                imagejpeg($this->imageResource, null, $this->outputFormatOptions['jpeg_quality']);
                break;
        }
        $buffer = ob_get_contents();
        ob_clean();
        return $buffer;
    }

}

<?php

/*
 * Copyright 2019 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Image;

use Exception;
use Imagick;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class ImageProcessorImagick implements ImageProcessorInterface
{
    private $sourceImage;
    private $outputFormatMimeType;
    private $outputFormatOptions;
    private $transformations;
    private $imagick;
    private $width;
    private $height;
    private $isLazyLoaded;

    public function __construct(SourceImage $source_image)
    {
        $this->sourceImage = $source_image;
        $this->outputFormatMimeType = 'image/png';
        $this->outputFormatOptions = array();
        $this->transformations = array();
        $this->imagick = null;
        $this->width = 0;
        $this->height = 0;
        $this->isLazyLoaded = false;
    }

    public function __destruct()
    {
        if ($this->imagick !== null) {
            $this->imagick->clear();
        }
    }

    public function getSourceImage()
    {
        return $this->sourceImage;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function load()
    {
        $this->isLazyLoaded = false;
        $complete_file_path = $this->getSourceImage()->getPath();
        if ($complete_file_path == '') {
            throw new Exception('The source image is not specified.');
        }
        try {
            $this->imagick = new Imagick();
            $this->imagick->setBackgroundColor('white');
            $this->imagick->pingImage($complete_file_path);
            $this->width = (int) $this->imagick->getImageWidth();
            $this->height = (int) $this->imagick->getImageHeight();
            $orientation = $this->imagick->getImageOrientation();
            if (in_array($orientation, array(
                Imagick::ORIENTATION_LEFTTOP,
                Imagick::ORIENTATION_RIGHTTOP,
                Imagick::ORIENTATION_RIGHTBOTTOM,
                Imagick::ORIENTATION_LEFTBOTTOM,
            ))) {
                $tmp = $this->width;
                $this->width = $this->height;
                $this->height = $tmp;
            }
        } catch (Exception $e) {
            throw new Exception('Invalid source image file. Exception: ' . $e->getMessage());
        }
        return $this;
    }

    public function assertLoaded()
    {
        if ($this->isLazyLoaded) {
            return;
        }
        $complete_file_path = $this->getSourceImage()->getPath();
        // HACK for large jpgs: if the size is really big, we pass a hint to ImageMagick so that 
        // it can try shrink-on-load.
        if ($this->width > 6000 || $this->height > 6000) {
            $this->imagick->setOption('jpeg:size', '6000x6000');
        }
        $this->imagick->readImage($complete_file_path); // Throws an exception if something goes wrong.
        // Image orientation handling.
        $orientation = $this->imagick->getImageOrientation();
        switch($orientation) {
            case Imagick::ORIENTATION_TOPRIGHT:
                $this->imagick->flopImage();
                break;
            case Imagick::ORIENTATION_BOTTOMRIGHT:
                $this->imagick->rotateImage('#000', 180);
                break;
            case Imagick::ORIENTATION_BOTTOMLEFT:
                $this->imagick->flipImage();
                break;
            case Imagick::ORIENTATION_LEFTTOP:
                $this->imagick->flipImage();
                $this->imagick->rotateImage('#000', 90);
                break;
            case Imagick::ORIENTATION_RIGHTTOP:
                $this->imagick->rotateImage('#000', 90);
                break;
            case Imagick::ORIENTATION_RIGHTBOTTOM:
                $this->imagick->flipImage();
                $this->imagick->rotateImage('#000', 270);
                break;
            case Imagick::ORIENTATION_LEFTBOTTOM:
                $this->imagick->rotateImage('#000', 270);
                break;
        }
        $this->imagick->setImageOrientation(Imagick::ORIENTATION_TOPLEFT);
        if ($this->imagick->getImageColorspace() != Imagick::COLORSPACE_SRGB) {
            $this->imagick->transformImageColorspace(Imagick::COLORSPACE_SRGB);
        }
        $this->isLazyLoaded = true;
    }

    public function getAvailableInputFormats()
    {
        return array(
            'image/png',
            'image/jpeg',
            'image/gif',
            'image/tiff',
            'image/bmp',
            'image/webp',
            'application/pdf',
        );
    }

    public function getAvailableOutputFormats()
    {
        return array(
            'image/png' => array(
                'extension' => 'png',
                'default_options' => array(),
                'imagick_format' => 'png',
            ),
            'image/jpeg' => array(
                'extension' => 'jpeg',
                'default_options' => array(
                    'jpeg_quality' => 85,
                    'jpeg_interlace' => true,
                ),
                'imagick_format' => 'jpeg',
            ),
            'image/webp' => array(
                'extension' => 'webp',
                'default_options' => array(
                    'webp_quality' => 85,
                    'webp_lossless' => false,
                ),
                'imagick_format' => 'webp',
            ),
        );
    }

    public function getOutputFormat()
    {
        $available_output_formats = $this->getAvailableOutputFormats();
        return $available_output_formats[$this->outputFormatMimeType];
    }

    public function getOutputFormatMimeType()
    {
        return $this->outputFormatMimeType;
    }

    public function setOutputFormat($mime_type, array $options = array())
    {
        $mime_type = (string) $mime_type;
        $options = (array) $options;
        $available_output_formats = $this->getAvailableOutputFormats();
        if (isset($available_output_formats[$mime_type])) {
            $this->outputFormatMimeType = $mime_type;
            $this->outputFormatOptions = array_merge($available_output_formats[$mime_type]['default_options'], $options);
        }
        return $this;
    }

    public function addTransformation(array $transformation)
    {
        $this->transformations[] = $transformation;
        return $this;
    }

    public function addTransformations(array $transformations)
    {
        foreach ($transformations as $transformation) {
            $this->addTransformation($transformation);
        }
        return $this;
    }

    public function applyTransformationFitToWidth(array $options)
    {
        $default_options = array(
            'width' => 200,
        );
        $options = array_merge($default_options, $options);
        $new_width = (int) $options['width'];
        $new_height = round($new_width * $this->height / $this->width);
        $this->assertLoaded();
        $this->imagick->resizeImage($new_width, $new_height, Imagick::FILTER_LANCZOS, 1);
        $this->width = $new_width;
        $this->height = $new_height;
    }

    public function applyTransformationFitToHeight(array $options)
    {
        $default_options = array(
            'height' => 200,
        );
        $options = array_merge($default_options, $options);
        $new_height = (int) $options['height'];
        $new_width = round($new_height * $this->width / $this->height);
        $this->assertLoaded();
        $this->imagick->resizeImage($new_width, $new_height, Imagick::FILTER_LANCZOS, 1);
        $this->width = $new_width;
        $this->height = $new_height;
    }

    public function applyTransformationFitToMinDimensions(array $options)
    {
        $default_options = array(
            'width' => 200,
            'height' => 200,
        );
        $options = array_merge($default_options, $options);
        $min_width = (int) $options['width'];
        $min_height = (int) $options['height'];
        $new_width = round($min_height * $this->width / $this->height);
        $new_height = round($min_width * $this->height / $this->width);
        if ($new_height < $min_height) {
            $new_height = $min_height;
        } else {
            $new_width = $min_width;
        }
        $this->assertLoaded();
        $this->imagick->resizeImage($new_width, $new_height, Imagick::FILTER_LANCZOS, 1);
        $this->width = $new_width;
        $this->height = $new_height;
    }

    public function applyTransformationFitToMaxDimensions(array $options)
    {
        $default_options = array(
            'width' => 200,
            'height' => 200,
        );
        $options = array_merge($default_options, $options);
        $max_width = (int) $options['width'];
        $max_height = (int) $options['height'];
        $new_width = round($max_height * $this->width / $this->height);
        $new_height = round($max_width * $this->height / $this->width);
        if ($new_height > $max_height) {
            $new_height = $max_height;
        } else {
            $new_width = $max_width;
        }
        $this->assertLoaded();
        $this->imagick->resizeImage($new_width, $new_height, Imagick::FILTER_LANCZOS, 1);
        $this->width = $new_width;
        $this->height = $new_height;
    }

    public function applyTransformationCropCenter(array $options)
    {
        $default_options = array(
            'width' => 200,
            'height' => 200,
        );
        $options = array_merge($default_options, $options);
        $new_width = (int) $options['width'];
        $new_height = (int) $options['height'];
        if ($new_width <= $this->width) {
            $x = round(($this->width - $new_width) / 2);
        } else {
            $x = 0;
            $new_width = $this->width;
        }
        if ($new_height <= $this->height) {
            $y = round(($this->height - $new_height) / 2);
        } else {
            $y = 0;
            $new_height = $this->height;
        }
        $this->assertLoaded();
        $this->imagick->cropImage($new_width, $new_height, $x, $y);
        $this->width = $new_width;
        $this->height = $new_height;
    }

    public function applyTransformationResizeAroundPivotPoint(array $options)
    {
        $default_options = array(
            'width' => 200,
            'height' => 200,
        );
        $options = array_merge($default_options, $options);
        $new_width = (int) $options['width'];
        $new_height = (int) $options['height'];
        $pivot_x_pc = (float) $this->getSourceImage()->getPivotX();
        $pivot_y_pc = (float) $this->getSourceImage()->getPivotY();
        // Get pivot point coordinates.
        $pivot_x = (int) floor($this->width * $pivot_x_pc / 100.0);
        $pivot_y = (int) floor($this->height * $pivot_y_pc / 100.0);
        // Take a slice of the original image, centered around the pivot.
        $new_ratio = $new_width / $new_height;
        $old_ratio = $this->width / $this->height;
        if ($old_ratio > $new_ratio) {
            // We take a vertical slice of the original image.
            $slice_width = floor($this->height * $new_ratio);
            $slice_height = $this->height;
        } else {
            // We take an horizontal slice of the original image.
            $slice_width = $this->width;
            $slice_height = floor($this->width / $new_ratio);
        }
        $slice_x = floor($pivot_x - ($slice_width / 2));
        $slice_y = floor($pivot_y - ($slice_height / 2));
        if ($slice_x < 0) {
            $slice_x = 0;
        }
        if ($slice_y < 0) {
            $slice_y = 0;
        }
        if ($slice_x + $slice_width > $this->width) {
            $slice_x = $this->width - $slice_width;
        }
        if ($slice_y + $slice_height > $this->height) {
            $slice_y = $this->height - $slice_height;
        }
        $this->assertLoaded();
        $this->imagick->cropImage($slice_width, $slice_height, $slice_x, $slice_y);
        $this->imagick->resizeImage($new_width, $new_height, Imagick::FILTER_LANCZOS, 1);
        $this->width = $new_width;
        $this->height = $new_height;
    }

    public function applyTransformationWatermark(array $options)
    {
        $default_options = array(
            'hpos' => 'right',
            'vpos' => 'bottom',
            'path' => '',
        );
        $options = array_merge($default_options, $options);
        $hpos = (string) $options['hpos'];
        $vpos = (string) $options['vpos'];
        $path = (string) $options['path'];
        $watermark = new Imagick($path);
        if ($watermark->getImageColorspace() != Imagick::COLORSPACE_SRGB) {
            $watermark->transformImageColorspace(Imagick::COLORSPACE_SRGB);
        }
        switch ($hpos) {
            case 'left':
                $x = 0;
                break;
            case 'right':
                $x = $this->getWidth() - $watermark->getImageWidth();
                break;
            default:
                $x = round(($this->getWidth() - $watermark->getImageWidth()) / 2);
                break;
        }
        switch ($vpos) {
            case 'top':
                $y = 0;
                break;
            case 'bottom':
                $y = $this->getHeight() - $watermark->getImageHeight();
                break;
            default:
                $y = round(($this->getHeight() - $watermark->getImageHeight()) / 2);
                break;
        }
        $this->assertLoaded();
        $this->imagick->compositeImage($watermark, Imagick::COMPOSITE_OVER, $x, $y);
    }

    public function applyTransformationDesaturate(array $options)
    {
        $default_options = array(
        'amount' => 0,
        );
        $options = array_merge($default_options, $options);
        $amount = (int) $options['amount'];
        $this->assertLoaded();
        $this->imagick->modulateImage(100, $amount, 100);
    }

    public function applyTransformationBlur(array $options)
    {
        $default_options = array(
            'radius' => 3,
            'sigma' => 3,
        );
        $options = array_merge($default_options, $options);
        $radius = (int) $options['radius'];
        $sigma = (int) $options['sigma'];
        $this->assertLoaded();
        $this->imagick->blurImage($radius, $sigma);
    }

    public function applyTransformations()
    {
        foreach ($this->transformations as $transformation) {
            switch ($transformation['name']) {
                case 'fit_to_width':
                    $this->applyTransformationFitToWidth($transformation['options']);
                    break;
                case 'fit_to_height':
                    $this->applyTransformationFitToHeight($transformation['options']);
                    break;
                case 'fit_to_min_dimensions':
                    $this->applyTransformationFitToMinDimensions($transformation['options']);
                    break;
                case 'fit_to_max_dimensions':
                    $this->applyTransformationFitToMaxDimensions($transformation['options']);
                    break;
                case 'crop_center':
                    $this->applyTransformationCropCenter($transformation['options']);
                    break;
                case 'resize_around_pivot_point':
                    $this->applyTransformationResizeAroundPivotPoint($transformation['options']);
                    break;
                case 'watermark':
                    $this->applyTransformationWatermark($transformation['options']);
                    break;
                case 'desaturate':
                    $this->applyTransformationDesaturate($transformation['options']);
                    break;
                case 'blur':
                    $this->applyTransformationBlur($transformation['options']);
                    break;
                default:
                    throw new Exception('Unknown transformation.');
            }
        }
        return $this;
    }

    public function getProcessedImageContent()
    {
        $this->assertLoaded();
        $available_output_formats = $this->getAvailableOutputFormats();
        $imagick_format = $available_output_formats[$this->outputFormatMimeType]['imagick_format'];
        $this->imagick->setImageFormat($imagick_format);
        ob_start();
        switch($this->outputFormatMimeType) {
            case 'image/png':
                break;
            case 'image/jpeg':
                $this->imagick->setImageCompressionQuality($this->outputFormatOptions['jpeg_quality']);
                if ($this->outputFormatOptions['jpeg_interlace']) {
                    $this->imagick->setInterlaceScheme(Imagick::INTERLACE_JPEG);
                } else {
                    $this->imagick->setInterlaceScheme(Imagick::INTERLACE_NO);
                }
                break;
            case 'image/webp':
                $this->imagick->setImageCompressionQuality($this->outputFormatOptions['webp_quality']);
                if ($this->outputFormatOptions['webp_lossless']) {
                    $this->imagick->setOption('webp:lossless', 'true');
                } else {
                    $this->imagick->setOption('webp:lossless', 'false');
                }
                break;
        }
        echo $this->imagick;
        $buffer = ob_get_contents();
        ob_clean();
        return $buffer;
    }

}

<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Image;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\EventListener\AbstractSessionListener;
use Viktor\Application;
use Viktor\Pack\Base\Field\ImageField;
use Viktor\Pack\Base\I18n\TranslatableInterface;
use Viktor\Pack\Base\Image\SourceImage;
use Viktor\Pack\Base\Image\ImageGenerator;

class ImageAppController
{
    public function imageFromContentUnit(Request $request, Application $app, $language, $format, $type, $id)
    {
        $image_content_unit = $app['content_unit_factory']->load($type, $id);
        if ($image_content_unit == null) {
            $app->abort(404, 'This image could not be loaded.');
        }
        if ($image_content_unit instanceof TranslatableInterface) {
            $image_content_unit->setLanguage($language);
        }
        if (!$image_content_unit->canBeViewed()) {
            $app->abort(404, 'This image cannot be displayed.');
        }
        $source_image = $image_content_unit->getImage();
        $format_description = $app['image_generator.formats']->getFormatDescription($format);
        if (!is_array($format_description)) {
            $app->abort(404, 'Unknown format.');
        }
        $transformations = array();
        $mime_type = 'image/png';
        $output_format_options = array();
        $max_age = 0;
        $shared_max_age = 0;
        if (isset($format_description['transformations'])) {
            $transformations = $format_description['transformations'];
        }
        if (isset($format_description['output_format']) && is_array($format_description['output_format'])) {
            $output_format = $format_description['output_format'];
            if (isset($output_format['mime_type'])) {
                $mime_type = $output_format['mime_type'];
            }
            if (isset($output_format['options'])) {
                $output_format_options = $output_format['options'];
            }
        }
        if (isset($format_description['max_age'])) {
            $max_age = (int) $format_description['max_age'];
        }
        if (isset($format_description['shared_max_age'])) {
            $shared_max_age = (int) $format_description['shared_max_age'];
        }
        if ($source_image != null) {
            $response = new Response();
            $response->headers->set(AbstractSessionListener::NO_AUTO_CACHE_CONTROL_HEADER, 'true');
            $response->setETag(md5(''
                . $source_image->getEtag()
                . serialize($format_description)
            ));
            $response->setPublic();
            if ($response->isNotModified($request)) {
                return $response;
            }
            if ($max_age > 0) {
                $response->setMaxAge($max_age);
            }
            if ($shared_max_age > 0) {
                $response->setSharedMaxAge($shared_max_age);
            }
            $image_generator = new ImageGenerator($app, $source_image);
            foreach ($transformations as $transformation) {
                $image_generator->addTransformation($transformation);
            }
            $image_generator->setOutputFormat($mime_type, $output_format_options);
            return $image_generator->getResponse($response);
        } else {
            $app->abort(404, 'This content unit has no image.');
        }
    }

    public function download(Request $request, Application $app, $language, $type, $id)
    {
        $image_content_unit = $app['content_unit_factory']->load($type, $id);
        if ($image_content_unit == null) {
            $app->abort(404, 'This image could not be loaded.');
        }
        if (!$image_content_unit instanceof HasAnUploadedImageInterface) {
            $app->abort(404, 'This content unit has no image.');
        }
        if ($image_content_unit instanceof TranslatableInterface) {
            $image_content_unit->setLanguage($language);
        }
        if (!$image_content_unit->canBeViewed()) {
            $app->abort(404, 'This image cannot be downloaded.');
        }
        $source_image = $image_content_unit->getImage();
        if ($source_image == null) {
            $app->abort(404, 'This content unit has no image.');
        }
        $file_path = $source_image->getPath();
        if (!file_exists($file_path)) {
            $app->abort(404, 'The attached file is missing.');
        }
        $response = new BinaryFileResponse($file_path);
        $fallback_name = basename($file_path);
        $info = pathinfo($fallback_name);
        $fallback_name = basename($fallback_name, '.' . $info['extension']);
        $fallback_name = @iconv('UTF-8', 'ASCII//IGNORE', $fallback_name);
        $fallback_name = preg_replace('#[^a-zA-Z0-9_\-\ ]#', '', $fallback_name);
        if (trim($fallback_name) == '') {
            $fallback_name = 'image';
        }
        $fallback_extension = (string) $info['extension'];
        if (trim($fallback_extension) == '') {
            $fallback_extension = 'bin';
        }
        $fallback_name .= '.' . $fallback_extension;
        $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $fallback_name);
        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Content-Type', mime_content_type($file_path));
        return $response;
    }

}

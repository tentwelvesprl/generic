<?php

/*
 * Copyright 2019 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Image;

use Exception;
use Jcupitt\Vips\Image;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class ImageProcessorVips implements ImageProcessorInterface
{
    private $sourceImage;
    private $outputFormatMimeType;
    private $outputFormatOptions;
    private $transformations;
    private $vips;
    private $width;
    private $height;

    public function __construct(SourceImage $source_image)
    {
        $this->sourceImage = $source_image;
        $this->outputFormatMimeType = 'image/png';
        $this->outputFormatOptions = array();
        $this->transformations = array();
        $this->vips = null;
        $this->width = 0;
        $this->height = 0;
    }

    public function __destruct()
    {
    }

    public function getSourceImage()
    {
        return $this->sourceImage;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function load()
    {
        $complete_file_path = $this->getSourceImage()->getPath();
        if ($complete_file_path == '') {
            throw new Exception('The source image is not specified.');
        }
        if (preg_match('/\.pdf$/', $complete_file_path)) {
            $this->vips = Image::newFromFile($complete_file_path, array('dpi' => 300));
        } else {
            $this->vips = Image::newFromFile($complete_file_path);
        }
        $this->width = (int) $this->vips->get('width');
        $this->height = (int) $this->vips->get('height');
        return $this;
    }

    public function getAvailableInputFormats()
    {
        return array(
            'image/png',
            'image/jpeg',
            'image/gif',
            'image/tiff',
            'image/bmp',
            'image/webp',
            'application/pdf',
        );
    }

    public function getAvailableOutputFormats()
    {
        return array(
            'image/png' => array(
                'extension' => 'png',
                'default_options' => array(),
            ),
            'image/jpeg' => array(
                'extension' => 'jpeg',
                'default_options' => array(
                    'jpeg_quality' => 85,
                    'jpeg_interlace' => true,
                ),
            ),
            'image/webp' => array(
                'extension' => 'webp',
                'default_options' => array(
                    'webp_quality' => 85,
                    'webp_lossless' => false,
                ),
            ),
        );
    }

    public function getOutputFormat()
    {
        $available_output_formats = $this->getAvailableOutputFormats();
        return $available_output_formats[$this->outputFormatMimeType];
    }

    public function getOutputFormatMimeType()
    {
        return $this->outputFormatMimeType;
    }

    public function setOutputFormat($mime_type, array $options = array())
    {
        $mime_type = (string) $mime_type;
        $options = (array) $options;
        $available_output_formats = $this->getAvailableOutputFormats();
        if (isset($available_output_formats[$mime_type])) {
            $this->outputFormatMimeType = $mime_type;
            $this->outputFormatOptions = array_merge($available_output_formats[$mime_type]['default_options'], $options);
        }
        return $this;
    }

    public function addTransformation(array $transformation)
    {
        $this->transformations[] = $transformation;
        return $this;
    }

    public function addTransformations(array $transformations)
    {
        foreach ($transformations as $transformation) {
            $this->addTransformation($transformation);
        }
        return $this;
    }

    public function applyTransformationFitToWidth(array $options)
    {
        $default_options = array(
            'width' => 200,
        );
        $options = array_merge($default_options, $options);
        $new_width = (int) $options['width'];
        $factor = $new_width / $this->width;
        $this->vips = $this->vips->resize($factor);
        $this->width = (int) $this->vips->get('width');
        $this->height = (int) $this->vips->get('height');
    }

    public function applyTransformationFitToHeight(array $options)
    {
        $default_options = array(
            'height' => 200,
        );
        $options = array_merge($default_options, $options);
        $new_height = (int) $options['height'];
        $factor = $new_height / $this->height;
        $this->vips = $this->vips->resize($factor);
        $this->width = (int) $this->vips->get('width');
        $this->height = (int) $this->vips->get('height');
    }

    public function applyTransformationFitToMinDimensions(array $options)
    {
        $default_options = array(
            'width' => 200,
            'height' => 200,
        );
        $options = array_merge($default_options, $options);
        $min_width = (int) $options['width'];
        $min_height = (int) $options['height'];
        $new_width = round($min_height * $this->width / $this->height);
        $new_height = round($min_width * $this->height / $this->width);
        if ($new_height < $min_height) {
            $new_height = $min_height;
        } else {
            $new_width = $min_width;
        }
        $factor = $new_width / $this->width;
        $this->vips = $this->vips->resize($factor);
        $this->width = (int) $this->vips->get('width');
        $this->height = (int) $this->vips->get('height');
    }

    public function applyTransformationFitToMaxDimensions(array $options)
    {
        $default_options = array(
            'width' => 200,
            'height' => 200,
        );
        $options = array_merge($default_options, $options);
        $max_width = (int) $options['width'];
        $max_height = (int) $options['height'];
        $new_width = round($max_height * $this->width / $this->height);
        $new_height = round($max_width * $this->height / $this->width);
        if ($new_height > $max_height) {
            $new_height = $max_height;
        } else {
            $new_width = $max_width;
        }
        $factor = $new_width / $this->width;
        $this->vips = $this->vips->resize($factor);
        $this->width = (int) $this->vips->get('width');
        $this->height = (int) $this->vips->get('height');
    }

    public function applyTransformationCropCenter(array $options)
    {
        $default_options = array(
            'width' => 200,
            'height' => 200,
        );
        $options = array_merge($default_options, $options);
        $new_width = (int) $options['width'];
        $new_height = (int) $options['height'];
        if ($new_width <= $this->width) {
            $x = round(($this->width - $new_width) / 2);
        } else {
            $x = 0;
            $new_width = $this->width;
        }
        if ($new_height <= $this->height) {
            $y = round(($this->height - $new_height) / 2);
        } else {
            $y = 0;
            $new_height = $this->height;
        }
        $this->vips = $this->vips->crop($x, $y, $new_width, $new_height);
        $this->width = (int) $this->vips->get('width');
        $this->height = (int) $this->vips->get('height');
    }

    public function applyTransformationResizeAroundPivotPoint(array $options)
    {
        $default_options = array(
            'width' => 200,
            'height' => 200,
        );
        $options = array_merge($default_options, $options);
        $new_width = (int) $options['width'];
        $new_height = (int) $options['height'];
        $pivot_x_pc = (float) $this->getSourceImage()->getPivotX();
        $pivot_y_pc = (float) $this->getSourceImage()->getPivotY();
        // Get pivot point coordinates.
        $pivot_x = (int) floor($this->width * $pivot_x_pc / 100.0);
        $pivot_y = (int) floor($this->height * $pivot_y_pc / 100.0);
        // Take a slice of the original image, centered around the pivot.
        $new_ratio = $new_width / $new_height;
        $old_ratio = $this->width / $this->height;
        if ($old_ratio > $new_ratio) {
            // We take a vertical slice of the original image.
            $slice_width = floor($this->height * $new_ratio);
            $slice_height = $this->height;
        } else {
            // We take an horizontal slice of the original image.
            $slice_width = $this->width;
            $slice_height = floor($this->width / $new_ratio);
        }
        $slice_x = floor($pivot_x - ($slice_width / 2));
        $slice_y = floor($pivot_y - ($slice_height / 2));
        if ($slice_x < 0) {
            $slice_x = 0;
        }
        if ($slice_y < 0) {
            $slice_y = 0;
        }
        if ($slice_x + $slice_width > $this->width) {
            $slice_x = $this->width - $slice_width;
        }
        if ($slice_y + $slice_height > $this->height) {
            $slice_y = $this->height - $slice_height;
        }
        $this->vips = $this->vips->cropImage($slice_x, $slice_y, $slice_width, $slice_height);
        $factor = $new_width / $this->width;
        $this->vips = $this->vips->resize($factor);
        $this->width = (int) $this->vips->get('width');
        $this->height = (int) $this->vips->get('height');
    }

    public function applyTransformationWatermark(array $options)
    {
        // TODO.
        // Not supported yet.
    }

    public function applyTransformationDesaturate(array $options)
    {
        // TODO.
        // Not supported yet.
    }

    public function applyTransformationBlur(array $options)
    {
        $default_options = array(
            'radius' => 3,
            'sigma' => 3,
        );
        $options = array_merge($default_options, $options);
        $radius = (int) $options['radius'];
        $sigma = (int) $options['sigma'];
        $this->vips = $this->vips->gaussblur($radius);
    }

    public function applyTransformations()
    {
        foreach ($this->transformations as $transformation) {
            switch ($transformation['name']) {
                case 'fit_to_width':
                    $this->applyTransformationFitToWidth($transformation['options']);
                    break;
                case 'fit_to_height':
                    $this->applyTransformationFitToHeight($transformation['options']);
                    break;
                case 'fit_to_min_dimensions':
                    $this->applyTransformationFitToMinDimensions($transformation['options']);
                    break;
                case 'fit_to_max_dimensions':
                    $this->applyTransformationFitToMaxDimensions($transformation['options']);
                    break;
                case 'crop_center':
                    $this->applyTransformationCropCenter($transformation['options']);
                    break;
                case 'resize_around_pivot_point':
                    $this->applyTransformationResizeAroundPivotPoint($transformation['options']);
                    break;
                case 'watermark':
                    $this->applyTransformationWatermark($transformation['options']);
                    break;
                case 'desaturate':
                    $this->applyTransformationDesaturate($transformation['options']);
                    break;
                case 'blur':
                    $this->applyTransformationBlur($transformation['options']);
                    break;
                default:
                    throw new Exception('Unknown transformation.');
            }
        }
        return $this;
    }

    public function getProcessedImageContent()
    {
        $available_output_formats = $this->getAvailableOutputFormats();
        switch($this->outputFormatMimeType) {
            case 'image/png':
                $buffer = $this->vips->writeToBuffer('.png');
                break;
            case 'image/jpeg':
                $buffer = $this->vips->writeToBuffer('.jpg', array(
                    'Q' => $this->outputFormatOptions['jpeg_quality'],
                    'interlace' => $this->outputFormatOptions['jpeg_interlace'],
                ));
                break;
            case 'image/webp':
                $buffer = $this->vips->writeToBuffer('.webp', array(
                    'Q' => $this->outputFormatOptions['webp_quality'],
                    'lossless' => $this->outputFormatOptions['webp_lossless'],
                ));
                break;
        }
        return $buffer;
    }

}

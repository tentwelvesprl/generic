<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Pagination;

use Symfony\Component\HttpFoundation\Request;
use Viktor\Application;

class Pagination
{
    private $request;
    private $app;
    private $nbItems;
    private $nbItemsPerPage;
    private $currentPage;
    private $possibleNbsItemsPerPage;
    private $prefix;

    public function __construct(Request $request, Application $app, $nb_items, $nb_items_per_page, $current_page, $possible_nbs_items_per_page, $prefix)
    {
        $this->request = $request;
        $this->app = $app;
        $this->nbItems = (int) $nb_items;
        if ($this->nbItems < 0) {
            $this->nbItems = 0;
        }
        $this->setPossibleNbsItemsPerPage($possible_nbs_items_per_page);
        $this->setNbItemsPerPage((int) $nb_items_per_page);
        $this->setCurrentPage((int) $current_page);
        $this->prefix = (string) $prefix;
    }

    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    public function setCurrentPage($nb)
    {
        if ($nb > 0) {
            $this->currentPage = (int) $nb;
        } else {
            $this->currentPage = 1;
        }
    }

    public function getPossibleNbsItemsPerPage()
    {
        return $this->possibleNbsItemsPerPage;
    }

    public function setPossibleNbsItemsPerPage(array $possible_nbs_items_per_page)
    {
        if (is_array($possible_nbs_items_per_page)) {
            foreach ($possible_nbs_items_per_page as $possible_nb_items_per_page) {
                if (((int) $possible_nb_items_per_page) > 0) {
                    $this->possibleNbsItemsPerPage[] = (int) $possible_nb_items_per_page;
                }
            }
        }
        if (count($this->possibleNbsItemsPerPage) == 0) {
            $this->possibleNbsItemsPerPage = array(10);
        }
    }

    public function getNbItemsPerPage()
    {
        return $this->nbItemsPerPage;
    }

    public function setNbItemsPerPage($nb)
    {
        if ($nb > 0 && in_array($nb, $this->possibleNbsItemsPerPage)) {
            $this->nbItemsPerPage = (int) $nb;
        } else {
            $this->nbItemsPerPage = $this->possibleNbsItemsPerPage[0];
        }
    }

    public function generateUrl($page, $nb_items_per_page)
    {
        $new_url = $this->request->getBaseUrl() . $this->request->getPathInfo() . '?';
        $new_query_parameters = $this->request->query->all();
        $new_query_parameters[$this->prefix . '_current_page'] = $page;
        $new_query_parameters[$this->prefix . '_nb_items_per_page'] = $nb_items_per_page;
        $new_url .=  http_build_query($new_query_parameters);
        return $new_url;
    }

    public function getPaginationItems()
    {
        $items = array();
        $nb_pages = ceil($this->nbItems / $this->nbItemsPerPage);
        $first_item_position = min((($this->currentPage - 1) * $this->nbItemsPerPage) + 1, $this->nbItems);
        $last_item_position = min($this->currentPage * $this->nbItemsPerPage, $this->nbItems);
        $items['previous'] = null;
        $items['next'] = null;
        $items['pages'] = array();
        $items['possible_nbs_items_per_page'] = array();
        $items['first_item_position'] = $first_item_position;
        $items['last_item_position'] = $last_item_position;
        $items['nb_items'] = $this->nbItems;
        // We don't want to show a huge number of pages, so we determine
        // which pages numbers to display.
        $pages_to_display = array();
        for ($i = 1; $i <= 3; $i++) {
            if ($i <= $nb_pages) {
                $pages_to_display[] = $i;
            }
        }
        for ($i = ($this->currentPage - 1); $i <= ($this->currentPage + 1); $i++) {
            if ($i > 0 && $i <= $nb_pages && !(in_array($i, $pages_to_display))) {
                $pages_to_display[] = $i;
            }
        }
        for ($i = ($nb_pages - 2); $i <= $nb_pages; $i++) {
            if ($i > 0 && !(in_array($i, $pages_to_display))) {
                $pages_to_display[] = $i;
            }
        }
        asort($pages_to_display);
        // Now, populate!
        if ($this->currentPage > 1) {
            $url_previous = $this->generateUrl($this->currentPage - 1, $this->nbItemsPerPage);
        } else {
            $url_previous = '';
        }
        $items['previous'] = array(
            'url' => $url_previous,
        );
        $previous_i = 0;
        $pages = array();
        foreach ($pages_to_display as $i) {
            // Ellipsis...
            if ($i != ($previous_i + 1)) {
                $pages[] = array(
                    'url'    => '',
                    'label'  => '...',
                    'active' => false,
                );
            }
            // Regular page_number.
            $pages[] = array(
                'url'    => $this->generateUrl($i, $this->nbItemsPerPage),
                'label'  => $i,
                'active' => ($i == $this->currentPage),
            );
            $previous_i = $i;
        }
        $items['pages'] = $pages;
        if ($this->currentPage < $nb_pages) {
            $url_next = $this->generateUrl($this->currentPage + 1, $this->nbItemsPerPage);
        } else {
            $url_next = '';
        }
        $items['next'] = array(
            'url' => $url_next,
        );
        // Number of items per page selection.
        $first_item_offset = ($this->currentPage - 1) * $this->nbItemsPerPage;
        $possible_nbs_items_per_page_details = array();
        foreach ($this->possibleNbsItemsPerPage as $possible_nb_items_per_page) {
            $first_item_new_page = floor($first_item_offset / $possible_nb_items_per_page) + 1;
            $new_url = $this->generateUrl($first_item_new_page, $possible_nb_items_per_page);
            $possible_nbs_items_per_page_details[] = array(
                'url'    => $new_url,
                'label'  => $possible_nb_items_per_page,
                'active' => ($possible_nb_items_per_page == $this->nbItemsPerPage),
            );
        }
        $items['possible_nbs_items_per_page'] = $possible_nbs_items_per_page_details;
        $items['current_page'] = $this->currentPage;
        $items['nb_pages'] = $nb_pages;
        return $items;
    }

}

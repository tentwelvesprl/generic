<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\DbStore;

use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Viktor\Pack\Base\Console\ViktorCommand;
use Viktor\Pack\Base\Console\ViktorStyle;
use Viktor\Pack\Base\DbStore\HasADbStoreComponentInterface;
use Viktor\Pack\Base\User\SecretaryUser;

class DbStoreCommand extends ViktorCommand
{
    protected function configure()
    {
        $this
            ->setName('viktor:dbstore')
            ->setDescription('Performs various tasks on content units in the db store')
            ->setHelp('This command can remove orphaned content units.')
            ->setDefinition(
                new InputDefinition(array(
                    new InputOption('delete-orphans', 'd', InputOption::VALUE_NONE, 'Delete orphaned content units'),
                    new InputOption('type', 't', InputOption::VALUE_REQUIRED, 'Restrict actions to this content unit type'),
                ))
            );
        return;
    }

    protected function executeWithStyle(ViktorStyle $io, InputInterface $input, OutputInterface $output)
    {
        $app = $this->getSilexApplication();
        $user = new SecretaryUser($app);
        $type = $input->getOption('type');
        if ($input->getOption('delete-orphans')) {
            foreach ($app['content_unit_factory']->getAvailableTypes() as $content_unit_type => $content_unit_class_name) {
                if ($type != $content_unit_type) {
                    continue;
                }
                $content_unit_model = $app['content_unit_factory']->create($content_unit_type);
                if (!($content_unit_model instanceof HasADbStoreComponentInterface)) {
                    continue;
                }
                $io->section($content_unit_model->getPluralHumanType());
                $ids = $content_unit_model->getIds();
                foreach ($ids as $id) {
                    $content_unit = $app['content_unit_factory']->create($content_unit_type);
                    $content_unit->load($id);
                    if ($content_unit->getId() == 0) {
                        $io->text(''
                            . '<error>#' . $id . '</error> '
                            . 'This unit could not be loaded'
                        );
                    } else {
                        $referencing_content_units_array = $content_unit->getDbStoreComponent()->getReferencingContentUnits();
                        if (count($referencing_content_units_array) == 0) {
                            $io->text(''
                                . 'Deleting '
                                . '<info>#' . $id . '</info> '
                                . $content_unit->getTitle()
                            );
                            $content_unit->delete($user);
                        }
                    }
                    unset($content_unit);
                }
                $output->writeln('');
            }
        }
        return;
    }

}

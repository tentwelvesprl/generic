<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\DbStore;

use Viktor\Pack\Base\User\UserInterface;

abstract class AbstractDbStoreDecorator implements DbStoreComponentInterface
{
    protected $component;

    public function __construct(DbStoreComponentInterface $component)
    {
        $this->component = $component;
        return;
    }

    public function getApp()
    {
        return $this->component->getApp();
    }

    public function getComponent()
    {
        return $this->component;
    }

    public function getRowQueryBuilder($id)
    {
        $query_builder = $this->component->getRowQueryBuilder($id);
        return $query_builder;
    }

    public function setFromRow(array $row)
    {
        $this->component->setFromRow($row);
        return;
    }

    public function load($id)
    {
        $id = (int) $id;
        $result = $this->getRowQueryBuilder($id)->execute();
        if ($row = $result->fetch()) {
            $this->setFromRow($row);
            return true;
        }
        return false;
    }

    public function save(UserInterface $user)
    {
        return $this->component->save($user);
    }

    public function delete()
    {
        return $this->component->delete();
    }

    public function getType()
    {
        return $this->component->getType();
    }

    public function getId()
    {
        return $this->component->getId();
    }

    public function getDigest()
    {
        return $this->component->getDigest();
    }

    public function getIdsQueryBuilder(array $filters = array(), array $sort_options = array())
    {
        return $this->component->getIdsQueryBuilder($filters, $sort_options);
    }

    public function getIds(array $filters = array(), array $sort_options = array())
    {
        $deduplicate = false;
        foreach ($filters as $filter) {
            if ($filter['type'] == 'deduplicate') {
                if ($filter['value'] == true) {
                    $deduplicate = true;
                }
            }
        }
        $query_builder = $this->getIdsQueryBuilder($filters, $sort_options);
        $query_builder->addOrderBy('component.id', 'DESC');
        $result = $query_builder->execute();
        $ids = array();
        while ($row = $result->fetch()) {
            $ids[] = $row['component$id'];
        }
        if ($deduplicate) {
            $ids = array_unique($ids);
        }
        return $ids;
    }

    public function getCreationUserType()
    {
        return $this->component->getCreationUserType();
    }

    public function getCreationUserId()
    {
        return $this->component->getCreationUserId();
    }

    public function getCreationDateTime()
    {
        return $this->component->getCreationDateTime();
    }

    public function getModificationUserType()
    {
        return $this->component->getModificationUserType();
    }

    public function getModificationUserId()
    {
        return $this->component->getModificationUserId();
    }

    public function getModificationDateTime()
    {
        return $this->component->getModificationDateTime();
    }

    public function getReferencingContentUnits()
    {
        return $this->component->getReferencingContentUnits();
    }

}

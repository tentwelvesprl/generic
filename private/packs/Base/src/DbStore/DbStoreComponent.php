<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\DbStore;

use DateTime;
use Viktor\Application;
use Viktor\Pack\Base\User\UserInterface;

class DbStoreComponent implements DbStoreComponentInterface
{
    private $app;
    private $tableName;
    private $type;
    private $id;
    private $creationUserType;
    private $creationUserId;
    private $creationDateTime;
    private $modificationUserType;
    private $modificationUserId;
    private $modificationDateTime;

    public function __construct(Application $app, $table_name, $type)
    {
        $this->app = $app;
        $this->tableName = (string) $table_name;
        $this->type = (string) $type;
        $this->id = 0;
        $this->creationUserType = 'unknown-user';
        $this->creationUserId = 0;
        $this->creationDateTime = new DateTime();
        $this->modificationUserType = 'unknown-user';
        $this->modificationUserId = 0;
        $this->modificationDateTime = new DateTime();
        return;
    }

    public function getApp()
    {
        return $this->app;
    }

    public function getRowQueryBuilder($id)
    {
        $id = (int) $id;
        $query_builder = $this->app['db']->createQueryBuilder();
        $query_builder = $query_builder
            ->select(
                'component.type AS component$type',
                'component.id AS component$id',
                'component.creation_user_type AS component$creation_user_type',
                'component.creation_user_id AS component$creation_user_id',
                'component.creation_uts AS component$creation_uts',
                'component.modification_user_type AS component$modification_user_type',
                'component.modification_user_id AS component$modification_user_id',
                'component.modification_uts AS component$modification_uts'
            )
            ->from($this->tableName, 'component')
            ->where('component.type = ' .  $query_builder->createNamedParameter($this->getType()))
            ->andWhere('component.id = ' .  $query_builder->createNamedParameter($id))
        ;
        return $query_builder;
    }

    public function setFromRow(array $row)
    {
        static $i = 1;
        $this->setId($row['component$id']);
        $this->creationUserType = $row['component$creation_user_type'];
        $this->creationUserId = $row['component$creation_user_id'];
        $this->creationDateTime->setTimestamp($row['component$creation_uts']);
        $this->modificationUserType = $row['component$modification_user_type'];
        $this->modificationUserId = $row['component$modification_user_id'];
        $this->modificationDateTime->setTimestamp($row['component$modification_uts']);
        return;
    }

    public function load($id)
    {
        $id = (int) $id;
        $result = $this->getRowQueryBuilder($id)->execute();
        if ($row = $result->fetch()) {
            $this->setFromRow($row);
            return true;
        }
        return false;
    }

    public function save(UserInterface $user)
    {
        $uts = time();
        if ($this->getId() == 0) {
            // New element.
            $query_builder = $this->app['db']->createQueryBuilder();
            $query_builder
                ->insert($this->tableName)
                ->setValue('type', '?')
                ->setValue('creation_user_type', '?')
                ->setValue('creation_user_id', '?')
                ->setValue('creation_uts', '?')
                ->setValue('modification_user_type', '?')
                ->setValue('modification_user_id', '?')
                ->setValue('modification_uts', '?')
                ->setParameter(0, $this->getType())
                ->setParameter(1, $user->getType())
                ->setParameter(2, $user->getId())
                ->setParameter(3, $uts)
                ->setParameter(4, $user->getType())
                ->setParameter(5, $user->getId())
                ->setParameter(6, $uts);
            if ($query_builder->execute() == 1) {
                $new_id = $this->app['db']->lastInsertId();
                $this->setId($new_id);
                $this->getCreationDateTime()->setTimestamp($uts);
                $this->getModificationDateTime()->setTimestamp($uts);
                return true;
            } else {
                return false;
            }
        } else {
            // Already existing element.
            $query_builder = $this->app['db']->createQueryBuilder();
            $query_builder
                ->update($this->tableName)
                ->set('modification_user_type', '?')
                ->set('modification_user_id', '?')
                ->set('modification_uts', '?')
                ->where('type = ? AND id = ?')
                ->setParameter(0, $user->getType())
                ->setParameter(1, $user->getId())
                ->setParameter(2, $uts)
                ->setParameter(3, $this->getType())
                ->setParameter(4, $this->getId());
            if ($query_builder->execute() == 1) {
                $this->getModificationDateTime()->setTimestamp($uts);
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public function delete()
    {
        $query_builder = $this->app['db']->createQueryBuilder();
        $query_builder
            ->delete($this->tableName)
            ->where('type = :type AND id = :id')
            ->setParameter('type', $this->getType())
            ->setParameter('id', $this->getId());
        if ($query_builder->execute() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function getType()
    {
        return $this->type;
    }

    public function getId()
    {
        return $this->id;
    }

    private function setId($id)
    {
        $id = (int) $id;
        if ($id >= 0) {
            $this->id = $id;
        }
        return $this;
    }

    public function getDigest()
    {
        $digest = array(
            'creation_user_type'     => $this->getCreationUserType(),
            'creation_user_id'       => $this->getCreationUserId(),
            'creation_datetime'      => $this->getCreationDateTime(),
            'modification_user_type' => $this->getModificationUserType(),
            'modification_user_id'   => $this->getModificationUserId(),
            'modification_datetime'  => $this->getModificationDateTime(),
        );
        return $digest;
    }

    public function getIdsQueryBuilder(array $filters = array(), array $sort_options = array())
    {
        $query_builder = $this->app['db']->createQueryBuilder();
        $query_builder = $query_builder
            ->select('component.id AS component$id')
            ->from($this->tableName, 'component')
            ->where('component.type = :component_type')
            ->setParameter('component_type', $this->getType())
        ;
        foreach ($sort_options as $sort_option) {
            if (!is_array($sort_option)) {
                continue;
            }
            $option = (string) $sort_option['option'];
            $direction = (string) $sort_option['direction'];
            switch ($option) {
                case 'creation_datetime':
                    $query_builder->addOrderBy('component.creation_uts', $direction);
                    break;
                case 'modification_datetime':
                    $query_builder->addOrderBy('component.modification_uts', $direction);
                    break;
            }
        }
        return $query_builder;
    }

    public function getIds(array $filters = array(), array $sort_options = array())
    {
        $deduplicate = false;
        foreach ($filters as $filter) {
            if ($filter['type'] == 'deduplicate') {
                if ($filter['value'] == true) {
                    $deduplicate = true;
                }
            }
        }
        $query_builder = $this->getIdsQueryBuilder($filters, $sort_options);
        $query_builder->addOrderBy('component.id', 'DESC');
        $result = $query_builder->execute();
        $ids = array();
        while ($row = $result->fetch()) {
            $ids[] = $row['component$id'];
        }
        if ($deduplicate) {
            $ids = array_unique($ids);
        }
        return $ids;
    }

    public function getCreationUserType()
    {
        return $this->creationUserType;
    }

    public function getCreationUserId()
    {
        return $this->creationUserId;
    }

    public function getCreationDateTime()
    {
        return $this->creationDateTime;
    }

    public function getModificationUserType()
    {
        return $this->modificationUserType;
    }

    public function getModificationUserId()
    {
        return $this->modificationUserId;
    }

    public function getModificationDateTime()
    {
        return $this->modificationDateTime;
    }

    public function getReferencingContentUnits()
    {
        $query_builder = $this->getApp()['db']->createQueryBuilder();
        $query_builder
            ->select('type', 'id')
            ->from('content_unit_references_index')
            ->where('reference_type = :reference_type')
            ->andWhere('reference_id = :reference_id')
            ->setParameter('reference_type', $this->getType())
            ->setParameter('reference_id', $this->getId())
            ->orderBy('reference_type', 'ASC')
            ->orderBy('reference_id', 'ASC');
        $result = $query_builder->execute();
        $a = array();
        while ($row = $result->fetch()) {
            $a[$row['type'] . ':' . $row['id']] = array(
                'type' => $row['type'],
                'id' => $row['id'],
            );
        }
        return $a;
    }

}

<?php

/*
 * Copyright 2021 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Field;

use Doctrine\DBAL\Query\QueryBuilder;
use Symfony\Component\Yaml\Yaml;
use Viktor\Pack\Base\Dataset\Dataset;
use Viktor\Pack\Base\Field\AbstractField;

class TableField extends AbstractField
{
    private $value;

    public function __construct(Dataset $dataset, array $options, $field_name, array $metadata, array $full_text_index_weights)
    {
        parent::__construct($dataset, $options, $field_name, $metadata, $full_text_index_weights);
        $this->value = array();
        $columns_description = array();
        if (isset($options['columns']) && is_array($options['columns'])) {
            foreach ($options['columns'] as $k => $column_description) {
                $k = (string) $k;
                $columns_description[$k] = array();
                $columns_description[$k]['type'] = 'text';
                $columns_description[$k]['label'] = $k;
                $columns_description[$k]['width'] = '33';
                $columns_description[$k]['items'] = array();
                if (isset($column_description['type']) && in_array($column_description['type'], array('text', 'long_text', 'single_choice_list'))) {
                    $columns_description[$k]['type'] = $column_description['type'];
                }
                if (isset($column_description['label'])) {
                    $columns_description[$k]['label'] = (string) $column_description['label'];
                }
                if (isset($column_description['width']) && in_array((string) $column_description['width'], array('20', '25', '33', '40', '50', '60', '66', '80', '100'))) {
                    $columns_description[$k]['width'] = (string) $column_description['width'];
                }
                if (isset($column_description['items']) && is_array($column_description['items'])) {
                    foreach ($column_description['items'] as $item_value => $item_label) {
                        $columns_description[$k]['items'][(string) $item_value] = (string) $item_label;
                    }
                }
            }
        }
        $this->options['columns'] = $columns_description;
        return;
    }

    public function getDefaultOptions()
    {
        return array();
    }

    public function get()
    {
        return $this->value;
    }

    public function set($value = null)
    {
        $sanitized_value = array();
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $id = (string) $k;
                $sanitized_value[$k] = array();
                foreach ($this->options['columns'] as $column_name => $column_description) {
                    $sanitized_value[$k][$column_name] = (string) (isset($v[$column_name]) ? $v[$column_name] : '');
                }
            }
        }
        $this->value = $sanitized_value;
        return $this;
    }

    public function getDigest()
    {
        return $this->value;
    }

    public function updateIndex()
    {
        return true;
    }

    public function deleteFromIndex()
    {
        return true;
    }

    public function getPlainTextRepresentation()
    {
        $s = print_r($this->value, true);
        return $s;
    }

    public function getDecoratedIdsQueryBuilder(QueryBuilder $query_builder, array $filters, array $sort_options)
    {
        return $query_builder;
    }

}

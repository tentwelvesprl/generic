<?php

/*
 * Copyright 2023 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Field;

use Doctrine\DBAL\Query\QueryBuilder;
use Viktor\Pack\Base\Dataset\Dataset;
use Viktor\Pack\Base\Field\AbstractField;

class IntegerField extends AbstractField
{
    private $value;

    public function __construct(Dataset $dataset, array $options, $field_name, array $metadata, array $full_text_index_weights)
    {
        parent::__construct($dataset, $options, $field_name, $metadata, $full_text_index_weights);
        $this->value = (int) $this->options['default_value'];
        return;
    }

    public function getDefaultOptions()
    {
        return array(
            'indexed' => false,
            'default_value' => 0,
            'minimum_value' => 0,
            'maximum_value' => 100,
        );
    }

    public function get()
    {
        return $this->value;
    }

    public function set($value = null)
    {
        if ($value !== null) {
            $this->value = (int) $value;
            if ($this->value < (int) $this->options['minimum_value']) {
                $this->value = (int) $this->options['minimum_value'];
            }
            if ($this->value > (int) $this->options['maximum_value']) {
                $this->value = (int) $this->options['maximum_value'];
            }
        } else {
            $this->value = (int) $this->options['default_value'];
        }
        return $this;
    }

    public function getDigest()
    {
        return $this->value;
    }

    public function updateIndex()
    {
        $options = $this->getOptions();
        if ($options['indexed']) {
            $this->deleteFromIndex();
            $query_builder = $this->getApp()['db']->createQueryBuilder();
            $query_builder
                ->insert('integer_field_index')
                ->setValue('type', ':type')
                ->setValue('id', ':id')
                ->setValue('field_name', ':field_name')
                ->setValue('value', ':value')
                ->setParameter('type', $this->getContentUnit()->getType())
                ->setParameter('id', $this->getContentUnit()->getId())
                ->setParameter('field_name', $this->getName())
                ->setParameter('value', (int) $this->value);
            $query_builder->execute();
        }
        return true;
    }

    public function deleteFromIndex()
    {
        $options = $this->getOptions();
        if ($options['indexed']) {
            $query_builder = $this->getApp()['db']->createQueryBuilder();
            $query_builder
                ->delete('integer_field_index')
                ->where('type = :type AND id = :id AND field_name = :field_name')
                ->setParameter('type', $this->getContentUnit()->getType())
                ->setParameter('id', $this->getContentUnit()->getId())
                ->setParameter('field_name', $this->getName());
            $query_builder->execute();
        }
        return true;
    }

    public function getPlainTextRepresentation()
    {
        $s = (string) $this->value;
        return $s;
    }

    public function getDecoratedIdsQueryBuilder(QueryBuilder $query_builder, array $filters, array $sort_options)
    {
        static $counter = 0;
        foreach ($filters as $filter) {
            $counter++;
            if ($filter['type'] == 'integer_field' && $filter['field_name'] == $this->getName()) {
                $value = (int) $filter['value'];
                $field_name = trim((string) $filter['field_name']);
                $operator = trim((string) $filter['operator']);
                switch ($operator) {
                    case 'less_than':
                        $sql_operator = '<';
                        break;
                    case 'less_than_or_equal_to':
                        $sql_operator = '<=';
                        break;
                    case 'equal_to':
                        $sql_operator = '=';
                        break;
                    case 'greater_than_or_equal_to':
                        $sql_operator = '>=';
                        break;
                    case 'greater_than':
                        $sql_operator = '>';
                        break;
                    default:
                        $sql_operator = '=';
                }
                $field_alias = 'f' . md5('integer_field_index_' . $this->getName() . $counter);
                $value_alias = 'v' . md5('integer_field_index_' . $this->getName() . '_value' . $counter);
                $field_name_alias = 'n' . md5('integer_field_index_' . $this->getName() . '_field_name' . $counter);
                // Note that the aliases are prefixed with a letter, so that
                // Doctrine consider them as named parameters.
                $query_builder
                    ->innerJoin(
                        'component',
                        'integer_field_index', $field_alias,
                        'component.type = ' . $field_alias . '.type AND component.id = ' . $field_alias . '.id'
                    )
                    ->andWhere($field_alias . '.value ' . $operator . ' :' . $value_alias)
                    ->andWhere($field_alias . '.field_name = :' . $field_name_alias)
                    ->setParameter($value_alias, $value)
                    ->setParameter($field_name_alias, $field_name)
                ;
            }
        }
        foreach ($sort_options as $sort_option) {
            if (!is_array($sort_option)) {
                continue;
            }
            $option = (string) $sort_option['option'];
            $direction = (string) $sort_option['direction'];
            switch ($option) {
                case 'field_' . $this->getName():
                    $field_alias = 'f' . md5($option);
                    $query_builder
                        ->innerJoin(
                            'component',
                            'integer_field_index', $field_alias,
                            'component.type = ' . $field_alias . '.type AND component.id = ' . $field_alias . '.id'
                        )
                        ->andWhere($field_alias . '.field_name = \'' . $this->getName() . '\'')
                        ->addOrderBy($field_alias . '.value', $direction);
                    break;
            }
        }
        return $query_builder;
    }

}

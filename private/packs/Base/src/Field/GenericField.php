<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Field;

use Doctrine\DBAL\Query\QueryBuilder;
use Symfony\Component\Yaml\Yaml;
use Viktor\Pack\Base\Dataset\Dataset;
use Viktor\Pack\Base\Field\AbstractField;

class GenericField extends AbstractField
{
    private $value;

    public function __construct(Dataset $dataset, array $options, $field_name, array $metadata, array $full_text_index_weights)
    {
        parent::__construct($dataset, $options, $field_name, $metadata, $full_text_index_weights);
        $this->value = null;
        return;
    }

    public function getDefaultOptions()
    {
        return array();
    }

    public function get()
    {
        return $this->value;
    }

    public function set($value = null)
    {
        $this->value = $value;
        return $this;
    }

    public function getDigest()
    {
        return $this->value;
    }

    public function updateIndex()
    {
        return true;
    }

    public function deleteFromIndex()
    {
        return true;
    }

    public function getPlainTextRepresentation()
    {
        $s = Yaml::dump($this->value);
        return $s;
    }

    public function getDecoratedIdsQueryBuilder(QueryBuilder $query_builder, array $filters, array $sort_options)
    {
        return $query_builder;
    }

}

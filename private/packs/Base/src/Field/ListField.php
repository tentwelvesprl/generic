<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Field;

use Doctrine\DBAL\Query\QueryBuilder;
use Viktor\Pack\Base\Dataset\Dataset;
use Viktor\Pack\Base\Field\AbstractField;

class ListField extends AbstractField
{
    private $value;

    public function __construct(Dataset $dataset, array $options, $field_name, array $metadata, array $full_text_index_weights)
    {
        parent::__construct($dataset, $options, $field_name, $metadata, $full_text_index_weights);
        $this->set(null); // Set the value to default.
        return;
    }

    public function getDefaultOptions()
    {
        return array(
            'indexed' => false,
            'items' => array(
                'default' => array(
                    'label' => 'Default',
                    'selected' => 0,
                ),
            ),
            'multiple' => true,
        );
    }

    public function get()
    {
        return $this->value;
    }

    public function set($value = null)
    {
        $new_value = array();
        $options = $this->getOptions();
        $one_item_selected = false;
        $another_item_can_be_selected = true;
        foreach ($options['items'] as $k => $item) {
            $new_value[$k] = (((bool) $item['selected']) && $another_item_can_be_selected);
            if (is_array($value) && isset($value[$k])) {
                $new_value[$k] = (((bool) $value[$k]) && $another_item_can_be_selected);
            }
            if ($new_value[$k]) {
                $one_item_selected = true;
                if (!$options['multiple']) {
                    $another_item_can_be_selected = false;
                }
            }
        }
        $this->value = $new_value;
        return $this;
    }

    public function getDigest()
    {
        $items = array();
        foreach ($this->value as $k => $selected) {
            if ($selected) {
                $items[] = $k;
            }
        }
        return $items;
    }

    public function updateIndex()
    {
        $options = $this->getOptions();
        if ($options['indexed']) {
            $this->deleteFromIndex();
            foreach ($this->value as $item => $selected) {
                $query_builder = $this->getApp()['db']->createQueryBuilder();
                $query_builder
                    ->insert('list_field_index')
                    ->setValue('type', ':type')
                    ->setValue('id', ':id')
                    ->setValue('field_name', ':field_name')
                    ->setValue('item', ':item')
                    ->setValue('selected', ':selected')
                    ->setParameter('type', $this->getContentUnit()->getType())
                    ->setParameter('id', $this->getContentUnit()->getId())
                    ->setParameter('field_name', $this->getName())
                    ->setParameter('item', $item)
                    ->setParameter('selected', (int) $selected);
                $query_builder->execute();
            }
        }
        return true;
    }

    public function deleteFromIndex()
    {
        $options = $this->getOptions();
        if ($options['indexed']) {
            $query_builder = $this->getApp()['db']->createQueryBuilder();
            $query_builder
                ->delete('list_field_index')
                ->where('type = :type AND id = :id AND field_name = :field_name')
                ->setParameter('type', $this->getContentUnit()->getType())
                ->setParameter('id', $this->getContentUnit()->getId())
                ->setParameter('field_name', $this->getName());
            $query_builder->execute();
        }
        return true;
    }

    public function getPlainTextRepresentation()
    {
        $options = $this->getOptions();
        $labels_array = array();
        foreach ($this->value as $k => $selected) {
            if ($selected) {
                $labels_array[] = $options['items'][$k]['label'];
            }
        }
        $s = implode(', ', $labels_array);
        return $s;
    }

    public function getDecoratedIdsQueryBuilder(QueryBuilder $query_builder, array $filters, array $sort_options)
    {
        static $counter = 0;
        foreach ($filters as $filter) {
            $counter++;
            if ($filter['type'] == 'list_field' && $filter['field_name'] == $this->getName()) {
                $value = trim((string) $filter['value']);
                $field_name = trim((string) $filter['field_name']);
                if ($value != '') {
                    $field_alias = 'f' . md5('list_field_index_' . $this->getName() . $counter);
                    $value_alias = 'v' . md5('list_field_index_' . $this->getName() . '_value' . $counter);
                    $field_name_alias = 'n' . md5('list_field_index_' . $this->getName() . '_field_name' . $counter);
                    // Note that the aliases are prefixed with a letter, so that
                    // Doctrine consider them as named parameters.
                    $query_builder
                        ->innerJoin(
                            'component',
                            'list_field_index', $field_alias,
                            'component.type = ' . $field_alias . '.type AND component.id = ' . $field_alias . '.id'
                        )
                        ->andWhere($field_alias . '.item = :' . $value_alias)
                        ->andWhere($field_alias . '.selected = 1')
                        ->andWhere($field_alias . '.field_name = :' . $field_name_alias)
                        ->setParameter($value_alias, $value)
                        ->setParameter($field_name_alias, $field_name)
                    ;
                }
            }
            if ($filter['type'] == 'list_field_unselected' && $filter['field_name'] == $this->getName()) {
                $value = trim((string) $filter['value']);
                $field_name = trim((string) $filter['field_name']);
                if ($value != '') {
                    $field_alias = 'f' . md5('list_field_index_' . $this->getName() . $counter);
                    $value_alias = 'v' . md5('list_field_index_' . $this->getName() . '_value' . $counter);
                    $field_name_alias = 'n' . md5('list_field_index_' . $this->getName() . '_field_name' . $counter);
                    // Note that the aliases are prefixed with a letter, so that
                    // Doctrine consider them as named parameters.
                    $query_builder
                        ->innerJoin(
                            'component',
                            'list_field_index', $field_alias,
                            'component.type = ' . $field_alias . '.type AND component.id = ' . $field_alias . '.id'
                        )
                        ->andWhere($field_alias . '.item = :' . $value_alias)
                        ->andWhere($field_alias . '.selected = 0')
                        ->andWhere($field_alias . '.field_name = :' . $field_name_alias)
                        ->setParameter($value_alias, $value)
                        ->setParameter($field_name_alias, $field_name)
                    ;
                }
            }
        }
        foreach ($sort_options as $sort_option) {
            if (!is_array($sort_option)) {
                continue;
            }
            $counter++;
            $option = '';
            if (isset($sort_option['option'])) {
                $option = trim((string) $sort_option['option']);
            }
            $direction = '';
            if (isset($sort_option['direction'])) {
                $direction = trim((string) $sort_option['direction']);
            }
            $value = '';
            if (isset($sort_option['value'])) {
                $value = trim((string) $sort_option['value']);
            }
            switch ($option) {
                case 'field_' . $this->getName():
                    $field_name = $this->getName();
                    $field_alias = 'f' . md5('list_field_index_' . $this->getName() . $counter);
                    $value_alias = 'v' . md5('list_field_index_' . $this->getName() . '_value' . $counter);
                    $field_name_alias = 'n' . md5('list_field_index_' . $this->getName() . '_field_name' . $counter);
                    $query_builder
                        ->innerJoin(
                            'component',
                            'list_field_index', $field_alias,
                            'component.type = ' . $field_alias . '.type AND component.id = ' . $field_alias . '.id'
                        )
                        ->andWhere($field_alias . '.item = :' . $value_alias)
                        ->andWhere($field_alias . '.field_name = :' . $field_name_alias)
                        ->addOrderBy($field_alias . '.selected', $direction)
                        ->setParameter($value_alias, $value)
                        ->setParameter($field_name_alias, $field_name)
                    ;
                    break;
            }
        }
        return $query_builder;
    }

}

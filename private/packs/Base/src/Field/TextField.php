<?php

/*
 * Copyright 2022 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Field;

use Doctrine\DBAL\Query\QueryBuilder;
use Normalizer;
use Parsedown;
use Viktor\Pack\Base\Dataset\Dataset;
use Viktor\Pack\Base\Field\AbstractField;

class TextField extends AbstractField
{
    private $value;
    private $processedValue;

    public function __construct(Dataset $dataset, array $options, $field_name, array $metadata, array $full_text_index_weights)
    {
        parent::__construct($dataset, $options, $field_name, $metadata, $full_text_index_weights);
        $this->value = '';
        $this->processedValue = '';
        return;
    }

    public function getDefaultOptions()
    {
        return array(
            'indexed' => false,
            'authorized_tags' => '<p><em><strong><ol><ul><li><hr><br><sub><sup>',
            'source_type' => 'plain_text', // Possible values: plain_text, multiline_plain_text, markdown, multiline_markdown, html
        );
    }

    public function get()
    {
        return $this->value;
    }

    public function set($value = null)
    {
        // For compatibility with deprecated RichTextField
        if (is_array($value) && isset($value['source'])) {
            $value = (string) $value['source'];
        } else {
            $value = (string) $value;
        }
        $value = Normalizer::normalize((string) $value, Normalizer::FORM_C);
        $options = $this->getOptions();
        $processed_value = $value;
        if ($options['source_type'] == 'html') {
            // Nothing to do
        } elseif ($options['source_type'] == 'multiline_markdown') {
            $parsedown = new Parsedown();
            $parsedown
                ->setBreaksEnabled(true)
                ->setMarkupEscaped(true)
                ->setUrlsLinked(false);
            $processed_value = $parsedown->text($processed_value);
        } elseif ($options['source_type'] == 'markdown') {
            $parsedown = new Parsedown();
            $parsedown
                ->setBreaksEnabled(true)
                ->setMarkupEscaped(true)
                ->setUrlsLinked(false);
            $processed_value = $parsedown->line($processed_value);
        } elseif ($options['source_type'] == 'multiline_plain_text') {
            $processed_value = htmlspecialchars($processed_value);
            $processed_value = nl2br($processed_value);
        } else { // Default case is plain_text
            $processed_value = htmlspecialchars($processed_value);
        }
        $processed_value = strip_tags($processed_value, $options['authorized_tags']);
        $this->value = $value;
        $this->processedValue = $processed_value;
        return $this;
    }

    public function getDigest()
    {
        $options = $this->getOptions();
        if ($options['source_type'] == 'multiline_plain_text' || $options['source_type'] == 'plain_text') {
            return $this->value;
        }
        return $this->processedValue;
    }

    public function updateIndex()
    {
        $options = $this->getOptions();
        if ($options['indexed']) {
            $this->deleteFromIndex();
            $query_builder = $this->getApp()['db']->createQueryBuilder();
            $query_builder
                ->insert('text_field_index')
                ->setValue('type', ':type')
                ->setValue('id', ':id')
                ->setValue('field_name', ':field_name')
                ->setValue('value', ':value')
                ->setParameter('type', $this->getContentUnit()->getType())
                ->setParameter('id', $this->getContentUnit()->getId())
                ->setParameter('field_name', $this->getName())
                ->setParameter('value', mb_substr($this->getPlainTextRepresentation(), 0, 256));
            $query_builder->execute();
        }
        return true;
    }

    public function deleteFromIndex()
    {
        $options = $this->getOptions();
        if ($options['indexed']) {
            $query_builder = $this->getApp()['db']->createQueryBuilder();
            $query_builder
                ->delete('text_field_index')
                ->where('type = :type AND id = :id AND field_name = :field_name')
                ->setParameter('type', $this->getContentUnit()->getType())
                ->setParameter('id', $this->getContentUnit()->getId())
                ->setParameter('field_name', $this->getName());
            $query_builder->execute();
        }
        return true;
    }

    public function getPlainTextRepresentation()
    {
        $plain_text = preg_replace('#<br\s*/?>#i', "\n", $this->processedValue);
        $plain_text = html_entity_decode(strip_tags($plain_text), ENT_QUOTES | ENT_HTML5);
        $plain_text = trim($plain_text);
        return $plain_text;
    }

    public function getDecoratedIdsQueryBuilder(QueryBuilder $query_builder, array $filters, array $sort_options)
    {
        static $counter = 0;
        foreach ($filters as $filter) {
            $counter++;
            if ($filter['type'] == 'text_field' && $filter['field_name'] == $this->getName()) {
                $value = trim((string) $filter['value']);
                $field_name = trim((string) $filter['field_name']);
                if ($value != '') {
                    $field_alias = 'f' . md5('text_field_index_' . $this->getName() . $counter);
                    $value_alias = 'v' . md5('text_field_index_' . $this->getName() . '_value' . $counter);
                    $field_name_alias = 'n' . md5('text_field_index_' . $this->getName() . '_field_name' . $counter);
                    // Note that the aliases are prefixed with a letter, so that
                    // Doctrine consider them as named parameters.
                    $query_builder
                        ->innerJoin(
                            'component',
                            'text_field_index', $field_alias,
                            'component.type = ' . $field_alias . '.type AND component.id = ' . $field_alias . '.id'
                        )
                        ->andWhere($field_alias . '.value LIKE :' . $value_alias)
                        ->andWhere($field_alias . '.field_name = :' . $field_name_alias)
                        ->setParameter($value_alias, $value)
                        ->setParameter($field_name_alias, $field_name)
                    ;
                }
            }
        }
        foreach ($sort_options as $sort_option) {
            if (!is_array($sort_option)) {
                continue;
            }
            $option = (string) $sort_option['option'];
            $direction = (string) $sort_option['direction'];
            switch ($option) {
                case 'field_' . $this->getName():
                    $field_alias = 'f' . md5($option);
                    $query_builder
                        ->innerJoin(
                            'component',
                            'text_field_index', $field_alias,
                            'component.type = ' . $field_alias . '.type AND component.id = ' . $field_alias . '.id'
                        )
                        ->andWhere($field_alias . '.field_name = \'' . $this->getName() . '\'')
                        ->addOrderBy($field_alias . '.value', $direction);
                    break;
            }
        }
        return $query_builder;
    }

    public function getProcessedValue()
    {
        return $this->processedValue;
    }

}

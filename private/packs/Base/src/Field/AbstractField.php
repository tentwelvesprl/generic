<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Field;

use Doctrine\DBAL\Query\QueryBuilder;
use Viktor\Pack\Base\Dataset\Dataset;

abstract class AbstractField
{
    private $app;
    private $contentUnit;
    private $name;
    protected $options;
    protected $metadata;
    protected $fullTextIndexWeights;

    public function __construct(Dataset $dataset, array $options, $field_name, array $metadata, array $full_text_index_weights)
    {
        $this->app = $dataset->getApp();
        $this->contentUnit = $dataset->getContentUnit();
        $this->name = (string) $field_name;
        $this->options = $this->getDefaultOptions();
        $this->options = array_merge($this->options, $options);
        $this->metadata = array();
        foreach ($metadata as $k => $v) {
            $this->setMetadata($k, $v);
        }
        $this->fullTextIndexWeights = array();
        foreach ($full_text_index_weights as $index_id => $weight) {
            $index_id = (string) $index_id;
            $weight = (int) $weight;
            if ($weight > 0) {
                $this->fullTextIndexWeights[$index_id] = $weight;
            }
        }
        return;
    }

    public function getApp()
    {
        return $this->app;
    }

    public function getContentUnit()
    {
        return $this->contentUnit;
    }

    public function getDefaultOptions()
    {
        return array();
    }

    public function getName()
    {
        return $this->name;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function getMetadata($key)
    {
        $key = (string) $key;
        if (isset($this->metadata[$key])) {
            return $this->metadata[$key];
        } else {
            return null;
        }
    }

    public function getAllMetadata()
    {
        return $this->metadata;
    }

    public function getFullTextIndexWeights()
    {
        return $this->fullTextIndexWeights;
    }

    public function setMetadata($key, $value)
    {
        $key = (string) $key;
        $this->metadata[$key] = $value;
        return $this;
    }

    public function get()
    {
        return '';
    }

    public function set($value = null)
    {
        return $this;
    }

    public function getDigest()
    {
        return '';
    }

    public function updateIndex()
    {
        return true;
    }

    public function deleteFromIndex()
    {
        return true;
    }

    public function getPlainTextRepresentation()
    {
        $s = '';
        return $s;
    }

    public function getImage()
    {
        return null;
    }

    public function getDecoratedIdsQueryBuilder(QueryBuilder $query_builder, array $filters, array $sort_options)
    {
        return $query_builder;
    }


}

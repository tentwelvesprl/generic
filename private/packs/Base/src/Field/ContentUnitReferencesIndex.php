<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Field;

use Doctrine\DBAL\Query\QueryBuilder;
use Viktor\Application;

class ContentUnitReferencesIndex
{
    private $app;
    private $type;
    private $id;
    private $fieldName;
    private $value;

    public function __construct(Application $app, $type, $id, $field_name)
    {
        $this->app = $app;
        $this->type = (string) $type;
        $this->id = (int) $id;
        $this->fieldName = (string) $field_name;
    }

    public function getApp()
    {
        return $this->app;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFieldName()
    {
        return $this->fieldName;
    }

    public function set(array $value)
    {
        $new_value = array();
        foreach ($value as $k => $v) {
            if (isset($v['type']) && isset($v['id'])) {
                $type = (string) $v['type'];
                $id = (int) $v['id'];
                $new_value[] = array(
                    'type' => $type,
                    'id' => $id,
                );
            }
        }
        $this->value = $value;
    }

    public function get()
    {
        return $this->value;
    }

    public function updateIndex()
    {
        $this->deleteFromIndex();
        $k = 0;
        $value = $this->get();
        foreach ($value as $content_unit_reference) {
            $query_builder = $this->getApp()['db']->createQueryBuilder();
            $query_builder
                ->insert('content_unit_references_index')
                ->setValue('type', ':type')
                ->setValue('id', ':id')
                ->setValue('field_name', ':field_name')
                ->setValue('k', ':k')
                ->setValue('reference_type', ':reference_type')
                ->setValue('reference_id', ':reference_id')
                ->setParameter('type', $this->getType())
                ->setParameter('id', $this->getId())
                ->setParameter('field_name', $this->getFieldName())
                ->setParameter('k', $k)
                ->setParameter('reference_type', $content_unit_reference['type'])
                ->setParameter('reference_id', $content_unit_reference['id']);
            $query_builder->execute();
            $k++;
        }
        return true;
    }

    public function deleteFromIndex()
    {
        $query_builder = $this->getApp()['db']->createQueryBuilder();
        $query_builder
            ->delete('content_unit_references_index')
            ->where('type = :type AND id = :id AND field_name = :field_name')
            ->setParameter('type', $this->getType())
            ->setParameter('id', $this->getId())
            ->setParameter('field_name', $this->getFieldName());
        $query_builder->execute();
        return true;
    }

    public function getDecoratedIdsQueryBuilder(QueryBuilder $query_builder, array $filters, array $sort_options)
    {
        static $counter = 0;
        foreach ($filters as $filter) {
            $counter++;
            if ($filter['type'] == 'content_unit_references_index' && $filter['field_name'] == $this->getFieldName()) {
                $reference_type = trim((string) $filter['reference_type']);
                $reference_id = 0;
                $reference_ids = array();
                if (is_array($filter['reference_id'])) {
                    foreach ($filter['reference_id'] as $rid) {
                        $reference_ids[] = (int) $rid;
                    }
                } else {
                    $reference_id = (int) $filter['reference_id'];
                }
                if ($reference_type != '') {
                    $field_alias = 'f' . md5('content_unit_references_index_' . $this->getFieldName() . $counter);
                    $type_alias = 't' . md5('content_unit_references_index_' . $this->getFieldName() . '_type' . $counter);
                    $id_alias = 'i' . md5('content_unit_references_index_' . $this->getFieldName() . '_id' . $counter);
                    $field_name_alias = 'n' . md5('simple_text_field_index_' . $this->getFieldName() . '_field_name' . $counter);
                    // Note that the aliases are prefixed with a letter, so that
                    // Doctrine consider them as named parameters.
                    $query_builder
                        ->innerJoin(
                            'component',
                            'content_unit_references_index', $field_alias,
                            'component.type = ' . $field_alias . '.type AND component.id = ' . $field_alias . '.id'
                        )
                        ->andWhere($field_alias . '.reference_type LIKE :' . $type_alias)
                        ->andWhere($field_alias . '.field_name = :' . $field_name_alias)
                        ->setParameter($type_alias, $reference_type)
                        ->setParameter($id_alias, $reference_id)
                        ->setParameter($field_name_alias, $this->getFieldName())
                    ;
                    if (count($reference_ids) > 0) {
                        $query_builder->andWhere($field_alias . '.reference_id IN (' . implode(', ', $reference_ids) . ')');
                    } else {
                        $query_builder->andWhere($field_alias . '.reference_id = ' . $reference_id);
                    }
                }
            }
        }
        return $query_builder;
    }

}

<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Field;

use Doctrine\DBAL\Query\QueryBuilder;
use Exception;
use Symfony\Component\Yaml\Yaml;
use Viktor\Pack\Base\Dataset\Dataset;
use Viktor\Pack\Base\Field\AbstractField;

class ImageField extends AbstractField
{
    private $value;

    public function __construct(Dataset $dataset, array $options, $field_name, array $metadata, array $full_text_index_weights)
    {
        parent::__construct($dataset, $options, $field_name, $metadata, $full_text_index_weights);
        $this->value = array();
        return;
    }

    public function getDefaultOptions()
    {
        $app = $this->getApp();
        $default_options = array(
            'image_type' => $app['viktor.config']['default_image_content_unit_type'],
            'max_number' => 999,
        );
        return $default_options;
    }

    public function get()
    {
        return $this->value;
    }

    public function set($value = null)
    {
        $options = $this->getOptions();
        $new_value = array();
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $type = (string) $v['type'];
                $id = (int) $v['id'];
                if ($type == $options['image_type'] && $id > 0)
                $new_value[] = array(
                    'type' => $type,
                    'id' => $id,
                );
            }
            $this->value = array_slice($new_value, 0, $options['max_number']);
        }
        return $this;
    }

    public function getDigest()
    {
        return $this->value;
    }

    public function updateIndex()
    {
        $content_unit_references_index = new ContentUnitReferencesIndex(
            $this->getApp(),
            $this->getContentUnit()->getType(),
            $this->getContentUnit()->getId(),
            $this->getName()
        );
        $content_unit_references_index->set($this->get());
        $result = $content_unit_references_index->updateIndex();
        return $result;
    }

    public function deleteFromIndex()
    {
        $content_unit_references_index = new ContentUnitReferencesIndex(
            $this->getApp(),
            $this->getContentUnit()->getType(),
            $this->getContentUnit()->getId(),
            $this->getName()
        );
        $content_unit_references_index->set($this->get());
        $result = $content_unit_references_index->deleteFromIndex();
        return $result;
    }

    public function getPlainTextRepresentation()
    {
        $s = '';
        return $s;
    }

    public function getDecoratedIdsQueryBuilder(QueryBuilder $query_builder, array $filters, array $sort_options)
    {
        $content_unit_references_index = new ContentUnitReferencesIndex(
            $this->getApp(),
            $this->getContentUnit()->getType(),
            $this->getContentUnit()->getId(),
            $this->getName()
        );
        $content_unit_references_index->set($this->get());
        $query_builder = $content_unit_references_index->getDecoratedIdsQueryBuilder($query_builder, $filters, $sort_options);
        return $query_builder;
    }

    public function getImage()
    {
        $image = null;
        if (count($this->value) > 0) {
            $app = $this->getApp();
            $image_content_unit = $app['content_unit_factory']->load($this->value[0]['type'], $this->value[0]['id']);
            if ($image_content_unit != null) {
                $image = $image_content_unit->getImage();
            }
        }
        return $image;
    }

}

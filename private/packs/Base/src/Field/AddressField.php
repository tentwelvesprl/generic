<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor\Pack\Base\Field;

use Doctrine\DBAL\Query\QueryBuilder;
use Viktor\Pack\Base\Dataset\Dataset;
use Viktor\Pack\Base\Field\AbstractField;

class AddressField extends AbstractField
{
    private $value;

    public function __construct(Dataset $dataset, array $options, $field_name, array $metadata, array $full_text_index_weights)
    {
        parent::__construct($dataset, $options, $field_name, $metadata, $full_text_index_weights);
        $this->set(null); // Will use the default value.
        return;
    }

    public function getDefaultOptions()
    {
        return array(
            'indexed' => false,
            'default_country' => '',
        );
    }

    public function get()
    {
        return $this->value;
    }

    public function set($value = null)
    {
        $this->value = $this->getDefaultValue();
        if (is_array($value)) {
            if (isset($value['street_1'])) {
                $this->value['street_1'] = (string) $value['street_1'];
            }
            if (isset($value['street_2'])) {
                $this->value['street_2'] = (string) $value['street_2'];
            }
            if (isset($value['postal_code'])) {
                $this->value['postal_code'] = (string) $value['postal_code'];
            }
            if (isset($value['city'])) {
                $this->value['city'] = (string) $value['city'];
            }
            if (isset($value['country']) && isset($this->getCountriesList()[$value['country']])) {
                $this->value['country'] = (string) $value['country'];
            }
            if (isset($value['latitude'])) {
                $this->value['latitude'] = (float) $value['latitude'];
            }
            if (isset($value['longitude'])) {
                $this->value['longitude'] = (float) $value['longitude'];
            }
        }
        return $this;
    }

    public function getDigest()
    {
        return $this->value;
    }

    public function updateIndex()
    {
        $options = $this->getOptions();
        if ($options['indexed']) {
            $this->deleteFromIndex();
            $query_builder = $this->getApp()['db']->createQueryBuilder();
            $query_builder
                ->insert('address_field_index')
                ->setValue('type', ':type')
                ->setValue('id', ':id')
                ->setValue('field_name', ':field_name')
                ->setValue('postal_code', ':postal_code')
                ->setValue('country', ':country')
                ->setValue('latitude', ':latitude')
                ->setValue('longitude', ':longitude')
                ->setParameter('type', (string) $this->getContentUnit()->getType())
                ->setParameter('id', (int) $this->getContentUnit()->getId())
                ->setParameter('field_name', (string) $this->getName())
                ->setParameter('postal_code', (string) $this->get()['postal_code'])
                ->setParameter('country', (string) $this->get()['country'])
                ->setParameter('latitude', (float) $this->get()['latitude'])
                ->setParameter('longitude', (float) $this->get()['longitude']);
            $query_builder->execute();
        }
        return true;
    }

    public function deleteFromIndex()
    {
        $options = $this->getOptions();
        if ($options['indexed']) {
            $query_builder = $this->getApp()['db']->createQueryBuilder();
            $query_builder
                ->delete('address_field_index')
                ->where('type = :type AND id = :id AND field_name = :field_name')
                ->setParameter('type', $this->getContentUnit()->getType())
                ->setParameter('id', $this->getContentUnit()->getId())
                ->setParameter('field_name', $this->getName());
            $query_builder->execute();
        }
        return true;
    }

    public function getPlainTextRepresentation()
    {
        $s = ''
            . $this->value['street_1']
            . ($this->value['street_2'] != '' ? "\n" : '')
            . $this->value['street_2']
            . "\n"
            . $this->value['postal_code'] . ' ' . $this->value['city']
            . ($this->value['country'] != '' ? "\n" : '')
            . $this->value['country']
        ;
        return $s;
    }

    public function getDecoratedIdsQueryBuilder(QueryBuilder $query_builder, array $filters, array $sort_options)
    {
        static $counter = 0;
        foreach ($filters as $filter) {
            $counter++;
            if ($filter['type'] == 'address_field_country' && $filter['field_name'] == $this->getName()) {
                $value = trim((string) $filter['value']);
                if ($value != '') {
                    $field_alias = 'f' . md5('address_field_index_' . $this->getName() . $counter);
                    $value_alias = 'v' . md5('address_field_index_' . $this->getName() . '_value' . $counter);
                    $field_name_alias = 'n' . md5('address_field_index_' . $this->getName() . '_field_name' . $counter);
                    // Note that the aliases are prefixed with a letter, so that
                    // Doctrine consider them as named parameters.
                    $query_builder
                        ->innerJoin(
                            'component',
                            'address_field_index', $field_alias,
                            'component.type = ' . $field_alias . '.type AND component.id = ' . $field_alias . '.id'
                        )
                        ->andWhere($field_alias . '.field_name = :' . $field_name_alias)
                        ->andWhere($field_alias . '.country = :' . $value_alias)
                        ->setParameter($field_name_alias, $this->getName())
                        ->setParameter($value_alias, $value)
                    ;
                }
            }
            if ($filter['type'] == 'address_field_postal_code' && $filter['field_name'] == $this->getName()) {
                $value = trim((string) $filter['value']);
                if ($value != '') {
                    $field_alias = 'f' . md5('address_field_index_' . $this->getName() . $counter);
                    $value_alias = 'v' . md5('address_field_index_' . $this->getName() . '_value' . $counter);
                    $field_name_alias = 'n' . md5('address_field_index_' . $this->getName() . '_field_name' . $counter);
                    // Note that the aliases are prefixed with a letter, so that
                    // Doctrine consider them as named parameters.
                    $query_builder
                        ->innerJoin(
                            'component',
                            'address_field_index', $field_alias,
                            'component.type = ' . $field_alias . '.type AND component.id = ' . $field_alias . '.id'
                        )
                        ->andWhere($field_alias . '.field_name = :' . $field_name_alias)
                        ->andWhere($field_alias . '.postal_code = :' . $value_alias)
                        ->setParameter($field_name_alias, $this->getName())
                        ->setParameter($value_alias, $value)
                    ;
                }
            }
        }
        foreach ($sort_options as $sort_option) {
            if (!is_array($sort_option)) {
                continue;
            }
            $option = (string) $sort_option['option'];
            $direction = (string) $sort_option['direction'];
            if (strtoupper($direction) == 'ASC') {
                $direction = 'ASC';
                $contrary_direction = 'DESC';
            } else {
                $direction = 'DESC';
                $contrary_direction = 'ASC';
            }
            switch ($option) {
                case 'field_' . $this->getName():
                    $field_alias = 'f' . md5($option);
                    $query_builder
                        ->innerJoin(
                            'component',
                            'address_field_index', $field_alias,
                            'component.type = ' . $field_alias . '.type AND component.id = ' . $field_alias . '.id'
                        )
                        ->andWhere($field_alias . '.field_name = \'' . $this->getName() . '\'')
                        ->addOrderBy($field_alias . '.country', $direction)
                        ->addOrderBy($field_alias . '.postal_code', $direction)
                        ->addOrderBy($field_alias . '.latitude', $contrary_direction)
                        ->addOrderBy($field_alias . '.longitude', $direction);
                    break;
            }
        }
        return $query_builder;
    }

    public function getDefaultValue()
    {
        return array(
            'street_1' => '',
            'street_2' => '',
            'postal_code' => '',
            'city' => '',
            'country' => $this->getOptions()['default_country'],
            'latitude' => '',
            'longitude' => '',
        );
    }

    public static function getCountriesList()
    {
        return array(
            ''   => "",
            'AF' => "Afghanistan",
            'AX' => "Åland Islands",
            'AL' => "Albania",
            'DZ' => "Algeria",
            'AS' => "American Samoa",
            'AD' => "Andorra",
            'AO' => "Angola",
            'AI' => "Anguilla",
            'AQ' => "Antarctica",
            'AG' => "Antigua and Barbuda",
            'AR' => "Argentina",
            'AM' => "Armenia",
            'AW' => "Aruba",
            'AU' => "Australia",
            'AT' => "Austria",
            'AZ' => "Azerbaijan",
            'BS' => "Bahamas",
            'BH' => "Bahrain",
            'BD' => "Bangladesh",
            'BB' => "Barbados",
            'BY' => "Belarus",
            'BE' => "Belgium",
            'BZ' => "Belize",
            'BJ' => "Benin",
            'BM' => "Bermuda",
            'BT' => "Bhutan",
            'BO' => "Bolivia, Plurinational State of",
            'BQ' => "Bonaire, Sint Eustatius and Saba",
            'BA' => "Bosnia and Herzegovina",
            'BW' => "Botswana",
            'BV' => "Bouvet Island",
            'BR' => "Brazil",
            'IO' => "British Indian Ocean Territory",
            'BN' => "Brunei Darussalam",
            'BG' => "Bulgaria",
            'BF' => "Burkina Faso",
            'BI' => "Burundi",
            'KH' => "Cambodia",
            'CM' => "Cameroon",
            'CA' => "Canada",
            'CV' => "Cape Verde",
            'KY' => "Cayman Islands",
            'CF' => "Central African Republic",
            'TD' => "Chad",
            'CL' => "Chile",
            'CN' => "China",
            'CX' => "Christmas Island",
            'CC' => "Cocos (Keeling) Islands",
            'CO' => "Colombia",
            'KM' => "Comoros",
            'CG' => "Congo",
            'CD' => "Congo, the Democratic Republic of the",
            'CK' => "Cook Islands",
            'CR' => "Costa Rica",
            'CI' => "Côte d'Ivoire",
            'HR' => "Croatia",
            'CU' => "Cuba",
            'CW' => "Curaçao",
            'CY' => "Cyprus",
            'CZ' => "Czech Republic",
            'DK' => "Denmark",
            'DJ' => "Djibouti",
            'DM' => "Dominica",
            'DO' => "Dominican Republic",
            'EC' => "Ecuador",
            'EG' => "Egypt",
            'SV' => "El Salvador",
            'GQ' => "Equatorial Guinea",
            'ER' => "Eritrea",
            'EE' => "Estonia",
            'ET' => "Ethiopia",
            'FK' => "Falkland Islands (Malvinas)",
            'FO' => "Faroe Islands",
            'FJ' => "Fiji",
            'FI' => "Finland",
            'FR' => "France",
            'GF' => "French Guiana",
            'PF' => "French Polynesia",
            'TF' => "French Southern Territories",
            'GA' => "Gabon",
            'GM' => "Gambia",
            'GE' => "Georgia",
            'DE' => "Germany",
            'GH' => "Ghana",
            'GI' => "Gibraltar",
            'GR' => "Greece",
            'GL' => "Greenland",
            'GD' => "Grenada",
            'GP' => "Guadeloupe",
            'GU' => "Guam",
            'GT' => "Guatemala",
            'GG' => "Guernsey",
            'GN' => "Guinea",
            'GW' => "Guinea-Bissau",
            'GY' => "Guyana",
            'HT' => "Haiti",
            'HM' => "Heard Island and McDonald Islands",
            'VA' => "Holy See (Vatican City State)",
            'HN' => "Honduras",
            'HK' => "Hong Kong",
            'HU' => "Hungary",
            'IS' => "Iceland",
            'IN' => "India",
            'ID' => "Indonesia",
            'IR' => "Iran, Islamic Republic of",
            'IQ' => "Iraq",
            'IE' => "Ireland",
            'IM' => "Isle of Man",
            'IL' => "Israel",
            'IT' => "Italy",
            'JM' => "Jamaica",
            'JP' => "Japan",
            'JE' => "Jersey",
            'JO' => "Jordan",
            'KZ' => "Kazakhstan",
            'KE' => "Kenya",
            'KI' => "Kiribati",
            'KP' => "Korea, Democratic People's Republic of",
            'KR' => "Korea, Republic of",
            'KW' => "Kuwait",
            'KG' => "Kyrgyzstan",
            'LA' => "Lao People's Democratic Republic",
            'LV' => "Latvia",
            'LB' => "Lebanon",
            'LS' => "Lesotho",
            'LR' => "Liberia",
            'LY' => "Libya",
            'LI' => "Liechtenstein",
            'LT' => "Lithuania",
            'LU' => "Luxembourg",
            'MO' => "Macao",
            'MK' => "Macedonia, The Former Yugoslav Republic of",
            'MG' => "Madagascar",
            'MW' => "Malawi",
            'MY' => "Malaysia",
            'MV' => "Maldives",
            'ML' => "Mali",
            'MT' => "Malta",
            'MH' => "Marshall Islands",
            'MQ' => "Martinique",
            'MR' => "Mauritania",
            'MU' => "Mauritius",
            'YT' => "Mayotte",
            'MX' => "Mexico",
            'FM' => "Micronesia, Federated States of",
            'MD' => "Moldova, Republic of",
            'MC' => "Monaco",
            'MN' => "Mongolia",
            'ME' => "Montenegro",
            'MS' => "Montserrat",
            'MA' => "Morocco",
            'MZ' => "Mozambique",
            'MM' => "Myanmar",
            'NA' => "Namibia",
            'NR' => "Nauru",
            'NP' => "Nepal",
            'NL' => "Netherlands",
            'NC' => "New Caledonia",
            'NZ' => "New Zealand",
            'NI' => "Nicaragua",
            'NE' => "Niger",
            'NG' => "Nigeria",
            'NU' => "Niue",
            'NF' => "Norfolk Island",
            'MP' => "Northern Mariana Islands",
            'NO' => "Norway",
            'OM' => "Oman",
            'PK' => "Pakistan",
            'PW' => "Palau",
            'PS' => "Palestine, State of",
            'PA' => "Panama",
            'PG' => "Papua New Guinea",
            'PY' => "Paraguay",
            'PE' => "Peru",
            'PH' => "Philippines",
            'PN' => "Pitcairn",
            'PL' => "Poland",
            'PT' => "Portugal",
            'PR' => "Puerto Rico",
            'QA' => "Qatar",
            'RE' => "Réunion",
            'RO' => "Romania",
            'RU' => "Russian Federation",
            'RW' => "Rwanda",
            'BL' => "Saint Barthélemy",
            'SH' => "Saint Helena, Ascension and Tristan da Cunha",
            'KN' => "Saint Kitts and Nevis",
            'LC' => "Saint Lucia",
            'MF' => "Saint Martin (French part)",
            'PM' => "Saint Pierre and Miquelon",
            'VC' => "Saint Vincent and the Grenadines",
            'WS' => "Samoa",
            'SM' => "San Marino",
            'ST' => "Sao Tome and Principe",
            'SA' => "Saudi Arabia",
            'SN' => "Senegal",
            'RS' => "Serbia",
            'SC' => "Seychelles",
            'SL' => "Sierra Leone",
            'SG' => "Singapore",
            'SX' => "Sint Maarten (Dutch part)",
            'SK' => "Slovakia",
            'SI' => "Slovenia",
            'SB' => "Solomon Islands",
            'SO' => "Somalia",
            'ZA' => "South Africa",
            'GS' => "South Georgia and the South Sandwich Islands",
            'SS' => "South Sudan",
            'ES' => "Spain",
            'LK' => "Sri Lanka",
            'SD' => "Sudan",
            'SR' => "Suriname",
            'SJ' => "Svalbard and Jan Mayen",
            'SZ' => "Swaziland",
            'SE' => "Sweden",
            'CH' => "Switzerland",
            'SY' => "Syrian Arab Republic",
            'TW' => "Taiwan, Province of China",
            'TJ' => "Tajikistan",
            'TZ' => "Tanzania, United Republic of",
            'TH' => "Thailand",
            'TL' => "Timor-Leste",
            'TG' => "Togo",
            'TK' => "Tokelau",
            'TO' => "Tonga",
            'TT' => "Trinidad and Tobago",
            'TN' => "Tunisia",
            'TR' => "Turkey",
            'TM' => "Turkmenistan",
            'TC' => "Turks and Caicos Islands",
            'TV' => "Tuvalu",
            'UG' => "Uganda",
            'UA' => "Ukraine",
            'AE' => "United Arab Emirates",
            'GB' => "United Kingdom",
            'US' => "United States",
            'UM' => "United States Minor Outlying Islands",
            'UY' => "Uruguay",
            'UZ' => "Uzbekistan",
            'VU' => "Vanuatu",
            'VE' => "Venezuela, Bolivarian Republic of",
            'VN' => "Viet Nam",
            'VG' => "Virgin Islands, British",
            'VI' => "Virgin Islands, U.S.",
            'WF' => "Wallis and Futuna",
            'EH' => "Western Sahara",
            'YE' => "Yemen",
            'ZM' => "Zambia",
            'ZW' => "Zimbabwe",
        );
    }

}

<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor;

use ArrayObject;
use Composer\Autoload\ClassLoader;
use Silex\Application as Silex_Application;
use Silex\Application\TwigTrait;
use Silex\Application\UrlGeneratorTrait;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpCache\HttpCache;
use Symfony\Component\HttpKernel\HttpCache\Store;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Yaml\Parser;

class Application extends Silex_Application
{
    use TwigTrait;
    use UrlGeneratorTrait;

    private $appNamespace;
    private $autoloader;
    private $packsNames;
    private $packs;

    const VIKTOR_VERSION = '4.2.0';

    public function __construct(
        $environment,
        $app_namespace,
        array $packs_names,
        ClassLoader $autoloader,
        array $values = array()
    ) {
        $this->appNamespace = strval($app_namespace);
        $this->packsNames = array_map('strval', $packs_names);
        $this->packs = array();
        $this->autoloader = $autoloader;
        $default_values = array();
        $default_values['viktor.environment'] = (string) $environment;
        $default_base_path = __DIR__ . '/../..';
        $default_values['viktor.app_path'] = realpath($default_base_path . '/private/app');
        $default_values['viktor.data_path'] = realpath($default_base_path . '/private/data');
        $default_values['viktor.cache_path'] = realpath($default_base_path . '/private/cache');
        $default_values['viktor.log_path'] = realpath($default_base_path . '/private/log');
        $default_values['viktor.packs_path'] = realpath($default_base_path . '/private/packs');
        $default_values['viktor.tmp_path'] = realpath($default_base_path . '/private/tmp');
        $default_values['viktor.web_path'] = realpath($default_base_path . '/web');
        $default_values['viktor.config'] = array();
        $values = array_merge($default_values, $values);
        parent::__construct($values);
        $this->configureAutoloader();
        $this->loadConfig();
        if ($this['viktor.config']['debug']) {
            $this['debug'] = true;
        } else {
            $this['debug'] = false;
        }
        // Configure error handling.
        error_reporting(E_ALL ^ E_NOTICE);
        ini_set('display_errors', ($this['debug'] ? '1' : '0'));
        // Configure safe defaults for cookies.
        ini_set('session.cookie_samesite', 'Strict');
        // Routing
        $this->loadRoutes();
        // Prepare the security configuration.
        $this['viktor.security.firewalls'] = new ArrayObject();
        $this['viktor.security.access_rules'] = new ArrayObject();
        // Sets the default locale.
        // Useful when converting strings with iconv.
        // FIXME:
        // - This is not thread-safe.
        // - This assumes en_US.UTF8 is installed on the system.
        @setlocale(LC_ALL, 'en_US.UTF8');
        return $this;
    }

    public function getPacks() {
        return $this->packs;
    }

    private function configureAutoloader()
    {
        foreach ($this->packsNames as $pack_name) {
            $this->autoloader->addPsr4('Viktor\\Pack\\' . $pack_name . '\\', $this['viktor.packs_path'] . '/' . $pack_name . '/src');
        }
        $this->autoloader->addPsr4($this->appNamespace . '\\', $this['viktor.app_path'] . '/src');
        return $this;
    }

    private function loadConfig()
    {
        $yaml = new Parser();
        // Initial config.
        $config_file = __DIR__ . '/config.yaml';
        if (file_exists($config_file)) {
            $config_array = $yaml->parse(file_get_contents($config_file));
            if (is_array($config_array)) {
                $this['viktor.config'] = array_merge($this['viktor.config'], $config_array);
            }
        }
        // Packs config.
        foreach ($this->packsNames as $pack_name) {
            $pack_path = $this['viktor.packs_path'] . '/' . $pack_name;
            $config_file = $pack_path . '/config/config.yaml';
            if (file_exists($config_file)) {
                $config_array = $yaml->parse(file_get_contents($config_file));
                if (is_array($config_array)) {
                    $this['viktor.config'] = array_merge($this['viktor.config'], $config_array);
                }
            }
        }
        // General app config.
        $config_file = $this['viktor.app_path'] . '/config/config.yaml';
        if (file_exists($config_file)) {
            $config_array = $yaml->parse(file_get_contents($config_file));
            if (is_array($config_array)) {
                $this['viktor.config'] = array_merge($this['viktor.config'], $config_array);
            }
        }
        // App config for the current environment.
        $config_file = $this['viktor.app_path'] . '/config/config-' . $this['viktor.environment'] . '.yaml';
        if (file_exists($config_file)) {
            $config_array = $yaml->parse(file_get_contents($config_file));
            if (is_array($config_array)) {
                $this['viktor.config'] = array_merge($this['viktor.config'], $config_array);
            }
        }
        return $this;
    }

    private function loadRoutes()
    {
        $app = $this;
        $app['routes'] = $app->extend('routes', function (RouteCollection $routes, Application $app) {
            $possible_config_paths = array();
            foreach ($this->packsNames as $pack_name) {
                $possible_config_path = $this['viktor.packs_path'] . '/' . $pack_name . '/config';
                if (file_exists($possible_config_path . '/routes.yaml')) {
                    $possible_config_paths[] = $possible_config_path;
                }
            }
            if (file_exists($app['viktor.app_path'] . '/config/routes.yaml')) {
                $possible_config_paths[] = $app['viktor.app_path'] . '/config';
            }
            foreach ($possible_config_paths as $possible_config_path) {
                $loader = new YamlFileLoader(new FileLocator($possible_config_path));
                $collection = $loader->load('routes.yaml');
                if ($app['viktor.config']['require_https']) {
                    $collection->setSchemes('https');
                }
                $routes->addCollection($collection);
            }
            // Environment dependant config.
            if (file_exists($app['viktor.app_path'] . '/config/routes-' . $this['viktor.environment'] . '.yaml')) {
                $loader = new YamlFileLoader(new FileLocator($app['viktor.app_path'] . '/config'));
                $collection = $loader->load('routes-' . $this['viktor.environment'] . '.yaml');
                if ($app['viktor.config']['require_https']) {
                    $collection->setSchemes('https');
                }
                $routes->addCollection($collection);
            }
            return $routes;
        });
        return $this;
    }

    public function init()
    {
        $app = $this;
        // Default time zone configuration.
        ini_set('date.timezone', $this['viktor.config']['default_timezone']);
        // Cache.
        if ($this['viktor.config']['enable_cache']) {
            $this['kernel'] = $this->extend('kernel', function (HttpKernelInterface $kernel, Application $app) {
                $kernel = new HttpCache(
                    $kernel,
                    new Store($app['viktor.cache_path']),
                    null,
                    array(
                        'debug'                  => $app['debug'],
                        'default_ttl'            => 0,
                        'private_headers'        => array('Authorization', 'Cookie'),
                        'allow_reload'           => false,
                        'allow_revalidate'       => false,
                        'stale_while_revalidate' => 2,
                        'stale_if_error'         => 60,
                    )
                );
                return $kernel;
            });
        }
        // Packs initialization.
        foreach ($this->packsNames as $pack_name) {
            $pack_class = 'Viktor\\Pack\\' . $pack_name . '\\' . $pack_name . 'Pack';
            $pack = new $pack_class($this);
            $pack->init();
            $this->packs[] = $pack;
        }
        return $this;
    }

    public function addConsoleCommands()
    {
        foreach ($this->packs as $pack) {
            $pack->addConsoleCommands();
        }
        return $this;
    }

    public function run(Request $request = null)
    {
        return parent::run($request);
    }

    public function __toString()
    {
        return 'Viktor application';
    }

    public function __debugInfo()
    {
        return array(
            'environment' => $this['viktor.environment'],
            'debug' => $this['debug'],
            'cache' => $this['viktor.config']['enable_cache'],
        );
    }

}

<?php

/*
 * Copyright 2018 TENTWELVE SPRL
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Viktor;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Viktor\Pack\Base\Console\ViktorCommand;
use Viktor\Pack\Base\Console\ViktorStyle;

class InstallCommand extends ViktorCommand
{
    protected function configure()
    {
        $this
            ->setName('viktor:install')
            ->setDescription('Installs a Viktor system and its packs')
            ->setHelp('This command installs a Viktor system and its packs.');
        return;
    }

    protected function executeWithStyle(ViktorStyle $io, InputInterface $input, OutputInterface $output)
    {
        $app = $this->getSilexApplication();
        foreach ($app->getPacks() as $pack) {
            $pack->install();
            $io->text($pack->getName() . ' pack installed.');
        }
        return;
    }

}

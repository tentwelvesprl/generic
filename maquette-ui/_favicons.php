
<link rel="apple-touch-icon" sizes="180x180" href="../web/apple-touch-icon.png">
<link rel="icon" type="image/png" href="../web/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="../web/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="../web/manifest.json">
<link rel="mask-icon" href="../web/safari-pinned-tab.svg" color="#1e51e5">
<meta name="apple-mobile-web-app-title" content="Viktor">
<meta name="application-name" content="Viktor">
<meta name="theme-color" content="#1e51e5">

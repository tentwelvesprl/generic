<?php

$INIT['base_href'] = $_SERVER['REQUEST_URI'];

$words = explode(" ", $lorem);

function UrlStrPrepare($s){
    $s = str_replace("'", ' ', $s);
    $s = str_replace("’", ' ', $s);
    $s = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $s);
    $s = preg_replace('/[^[:alnum:]\\s\\-]/', '', $s);
    $s = preg_replace('/\\s+/', '-', trim($s));
    $s = strtolower($s);
    return $s;
}

function randIpsum($min=1, $max=20, $w){
    $m = count($w)-1;
    for ($x=1, $rw=rand($min,$max); $x<=$rw; $x++) {
        $ar[]=$w[rand(0,$m)];
    }
    return ucfirst(implode(' ',$ar));
}

function t($str) {
    if (isset($dict[$str])) {
        return $dict[$str];
    } else {
        return $str;
    }
}

$formats_json = json_decode(file_get_contents("./img_formats.json"), true);
$formats = array();
foreach ($formats_json['img_generation_config'] as $format_ref => $format_def) {
    $f = '';
    if(isset($format_def['transformations'])) {
        foreach ($format_def['transformations'] as $k => $v) {
            switch ($k) {
                case 'resize_around_pivot_point':
                $f .= '&amp;mode=resize_around_pivot_point&amp;width='.$v['width'].'&amp;height='.$v['height'];
                break;
                case 'fit_to_max_dimensions':
                $f .= '&amp;mode=fit_to_max_dimensions&amp;width='.$v['width'].'&amp;height='.$v['height'];
                break;
                case 'fit_to_height':
                $f .= '&amp;mode='.$k.'&amp;height='.$v;
                break;
                default:
                $f .= '&amp;mode='.$k.'&amp;width='.$v;
                break;
            }
        }
    }
    if (isset($format_def['jpeg_quality'])) $f .= '&amp;q='.$format_def['jpeg_quality'];
    if (isset($format_def['mime_type'])) $f .= '&amp;type='.str_replace('image/','',$format_def['mime_type']);
    $formats[$format_ref] = $f;
};



function resImgHelper($src, $format, $alt="", $class="") {
    if (isset($src) && isset($GLOBALS['formats_json']['img_helper_config'][$format]) ) {
        $settings = $GLOBALS['formats_json']['img_helper_config'][$format];

        if (isset($settings['sources'])) :

            if (!empty($class)) $picture = '<picture>'; else $picture = '<picture class="'.$class.'">';
        $picture .= '<!--[if IE 9]><video style="display: none;"><![endif]-->
        ';
        foreach($settings['sources'] as $source) {

            $picture .= '<source';
            $srcsetstr = array();
            foreach ($source['srcset'] as $n => $srcset) {
                $srcsetstr[$n] = 'img/img.php?file='.$src.$GLOBALS['formats'][$srcset[0]];
                if (isset($srcset[1])) $srcsetstr[$n] .= ' '.$srcset[1];
            }
            if (!empty($srcsetstr)) $picture .= ' srcset="'.implode(', ', $srcsetstr).'"';
            if (isset($source['params'])) $picture .= ' '.$source['params'];
            $picture .= ' >';

        }
        $picture .= '
        <!--[if IE 9]></video><![endif]-->
        ';

        $picture .= '<img';
        if (isset($settings['srcset'])) {
            $srcsetstr = array();
            foreach ($settings['srcset'] as $n => $srcset) {
                $srcsetstr[$n] = 'img/img.php?file='.$src.$GLOBALS['formats'][$srcset[0]];
                if (isset($srcset[1])) $srcsetstr[$n] .= ' '.$srcset[1];
            }
            $picture .= ' srcset="'.implode(', ', $srcsetstr).'"';
        }
        $picture .= ' src="img/img.php?file='.$src.$GLOBALS['formats'][$settings['src']].'" alt="'.$alt.'"';
        if (isset($settings['params'])) $picture .= ' '.$settings['params'];
        $picture .= ' >';
        $picture .= '
    </picture>
    ';
    return $picture;

    else :
        $img = '<img';
    if (!empty($settings) && is_array($settings)) {

        if (isset($settings['srcset'])) {
            $srcsetstr = array();
            foreach ($settings['srcset'] as $n => $srcset) {
                $srcsetstr[$n] = 'img/img.php?file='.$src.$GLOBALS['formats'][$srcset[0]];
                if (isset($srcset[1])) $srcsetstr[$n] .= ' '.$srcset[1];
            }
            $img .= ' srcset="'.implode(', ', $srcsetstr).'"';
            if (isset($settings['params'])) $img .= ' '.$settings['params'];
        }
        $img .= ' src="img/img.php?file='.$src.$GLOBALS['formats'][$settings['src']].'"';

    }
    if (!empty($class)) $img .= ' class="'.$class.'"';
    $img .= ' alt="'.$alt.'" >';
    return $img;

    endif;

}
}

if (isset($_GET['inc'])) $inc = $_GET['inc']; else $inc='examples/home';

// Obsolete since it’s unused in Viktor
//$INIT['ajax'] = false;
//if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
//    $INIT['ajax'] = true;
//}

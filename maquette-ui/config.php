<?php
$CONFIG['site_name'] = "Viktor";

$figs = glob("img/figs/*.jpg");
array_unshift($figs, count($figs));

$icons = array('activity', 'alert-circle', 'archive', 'arrow-down-right', 'bar-chart', 'bell', 'bluetooth', 'check-square', 'chevrons-right', 'chrome', 'clipboard', 'clock', 'cloud-lightning', 'coffee', 'compass', 'delete', 'trash', 'eye', 'flag', 'folder-plus');
array_unshift($icons, count($icons));

$CONFIG['tree'] = array(
    'Dashboard' => array('templates/dashboard', 'home'),
    'Style Guide' => array('', 'trello', array(
        'Font specimen' => 'styleguide/font_specimen',
        'Defaults' => 'styleguide/defaults',
        'Inputs' => 'styleguide/inputs',
        'Buttons & icons' => 'styleguide/buttons',
        )),
    'Templates' => array('', 'sidebar', array(
        'Index' => 'templates/index',
        // 'Form' => 'templates/form',
        // 'Tree' => 'templates/tree',
        )),
    'Examples' => array('', 'layers', array(
        // 'Index article' => 'examples/article_index',
        // 'Edit article' => 'examples/article_update',
        'Index users' => 'examples/user_index',
        'New user' => 'examples/user_create',
        'Edit user' => 'examples/user_update',
        )),
    );

$dict = array(
    );

$lorem = "suddenly the foremost martian lowered his tube and discharged a canister of the black gas at the ironclad. it hit her larboard side and glanced off in an inky jet that rolled away to seaward, an unfolding torrent of black smoke, from which the ironclad drove clear. to the watchers from the steamer, low in the water and with the sun in their eyes, it seemed as though she were already among the martians. they saw the gaunt figures separating and rising out of the water as they retreated shoreward, and one of them raised the camera-like generator of the heat-ray. he held it pointing obliquely downward, and a bank of steam sprang from the water at its touch. it must have driven through the iron of the ship’s side like a white-hot iron rod through paper. but no one heeded that very much. at the sight of the martian’s collapse the captain on the bridge yelled inarticulately, and all the crowding passengers on the steamer’s stern shouted together. and then they yelled again. for, surging out beyond the white tumult, drove something long and black, the flames streaming from its middle parts, its ventilators and funnels spouting fire. she was alive still; the steering gear, it seems, was intact and her engines working. she headed straight for a second martian, and was within a hundred yards of him when the heat-ray came to bear. then with a violent thud, a blinding flash, her decks, her funnels, leaped upward. the martian staggered with the violence of her explosion, and in another moment the flaming wreckage, still driving forward with the impetus of its pace, had struck him and crumpled him up like a thing of cardboard. my brother shouted involuntarily. a boiling tumult of steam hid everything again. two! yelled the captain. everyone was shouting. the whole steamer from end to end rang with frantic cheering that was taken up first by one and then by all in the crowding multitude of ships and boats that was driving out to sea. the little vessel continued to beat its way seaward, and the ironclads receded slowly towards the coast, which was hidden still by a marbled bank of vapour, part steam, part black gas, eddying and combining in the strangest way. the fleet of refugees was scattering to the northeast; several smacks were sailing between the ironclads and the steamboat. after a time, and before they reached the sinking cloud bank, the warships turned northward, and then abruptly went about and passed into the thickening haze of evening southward. the coast grew faint, and at last indistinguishable amid the low banks of clouds that were gathering about the sinking sun. then suddenly out of the golden haze of the sunset came the vibration of guns, and a form of black shadows moving. everyone struggled to the rail of the steamer and peered into the blinding furnace of the west, but nothing was to be distinguished clearly. a mass of smoke rose slanting and barred the face of the sun. the steamboat throbbed on its way through an interminable suspense. the sun sank into grey clouds, the sky flushed and darkened, the evening star trembled into sight. it was deep twilight when the captain cried out and pointed. my brother strained his eyes. something rushed up into the sky out of the greyness rushed slantingly upward and very swiftly into the luminous clearness above the clouds in the western sky; something flat and broad, and very large, that swept round in a vast curve, grew smaller, sank slowly, and vanished again into the grey mystery of the night. and as it flew it rained down darkness upon the land. dorothy lived in the midst of the great kansas prairies, with uncle henry, who was a farmer, and aunt em, who was the farmer’s wife.";

$pangrams = array(
    'Bâchez la queue du wagon-taxi avec les pyjamas du fakir',
    'Démasquez-vous ou je gifle les eaux blanches du WC au krypton !',
    'Portez ce vieux whisky à la juge blonde qui fume',
    'Refoulez ces barbus fatigués venus hanter les pommiers que Jacky Kennedy planta aux confins du Delaware',
    'Pour faire ce bon punch: rhum vieux, kiwi, jus d’orange, quelques noix de coco, zestes de citron et vanille. raccourci mais manque toujours le Y',
    'Vous parlez d’une histoire ! Ce xylophoniste belge a flashé sur moi alors que je mangeais un kiwi en pianotant sur mon ordinateur portable !',
    'Zéphyrin allait au zoo chaque week-end jouer avec les animaux et surtout voir les belles girafes',
    'Portez ce vieux whisky au juge blond qui fume',
    'Voyez ce bon fakir moqueur pousser un wagon en jouant du xylophone',
    'The quick brown fox jumps over the lazy dog',
    'Le vif renard brun saute par-dessus le chien paresseux.',
    'Le vif lynx grimpe jusqu’au wok du chat zébré',
    'Voix ambiguë d’un cœur qui, au zéphyr, préfère les jattes de kiwis',
    'Mon pauvre zébu ankylosé choque deux fois ton wagon jaune',
    'Pomme exquise chez La Frite: Bintje, Nevsky, King Edward',
    'Voyez ce jeu exquis wallon, de graphie en kit mais bref',
    'Prouvez, beau juge, que le fameux sandwich au yak tue',
    'Vieux pelage que je modifie : breitschwanz ou yak ?',
    'Fougueux, j’enivre la squaw au pack de beau zythum',
    'Ketch, yawl, jonque flambant neuve... jugez des prix !',
    'Voyez le brick géant que j’examine près du wharf',
    'Vous jugez bien plus fameux ce quart de whisky',
    'Portez au juge cinq bols de vos fameux whisky',
    'Jugez qu’un vieux whisky blond pur malt fonce',
    'Le moujik équipé de faux breitschwanz voyage',
    'Faux kwachas ? Quel projet de voyage zambien !',
    'Kiwi fade, aptéryx, quel jambon vous gâchez !',
    'Fripon, mixez l’abject whisky qui vidange',
    'Vif juge, trempez ce blond whisky aqueux',
    'Vif P-DG mentor, exhibez la squaw jockey',
    'Juge, flambez l’exquis patchwork d’Yvon',
    'Perchez dix, vingt woks. Qu’y flambé-je ?',
    'Whisky vert : jugez cinq fox d’aplomb',
    );
array_unshift($pangrams, count($pangrams));
$CONFIG['pangrams'] = $pangrams;

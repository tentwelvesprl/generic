<?php
require_once "config.php";
require_once "init.php";
?><!doctype html>
    <html class="mdzr-no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $CONFIG['site_name']; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="noindex, nofollow">
        <link rel="stylesheet" href="../web/assets/Admin/css/main.css?3df2e84e31406065">
        <?php include "_favicons.php"; ?>
    <body>
        <!--[if lt IE 9]><div class="load-alert ie-alert">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade it</a> for a better web experience.</div><![endif]-->
        <noscript>
            <div class="load-alert js-alert">For full functionality of Viktor, it is best to <a href="https://www.whatismybrowser.com/guides/how-to-enable-javascript/" target="_blank">enable Javascript</a> in your web browser.</div>
        </noscript>
        <?php include "_toolbar.php"; ?>
        <?php include "_navbar.php"; ?>
        <div class="main-container">
            <?php include $inc.".php"; ?>
        </div>

        <script src="../assets/Admin/js/init.js"></script>
        <script defer src="../assets/Admin/js/vendor/jquery-3.6.0.min.js"></script>
        <script defer src="../web/assets/Admin/js/plugins.min.js?ef79d490003b8a3d"></script>
        <script defer src="../web/assets/Admin/js/main.min.js?5f2a42c289b763f5"></script>
    </body>
</html>

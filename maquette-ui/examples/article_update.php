<?php
$fake_id = rand(1,255);
$fake_title_fr = randIpsum(2, 10, $words);
$fake_title_en = randIpsum(2, 10, $words);
$fake_title_nl = randIpsum(2, 10, $words);
?>



<header class="page-header">
    <div class="container">
        <h1>Edit article <span># <?php echo $fake_id; ?></span></h1>
        <p class="i18n-toggle i18n-show" data-language="fr">
            <strong>/fr/article/<?php echo $fake_id; ?>/<?php echo preg_replace('/[^A-Za-z0-9-]+/', '-', $fake_title_fr); ?></strong> <a href="#">preview&nbsp;fr</a>
        </p>
        <p class="i18n-toggle" data-language="nl">
            <strong>/nl/article/<?php echo $fake_id; ?>/<?php echo preg_replace('/[^A-Za-z0-9-]+/', '-', $fake_title_nl); ?></strong> <a href="#">preview&nbsp;nl</a>
        </p>
        <p class="i18n-toggle" data-language="en">
            <strong>/en/article/<?php echo $fake_id; ?>/<?php echo preg_replace('/[^A-Za-z0-9-]+/', '-', $fake_title_en); ?></strong> <a href="#">preview&nbsp;en</a>
        </p>
    </div>
</header>

<form action="#" class="split-container">

    <div class="split-body">

        <div class="container">

            <?php include "elements/_lang_switch.php"; ?>

            <div class="msg msg-error">
                <strong>titre du message d’erreur</strong><br>
                <?php echo substr($lorem, 0, rand(20,80)); ?> <a href="#">link</a>
                <a href="#" class="close close-msg">×</a>
            </div>

            <div class="input">
                <div class="i18n-toggle i18n-show" data-language="fr">
                    <label for="title_fr">title [fr]</label>
                    <input type="text" id="title_fr" class="size-xl" value="<?php echo $fake_title_fr; ?>" maxlength="60" />
                </div>
                <div class="i18n-toggle" data-language="en">
                    <label for="title_en">title [en]</label>
                    <input type="text" id="title_en" class="size-xl" value="<?php echo $fake_title_en; ?>" maxlength="60" />
                </div>
                <div class="i18n-toggle" data-language="nl">
                    <label for="title_nl">title [nl]</label>
                    <input type="text" id="title_nl" class="size-xl" value="<?php echo $fake_title_nl; ?>" maxlength="60" />
                </div>
            </div>

            <div class="input">
                <div class="i18n-toggle i18n-show" data-language="fr">
                    <label for="subtitle_fr">subtitle [fr]</label>
                    <input type="text" id="subtitle_fr" class="size-l" value="<?php echo randIpsum(2, 10, $words); ?>" maxlength="120" />
                </div>
                <div class="i18n-toggle" data-language="en">
                    <label for="subtitle_en">subtitle [en]</label>
                    <input type="text" id="subtitle_en" class="size-l" value="<?php echo randIpsum(2, 10, $words); ?>" maxlength="120" />
                </div>
                <div class="i18n-toggle" data-language="nl">
                    <label for="subtitle_nl">subtitle [nl]</label>
                    <input type="text" id="subtitle_nl" class="size-l" value="<?php echo randIpsum(2, 10, $words); ?>" maxlength="120" />
                </div>
            </div>

            <div class="input">
                <div class="i18n-toggle i18n-show" data-language="fr">
                    <label for="teaser_fr">teaser [fr]</label>
                    <textarea id="teaser_fr"><?php echo randIpsum(10, 20, $words); ?></textarea>
                </div>
                <div class="i18n-toggle" data-language="en">
                    <label for="teaser_en">teaser [en]</label>
                    <textarea d="teaser_en" ><?php echo randIpsum(10, 20, $words); ?></textarea>
                </div>
                <div class="i18n-toggle" data-language="nl">
                    <label for="teaser_nl">teaser [nl]</label>
                    <textarea d="teaser_nl" ><?php echo randIpsum(10, 20, $words); ?></textarea>
                </div>
            </div>

            <div class="input">
                <div class="i18n-toggle i18n-show" data-language="fr">
                    <label for="body_fr">body [fr]</label>
                    <textarea id="body_fr"><?php echo randIpsum(20, 100, $words); ?></textarea>
                </div>
                <div class="i18n-toggle" data-language="en">
                    <label for="body_en">body [en]</label>
                    <textarea d="body_en" ><?php echo randIpsum(20, 100, $words); ?></textarea>
                </div>
                <div class="i18n-toggle" data-language="nl">
                    <label for="body_nl">body [nl]</label>
                    <textarea d="body_nl" ><?php echo randIpsum(20, 100, $words); ?></textarea>
                </div>
            </div>


            <?php include "elements/_address_field.php"; ?>

        </div>
    </div>

    <footer class="split-aside">
        <div class="container">
            <?php include "elements/_pub_status.php"; ?>
            <?php include "elements/_form_actions.php"; ?>
        </div>
    </footer>

</form>
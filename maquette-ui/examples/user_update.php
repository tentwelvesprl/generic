<?php
$fake_id = rand(1,255);
$fake_username = randIpsum(1, 3, $words);
?>
<header class="header">

    <a href="?inc=examples/user_index" class="link link--large js-history">
        <svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#arrow-left-circle"></use></svg>
    </a>

    <h1 class="title title--header">Edit user</h1>
    <div class="header__tools">
        <a href="?inc=examples/user_create" class="link">
            <svg class="icon">
                <use xlink:href="../web/assets/Admin/img/feather-sprite.svg#plus-circle"></use>
            </svg>New user
        </a>
    </div>

</header>


<form action="#" method="POST" enctype="multipart/form-data" class="split" id="user_edition_form">

    <div class="split__left">

        <div class="datagroup js-toggle js-toggle-datagroup" data-section-name="main">
            <header class="datagroup__header datagroup__header--toggle js-toggle-btn active" data-target="#datagroup_main" data-section-name="main">
                <h3 class="datagroup__title">Main</h3>
                <div class="datagroup__toggle">
                    <svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#chevron-down"></use></svg>
                </div>
            </header>
            <div class="datagroup__content js-toggle-target" id="datagroup_main">
                <div class="input" data-base_name="first_name">
                    <label for="user_first_name">
                        First name
                    </label>
                    <input type="text" id="user_first_name" name="user_first_name">
                </div>

                <div class="input" data-base_name="last_name">
                    <label for="user_last_name">
                        Last name
                    </label>
                    <input type="text" id="user_last_name" name="user_last_name">
                </div>

            </div>
        </div>


        <div class="datagroup">

            <header class="datagroup__header datagroup__header--toggle js-toggle-btn active" data-target="#datagroup_credentials">
                <h3 class="datagroup__title">Credentials</h3>
                <button class="datagroup__toggle">
                    <svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#chevron-down"></use></svg>
                </button>
            </header>

            <div class="datagroup__content js-toggle-target" id="datagroup_credentials">


                <div class="input input-50 alpha ">
                    <label>
                        Username
                        <button class="help inactive">?</button>
                        <div class="msg helptip">
                            Let it be unique, short and obvious.
                        </div>
                    </label>
                    <input type="text" name="user_user_credentials_username" value="<?php echo $fake_username; ?>" placeholder="Username" maxlength="60">
                </div>

                <div class="input input-50 omega ">
                    <label>
                        Email address
                    </label>
                    <input type="email" name="user_user_credentials_email" placeholder="Email address">
                </div>

                <div class="input input-50 alpha ">
                    <label>
                        Password
                        <button class="help inactive">?</button>
                        <div class="msg helptip">
                            <figure>
                                <img src="../web/assets/Admin/img/XKCD_pwd.png" alt="">
                                <figcaption><a href="http://xkcd.com/936/">© XKCD</a></figcaption>
                            </figure>
                        </div>
                    </label>
                    <input type="password" name="user_user_credentials_pw1" autocomplete="new-password">
                </div>

                <div class="input input-50 omega ">
                    <label>
                        Password confirmation
                    </label>
                    <input type="password" name="user_user_credentials_pw2" autocomplete="new-password">
                </div>

                <fieldset>
                    <legend>Roles</legend>
                    <div class="input checkboxes">
                        <label>
                            <input type="checkbox" name="user_user_credentials_roles[]" value="ROLE_AUDITOR">
                            Auditor
                        </label>
                        <label>
                            <input type="checkbox" name="user_user_credentials_roles[]" value="ROLE_WEBMASTER">
                            Webmaster
                        </label>
                        <label>
                            <input type="checkbox" name="user_user_credentials_roles[]" value="ROLE_ADMIN" checked="">
                            Administrator
                        </label>
                    </div>
                </fieldset>


            </div>
        </div>


    </div>

    <div class="split__right">

        <div class="sticky-wrapper">

            <div class="block block--color-ui">
                <ul class="i18n-switch js-i18n-switch">
                    <li><button class="active" data-language="fr">fr</button></li>
                    <li><button data-language="nl">nl</button></li>
                    <li><button data-language="de">de</button></li>
                    <li><button data-language="en">en</button></li>
                </ul>
            </div>

            <div class="block block--preview">
                <div class="row__wrapper">
                    <div class="row__header">
                        <div class="row__id">#<?php echo $fake_id; ?></div>
                        <h2 class="row__title"> <?php echo $fake_username; ?></h2>
                    </div>
                </div>
            </div>

            <div class="form__actions">

                <button type="submit" name="submit" value="Save" class="btn btn--primary btn--large">
                    <svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#save"></use></svg>Save</button>

                </div>

                <div class="form__infos">
                    <dl>
                        <dt>Creation</dt>
                        <dd>
                            <time>13.05.2019 16:07:00</time>
                            <span>by <strong>Viktor's secretary</strong></span>
                        </dd>
                    </dl>
                    <dl>
                        <dt>Last modification</dt>
                        <dd>
                            <time>18.07.2019 16:16:17</time>
                            <span>by <strong> Tentwelve</strong></span>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
    </form>

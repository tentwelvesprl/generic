<header class="header">
    <a href="?inc=examples/user_index" class="link link--large js-history">
        <svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#arrow-left-circle"></use></svg>
    </a>
    <h1 class="title title--header">Create user</h1>
</header>


<form action="#" method="POST" enctype="multipart/form-data" class="split" id="user_edition_form">

    <div class="split__left">

        <div class="datagroup" data-section-name="main">
            <header class="datagroup__header datagroup__header--toggle js-toggle-btn active" data-target="#datagroup_main" data-section-name="main">
                <h3 class="datagroup__title">Main</h3>
                <div class="datagroup__toggle">
                    <svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#chevron-down"></use></svg>
                </div>
            </header>
            <div class="datagroup__content js-toggle-target" id="datagroup_main">
                <div class="input" data-base_name="first_name">
                    <label for="user_first_name">First name</label>
                    <input type="text" id="user_first_name" name="user_first_name">
                </div>
                <div class="input" data-base_name="last_name">
                    <label for="user_last_name">Last name</label>
                    <input type="text" id="user_last_name" name="user_last_name">
                </div>
            </div>
        </div>

        <div class="datagroup">

            <header class="datagroup__header datagroup__header--toggle js-toggle-btn" data-target="#datagroup_credentials">
                <h3 class="datagroup__title">Credentials</h3>
                <button class="datagroup__toggle">
                    <svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#chevron-down"></use></svg>
                </button>
            </header>

            <div class="datagroup__content js-toggle-target" id="datagroup_credentials" style="display:none">


                <div class="input input-50 alpha ">
                    <label>
                        Username
                        <button class="help inactive">?</button>
                        <div class="msg helptip">
                            Let it be unique, short and obvious.
                        </div>
                    </label>
                    <input type="text" name="user_user_credentials_username" placeholder="Username" maxlength="60">
                </div>

                <div class="input input-50 omega ">
                    <label>Email address</label>
                    <input type="email" name="user_user_credentials_email" placeholder="Email address">
                </div>

                <div class="input input-50 alpha ">
                    <label>Password
                        <button class="help inactive">?</button>
                        <div class="msg helptip">
                            <figure>
                                <img src="../web/assets/Admin/img/XKCD_pwd.png" alt="">
                                <figcaption><a href="http://xkcd.com/936/">© XKCD</a></figcaption>
                            </figure>
                        </div>
                    </label>
                    <input type="password" name="user_user_credentials_pw1" autocomplete="new-password">
                </div>

                <div class="input input-50 omega ">
                    <label>
                        Password confirmation
                    </label>
                    <input type="password" name="user_user_credentials_pw2" autocomplete="new-password">
                </div>

                <fieldset>
                    <legend>Roles</legend>
                    <div class="input checkboxes">
                        <label>
                            <input type="checkbox" name="user_user_credentials_roles[]" value="ROLE_AUDITOR">
                            Auditor
                        </label>
                        <label>
                            <input type="checkbox" name="user_user_credentials_roles[]" value="ROLE_WEBMASTER">
                            Webmaster
                        </label>
                        <label>
                            <input type="checkbox" name="user_user_credentials_roles[]" value="ROLE_ADMIN">
                            Administrator
                        </label>
                    </div>
                </fieldset>


            </div>
        </div>


    </div>

    <div class="split__right">

        <div class="sticky-wrapper">

            <div class="block block--color-ui">
                <ul class="i18n-switch js-i18n-switch">
                    <li><button class="active" data-language="fr">fr</button></li>
                    <li><button data-language="nl">nl</button></li>
                    <li><button data-language="de">de</button></li>
                    <li><button data-language="en">en</button></li>
                </ul>
            </div>

            <div class="form__actions">
                <button type="submit" name="submit" value="Save" class="btn btn--primary btn--large">
                    <svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#save"></use></svg>Save
                </button>
            </div>

        </div>
    </div>
</form>


<header class="section">
    <div class="wrapper">
        <h1 class="title title--header">Users</h1>
        <a href="?inc=examples/user_create" class="btn btn--primary btn--resp"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#plus"></use></svg>New user</a>
    </div>
</header>

<div class="section">
    <div class="wrapper">

        <div class="block block--tools">

            <form method="GET" action="#" class="sort-options">
                <label>Sort by</label>
                <select name="basic-user_en_sort_option_option">
                    <option value="">—</option>
                    <option value="username">Username</option>
                    <option value="creation_datetime">Creation time</option>
                    <option value="modification_datetime">Modification time</option>
                </select>
                <select name="basic-user_en_sort_option_direction">
                    <option value="ASC" selected="">Ascending (A → Z, 0 → 9)</option>
                    <option value="DESC">Descending (Z → A, 9 → 0)</option>
                </select>
                <input name="basic-user_en_sort_option_submit" type="submit" value="Sort" class="btn">
            </form>


            <button class="btn filter-btn js-toggle-btn" data-target="#index_filters"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#filter"></use></svg>Refine list...</button>

            <form method="GET" action="#" class="filters js-toggle-target" id="index_filters">


                <input type="hidden" name="reset" value="1">

                <div class="input input--actions">
                    <a class="link" href="#">Clear options</a>
                    <button type="submit" name="submit" value="Filter" class="btn btn--primary">Refine</button>
                </div>
            </form>
        </div>

        <form method="POST" action="#">

        <ul class="list">

            <li class="row actions actions--top">
                <div class="row__wrapper row__wrapper--actions">
                    <input type="checkbox" id="checkall" class="row__select row__select--all">
                    <label for="checkall" class="row__select__checkbox"></label>
                    <a href="#" class="link link--primary" disabled><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#check-circle"/></svg>Publish [fr]</a>
                    <span>|</span>
                    <a href="#" class="link" disabled><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#trash-2"/></svg>Delete</a>
                </div>
            </li>

                <?php for ($n=1, $r=20; $n <= $r; $n++) { ?>

                    <li class="row">
                        <div class="row__wrapper">

                            <input type="checkbox" name="selected_user_digests[]" id="basic-user_<?php echo $n; ?>" value="basic-user:<?php echo $n; ?>" class="row__select row__select--one">
                            <label for="basic-user_<?php echo $n; ?>" class="row__select__checkbox"></label>

                            <div class="row__header">

                                <div class="row__id">User #<?php echo $n; ?></div>
                                <h3 class="row__title">
                                    <a href="?inc=examples/user_create"><?php echo randIpsum(2, 12, $words); ?></a>
                                </h3>
                            </div>

                            <div class="row__tools">
                                <a href="?inc=examples/user_create" class="btn btn--resp"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#edit"></use></svg>Edit</a>
                            </div>

                        </div>

                    </li>
                <?php } ?>

                <li class="row actions actions--bottom">
                    <div class="row__wrapper row__wrapper--actions">
                        <input type="checkbox" id="checkall" class="row__select row__select--all">
                        <label for="checkall" class="row__select__checkbox"></label>
                        <a href="#" class="link link--primary" disabled><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#check-circle"/></svg>Publish [fr]</a>
                        <span>|</span>
                        <a href="#" class="link" disabled><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#trash-2"/></svg>Delete</a>
                    </div>
                </li>

            </ul>
        </div>
    </div>

    <div class="section">
        <div class="wrapper">
            <?php include "elements/_pagination.php"; ?>
        </div>
    </div>

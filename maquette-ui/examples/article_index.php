<header class="page-header">
    <div class="container">
        <h1>Articles <span><?php echo rand (1, 120); ?></span></h1>
        <a href="?inc=examples/user_create" class="btn btn-warning icon icon-plus"><span>New article</span></a>
        <button class="btn btn-neutral btn-rounded icon icon-resp icon-gears js-display-options"><span>options</span></button>
    </div>
</header>

<div class="split-container">

    <div class="split-aside">

        <div class="display-options">
            <div class="container">
                <form action="">
                    <div class="input">
                        <label>Category</label>
                        <select class="size-s">
                            <option>Category 01</option>
                            <option>Category 02</option>
                            <option>Category 03</option>
                        </select>
                    </div>
                    <div class="input">
                        <label>Publication</label>
                        <select class="size-s">
                            <option>Published</option>
                            <option>Upcoming</option>
                            <option>Draft</option>
                        </select>
                    </div>
                    <div class="input">
                        <label>Title</label>
                        <input type="search" value="" class="size-s">
                    </div>
                    <div class="input">
                        <label>Order by</label>
                        <select class="size-s">
                            <option>Publication date ASC</option>
                            <option>Publication date DESC</option>
                        </select>
                    </div>
                    <div class="options-actions">
                        <button class="btn">filter</button>
                        <a href="#" class="btn btn-neutral">reset</a>
                    </div>
                </form>
            </div>
        </div>

        <div class="container">
            <?php include "elements/_pagination.php"; ?>
        </div>

    </div>


    <div class="split-body">
        <div class="container">
            <ul class="list">
                <?php for ($n=1, $r=$page_amount; $n <= $r; $n++) { ?>
                <li class="row">

                    <div class="row-wrapper">
                        <div class="row-container">
                            <figure>
                            <img src="http://placehold.it/32x32" alt=""" class="checkered">
                            </figure>
                            <div class="row-inner">
                                <h3><strong>#<?php echo (($page-1)*$page_amount)+$n; ?></strong> <a href="?inc=examples/article_update"><?php echo randIpsum(1, 24, $words); ?></a></h3>
                                <h4><?php echo randIpsum(1, 24, $words); ?></h4>
                            </div>
                        </div>

                        <div class="row-tools">

                            <input type="checkbox" name="id_<?php echo $n; ?>" id="id_<?php echo $n; ?>" value="<?php echo $n; ?>" class="row-select">

                            <input type="checkbox" name="pub_fr_<?php echo $n; ?>" id="pub_fr_<?php echo $n; ?>" value="<?php echo $n; ?>" class="pub-check">
                            <label for="pub_fr_<?php echo $n; ?>">fr</label>

                            <input type="checkbox" name="pub_nl_<?php echo $n; ?>" id="pub_nl_<?php echo $n; ?>" value="<?php echo $n; ?>" class="pub-check">
                            <label for="pub_nl_<?php echo $n; ?>">nl</label>

                            <input type="checkbox" name="pub_en_<?php echo $n; ?>" id="pub_en_<?php echo $n; ?>" value="<?php echo $n; ?>" class="pub-check">
                            <label for="pub_en_<?php echo $n; ?>">en</label>

                            <button class="btn-more">more</button>
                        </div>

                    </div>

                    <div class="row-more">
                        <h5>teaser</h5>
                        <p>
                            <?php echo randIpsum(16, 64, $words); ?>
                        </p>
                        <a class="btn icon icon-resp icon-edit"><span>edit</span></a>
                        <a class="btn icon icon-resp icon-view"><span>preview</span></a>
                        <a class="btn icon icon-resp icon-copy"><span>duplicate</span></a>
                        <a class="btn btn-alert icon icon-resp icon-trash"><span>delete</span></a>
                    </div>

                </li>
                <?php } ?>
            </ul>

            <?php include "elements/_pagination.php"; ?>

        </div>
    </div>

    <div class="split-aside">
        <div class="container">

            <div class="multi-actions">
                <div class="input checkboxes">
                    <label for="all">
                        <input type="checkbox" name="all" id="all" value="all">
                        select all
                    </label>
                </div>
                <button>publish</button>
                <button>unpublish</button>
                <button>delete</button>
            </div>

        </div>
    </div>
</div>
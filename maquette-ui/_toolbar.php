<div class="toolbar">

    <div class="toolbar__brand">
        <picture>
            <source srcset="../web/assets/Admin/img/svg/viktor-logo.svg" type="image/svg+xml" />
            <img srcset="../web/assets/Admin/img/svg/viktor-logo_2x.png 2x" src="../web/assets/Admin/img/svg/viktor-logo.png" alt="Viktor" width="99" height="24">
        </picture>
    </div>

     <button class="toolbar__btn toolbar__btn--menu idle">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
            <defs>
                <rect id="def-slice" width="16" height="2" rx="1" ry="1"/>
            </defs>
            <use xlink:href="#def-slice" class="slice slice1" x="4" y="4" />
            <use xlink:href="#def-slice" class="slice slice2" x="4" y="11" />
            <use xlink:href="#def-slice" class="slice slice3" x="4" y="18" />
        </svg>
     </button>


    <div class="toolbar__website">
        <a href="#">
            <span>my_website_name</span>
            <svg class="icon">
                <use xlink:href="../web/assets/Admin/img/feather-sprite.svg#link"/>
            </svg>
        </a>
    </div>

    <form method="GET" action="./" class="toolbar__search">
        <input type="search" name="q" placeholder="Search" autocomplete="off">
        <button type="submit" class="toolbar__btn toolbar__btn--search">
            <svg class="icon">
                <use xlink:href="../web/assets/Admin/img/feather-sprite.svg#search"/>
            </svg>
        </button>
    </form>

    <div class="drop drop--user js-toggle js-toggle-transient">
        <button class="toolbar__btn toolbar__btn--user js-toggle-btn">
            <svg class="icon">
                <use xlink:href="../web/assets/Admin/img/feather-sprite.svg#user"/>
            </svg>
        </button>
        <ul class="drop__list js-toggle-target">
            <li class="drop--user__name"><span>{{username}}</span></li>
            <li><a href="#">My profile</a></li>
            <li><a href="#">Log out</a></li>
        </ul>
    </div>
</div>

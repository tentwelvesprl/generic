<header class="section section--liter">
    <div class="wrapper">
        <h1 class="title title--header">Buttons</h1>
    </div>
</header>

<div class="section">
<div class="wrapper">

<a href="#" class="btn">a.btn</a>
<a href="#" class="btn"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#<?php echo $icons[rand(1,$icons[0])]; ?>"/></svg>a.btn</a>
<button class="btn">button.btn</button>
<button class="btn"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#<?php echo $icons[rand(1,$icons[0])]; ?>"/></svg>button.btn</button>
<button class="btn" disabled><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#<?php echo $icons[rand(1,$icons[0])]; ?>"/></svg>button.btn disabled</button>
<button class="btn btn--rounded"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#<?php echo $icons[rand(1,$icons[0])]; ?>"/></svg>button.btn rounded</button>
<a href="#" class="link"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#<?php echo $icons[rand(1,$icons[0])]; ?>"/></svg>a.link</a>

<hr>

<a href="#" class="btn btn--primary">a.btn primary</a>
<a href="#" class="btn btn--primary"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#<?php echo $icons[rand(1,$icons[0])]; ?>"/></svg>a.btn primary</a>
<button class="btn btn--primary">button.btn primary</button>
<button class="btn btn--primary"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#<?php echo $icons[rand(1,$icons[0])]; ?>"/></svg>button.btn primary</button>
<button class="btn btn--primary btn--resp"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#<?php echo $icons[rand(1,$icons[0])]; ?>"/></svg>button.btn primary responsive</button>
<button class="btn btn--primary btn--rounded"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#<?php echo $icons[rand(1,$icons[0])]; ?>"/></svg>button.btn primary rounded</button>
<a href="#" class="link link--primary"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#<?php echo $icons[rand(1,$icons[0])]; ?>"/></svg>a.link primary</a>

<hr>

<a href="#" class="btn btn--large">a.btn large</a>
<a href="#" class="btn btn--large"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#<?php echo $icons[rand(1,$icons[0])]; ?>"/></svg>a.btn large</a>
<button class="btn btn--large">button.btn large</button>
<button class="btn btn--large"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#<?php echo $icons[rand(1,$icons[0])]; ?>"/></svg>button.btn large</button>
<button class="btn btn--large btn--rounded"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#<?php echo $icons[rand(1,$icons[0])]; ?>"/></svg>button.btn large rounded</button>
<a href="#" class="link link--large"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#<?php echo $icons[rand(1,$icons[0])]; ?>"/></svg>a.link large</a>

<hr>

<a href="#" class="btn btn--large btn--primary">a.btn primary large</a>
<a href="#" class="btn btn--large btn--primary"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#<?php echo $icons[rand(1,$icons[0])]; ?>"/></svg>a.btn primary large</a>
<button class="btn btn--large btn--primary">button.btn primary large</button>
<button class="btn btn--large btn--primary"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#<?php echo $icons[rand(1,$icons[0])]; ?>"/></svg>button.btn primary large</button>
<button class="btn btn--large btn--primary btn--rounded"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#<?php echo $icons[rand(1,$icons[0])]; ?>"/></svg>button.btn primary large rounded</button>
<a href="#" class="link link--large link--primary"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#<?php echo $icons[rand(1,$icons[0])]; ?>"/></svg>a.link primary large</a>

<hr>
<?php
foreach ($icons as $k => $icon) {
    if ($k < 1) continue;
    ?>
    <button class="btn btn--large"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#<?php echo $icon; ?>"/></svg><?php echo $icon; ?></button>
    <button class="btn btn--large btn--rounded"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#<?php echo $icon; ?>"/></svg><?php echo $icon; ?></button>
    <button class="btn"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#<?php echo $icon; ?>"/></svg><?php echo $icon; ?></button>
    <button class="btn btn--rounded"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#<?php echo $icon; ?>"/></svg><?php echo $icon; ?></button>
    <a href="#" class="link link--large"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#<?php echo $icon; ?>"/></svg><?php echo $icon; ?></a><br>
<?php } ?>

</div>
</div>

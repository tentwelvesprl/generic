<div class="row__wrapper">

    <input type="checkbox" name="id_<?php echo $n; ?>" id="id_<?php echo $n; ?>" value="<?php echo $n; ?>" class="row__select row__select--one">
    <label for="id_<?php echo $n; ?>"></label>

    <div class="row__header">

        <div class="row__id">#<?php echo (($page-1)*$page_amount)+$n; ?></div>
        <h3 class="row__title"><a href="?inc=templates/edit"><strong><?php echo randIpsum(2, 24, $words); ?></strong><?php if(rand(0, 2)) { ?><br><?php echo randIpsum(2, 8, $words); ?><?php } ?></a></h3>
        <?php if(rand(0, 2)) { ?><h4 class="row__hint"><?php echo randIpsum(2, 16, $words); ?></h4><?php } ?>
    </div>

    <div class="row__thumbnail">
        <?php if(rand(0, 2)) { ?>
            <figure class="thumbnail">
                <img src="https://placekitten.com/<?php echo rand(2,4)*100; ?>/<?php echo rand(2,4)*100; ?>" alt="" class="checkered">
            </figure>
        <?php } ?>
    </div>

    <div class="row__tools">

        <a href="#" class="link"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#copy"/></svg>New child</a>
        <a href="#" class="link"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#trash-2"/></svg>Delete</a>

        <div class="row__status">
            <span class="status status--yep active">fr</span>
            <span class="status status--later"></span>
            <span class="status status--yep"></span>
            <span class="status status--nope"></span>
        </div>

    </div>

</div>

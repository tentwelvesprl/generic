<?php
if (!isset($page)) {
	if (isset($_GET['page'])) $page = $_GET['page']; else $page = 1;
}
if (!isset($max)) $max = $page + rand(0,10);
$page_amounts = array(10, 20, 50);
if (!isset($page_amount)) $page_amount = $page_amounts[rand(0, 2)];
?>

<nav class="pagination">
	<div class="pager-position">
		<?php echo ($page-1)*$page_amount .'-' . $page*$page_amount .' / ' . $max*$page_amount; ?>
	</div>
	<div class="pager-amount">
		<?php
		$pa = array();
		foreach ($page_amounts as $p) {
			if ($p==$page_amount) {
				$pa[] =  '<span>'.$p.'</span>';
			} else {
				$pa[] =  '<a href="#">'.$p.'</a>';
			}
		}
		echo implode(' • ', $pa);
		?>
	</div>
	<ul class="pager-list">
		<?php if ($page != 1) {
			echo '<li class="pager-arrow prev"><a href="?inc='.$inc.'&amp;page='. ($page - 1) .'">&larr;</a></li>';
		} ?>
		<?php
		for ($i=1; $i<=$max; $i++) {
			if ($i == $page) {
				echo '<li><span>'. $i .'</span></li>';
			} else if ($i == 3 && $page > 4) {
				echo '<li><select>
				<option>…</option>';
				for ($n=$i; $n<$page; $n++) {
					echo '<option value="'.$n.'">'.$n.'</option>';
				}
				echo '</select></li>';
				$i = $n-1;
			} else if ($i == $page+1 && $page+3 < $max) {
				echo '<li><select>
				<option>…</option>';
				for ($n=$i; $n<=$max-2; $n++) {
					echo '<option value="'.$n.'">'.$n.'</option>';
				}
				echo '</select></li>';
				$i = $n-1;
			} else {
				echo '<li><a href="?inc='.$inc.'&amp;page='. $i .'">'. $i .'</a></li>';
			}
		} ?>
		<?php if ($page != $max) {
			echo '<li class="pager-arrow next"><a href="?inc='.$inc.'&amp;page='. ($page + 1) .'">&rarr;</a></li>';
		} ?>

	</ul>
</nav>

<fieldset>
    <legend>Address Field</legend>

    <div class="input input-80 alpha">
        <label for="streetAddress">street address <u>*</u></label>
        <input name="streetAddress" id="streetAddress" type="text" autocomplete="no" required data-valid="notEmpty" maxlength="255">
    </div>

    <div class="input input-20 omega">
        <label for="numberAddress">number <u>*</u></label>
        <input name="numberAddress" id="numberAddress" type="text" autocomplete="text" required data-valid="notEmpty" maxlength="10">
    </div>

    <div class="input"> 
        <label for="additionaLine">additional line</label>
        <input name="additionaLine" id="additionaLine" type="text" autocomplete="no" maxlength="255">
    </div>

    <div class="input input-25 alpha">
        <label for="inputPostcode">postcode <u>*</u></label>
        <input name="inputPostcode" id="inputPostcode" type="text" autocomplete="postal-code" required data-valid="notEmpty validPostcode">
    </div>

    <div class="input input-75 omega">
    <div class="i18n-toggle i18n-show" data-language="fr">
        <label for="city_fr">city [fr] <u>*</u></label>
        <input name="city_fr" id="city_fr" type="text" autocomplete="text" data-valid="notEmpty">
        </div>

        <div class="i18n-toggle i18n" data-language="nl">
        <label for="city_nl">city [nl] <u>*</u></label>
        <input name="city_nl" id="city_nl" type="text" autocomplete="text" data-valid="notEmpty">
        </div>

        <div class="i18n-toggle i18n" data-language="en">
        <label for="city_en">city [en] <u>*</u></label>
        <input name="city_en" id="city_en" type="text" autocomplete="text" data-valid="notEmpty">
        </div>
    </div>

    <div class="input">
        <label for="Country">Country</label>
        <select name="Country" id="Country" required data-valid="notEmpty">
            <option value="BE">Belgium</option>
            <option value="FR">France</option>
            <option value="NL">Nederlands</option>
            <option value="UK">United Kingdom</option>
            <option value="ES">Spain</option>
        </select>
    </div>

    <div class="input input-20 alpha">
        <label for="Lat">latitude</label>
        <input name="Lat" id="Lat" type="text" data-valid="geoCoords">
    </div>
    <div class="input input-20">
        <label for="Lng">longitude</label>
        <input name="Lng" id="Lng" type="text" data-valid="geoCoords">
    </div>
    <div class="input input-60 omega">
        <label>Google map</label>
        <button class="btn geo-btn">get coordinates from address</button>
        <a href="#" class="btn map-btn">view map</a>
    </div>

</fieldset>

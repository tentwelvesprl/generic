

<fieldset>
    <legend>Publication</legend>
    <div class="pub-status">
        <div class="pub-label">
            <input type="checkbox" name="pub_label_fr" id="pub_label_fr" value="fr" class="pub-check" checked>
            <label for="pub_label_fr">fr</label>
        </div>
        <div class="pub-date">
            <label>from</label>
            <input type="datetime-local" class="size-s">
        </div>
        <div class="pub-date">
            <label>to</label>
            <input type="datetime-local" class="size-s">
        </div>
    </div>

    <div class="pub-status">
        <div class="pub-label">
            <input type="checkbox" name="pub_label_nl" id="pub_label_nl" value="nl" class="pub-check">
            <label for="pub_label_nl">nl</label>
        </div>
        <div class="pub-date">
            <label>from</label>
            <input type="datetime-local" class="size-s">
        </div>
        <div class="pub-date">
            <label>to</label>
            <input type="datetime-local" class="size-s">
        </div>
    </div>

    <div class="pub-status">
        <div class="pub-label">
            <input type="checkbox" name="pub_label_en" id="pub_label_en" value="en" class="pub-check scheduled">
            <label for="pub_label_en">en</label>
        </div>
        <div class="pub-date">
            <label>from</label>
            <input type="datetime-local" class="size-s">
        </div>
        <div class="pub-date">
            <label>to</label>
            <input type="datetime-local" class="size-s">
        </div>
    </div>

</fieldset>
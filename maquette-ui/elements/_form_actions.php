
            <div class="form-actions">

                <a href="?inc=examples/article_index" class="btn btn-neutral icon icon-back"><span>Back to list</span></a>
                <button type="submit" name="submit" value="Save" class="btn btn-submit btn-warning icon icon-save"><span>Save</span></button>
                <div class="rec-infos">
                    <dl>
                        <dt>Creation</dt> <dd><time>dd:mm:yy hh:mm</time> <span>by {{username}}</span></dd>
                    </dl>
                    <dl>
                        <dt>Last modification</dt> <dd><time>dd:mm:yy hh:mm</time> <span>by {{username}}</span></dd>
                    </dl>
                </div>
            </div>
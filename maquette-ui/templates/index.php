<header class="section header">
    <div class="wrapper">
        <h1 class="title title--header">Content unit</h1>
        <a href="?inc=templates/" class="btn btn--primary btn--large btn--resp"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#plus"></use></svg>New Content Unit</a>
    </div>
</header>

<div class="section">

    <div class="wrapper">

        <div class="block block--tools">

            <div class="active-filters">
                <h6 class="active-filters__title">Active search filters:</h6>
                <dl class="active-filters__list">
                    <dt>Date:</dt>
                    <dd>from 11.03.2020</dd>
                   <dt>Size:</dt>
                    <dd>Medium</dd>
                </dl>
            </div>

            <form method="GET" action="#" class="sort-options">
                <label>Sort by</label>
                <select>
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                </select>
                <select>
                    <option><small>ASC</small></option>
                    <option><small>DESC</small></option>
                </select>
            </form>

            <button class="btn filter-btn js-toggle-btn" data-target="#index_filters"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#filter"></use></svg>Refine list...</button>

            <form method="GET" action="#" class="filters js-toggle-target" id="index_filters">
                <div class="input input-50">
                    <label>Title (en)</label>
                    <input type="text" name="activity_en_title_en_search" value="">
                </div>

                <div class="input input-50">
                    <label>Subtitle (en)</label>
                    <input type="text" name="activity_en_subtitle_en_search" value="">
                </div>

                <div class="input input-50">
                    <label>Performer name, company name (en)</label>
                    <input type="text" name="activity_en_performer_en_search" value="">
                </div>

                <div class="input input-50">
                    <label>Teaser (en)</label>
                    <input type="text" name="activity_en_teaser_en_search" value="">
                </div>

                <div class="input input-50">
                    <label>Headline (en)</label>
                    <input type="text" name="activity_en_headline_en_search" value="">
                </div>

                <div class="input input-50">
                    <label>Body (en)</label>
                    <input type="text" name="activity_en_body_en_search" value="">
                </div>

                <div class="input input-50">
                    <label>Credits (en)</label>
                    <input type="text" name="activity_en_additional_info_en_search" value="">
                </div>

                <div class="input input-50">
                    <label>Season</label>
                    <select name="activity_en_seasons_search">
                        <option value="" selected="">Any</option>
                        <option value="season:83"> Untitled season #83</option>
                        <option value="season:1"> Season 19-20</option>
                    </select>
                </div>

                <div class="input input-50">
                    <label>Categories</label>
                    <select name="activity_en_categories_search">
                        <option value="" selected="">Any</option>
                    </select>
                </div>

                <div class="input input-50">
                    <label>Publication status</label>
                    <select name="en_publication_search">
                        <option value="" selected="">Any</option>
                        <option value="published_only">Published only</option>
                        <option value="not_published_only">Not published only</option>
                    </select>
                </div>

                <div class="input input--actions">
                    <a class="link" href="#">Clear options</a>
                    <button type="submit" name="submit" value="Filter" class="btn btn--primary">Refine list...</button>
                </div>

            </form>

        </div>

        <ul class="list">

            <li class="row actions actions--top">
                <div class="row__wrapper row__wrapper--actions">
                    <input type="checkbox" id="checkall" class="row__select row__select--all">
                    <label for="checkall" class="row__select__checkbox"></label>
                    <a href="#" class="link link--primary" disabled><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#check-circle"/></svg>Publish [fr]</a>
                    <span>|</span>
                    <a href="#" class="link" disabled><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#trash-2"/></svg>Delete</a>
                </div>
            </li>

            <?php for ($n=1; $n <= 20; $n++) { ?>

                <li class="row">

                    <div class="row__wrapper">

                        <input type="checkbox" name="id_<?php echo $n; ?>" id="id_<?php echo $n; ?>" value="<?php echo $n; ?>" class="row__select row__select--one">
                        <label for="id_<?php echo $n; ?>" class="row__select__checkbox"></label>

                        <div class="row__header">
                            <div class="row__badge row__badge--dark" style="background-color: purple;"><?php if(rand(0, 2)) { ?><span><?php echo randIpsum(1, 1, $words); ?></span><?php } ?></div>
                            <div class="row__id">#<?php echo (($page-1)*$page_amount)+$n; ?></div>
                            <h3 class="row__title"><a href="?inc=templates/edit"><strong><?php echo randIpsum(2, 24, $words); ?></strong><?php if(rand(0, 2)) { ?><br><?php echo randIpsum(2, 8, $words); ?><?php } ?></a></h3>
                            <?php if(rand(0, 2)) { ?><h4 class="row__hint"><?php echo randIpsum(2, 16, $words); ?></h4><?php } ?>
                        </div>

                        <div class="row__thumbnail">
                            <?php if(rand(0, 2)) { ?>
                                <figure class="thumbnail">
                                    <img src="https://placekitten.com/<?php echo rand(2,4)*100; ?>/<?php echo rand(2,4)*100; ?>" alt="" class="checkered">
                                </figure>
                            <?php } ?>
                        </div>

                        <div class="row__tools">

                            <div class="row__status">
                                <span class="status status--yep status--active">fr</span>
                                <span class="status status--later"></span>
                                <span class="status status--yep"></span>
                                <span class="status status--nope"></span>
                                <span class="status status--nope"></span>
                                <span class="status status--nope"></span>
                            </div>

                            <button class="row__more-btn link link--large js-toggle-btn" data-target="#row_more_<?php echo $n; ?>"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#chevron-down"/></svg></button>

                        </div>

                    </div>

                    <div class="row__more">
                        <div class="row__inner js-toggle-target" id="row_more_<?php echo $n; ?>">
                            <div class="row__description">
                                <p><?php echo randIpsum(6, 64, $words); ?></p>
                            </div>
                            <a href="?inc=templates/edit" class="btn btn--primary btn--resp"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#edit"/></svg>Edit</a>
                            <a href="#" class="btn btn--resp"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#eye"/></svg>Preview</a>
                            <a href="#" class="btn btn--resp"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#copy"/></svg>Duplicate</a>
                            <a href="#" class="link"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#trash-2"/></svg>Delete</a>
                        </div>
                    </div>

                </li>
            <?php } ?>

            <li class="row actions actions--bottom">
                <div class="row__wrapper row__wrapper--actions">
                    <input type="checkbox" id="checkall" class="row__select row__select--all">
                    <label for="checkall" class="row__select__checkbox"></label>
                    <a href="#" class="link link--primary" disabled><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#check-circle"/></svg>Publish [fr]</a>
                    <span>|</span>
                    <a href="#" class="link" disabled><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#trash-2"/></svg>Delete</a>
                </div>
            </li>

        </ul>
    </div>
</div>

<div class="section">
    <div class="wrapper">
        <?php include "elements/_pagination.php"; ?>
    </div>
</div>

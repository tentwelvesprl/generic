<header class="page-header">
    <div class="container">
        <h1>Form</h1>
        <ul class="langs js-i18n-switch">
            <li><button class="active" data-language="fr">fr</button></li>
            <li><button data-language="nl">nl</button></li>
            <li><button data-language="en">en</button></li>
            <li><button data-language="all">all</button></li>
        </ul>
    </div>
</header>


<form action="#">


<div class="form-container">
<div class="container">

    <div class="msg msg--error">
        <svg class="icon">
            <use xlink:href="../web/assets/Admin/img/feather-sprite.svg#alert-octagon"/>
        </svg>
        <strong>titre du message d’erreur</strong><br>
        <?php echo substr($lorem, 0, rand(20,80)); ?> <a href="#">link</a>
        <span class="close close-msg">×</span>
    </div>

    <div class="msg msg--warning">
        <svg class="icon">
            <use xlink:href="../web/assets/Admin/img/feather-sprite.svg#alert-triangle"/>
        </svg>
        <strong>titre du message de confirmation</strong> <?php echo substr($lorem, 0, rand(20,80)); ?> <a href="#">link</a>
        <span class="close close-msg">×</span>
    </div>

    <div class="msg msg--success">
        <svg class="icon">
            <use xlink:href="../web/assets/Admin/img/feather-sprite.svg#star"/>
        </svg>
        <strong>titre du message d’aide</strong> <?php echo substr($lorem, 0, rand(20,80)); ?> <a href="#">link</a>
        <span class="close close-msg">×</span>
    </div>

</div>
</div>


<footer class="form-footer container">

    <div class="form-actions">

    <?php include "elements/_status.php"; ?>

        <a href="?inc=examples/article_index" class="btn icon icon-back"><span>Back to list</span></a>
        <input type="submit" name="submit" value="Save" class="btn btn-warning">
        <div class="rec-infos">
            <dl>
                <dt>Creation</dt> <dd><time>dd:mm:yy hh:mm</time> <span>by {{title}}</span></dd>
            </dl>
            <dl>
                <dt>Last modification</dt> <dd><time>dd:mm:yy hh:mm</time> <span>by {{title}}</span></dd>
            </dl>
        </div>



        <input type="checkbox" name="publish_fr_<?php echo $n; ?>" id="publish_fr_<?php echo $n; ?>" value="<?php echo $n; ?>" class="publish-checkbox">
        <label for="publish_fr_<?php echo $n; ?>">fr</label>
        <input type="checkbox" name="publish_nl_<?php echo $n; ?>" id="publish_nl_<?php echo $n; ?>" value="<?php echo $n; ?>" class="publish-checkbox">
        <label for="publish_nl_<?php echo $n; ?>">nl</label>
        <input type="checkbox" name="publish_en_<?php echo $n; ?>" id="publish_en_<?php echo $n; ?>" value="<?php echo $n; ?>" class="publish-checkbox">
        <label for="publish_en_<?php echo $n; ?>">en</label>

    </div>

</footer>

</form>

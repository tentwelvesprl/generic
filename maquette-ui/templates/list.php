<div class="container">
    <ul class="list">
        <?php for ($n=1, $r=rand(6, 24); $n <= $r; $n++) { ?>

        <li class="row">

            <div class="row-wrapper sort-row">
                <div class="sort-handle">h</div>
                <div class="row-container">
                    <?php if(rand(0, 2)) { ?>
                    <figure class="thumbnail">
                        <img src="http://placehold.it/32x32" alt="" class="checkered">
                    </figure>
                    <?php } ?>


                    <div class="row-inner">
                        <h3><strong>#<?php echo (($page-1)*$page_amount)+$n; ?></strong> <a href="?inc=examples/article_update"><?php echo randIpsum(1, 24, $words); ?></a></h3>
                        <h4><?php echo randIpsum(1, 24, $words); ?></h4>
                    </div>

                </div>

                <div class="row-tools">

                    <input type="checkbox" name="id_<?php echo $n; ?>" id="id_<?php echo $n; ?>" value="<?php echo $n; ?>" class="row-select">

                    <input type="checkbox" name="pub_fr_<?php echo $n; ?>" id="pub_fr_<?php echo $n; ?>" value="<?php echo $n; ?>" class="pub-check">
                    <label for="pub_fr_<?php echo $n; ?>">fr</label>

                    <input type="checkbox" name="pub_nl_<?php echo $n; ?>" id="pub_nl_<?php echo $n; ?>" value="<?php echo $n; ?>" class="pub-check">
                    <label for="pub_nl_<?php echo $n; ?>">nl</label>

                    <input type="checkbox" name="pub_en_<?php echo $n; ?>" id="pub_en_<?php echo $n; ?>" value="<?php echo $n; ?>" class="pub-check">
                    <label for="pub_en_<?php echo $n; ?>">en</label>

                    <button class="btn-more">more</button>
                </div>
            </div>

            <div class="row-more">
                <?php if(rand(0, 2)) { ?>
                <figure>
                    <img src="http://placehold.it/576x576" alt="" class="checkered">
                </figure>
                <?php } ?>
                <h5>teaser</h5>
                <p>
                    <?php echo randIpsum(6, 64, $words); ?>
                </p>
                <a class="btn icon icon-resp icon-edit"><span>edit</span></a>
                <a class="btn icon icon-resp icon-view"><span>preview</span></a>
                <a class="btn icon icon-resp icon-copy"><span>duplicate</span></a>
                <a class="btn btn-alert icon icon-resp icon-trash"><span>delete</span></a>
            </div>

        </li>
        <?php } ?>
    </ul>

    <?php include "elements/_pagination.php"; ?>
</div>
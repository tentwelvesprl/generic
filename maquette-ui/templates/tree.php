<header class="section header">
    <div class="wrapper">
        <h1 class="title title--header">Content unit</h1>
        <a href="?inc=templates/" class="btn btn--primary btn--resp"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#plus"></use></svg>New Content Unit</a>
    </div>
</header>

<div class="section">

    <div class="wrapper">

        <ol class="sortable">
            <?php for ($n=1; $n <= 10; $n++) { ?>
                <li class="row">
                    <div class="link sortable__handle"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#align-justify"/></svg></div>
                    <?php include "elements/_node.php"; ?>
                    <ol></ol>
                </li>
            <?php } ?>
        </ol>

        <ul class="list">
            <li class="row actions actions--bottom">
                <div class="row__wrapper row__wrapper--actions">
                    <a href="#" class="link link--primary"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#check-circle"/></svg>Publish [fr]</a>
                    <span>|</span>
                    <a href="#" class="link"><svg class="icon"><use xlink:href="../web/assets/Admin/img/feather-sprite.svg#trash-2"/></svg>Delete</a>
                </div>
            </li>
        </ul>

    </div>
</div>

<div class="section">
    <div class="wrapper">
        <?php include "elements/_pagination.php"; ?>
    </div>
</div>

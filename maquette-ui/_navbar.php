<nav class="navbar">

    <div class="navbar__overlay"></div>

    <div class="navbar__container">

        <div class="navbar__langs">
            <svg class="icon">
                <use xlink:href="../web/assets/Admin/img/feather-sprite.svg#flag"/>
            </svg>
            <ul>
                <li><span>fr</span></li>
                <li><a href="#">nl</a></li>
                <li><a href="#">de</a></li>
                <li><a href="#">en</a></li>
            </ul>
        </div>

        <ul class="navbar__menu">
            <?php foreach ($CONFIG['tree'] as $k1 => $v1) {
                if (isset($v1[2]) && is_array($v1[2])) {
                    $output = '<ul>';
                    $active  = false;
                    foreach ($v1[2] as $k2 => $v2) {
                        if ($v2 == $inc) {
                            $output .= '<li><a href="?inc='.$v2.'" class="active">'.$k2.'</a></li>';
                            $active  = true;
                        } else {
                            $output .= '<li><a'. ((!empty($v2))?' href="?inc='.$v2.'"':'').'>'.$k2.'</a></li>';
                        }
                    }
                    $output .= '</ul>';
                    echo '<li class="fold'. (($active)?' active':'') .'">
                    <svg class="icon">
                        <use xlink:href="../web/assets/Admin/img/feather-sprite.svg#'.$v1[1].'"/>
                    </svg>
                    <button class="toggle-fold">'.$k1.'</button>'.$output.'</li>';
                } else  {
                    echo '<li' . (($v1[0] == $inc)?' class="active"':'') .'>
                    <svg class="icon">
                        <use xlink:href="../web/assets/Admin/img/feather-sprite.svg#'.$v1[1].'"/>
                    </svg>
                    <a'. ((!empty($v1[0]))?' href="?inc='.$v1[0].'"':'') . (($v1[0] == $inc)?' class="active"':'') .'>'.$k1.'</a></li>';
                }

            } ?>
        </ul>

    </div>

</nav>

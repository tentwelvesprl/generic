<?php
if (isset($_GET['file'])) $file = $_GET['file'];
if (isset($_GET['width'])) $w = $_GET['width'];
if (isset($_GET['height'])) $h = $_GET['height'];
if (isset($_GET['mode'])) $mode = $_GET['mode']; else $mode = 'fit_to_width';
if (isset($_GET['type'])) $type = $_GET['type']; else $type = 'jpeg';
if (isset($_GET['q'])) $q = $_GET['q']; else $q = 90;

include('SimpleImage.php');

try {
	$img = new mockup\SimpleImage('../'.$file);
	$img->auto_orient();
	switch ($mode) {
		case 'fit':
		$img->best_fit($w, $h);
		break;
		case 'fit_to_width':
		$img->fit_to_width($w);
		break;
		case 'fit_to_height':
		$img->fit_to_height($h);
		break;
		case 'resize_around_pivot_point':
		$img->thumbnail($w, $h);
		break;
	}
	if (isset($_GET['blur']) && $_GET['blur']) $img->blur('gaussian', $_GET['blur']);
	if (isset($_GET['desaturate']) && $_GET['desaturate']) $img->desaturate();
	if (isset($_GET['pixelate']) && $_GET['pixelate']) $img->pixelate($_GET['pixelate']);
	if (isset($_GET['color']) && $_GET['color']) $img->colorize($_GET['color'], .5);
	$img->output($type, $q);
} catch(Exception $e) {
	echo 'Error: ' . $e->getMessage();
}

?>
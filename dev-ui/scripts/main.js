var $w = $(window),
    ww = $w.width();

var wwCalc = function() {
    $w.on('resize', $.debounce(100, function() {
        ww = $(this).width();
    }));
};

var validateForm = function() {
    $('form').find('input[data-valid]').each(function(e, i) {
        var $input = $(this),
            validationRules = $input.data('valid').split(' ');
        $input.on('blur keyup change', function() {
            var test = true,
                val = $input.val();
            $.each(validationRules, function(id, functionName) {
                if (window[functionName](val) !== true) {
                    test = false;
                }
            });
            if (test) {
                $input.closest('.input').removeClass('error');
            } else {
                $input.closest('.input').addClass('error');
            }
        });
    });
};

var maxlengthCounter = function() {
    $('form').find('[maxlength]').maxlength({
        alwaysShow: true,
        threshold: 5,
        warningClass: '',
        limitReachedClass: 'limit',
        showMaxLength: false,
        showCharsTyped: false,
        appendToParent: true,
        placement: 'top-right',
    });
};

var notEmpty = function(val) {
    return (/\S/.test(val));
};

var validPhone = function(val) {
    return (val === '' || /^\+?(?:[0-9.\-\(\)] ?){6,24}[0-9]$/.test(val));
};

var validPostcode = function(val) {
    return (val === '' || /^[0-9]{4,5}$/.test(val));
};

var validInteger = function(val) {
    return (val === '' || /^[0-9]{1,5}$/.test(val));
};

var validDate = function(val) {
    return (val === '' || /^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/.test(val));
};

var validEmail = function(val) {
    var sQtext = '[^\\x0d\\x22\\x5c\\x80-\\xff]';
    var sDtext = '[^\\x0d\\x5b-\\x5d\\x80-\\xff]';
    var sAtom = '[^\\x00-\\x20\\x22\\x28\\x29\\x2c\\x2e\\x3a-\\x3c\\x3e\\x40\\x5b-\\x5d\\x7f-\\xff]+';
    var sQuotedPair = '\\x5c[\\x00-\\x7f]';
    var sDomainLiteral = '\\x5b(' + sDtext + '|' + sQuotedPair + ')*\\x5d';
    var sQuotedString = '\\x22(' + sQtext + '|' + sQuotedPair + ')*\\x22';
    var sDomain_ref = sAtom;
    var sSubDomain = '(' + sDomain_ref + '|' + sDomainLiteral + ')';
    var sWord = '(' + sAtom + '|' + sQuotedString + ')';
    var sDomain = sSubDomain + '(\\x2e' + sSubDomain + ')*';
    var sLocalPart = sWord + '(\\x2e' + sWord + ')*';
    var sAddrSpec = sLocalPart + '\\x40' + sDomain; // complete RFC822 email address spec
    var sValidEmail = '^' + sAddrSpec + '$'; // as whole string
    var reValidEmail = new RegExp(sValidEmail);
    return (val === '' || reValidEmail.test(val));
};

var validColor = function(val) {
    return (val === '' || /^#?([a-f0-9]{6}|[a-f0-9]{3})$/.test(val));
};

var validUrl = function(val) {
    return (val === '' || /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/.test(val));
};

var urlInput = function() {
    $('form').find('.input-ext-url').on('click', 'button', function(e) {
        e.preventDefault();
        var $wrapper = $(this).closest('.input-ext-url'),
            val = $wrapper.find('input').val();
        if (val !== '' && !$wrapper.hasClass('error')) {
            window.open(val, '_blank');
        }
    });
};

var submitWarning = function() {
    $('form').each(function() {
        var $form = $(this);
        if ($form.find('[name=id]').val() > 0) {
            $form.find('.btn-submit').removeClass('btn-warning').addClass('btn-ok').find('span').text('Saved');
            $form.on('change', ':input', function() {
                if (!$form.hasClass('unsaved-changes')) {
                    $form.addClass('unsaved-changes').find('.btn-submit').removeClass('btn-ok').addClass('btn-warning').find('span').text('Save');
                }
            });
        }
    });
};

var closeMsg = function() {
    $('.msg').on('click', '.close-msg', function(e) {
        e.preventDefault();
        $(this).closest('.msg').slideUp('fast');
    });
};

var toolbarSearch = function() {
    var $ts = $('.toolbar__search'),
        $tbi = $ts.find('input');
    $tbi.on('focus', function(e) {
        $ts.addClass('focus');
    }).on('blur', function(e) {
        $ts.removeClass('focus');
    });
    $ts.on('click', '.toolbar__btn--search', function(e) {
        if (!$ts.hasClass('focus')) {
            e.preventDefault();
            $ts.addClass('focus');
            setTimeout(function() {
                $tbi.focus();
            }, 500);
        }
    });
};

var toggleNav = function() {
    var $nb = $('.navbar'),
        $mc = $('.main-container'),
        $tbm = $('.toolbar__btn--menu');
    $('.toolbar').on('click', '.toolbar__btn--menu', function(e) {
        e.preventDefault();
        $tbm.toggleClass('idle');
        $nb.toggleClass('unfold');
        $mc.toggleClass('pushed').closest('body').toggleClass('no-scroll');
    });
    $nb.on('click', '.navbar__overlay', function(e) {
        $nb.removeClass('unfold');
        $tbm.addClass('idle');
        $mc.removeClass('pushed').closest('body').removeClass('no-scroll');
    });
};

var toggleBtn = function() {

    var $jt = $('.js-toggle');
    var $jtDatagroup = $('.js-toggle-datagroup');
    var $jtTransient = $('.js-toggle-transient');
    var $jtBtn = $('.js-toggle-btn');
    var $jtTarget = $('.js-toggle-target');

    if (typeof sessionStorage !== 'undefined') {
        $jtDatagroup.each(function() {
            var toggleId = 'datagroup-toggle-' + $(this).attr('data-section-name');
            if (sessionStorage.getItem(toggleId) !== null) {
                if (sessionStorage.getItem(toggleId) === '1') {
                    $(this).find('.js-toggle-btn').addClass('active');
                    $(this).find('.js-toggle-target').css('display', 'block');
                } else {
                    $(this).find('.js-toggle-btn').removeClass('active');
                    $(this).find('.js-toggle-target').css('display', 'none');
                }
            }
        });
    }

    $jtBtn.on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        var $this = $(this);
        var target = $this.data('target');
        if (typeof target !== 'undefined') {
            $this.blur().toggleClass('active');
            $(target).slideToggle(250);
        } else {
            $this.blur().toggleClass('active').parent().find('.js-toggle-target').slideToggle(250);
        }
        if (typeof sessionStorage !== 'undefined' && typeof $this.attr('data-section-name') !== 'undefined') {
            $jtDatagroup.each(function() {
                var toggleId = 'datagroup-toggle-' + $(this).attr('data-section-name');
                var active = ($(this).find('.js-toggle-btn').hasClass('active') ? '1' : '0');
                sessionStorage.setItem(toggleId, active);
            });
        }
    });

    $jtTarget.on('click', function(e) {
        e.stopPropagation();
    });

    $w.on('click', function() {
        $jtTransient.find('.js-toggle-btn').removeClass('active');
        $jtTransient.find('.js-toggle-target').slideUp(250);
    });

};

var i18nSwitch = function() {

    var $is = $('.js-i18n-switch'),
        $it = $('.i18n-toggle'),
        $this, lng;

    if (typeof sessionStorage !== 'undefined') {
        $is.find('button').each(function() {
            $this = $(this);
            var lng = $(this).attr('data-language');
            var toggleId = 'i18n-toggle-' + lng;
            if (sessionStorage.getItem(toggleId) !== null) {
                if (sessionStorage.getItem(toggleId) === '1') {
                    $(this).addClass('active');
                    $it.filter('[data-language=' + lng + ']').addClass('i18n-show');
                } else {
                    $(this).removeClass('active');
                    $it.filter('[data-language=' + lng + ']').removeClass('i18n-show');
                }
                var $ib = $this.closest('.js-i18n-switch').find('button.active');
                $ib.prop('disabled', ($ib.length === 1));
            }
        });
    }

    $is.on('click', 'button', function(e) {
        e.preventDefault();
        $this = $(this);
        lng = $this.data('language');
        $this.toggleClass('active').blur();
        $it.filter('[data-language=' + lng + ']').toggleClass('i18n-show');
        var $ib = $this.closest('.js-i18n-switch').find('button.active');
        $ib.prop('disabled', ($ib.length === 1));
        if (typeof sessionStorage !== 'undefined') {
            $is.find('button').each(function() {
                var toggleId = 'i18n-toggle-' + $(this).attr('data-language');
                var active = ($(this).hasClass('active') ? '1' : '0');
                sessionStorage.setItem(toggleId, active);
            });
        }
    });

};

var helpTooltip = function() {
    $('.help-wrapper').on('click', 'button', function(e) {
        e.preventDefault();
    });
};
//
// var rowMore = function() {
// 	$('.row').on('click', '.row__more-btn', function(e) {
// 		e.preventDefault();
// 		$(this).toggleClass('active').closest('.row').find('.row__more').toggleClass('active').find('.row__inner').slideToggle(250);
// 	});
// };

var checkActions = function() {
    var $row = $('.row');
    var $actions = $('.actions').find('a, button');
    var $checkboxes = $row.find('.row__select');
    var $checkone = $row.find('.row__select--one');
    var $checkall = $row.find('.row__select--all');
    var totalCheckboxes = $checkone.length;
    var checkCheck = function() {
        var totalChecked = $checkone.filter(':checked').length;
        $actions.attr('disabled', (totalChecked === 0));
        $checkall.prop('checked', (totalChecked === totalCheckboxes));
    };
    $row.on('click', '.row__select--one', function(e) {
        checkCheck();
    }).on('click', '.row__select--all', function(e) {
        $checkboxes.prop('checked', $checkall.prop('checked'));
        checkCheck();
    });
};

var sortRows = function() {

    var elements = document.querySelectorAll('.js-sortable');
    Array.prototype.forEach.call(elements, function(el, i) {
        var group = el.dataset.group;
        var sortable = new Sortable(el, {
            group: group,
            handle: '.row__handle',
            ghostClass: 'row--ghost',
            draggable: '.row',
            animation: 150,
            fallbackOnBody: true,
            swapThreshold: 0.75
        });
    });
};

var sortNestedRows = function() {

    var nestedSortables = [].slice.call(document.querySelectorAll('.js-nested-sortable'));

    var serialize = function () {
        var serialized = [];
        var nesteds = [].slice.call(document.querySelectorAll('.js-nested-sortable'));
        for (var n = 0; n < nesteds.length; n++) {
            var nested = [];
            var children = [].slice.call(nesteds[n].children);
            for (var c = 0; c < children.length; c++) {
                nested[c] = children[c].dataset.id;
            }
            serialized.push({
              id: nesteds[n].dataset.ul,
              children: nested
            });
        }
        document.getElementById('tree_structure').value = JSON.stringify(serialized);
    };

    // Loop through each nested sortable element
    for (var i = 0; i < nestedSortables.length; i++) {
        Sortable.create(nestedSortables[i], {
            dataIdAttr: 'data-id',
            group: 'nested',
            handle: '.row__handle',
            ghostClass: 'row--ghost',
            draggable: '.row',
            //dragoverBubble: true,
            animation: 150,
            fallbackOnBody: true,
            swapThreshold: 0.75,
            //invertSwap: true,
            onEnd: function(evt) {
                $('.row__more-btn[data-target="#' + evt.from.id + '"]').toggleClass('visible', $('#' + evt.from.id).children('li').length > 0);
                $('.row__more-btn[data-target="#' + evt.to.id + '"]').toggleClass('visible', $('#' + evt.to.id).children('li').length > 0);
            },
            onSort: serialize
        });
    }

};

var pivotUI = function() {
    var pivot_x, pivot_y, pivot = false;
    var target_x_relative_to_preview = 0;
    var target_y_relative_to_preview = 0;
    var x_position_ratio = 0;
    var y_position_ratio = 0;
    $('.js-pivot').on('mousedown touchstart touchmove mousemove mouseup touchend', '.pivot__hint', function(e) {

        e.preventDefault();

        var $this = $(this),
            $hint = $this.find('.icon');

        target_x_relative_to_preview = e.pageX - $this.offset().left;
        target_y_relative_to_preview = e.pageY - $this.offset().top;
        try {
            x_position_ratio = target_x_relative_to_preview / $this.width();
            y_position_ratio = target_y_relative_to_preview / $this.height();
        } catch (err) {
            x_position_ratio = 0;
            y_position_ratio = 0;
        }
        if (x_position_ratio < 0) {
            x_position_ratio = 0;
        }
        if (y_position_ratio < 0) {
            y_position_ratio = 0;
        }
        if (x_position_ratio > 1) {
            x_position_ratio = 1;
        }
        if (y_position_ratio > 1) {
            y_position_ratio = 1;
        }
        pivot_x = (x_position_ratio * 100).toFixed(2);
        pivot_y = (y_position_ratio * 100).toFixed(2);

        switch (e.type) {
            case 'mousedown':
            case 'touchstart':
                $hint.css('left', pivot_x + '%');
                $hint.css('top', pivot_y + '%');
                pivot = true;
                break;
            case 'mousemove':
            case 'touchmove':
                if (pivot === true) {
                    $hint.css('left', pivot_x + '%');
                    $hint.css('top', pivot_y + '%');
                }
                break;
            case 'mouseup':
            case 'touchend':
                if (pivot === true) {
                    $this.closest('.pivot').find('.pivot-x').val(pivot_x);
                    $this.closest('.pivot').find('.pivot-y').val(pivot_y);
                    pivot = false;
                }
                break;
        }

        return;
    });
};

$(document).ready(function() {
    FastClick.attach(document.body);
    document.addEventListener('touchstart', function() {}, true);
    wwCalc();
    validateForm();
    maxlengthCounter();
    urlInput();
    submitWarning();
    closeMsg();
    toggleNav();
    toggleBtn();
    i18nSwitch();
    helpTooltip();
    pivotUI();
    checkActions();
    sortRows();
    sortNestedRows();
});

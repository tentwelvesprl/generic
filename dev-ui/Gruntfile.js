module.exports = function(grunt) {
	'use strict';
	var timestamp = Date.now();
	require('time-grunt')(grunt);

	const sass = require('sass');

	grunt.initConfig({

		pkg: grunt.file.readJSON('package.json'),
		env: grunt.file.readJSON('environment.json'),

		dir: {
			packs_assets: '../private/packs/Admin/assets/',
			web_assets: '../web/assets/Admin/'
		},

		concat: {
			options: {
				separator: ';\n'
			},
			plugins: {
				src: ['scripts/plugins/**/*.js', '!**/_*.js'],
				dest: '.tmp/plugins.js'
			}
		},

		uglify : {
			main: {
				options: {
					banner: '/*\n' +
					' * <%= pkg.name %>\n' +
					' *\n' +
					' * <%= pkg.version %> <%= grunt.template.today("dd-mm-yyyy") %>\n' +
					' * by <%= pkg.author %>\n' +
					' *\n' +
					' */\n'
				},
				files: {
					'<%= dir.packs_assets %>js/main.min.js' : 'scripts/main.js'
				}
			},
			plugins: {
				files: {
					'<%= dir.packs_assets %>js/plugins.min.js' : '.tmp/plugins.js'
				}
			}
		},

		sprite: {
			dpr1: {
				src: 'sprites/*.png',
				dest: '<%= dir.packs_assets %>img/sprites.png',
				destCss: 'sass/partials/_sprites.sass',
				imgPath: '../img/sprites.png?=' + timestamp,
				'algorithm': 'binary-tree',
				'padding': 2
			},
			dpr2: {
				src: 'sprites@2x/*.png',
				dest: '<%= dir.packs_assets %>img/sprites_2x.png',
				destCss: 'sass/partials/_sprites-retina.sass',
				imgPath: '../img/sprites_2x.png?=' + timestamp,
				algorithm: 'binary-tree',
				'padding': 4,
				cssVarMap: function (sprite) {
					sprite.name = sprite.name + '-retina';
				}
			}
		},

		sass: {
			options: {
				implementation: sass,
				sourceMap: false
			},
			dist: {
				 files: [{
					expand: true,
					cwd: 'sass/',
					src: ['*.sass', '*.scss'],
					dest: '.tmp/css/',
					ext: '.css'
			 }]
			}
		},

		postcss: {
			options: {
				map: false,
				processors: [
					require('postcss-normalize')(),
					require('pixrem')(),
					require('autoprefixer')(),
					require('cssnano')()
				],
				failOnError: true
			},
			dist: {
				expand: true,
				cwd: '.tmp/css/',
				src: '*.css',
				dest: '<%= dir.packs_assets %>css/'
			}
		},

		jshint: {
			files: ['scripts/main.js'],
			options: {
				jshintrc: '.jshintrc'
			}
		},

		watch: {
			options: {
				spawn: false,
			},
			spriting: {
				files: ['sprites/*.png'],
				tasks: ['sprite:dpr1', 'copy:dpr1']
			},
			spriting_retina: {
				files: ['sprites@2x/*.png'],
				tasks: ['sprite:dpr2', 'copy:dpr2']
			},
			css: {
				files: ['sass/**/*.sass', 'sass/**/*.scss'],
				tasks: ['sass', 'postcss', 'copy:css']
			},
			js: {
				files: ['scripts/main.js'],
				tasks: ['jshint', 'uglify:main', 'copy:js']
			},
			scripts: {
			 	files: ['scripts/plugins/**/*.js'],
			 	tasks: ['concat:plugins', 'uglify:plugins', 'copy:plugins']
			}
		},

		concurrent: {
			sprites: ['sprite:dpr1', 'sprite:dpr2']
		},

		copy: {
			dpr1: {
				expand: true,
				cwd: '<%= dir.packs_assets %>img/',
				src: 'sprites.png',
				dest: '<%= dir.web_assets %>img/'
			},
			dpr2: {
				expand: true,
				cwd: '<%= dir.packs_assets %>img/',
				src: 'sprites_2x.png',
				dest: '<%= dir.web_assets %>img/'
			},
			css: {
				expand: true,
				cwd: '<%= dir.packs_assets %>css/',
				src: '*.css',
				dest: '<%= dir.web_assets %>css/'
			},
			js: {
				expand: true,
				cwd: '<%= dir.packs_assets %>js/',
				src: 'main.min.js',
				dest: '<%= dir.web_assets %>js/'
			},
			plugins: {
				expand: true,
				cwd: '<%= dir.packs_assets %>js/',
				src: 'plugins.min.js',
				dest: '<%= dir.web_assets %>js/'
			}
		},

		browserSync: {
			dev: {
				files: {
					src : [
					'<%= dir.web_assets %>css/*.css',
					'<%= dir.web_assets %>js/*.min.js',
					'../maquette-ui/**/*.php',
					'../private/packs/Admin/views/**/*.twig'
					],
				},
				options: {
					watchTask: true,
					ghostMode: {
						clicks: false,
						forms: false,
						scroll: false
					},
					proxy: '<%= env.proxy %>',
					online: '<%= env.online %>',
					open: '<%= env.open %>',
					startPath: '<%= env.startPath %>'
				},
			},
		},

		modernizr: {

			dist: {
				'dest' : 'scripts/plugins/modernizr-custom.min.js',
				'options' : [
				'setClasses',
				'addTest',
				'testProp',
				'fnBind'
				],
				'cssprefix': 'mdzr-',
				'classPrefix' : 'mdzr-',
				'uglify' : false,
				files: {
					src: [
					'<%= dir.web_assets %>css/**/*.css',
					'scripts/main.js',
					]
				}
			}

		},

		filesToBust: ['maquette-ui/index.php', 'private/packs/Admin/views/admin-page-layout.twig'],
		replace: {
			options: {
				patterns: [
				{
					match: /\.(js|css)\?([0-9a-f]{16})"/g,
					replacement: '.$1"'
				}
				]
			},
			dist: {
				files: [{
					expand: true,
					cwd: '../',
					dest: '../',
					src: '<%= filesToBust %>'
				}]
			}
		},
		cacheBust: {
			options: {
				assets: ['css/*.css', 'js/*.js'],
				baseDir: '<%= dir.web_assets %>',
				queryString: true,
				createCopies: false,
				urlPrefixes: ['{{ global.request.basepath }}/assets/Admin', '../web/assets/Admin']
			},
			dist: {
				files: [{
					expand: true,
					cwd: '../',
					src: '<%= filesToBust %>'
				}]
			}
		}

	});

require('load-grunt-tasks')(grunt);

grunt.registerTask('dev', ['replace:dist', 'browserSync', 'watch']);
grunt.registerTask('css', ['concurrent:sprites', 'sass', 'postcss']);
grunt.registerTask('js', ['jshint', 'concat', 'uglify']);
grunt.registerTask('mdzr', 'modernizr:dist');
grunt.registerTask('bust', ['copy', 'replace:dist', 'cacheBust:dist']);
grunt.registerTask('default', ['css', 'jshint', 'mdzr', 'concat', 'uglify', 'bust']);

};
